package tr.com.khg.template.repository;

import java.util.List;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;
import tr.com.khg.template.domain.Meeting;

/**
 * Spring Data SQL repository for the Meeting entity.
 */
@SuppressWarnings("unused")
@Repository
public interface MeetingRepository extends JpaRepository<Meeting, Long> {
    List<Meeting> findByFinalized(Boolean finalized);
}
