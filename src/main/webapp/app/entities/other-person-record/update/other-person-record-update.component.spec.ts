jest.mock('@angular/router');

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { of, Subject } from 'rxjs';

import { OtherPersonRecordService } from '../service/other-person-record.service';
import { IOtherPersonRecord, OtherPersonRecord } from '../other-person-record.model';
import { IOtherPerson } from 'app/entities/other-person/other-person.model';
import { OtherPersonService } from 'app/entities/other-person/service/other-person.service';

import { OtherPersonRecordUpdateComponent } from './other-person-record-update.component';

describe('OtherPersonRecord Management Update Component', () => {
  let comp: OtherPersonRecordUpdateComponent;
  let fixture: ComponentFixture<OtherPersonRecordUpdateComponent>;
  let activatedRoute: ActivatedRoute;
  let otherPersonRecordService: OtherPersonRecordService;
  let otherPersonService: OtherPersonService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      declarations: [OtherPersonRecordUpdateComponent],
      providers: [FormBuilder, ActivatedRoute],
    })
      .overrideTemplate(OtherPersonRecordUpdateComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(OtherPersonRecordUpdateComponent);
    activatedRoute = TestBed.inject(ActivatedRoute);
    otherPersonRecordService = TestBed.inject(OtherPersonRecordService);
    otherPersonService = TestBed.inject(OtherPersonService);

    comp = fixture.componentInstance;
  });

  describe('ngOnInit', () => {
    it('Should call OtherPerson query and add missing value', () => {
      const otherPersonRecord: IOtherPersonRecord = { id: 456 };
      const otherPerson: IOtherPerson = { id: 41865 };
      otherPersonRecord.otherPerson = otherPerson;

      const otherPersonCollection: IOtherPerson[] = [{ id: 5279 }];
      jest.spyOn(otherPersonService, 'query').mockReturnValue(of(new HttpResponse({ body: otherPersonCollection })));
      const additionalOtherPeople = [otherPerson];
      const expectedCollection: IOtherPerson[] = [...additionalOtherPeople, ...otherPersonCollection];
      jest.spyOn(otherPersonService, 'addOtherPersonToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ otherPersonRecord });
      comp.ngOnInit();

      expect(otherPersonService.query).toHaveBeenCalled();
      expect(otherPersonService.addOtherPersonToCollectionIfMissing).toHaveBeenCalledWith(otherPersonCollection, ...additionalOtherPeople);
      expect(comp.otherPeopleSharedCollection).toEqual(expectedCollection);
    });

    it('Should update editForm', () => {
      const otherPersonRecord: IOtherPersonRecord = { id: 456 };
      const otherPerson: IOtherPerson = { id: 55528 };
      otherPersonRecord.otherPerson = otherPerson;

      activatedRoute.data = of({ otherPersonRecord });
      comp.ngOnInit();

      expect(comp.editForm.value).toEqual(expect.objectContaining(otherPersonRecord));
      expect(comp.otherPeopleSharedCollection).toContain(otherPerson);
    });
  });

  describe('save', () => {
    it('Should call update service on save for existing entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<OtherPersonRecord>>();
      const otherPersonRecord = { id: 123 };
      jest.spyOn(otherPersonRecordService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ otherPersonRecord });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: otherPersonRecord }));
      saveSubject.complete();

      // THEN
      expect(comp.previousState).toHaveBeenCalled();
      expect(otherPersonRecordService.update).toHaveBeenCalledWith(otherPersonRecord);
      expect(comp.isSaving).toEqual(false);
    });

    it('Should call create service on save for new entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<OtherPersonRecord>>();
      const otherPersonRecord = new OtherPersonRecord();
      jest.spyOn(otherPersonRecordService, 'create').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ otherPersonRecord });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: otherPersonRecord }));
      saveSubject.complete();

      // THEN
      expect(otherPersonRecordService.create).toHaveBeenCalledWith(otherPersonRecord);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).toHaveBeenCalled();
    });

    it('Should set isSaving to false on error', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<OtherPersonRecord>>();
      const otherPersonRecord = { id: 123 };
      jest.spyOn(otherPersonRecordService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ otherPersonRecord });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.error('This is an error!');

      // THEN
      expect(otherPersonRecordService.update).toHaveBeenCalledWith(otherPersonRecord);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).not.toHaveBeenCalled();
    });
  });

  describe('Tracking relationships identifiers', () => {
    describe('trackOtherPersonById', () => {
      it('Should return tracked OtherPerson primary key', () => {
        const entity = { id: 123 };
        const trackResult = comp.trackOtherPersonById(0, entity);
        expect(trackResult).toEqual(entity.id);
      });
    });
  });
});
