import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { IOtherPerson, OtherPerson } from '../other-person.model';

import { OtherPersonService } from './other-person.service';

describe('OtherPerson Service', () => {
  let service: OtherPersonService;
  let httpMock: HttpTestingController;
  let elemDefault: IOtherPerson;
  let expectedResult: IOtherPerson | IOtherPerson[] | boolean | null;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    expectedResult = null;
    service = TestBed.inject(OtherPersonService);
    httpMock = TestBed.inject(HttpTestingController);

    elemDefault = {
      id: 0,
    };
  });

  describe('Service methods', () => {
    it('should find an element', () => {
      const returnedFromService = Object.assign({}, elemDefault);

      service.find(123).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(elemDefault);
    });

    it('should create a OtherPerson', () => {
      const returnedFromService = Object.assign(
        {
          id: 0,
        },
        elemDefault
      );

      const expected = Object.assign({}, returnedFromService);

      service.create(new OtherPerson()).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'POST' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should update a OtherPerson', () => {
      const returnedFromService = Object.assign(
        {
          id: 1,
        },
        elemDefault
      );

      const expected = Object.assign({}, returnedFromService);

      service.update(expected).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PUT' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should partial update a OtherPerson', () => {
      const patchObject = Object.assign({}, new OtherPerson());

      const returnedFromService = Object.assign(patchObject, elemDefault);

      const expected = Object.assign({}, returnedFromService);

      service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PATCH' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should return a list of OtherPerson', () => {
      const returnedFromService = Object.assign(
        {
          id: 1,
        },
        elemDefault
      );

      const expected = Object.assign({}, returnedFromService);

      service.query().subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush([returnedFromService]);
      httpMock.verify();
      expect(expectedResult).toContainEqual(expected);
    });

    it('should delete a OtherPerson', () => {
      service.delete(123).subscribe(resp => (expectedResult = resp.ok));

      const req = httpMock.expectOne({ method: 'DELETE' });
      req.flush({ status: 200 });
      expect(expectedResult);
    });

    describe('addOtherPersonToCollectionIfMissing', () => {
      it('should add a OtherPerson to an empty array', () => {
        const otherPerson: IOtherPerson = { id: 123 };
        expectedResult = service.addOtherPersonToCollectionIfMissing([], otherPerson);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(otherPerson);
      });

      it('should not add a OtherPerson to an array that contains it', () => {
        const otherPerson: IOtherPerson = { id: 123 };
        const otherPersonCollection: IOtherPerson[] = [
          {
            ...otherPerson,
          },
          { id: 456 },
        ];
        expectedResult = service.addOtherPersonToCollectionIfMissing(otherPersonCollection, otherPerson);
        expect(expectedResult).toHaveLength(2);
      });

      it("should add a OtherPerson to an array that doesn't contain it", () => {
        const otherPerson: IOtherPerson = { id: 123 };
        const otherPersonCollection: IOtherPerson[] = [{ id: 456 }];
        expectedResult = service.addOtherPersonToCollectionIfMissing(otherPersonCollection, otherPerson);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(otherPerson);
      });

      it('should add only unique OtherPerson to an array', () => {
        const otherPersonArray: IOtherPerson[] = [{ id: 123 }, { id: 456 }, { id: 31482 }];
        const otherPersonCollection: IOtherPerson[] = [{ id: 123 }];
        expectedResult = service.addOtherPersonToCollectionIfMissing(otherPersonCollection, ...otherPersonArray);
        expect(expectedResult).toHaveLength(3);
      });

      it('should accept varargs', () => {
        const otherPerson: IOtherPerson = { id: 123 };
        const otherPerson2: IOtherPerson = { id: 456 };
        expectedResult = service.addOtherPersonToCollectionIfMissing([], otherPerson, otherPerson2);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(otherPerson);
        expect(expectedResult).toContain(otherPerson2);
      });

      it('should accept null and undefined values', () => {
        const otherPerson: IOtherPerson = { id: 123 };
        expectedResult = service.addOtherPersonToCollectionIfMissing([], null, otherPerson, undefined);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(otherPerson);
      });

      it('should return initial array if no OtherPerson is added', () => {
        const otherPersonCollection: IOtherPerson[] = [{ id: 123 }];
        expectedResult = service.addOtherPersonToCollectionIfMissing(otherPersonCollection, undefined, null);
        expect(expectedResult).toEqual(otherPersonCollection);
      });
    });
  });

  afterEach(() => {
    httpMock.verify();
  });
});
