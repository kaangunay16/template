import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { IOtherPerson } from '../other-person.model';
import { OtherPersonService } from '../service/other-person.service';

@Component({
  templateUrl: './other-person-delete-dialog.component.html',
})
export class OtherPersonDeleteDialogComponent {
  otherPerson?: IOtherPerson;

  constructor(protected otherPersonService: OtherPersonService, protected activeModal: NgbActiveModal) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.otherPersonService.delete(id).subscribe(() => {
      this.activeModal.close('deleted');
    });
  }
}
