package tr.com.khg.template.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;
import org.hibernate.annotations.ResultCheckStyle;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

/**
 * A Person.
 */
@Entity
@Table(name = "person")
@Where(clause = "deleted = false")
@SQLDelete(sql = "update person set deleted = true where id = ?", check = ResultCheckStyle.COUNT)
public class Person implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @Column(name = "description")
    private String description;

    @Column(name = "position")
    private String position;

    @Column(name = "section")
    private String section;

    @Column(name = "city")
    private String city;

    @Column(name = "district")
    private String district;

    @Column(name = "institution")
    private String institution;

    @Column(name = "jhi_organization")
    private String organization;

    @Column(name = "deleted")
    private Boolean deleted;

    @Column(name = "status")
    private String status;

    @OneToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(unique = true)
    private Identity identity;

    @OneToMany(mappedBy = "person", cascade = CascadeType.ALL)
    @JsonIgnoreProperties(value = { "person" }, allowSetters = true)
    private Set<AbroadActivity> abroadActivities = new HashSet<>();

    @OneToMany(mappedBy = "person", cascade = CascadeType.ALL)
    @JsonIgnoreProperties(value = { "person" }, allowSetters = true)
    private Set<ActualActivity> actualActivities = new HashSet<>();

    @OneToMany(mappedBy = "person", cascade = CascadeType.ALL)
    @JsonIgnoreProperties(value = { "person" }, allowSetters = true)
    private Set<PastActivity> pastActivities = new HashSet<>();

    @OneToMany(mappedBy = "person", cascade = CascadeType.ALL)
    @JsonIgnoreProperties(value = { "person" }, allowSetters = true)
    private Set<OtherName> otherNames = new HashSet<>();

    @OneToMany(mappedBy = "person", cascade = CascadeType.ALL)
    @JsonIgnoreProperties(value = { "person" }, allowSetters = true)
    private Set<Phone> phones = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public Person id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescription() {
        return this.description;
    }

    public Person description(String description) {
        this.setDescription(description);
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPosition() {
        return this.position;
    }

    public Person position(String position) {
        this.setPosition(position);
        return this;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getSection() {
        return this.section;
    }

    public Person section(String section) {
        this.setSection(section);
        return this;
    }

    public void setSection(String section) {
        this.section = section;
    }

    public String getCity() {
        return this.city;
    }

    public Person city(String city) {
        this.setCity(city);
        return this;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getDistrict() {
        return this.district;
    }

    public Person district(String district) {
        this.setDistrict(district);
        return this;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getInstitution() {
        return this.institution;
    }

    public Person institution(String institution) {
        this.setInstitution(institution);
        return this;
    }

    public void setInstitution(String institution) {
        this.institution = institution;
    }

    public String getOrganization() {
        return this.organization;
    }

    public Person organization(String organization) {
        this.setOrganization(organization);
        return this;
    }

    public void setOrganization(String organization) {
        this.organization = organization;
    }

    public Boolean getDeleted() {
        return this.deleted;
    }

    public Person deleted(Boolean deleted) {
        this.setDeleted(deleted);
        return this;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    public String getStatus() {
        return this.status;
    }

    public Person status(String status) {
        this.setStatus(status);
        return this;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Identity getIdentity() {
        return this.identity;
    }

    public void setIdentity(Identity identity) {
        this.identity = identity;
    }

    public Person identity(Identity identity) {
        this.setIdentity(identity);
        return this;
    }

    public Set<AbroadActivity> getAbroadActivities() {
        return this.abroadActivities;
    }

    public void setAbroadActivities(Set<AbroadActivity> abroadActivities) {
        if (this.abroadActivities != null) {
            this.abroadActivities.forEach(i -> i.setPerson(null));
        }
        if (abroadActivities != null) {
            abroadActivities.forEach(i -> i.setPerson(this));
        }
        this.abroadActivities = abroadActivities;
    }

    public Person abroadActivities(Set<AbroadActivity> abroadActivities) {
        this.setAbroadActivities(abroadActivities);
        return this;
    }

    public Person addAbroadActivity(AbroadActivity abroadActivity) {
        this.abroadActivities.add(abroadActivity);
        abroadActivity.setPerson(this);
        return this;
    }

    public Person removeAbroadActivity(AbroadActivity abroadActivity) {
        this.abroadActivities.remove(abroadActivity);
        abroadActivity.setPerson(null);
        return this;
    }

    public Set<ActualActivity> getActualActivities() {
        return this.actualActivities;
    }

    public void setActualActivities(Set<ActualActivity> actualActivities) {
        if (this.actualActivities != null) {
            this.actualActivities.forEach(i -> i.setPerson(null));
        }
        if (actualActivities != null) {
            actualActivities.forEach(i -> i.setPerson(this));
        }
        this.actualActivities = actualActivities;
    }

    public Person actualActivities(Set<ActualActivity> actualActivities) {
        this.setActualActivities(actualActivities);
        return this;
    }

    public Person addActualActivity(ActualActivity actualActivity) {
        this.actualActivities.add(actualActivity);
        actualActivity.setPerson(this);
        return this;
    }

    public Person removeActualActivity(ActualActivity actualActivity) {
        this.actualActivities.remove(actualActivity);
        actualActivity.setPerson(null);
        return this;
    }

    public Set<PastActivity> getPastActivities() {
        return this.pastActivities;
    }

    public void setPastActivities(Set<PastActivity> pastActivities) {
        if (this.pastActivities != null) {
            this.pastActivities.forEach(i -> i.setPerson(null));
        }
        if (pastActivities != null) {
            pastActivities.forEach(i -> i.setPerson(this));
        }
        this.pastActivities = pastActivities;
    }

    public Person pastActivities(Set<PastActivity> pastActivities) {
        this.setPastActivities(pastActivities);
        return this;
    }

    public Person addPastActivity(PastActivity pastActivity) {
        this.pastActivities.add(pastActivity);
        pastActivity.setPerson(this);
        return this;
    }

    public Person removePastActivity(PastActivity pastActivity) {
        this.pastActivities.remove(pastActivity);
        pastActivity.setPerson(null);
        return this;
    }

    public Set<OtherName> getOtherNames() {
        return this.otherNames;
    }

    public void setOtherNames(Set<OtherName> otherNames) {
        if (this.otherNames != null) {
            this.otherNames.forEach(i -> i.setPerson(null));
        }
        if (otherNames != null) {
            otherNames.forEach(i -> i.setPerson(this));
        }
        this.otherNames = otherNames;
    }

    public Person otherNames(Set<OtherName> otherNames) {
        this.setOtherNames(otherNames);
        return this;
    }

    public Person addOtherName(OtherName otherName) {
        this.otherNames.add(otherName);
        otherName.setPerson(this);
        return this;
    }

    public Person removeOtherName(OtherName otherName) {
        this.otherNames.remove(otherName);
        otherName.setPerson(null);
        return this;
    }

    public Set<Phone> getPhones() {
        return this.phones;
    }

    public void setPhones(Set<Phone> phones) {
        if (this.phones != null) {
            this.phones.forEach(i -> i.setPerson(null));
        }
        if (phones != null) {
            phones.forEach(i -> i.setPerson(this));
        }
        this.phones = phones;
    }

    public Person phones(Set<Phone> phones) {
        this.setPhones(phones);
        return this;
    }

    public Person addPhone(Phone phone) {
        this.phones.add(phone);
        phone.setPerson(this);
        return this;
    }

    public Person removePhone(Phone phone) {
        this.phones.remove(phone);
        phone.setPerson(null);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Person)) {
            return false;
        }
        return id != null && id.equals(((Person) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Person{" +
            "id=" + getId() +
            ", description='" + getDescription() + "'" +
            ", position='" + getPosition() + "'" +
            ", section='" + getSection() + "'" +
            ", city='" + getCity() + "'" +
            ", district='" + getDistrict() + "'" +
            ", institution='" + getInstitution() + "'" +
            ", organization='" + getOrganization() + "'" +
            ", deleted='" + getDeleted() + "'" +
            ", status='" + getStatus() + "'" +
            "}";
    }
}
