import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OtherPersonDeletionDemandsComponent } from './other-person-deletion-demands.component';

describe('OtherPersonDeletionDemandsComponent', () => {
  let component: OtherPersonDeletionDemandsComponent;
  let fixture: ComponentFixture<OtherPersonDeletionDemandsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [OtherPersonDeletionDemandsComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OtherPersonDeletionDemandsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
