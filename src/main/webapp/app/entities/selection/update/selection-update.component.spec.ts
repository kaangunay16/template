jest.mock('@angular/router');

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { of, Subject } from 'rxjs';

import { SelectionService } from '../service/selection.service';
import { ISelection, Selection } from '../selection.model';

import { SelectionUpdateComponent } from './selection-update.component';

describe('Selection Management Update Component', () => {
  let comp: SelectionUpdateComponent;
  let fixture: ComponentFixture<SelectionUpdateComponent>;
  let activatedRoute: ActivatedRoute;
  let selectionService: SelectionService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      declarations: [SelectionUpdateComponent],
      providers: [FormBuilder, ActivatedRoute],
    })
      .overrideTemplate(SelectionUpdateComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(SelectionUpdateComponent);
    activatedRoute = TestBed.inject(ActivatedRoute);
    selectionService = TestBed.inject(SelectionService);

    comp = fixture.componentInstance;
  });

  describe('ngOnInit', () => {
    it('Should update editForm', () => {
      const selection: ISelection = { id: 456 };

      activatedRoute.data = of({ selection });
      comp.ngOnInit();

      expect(comp.editForm.value).toEqual(expect.objectContaining(selection));
    });
  });

  describe('save', () => {
    it('Should call update service on save for existing entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<Selection>>();
      const selection = { id: 123 };
      jest.spyOn(selectionService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ selection });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: selection }));
      saveSubject.complete();

      // THEN
      expect(comp.previousState).toHaveBeenCalled();
      expect(selectionService.update).toHaveBeenCalledWith(selection);
      expect(comp.isSaving).toEqual(false);
    });

    it('Should call create service on save for new entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<Selection>>();
      const selection = new Selection();
      jest.spyOn(selectionService, 'create').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ selection });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: selection }));
      saveSubject.complete();

      // THEN
      expect(selectionService.create).toHaveBeenCalledWith(selection);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).toHaveBeenCalled();
    });

    it('Should set isSaving to false on error', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<Selection>>();
      const selection = { id: 123 };
      jest.spyOn(selectionService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ selection });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.error('This is an error!');

      // THEN
      expect(selectionService.update).toHaveBeenCalledWith(selection);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).not.toHaveBeenCalled();
    });
  });
});
