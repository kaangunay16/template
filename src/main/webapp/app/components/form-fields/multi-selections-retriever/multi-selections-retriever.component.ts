import { Component, EventEmitter, Input, Output } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { Observable } from 'rxjs';

import { ISelection } from 'app/entities/selection/selection.model';
import { SelectionsRetrieverService } from 'app/components/form-fields/selections-retriever/selections-retriever.service';

@Component({
  selector: 'jhi-multi-selections-retriever',
  templateUrl: './multi-selections-retriever.component.html',
  styleUrls: ['./multi-selections-retriever.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: MultiSelectionsRetrieverComponent,
      multi: true,
    },
  ],
})
export class MultiSelectionsRetrieverComponent implements ControlValueAccessor {
  @Input()
  readOnly?: boolean = false;

  @Input()
  value?: string[] | null = null;

  onChange!: (value: string[]) => void;

  onTouched!: () => void;

  options?: Observable<ISelection[]>;

  @Input()
  set selectionType(value: string | null | undefined) {
    this.options = this.selectionsRetrieverService.getOptions(value);
  }

  @Input()
  placeholder?: string = '--';

  @Input()
  isTranslationActive?: boolean = false;

  @Output()
  selectionChange: EventEmitter<string[]> = new EventEmitter();

  constructor(private selectionsRetrieverService: SelectionsRetrieverService) {}

  registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

  writeValue(obj: string[] | null | undefined): void {
    this.value = obj ? obj : null;
  }

  setDisabledState(isDisabled: boolean): void {
    this.readOnly = isDisabled;
  }

  change(): void {
    this.onChange(this.value!);
    this.selectionChange.emit(this.value!);
  }
}
