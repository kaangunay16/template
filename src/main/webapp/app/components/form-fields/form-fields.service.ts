import { Injectable } from '@angular/core';
import { ToastrService } from 'ngx-toastr';

import { IFormEntity } from 'app/entities/form-entity.model';
import { ErrorSupportService } from 'app/shared/services/error-support.service';

@Injectable({
  providedIn: 'root',
})
export class FormFieldsService {
  constructor(private toastr: ToastrService, private errorService: ErrorSupportService) {}

  remove(id: number, service: IFormEntity): void {
    service.delete(id).subscribe(
      () => {
        this.toastr.success('Kayıt başarılı şekilde silindi.');
      },
      err => this.errorService.errorHandler(err)
    );
  }
}
