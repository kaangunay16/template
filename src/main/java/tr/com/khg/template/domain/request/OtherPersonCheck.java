package tr.com.khg.template.domain.request;

import java.io.Serializable;
import java.util.List;

public class OtherPersonCheck implements Serializable {

    private List<String> identityNumbers;
    private String city;

    public List<String> getIdentityNumbers() {
        return identityNumbers;
    }

    public void setIdentityNumbers(List<String> identityNumbers) {
        this.identityNumbers = identityNumbers;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }
}
