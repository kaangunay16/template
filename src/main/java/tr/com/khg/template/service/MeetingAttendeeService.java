package tr.com.khg.template.service;

import java.util.List;
import java.util.Optional;
import tr.com.khg.template.domain.MeetingAttendee;

/**
 * Service Interface for managing {@link MeetingAttendee}.
 */
public interface MeetingAttendeeService {
    /**
     * Save a meetingAttendee.
     *
     * @param meetingAttendee the entity to save.
     * @return the persisted entity.
     */
    MeetingAttendee save(MeetingAttendee meetingAttendee);

    /**
     * Partially updates a meetingAttendee.
     *
     * @param meetingAttendee the entity to update partially.
     * @return the persisted entity.
     */
    Optional<MeetingAttendee> partialUpdate(MeetingAttendee meetingAttendee);

    /**
     * Get all the meetingAttendees.
     *
     * @return the list of entities.
     */
    List<MeetingAttendee> findAll();

    /**
     * Get the "id" meetingAttendee.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<MeetingAttendee> findOne(Long id);

    /**
     * Delete the "id" meetingAttendee.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
