import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NgrxDataAddPostComponent } from './ngrx-data-add-post.component';

describe('NgrxDataAddPostComponent', () => {
  let component: NgrxDataAddPostComponent;
  let fixture: ComponentFixture<NgrxDataAddPostComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [NgrxDataAddPostComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NgrxDataAddPostComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
