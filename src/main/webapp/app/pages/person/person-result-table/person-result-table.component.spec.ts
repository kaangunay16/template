import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PersonResultTableComponent } from './person-result-table.component';

describe('PersonResultTableComponent', () => {
  let component: PersonResultTableComponent;
  let fixture: ComponentFixture<PersonResultTableComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [PersonResultTableComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PersonResultTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
