import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { IMeetingAttendee, MeetingAttendee } from '../meeting-attendee.model';
import { MeetingAttendeeService } from '../service/meeting-attendee.service';

@Injectable({ providedIn: 'root' })
export class MeetingAttendeeRoutingResolveService implements Resolve<IMeetingAttendee> {
  constructor(protected service: MeetingAttendeeService, protected router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IMeetingAttendee> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        mergeMap((meetingAttendee: HttpResponse<MeetingAttendee>) => {
          if (meetingAttendee.body) {
            return of(meetingAttendee.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new MeetingAttendee());
  }
}
