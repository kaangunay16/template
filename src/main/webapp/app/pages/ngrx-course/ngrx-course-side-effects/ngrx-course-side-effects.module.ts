import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';

import { SIDE_EFFECT_STATE_NAME } from './state/side-effect.selector';
import { sideEffectReducer } from './state/side-effect.reducer';
import { SideEffectsPageSideEffects } from './state/side-effect.effects';
import { SharedModule } from 'app/shared/shared.module';
import { NgrxCourseSideEffectsRoutingModule } from './ngrx-course-side-effects-routing.module';
import { NgrxApiCallComponent } from './ngrx-api-call/ngrx-api-call.component';
import { NgrxSingleCallComponent } from './ngrx-single-call/ngrx-single-call.component';

@NgModule({
  declarations: [NgrxApiCallComponent, NgrxSingleCallComponent],
  imports: [
    CommonModule,
    SharedModule,
    NgrxCourseSideEffectsRoutingModule,
    EffectsModule.forFeature([SideEffectsPageSideEffects]),
    StoreModule.forFeature(SIDE_EFFECT_STATE_NAME, sideEffectReducer),
  ],
})
export class NgrxCourseSideEffectsModule {}
