package tr.com.khg.template.service.utils;

import javax.persistence.criteria.Join;

public class QueryJoin {

    private String name;
    private Join<Object, Object> join;

    public QueryJoin(String name, Join<Object, Object> join) {
        this.name = name;
        this.join = join;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Join<Object, Object> getJoin() {
        return join;
    }

    public void setJoin(Join<Object, Object> join) {
        this.join = join;
    }
}
