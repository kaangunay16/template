import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NgrxPostAddComponent } from './ngrx-post-add.component';

describe('NgrxPostAddComponent', () => {
  let component: NgrxPostAddComponent;
  let fixture: ComponentFixture<NgrxPostAddComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [NgrxPostAddComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NgrxPostAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
