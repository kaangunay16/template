import { IPerson } from 'app/entities/person/person.model';
import { HasId } from '../form-entity.model';

export interface IPastActivity extends HasId {
  id?: number;
  city?: string | null;
  startYear?: number | null;
  endYear?: number | null;
  deleted?: boolean | null;
  person?: IPerson | null;
}

export class PastActivity implements IPastActivity {
  constructor(
    public id?: number,
    public city?: string | null,
    public startYear?: number | null,
    public endYear?: number | null,
    public deleted?: boolean | null,
    public person?: IPerson | null
  ) {
    this.deleted = this.deleted ?? false;
  }
}

export function getPastActivityIdentifier(pastActivity: IPastActivity): number | undefined {
  return pastActivity.id;
}
