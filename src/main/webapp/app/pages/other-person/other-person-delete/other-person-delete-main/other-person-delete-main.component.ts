import { Component, OnDestroy } from '@angular/core';

import { EntityDemandService } from 'app/shared/services/entity-demand.service';

@Component({
  selector: 'jhi-other-person-delete-main',
  templateUrl: './other-person-delete-main.component.html',
  styleUrls: ['./other-person-delete-main.component.scss'],
})
export class OtherPersonDeleteMainComponent implements OnDestroy {
  constructor(private entityDemandService: EntityDemandService) {}

  ngOnDestroy(): void {
    this.entityDemandService.clearEntities();
  }
}
