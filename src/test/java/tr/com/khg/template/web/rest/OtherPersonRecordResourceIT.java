package tr.com.khg.template.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import tr.com.khg.template.IntegrationTest;
import tr.com.khg.template.domain.OtherPersonRecord;
import tr.com.khg.template.repository.OtherPersonRecordRepository;

/**
 * Integration tests for the {@link OtherPersonRecordResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class OtherPersonRecordResourceIT {

    private static final String DEFAULT_CITY = "AAAAAAAAAA";
    private static final String UPDATED_CITY = "BBBBBBBBBB";

    private static final String DEFAULT_RANK = "AAAAAAAAAA";
    private static final String UPDATED_RANK = "BBBBBBBBBB";

    private static final String DEFAULT_SECTION = "AAAAAAAAAA";
    private static final String UPDATED_SECTION = "BBBBBBBBBB";

    private static final String DEFAULT_MEETING_STATUS = "AAAAAAAAAA";
    private static final String UPDATED_MEETING_STATUS = "BBBBBBBBBB";

    private static final String DEFAULT_STATUS = "AAAAAAAAAA";
    private static final String UPDATED_STATUS = "BBBBBBBBBB";

    private static final Boolean DEFAULT_SPEAKER = false;
    private static final Boolean UPDATED_SPEAKER = true;

    private static final String DEFAULT_ADDED_BY_UNIT = "AAAAAAAAAA";
    private static final String UPDATED_ADDED_BY_UNIT = "BBBBBBBBBB";

    private static final String DEFAULT_PROFESSION = "AAAAAAAAAA";
    private static final String UPDATED_PROFESSION = "BBBBBBBBBB";

    private static final String DEFAULT_DOCUMENT = "AAAAAAAAAA";
    private static final String UPDATED_DOCUMENT = "BBBBBBBBBB";

    private static final String DEFAULT_TYPE = "AAAAAAAAAA";
    private static final String UPDATED_TYPE = "BBBBBBBBBB";

    private static final String ENTITY_API_URL = "/api/other-person-records";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private OtherPersonRecordRepository otherPersonRecordRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restOtherPersonRecordMockMvc;

    private OtherPersonRecord otherPersonRecord;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static OtherPersonRecord createEntity(EntityManager em) {
        OtherPersonRecord otherPersonRecord = new OtherPersonRecord()
            .city(DEFAULT_CITY)
            .rank(DEFAULT_RANK)
            .section(DEFAULT_SECTION)
            .meetingStatus(DEFAULT_MEETING_STATUS)
            .status(DEFAULT_STATUS)
            .speaker(DEFAULT_SPEAKER)
            .addedByUnit(DEFAULT_ADDED_BY_UNIT)
            .profession(DEFAULT_PROFESSION)
            .document(DEFAULT_DOCUMENT)
            .type(DEFAULT_TYPE);
        return otherPersonRecord;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static OtherPersonRecord createUpdatedEntity(EntityManager em) {
        OtherPersonRecord otherPersonRecord = new OtherPersonRecord()
            .city(UPDATED_CITY)
            .rank(UPDATED_RANK)
            .section(UPDATED_SECTION)
            .meetingStatus(UPDATED_MEETING_STATUS)
            .status(UPDATED_STATUS)
            .speaker(UPDATED_SPEAKER)
            .addedByUnit(UPDATED_ADDED_BY_UNIT)
            .profession(UPDATED_PROFESSION)
            .document(UPDATED_DOCUMENT)
            .type(UPDATED_TYPE);
        return otherPersonRecord;
    }

    @BeforeEach
    public void initTest() {
        otherPersonRecord = createEntity(em);
    }

    @Test
    @Transactional
    void createOtherPersonRecord() throws Exception {
        int databaseSizeBeforeCreate = otherPersonRecordRepository.findAll().size();
        // Create the OtherPersonRecord
        restOtherPersonRecordMockMvc
            .perform(
                post(ENTITY_API_URL)
                    .with(csrf())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(otherPersonRecord))
            )
            .andExpect(status().isCreated());

        // Validate the OtherPersonRecord in the database
        List<OtherPersonRecord> otherPersonRecordList = otherPersonRecordRepository.findAll();
        assertThat(otherPersonRecordList).hasSize(databaseSizeBeforeCreate + 1);
        OtherPersonRecord testOtherPersonRecord = otherPersonRecordList.get(otherPersonRecordList.size() - 1);
        assertThat(testOtherPersonRecord.getCity()).isEqualTo(DEFAULT_CITY);
        assertThat(testOtherPersonRecord.getRank()).isEqualTo(DEFAULT_RANK);
        assertThat(testOtherPersonRecord.getSection()).isEqualTo(DEFAULT_SECTION);
        assertThat(testOtherPersonRecord.getMeetingStatus()).isEqualTo(DEFAULT_MEETING_STATUS);
        assertThat(testOtherPersonRecord.getStatus()).isEqualTo(DEFAULT_STATUS);
        assertThat(testOtherPersonRecord.getSpeaker()).isEqualTo(DEFAULT_SPEAKER);
        assertThat(testOtherPersonRecord.getAddedByUnit()).isEqualTo(DEFAULT_ADDED_BY_UNIT);
        assertThat(testOtherPersonRecord.getProfession()).isEqualTo(DEFAULT_PROFESSION);
        assertThat(testOtherPersonRecord.getDocument()).isEqualTo(DEFAULT_DOCUMENT);
        assertThat(testOtherPersonRecord.getType()).isEqualTo(DEFAULT_TYPE);
    }

    @Test
    @Transactional
    void createOtherPersonRecordWithExistingId() throws Exception {
        // Create the OtherPersonRecord with an existing ID
        otherPersonRecord.setId(1L);

        int databaseSizeBeforeCreate = otherPersonRecordRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restOtherPersonRecordMockMvc
            .perform(
                post(ENTITY_API_URL)
                    .with(csrf())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(otherPersonRecord))
            )
            .andExpect(status().isBadRequest());

        // Validate the OtherPersonRecord in the database
        List<OtherPersonRecord> otherPersonRecordList = otherPersonRecordRepository.findAll();
        assertThat(otherPersonRecordList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void getAllOtherPersonRecords() throws Exception {
        // Initialize the database
        otherPersonRecordRepository.saveAndFlush(otherPersonRecord);

        // Get all the otherPersonRecordList
        restOtherPersonRecordMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(otherPersonRecord.getId().intValue())))
            .andExpect(jsonPath("$.[*].city").value(hasItem(DEFAULT_CITY)))
            .andExpect(jsonPath("$.[*].rank").value(hasItem(DEFAULT_RANK)))
            .andExpect(jsonPath("$.[*].section").value(hasItem(DEFAULT_SECTION)))
            .andExpect(jsonPath("$.[*].meetingStatus").value(hasItem(DEFAULT_MEETING_STATUS)))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS)))
            .andExpect(jsonPath("$.[*].speaker").value(hasItem(DEFAULT_SPEAKER.booleanValue())))
            .andExpect(jsonPath("$.[*].addedByUnit").value(hasItem(DEFAULT_ADDED_BY_UNIT)))
            .andExpect(jsonPath("$.[*].profession").value(hasItem(DEFAULT_PROFESSION)))
            .andExpect(jsonPath("$.[*].document").value(hasItem(DEFAULT_DOCUMENT)))
            .andExpect(jsonPath("$.[*].type").value(hasItem(DEFAULT_TYPE)));
    }

    @Test
    @Transactional
    void getOtherPersonRecord() throws Exception {
        // Initialize the database
        otherPersonRecordRepository.saveAndFlush(otherPersonRecord);

        // Get the otherPersonRecord
        restOtherPersonRecordMockMvc
            .perform(get(ENTITY_API_URL_ID, otherPersonRecord.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(otherPersonRecord.getId().intValue()))
            .andExpect(jsonPath("$.city").value(DEFAULT_CITY))
            .andExpect(jsonPath("$.rank").value(DEFAULT_RANK))
            .andExpect(jsonPath("$.section").value(DEFAULT_SECTION))
            .andExpect(jsonPath("$.meetingStatus").value(DEFAULT_MEETING_STATUS))
            .andExpect(jsonPath("$.status").value(DEFAULT_STATUS))
            .andExpect(jsonPath("$.speaker").value(DEFAULT_SPEAKER.booleanValue()))
            .andExpect(jsonPath("$.addedByUnit").value(DEFAULT_ADDED_BY_UNIT))
            .andExpect(jsonPath("$.profession").value(DEFAULT_PROFESSION))
            .andExpect(jsonPath("$.document").value(DEFAULT_DOCUMENT))
            .andExpect(jsonPath("$.type").value(DEFAULT_TYPE));
    }

    @Test
    @Transactional
    void getNonExistingOtherPersonRecord() throws Exception {
        // Get the otherPersonRecord
        restOtherPersonRecordMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putNewOtherPersonRecord() throws Exception {
        // Initialize the database
        otherPersonRecordRepository.saveAndFlush(otherPersonRecord);

        int databaseSizeBeforeUpdate = otherPersonRecordRepository.findAll().size();

        // Update the otherPersonRecord
        OtherPersonRecord updatedOtherPersonRecord = otherPersonRecordRepository.findById(otherPersonRecord.getId()).get();
        // Disconnect from session so that the updates on updatedOtherPersonRecord are not directly saved in db
        em.detach(updatedOtherPersonRecord);
        updatedOtherPersonRecord
            .city(UPDATED_CITY)
            .rank(UPDATED_RANK)
            .section(UPDATED_SECTION)
            .meetingStatus(UPDATED_MEETING_STATUS)
            .status(UPDATED_STATUS)
            .speaker(UPDATED_SPEAKER)
            .addedByUnit(UPDATED_ADDED_BY_UNIT)
            .profession(UPDATED_PROFESSION)
            .document(UPDATED_DOCUMENT)
            .type(UPDATED_TYPE);

        restOtherPersonRecordMockMvc
            .perform(
                put(ENTITY_API_URL_ID, updatedOtherPersonRecord.getId())
                    .with(csrf())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(updatedOtherPersonRecord))
            )
            .andExpect(status().isOk());

        // Validate the OtherPersonRecord in the database
        List<OtherPersonRecord> otherPersonRecordList = otherPersonRecordRepository.findAll();
        assertThat(otherPersonRecordList).hasSize(databaseSizeBeforeUpdate);
        OtherPersonRecord testOtherPersonRecord = otherPersonRecordList.get(otherPersonRecordList.size() - 1);
        assertThat(testOtherPersonRecord.getCity()).isEqualTo(UPDATED_CITY);
        assertThat(testOtherPersonRecord.getRank()).isEqualTo(UPDATED_RANK);
        assertThat(testOtherPersonRecord.getSection()).isEqualTo(UPDATED_SECTION);
        assertThat(testOtherPersonRecord.getMeetingStatus()).isEqualTo(UPDATED_MEETING_STATUS);
        assertThat(testOtherPersonRecord.getStatus()).isEqualTo(UPDATED_STATUS);
        assertThat(testOtherPersonRecord.getSpeaker()).isEqualTo(UPDATED_SPEAKER);
        assertThat(testOtherPersonRecord.getAddedByUnit()).isEqualTo(UPDATED_ADDED_BY_UNIT);
        assertThat(testOtherPersonRecord.getProfession()).isEqualTo(UPDATED_PROFESSION);
        assertThat(testOtherPersonRecord.getDocument()).isEqualTo(UPDATED_DOCUMENT);
        assertThat(testOtherPersonRecord.getType()).isEqualTo(UPDATED_TYPE);
    }

    @Test
    @Transactional
    void putNonExistingOtherPersonRecord() throws Exception {
        int databaseSizeBeforeUpdate = otherPersonRecordRepository.findAll().size();
        otherPersonRecord.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restOtherPersonRecordMockMvc
            .perform(
                put(ENTITY_API_URL_ID, otherPersonRecord.getId())
                    .with(csrf())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(otherPersonRecord))
            )
            .andExpect(status().isBadRequest());

        // Validate the OtherPersonRecord in the database
        List<OtherPersonRecord> otherPersonRecordList = otherPersonRecordRepository.findAll();
        assertThat(otherPersonRecordList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchOtherPersonRecord() throws Exception {
        int databaseSizeBeforeUpdate = otherPersonRecordRepository.findAll().size();
        otherPersonRecord.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restOtherPersonRecordMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .with(csrf())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(otherPersonRecord))
            )
            .andExpect(status().isBadRequest());

        // Validate the OtherPersonRecord in the database
        List<OtherPersonRecord> otherPersonRecordList = otherPersonRecordRepository.findAll();
        assertThat(otherPersonRecordList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamOtherPersonRecord() throws Exception {
        int databaseSizeBeforeUpdate = otherPersonRecordRepository.findAll().size();
        otherPersonRecord.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restOtherPersonRecordMockMvc
            .perform(
                put(ENTITY_API_URL)
                    .with(csrf())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(otherPersonRecord))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the OtherPersonRecord in the database
        List<OtherPersonRecord> otherPersonRecordList = otherPersonRecordRepository.findAll();
        assertThat(otherPersonRecordList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateOtherPersonRecordWithPatch() throws Exception {
        // Initialize the database
        otherPersonRecordRepository.saveAndFlush(otherPersonRecord);

        int databaseSizeBeforeUpdate = otherPersonRecordRepository.findAll().size();

        // Update the otherPersonRecord using partial update
        OtherPersonRecord partialUpdatedOtherPersonRecord = new OtherPersonRecord();
        partialUpdatedOtherPersonRecord.setId(otherPersonRecord.getId());

        partialUpdatedOtherPersonRecord
            .city(UPDATED_CITY)
            .rank(UPDATED_RANK)
            .section(UPDATED_SECTION)
            .addedByUnit(UPDATED_ADDED_BY_UNIT)
            .document(UPDATED_DOCUMENT)
            .type(UPDATED_TYPE);

        restOtherPersonRecordMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedOtherPersonRecord.getId())
                    .with(csrf())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedOtherPersonRecord))
            )
            .andExpect(status().isOk());

        // Validate the OtherPersonRecord in the database
        List<OtherPersonRecord> otherPersonRecordList = otherPersonRecordRepository.findAll();
        assertThat(otherPersonRecordList).hasSize(databaseSizeBeforeUpdate);
        OtherPersonRecord testOtherPersonRecord = otherPersonRecordList.get(otherPersonRecordList.size() - 1);
        assertThat(testOtherPersonRecord.getCity()).isEqualTo(UPDATED_CITY);
        assertThat(testOtherPersonRecord.getRank()).isEqualTo(UPDATED_RANK);
        assertThat(testOtherPersonRecord.getSection()).isEqualTo(UPDATED_SECTION);
        assertThat(testOtherPersonRecord.getMeetingStatus()).isEqualTo(DEFAULT_MEETING_STATUS);
        assertThat(testOtherPersonRecord.getStatus()).isEqualTo(DEFAULT_STATUS);
        assertThat(testOtherPersonRecord.getSpeaker()).isEqualTo(DEFAULT_SPEAKER);
        assertThat(testOtherPersonRecord.getAddedByUnit()).isEqualTo(UPDATED_ADDED_BY_UNIT);
        assertThat(testOtherPersonRecord.getProfession()).isEqualTo(DEFAULT_PROFESSION);
        assertThat(testOtherPersonRecord.getDocument()).isEqualTo(UPDATED_DOCUMENT);
        assertThat(testOtherPersonRecord.getType()).isEqualTo(UPDATED_TYPE);
    }

    @Test
    @Transactional
    void fullUpdateOtherPersonRecordWithPatch() throws Exception {
        // Initialize the database
        otherPersonRecordRepository.saveAndFlush(otherPersonRecord);

        int databaseSizeBeforeUpdate = otherPersonRecordRepository.findAll().size();

        // Update the otherPersonRecord using partial update
        OtherPersonRecord partialUpdatedOtherPersonRecord = new OtherPersonRecord();
        partialUpdatedOtherPersonRecord.setId(otherPersonRecord.getId());

        partialUpdatedOtherPersonRecord
            .city(UPDATED_CITY)
            .rank(UPDATED_RANK)
            .section(UPDATED_SECTION)
            .meetingStatus(UPDATED_MEETING_STATUS)
            .status(UPDATED_STATUS)
            .speaker(UPDATED_SPEAKER)
            .addedByUnit(UPDATED_ADDED_BY_UNIT)
            .profession(UPDATED_PROFESSION)
            .document(UPDATED_DOCUMENT)
            .type(UPDATED_TYPE);

        restOtherPersonRecordMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedOtherPersonRecord.getId())
                    .with(csrf())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedOtherPersonRecord))
            )
            .andExpect(status().isOk());

        // Validate the OtherPersonRecord in the database
        List<OtherPersonRecord> otherPersonRecordList = otherPersonRecordRepository.findAll();
        assertThat(otherPersonRecordList).hasSize(databaseSizeBeforeUpdate);
        OtherPersonRecord testOtherPersonRecord = otherPersonRecordList.get(otherPersonRecordList.size() - 1);
        assertThat(testOtherPersonRecord.getCity()).isEqualTo(UPDATED_CITY);
        assertThat(testOtherPersonRecord.getRank()).isEqualTo(UPDATED_RANK);
        assertThat(testOtherPersonRecord.getSection()).isEqualTo(UPDATED_SECTION);
        assertThat(testOtherPersonRecord.getMeetingStatus()).isEqualTo(UPDATED_MEETING_STATUS);
        assertThat(testOtherPersonRecord.getStatus()).isEqualTo(UPDATED_STATUS);
        assertThat(testOtherPersonRecord.getSpeaker()).isEqualTo(UPDATED_SPEAKER);
        assertThat(testOtherPersonRecord.getAddedByUnit()).isEqualTo(UPDATED_ADDED_BY_UNIT);
        assertThat(testOtherPersonRecord.getProfession()).isEqualTo(UPDATED_PROFESSION);
        assertThat(testOtherPersonRecord.getDocument()).isEqualTo(UPDATED_DOCUMENT);
        assertThat(testOtherPersonRecord.getType()).isEqualTo(UPDATED_TYPE);
    }

    @Test
    @Transactional
    void patchNonExistingOtherPersonRecord() throws Exception {
        int databaseSizeBeforeUpdate = otherPersonRecordRepository.findAll().size();
        otherPersonRecord.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restOtherPersonRecordMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, otherPersonRecord.getId())
                    .with(csrf())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(otherPersonRecord))
            )
            .andExpect(status().isBadRequest());

        // Validate the OtherPersonRecord in the database
        List<OtherPersonRecord> otherPersonRecordList = otherPersonRecordRepository.findAll();
        assertThat(otherPersonRecordList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchOtherPersonRecord() throws Exception {
        int databaseSizeBeforeUpdate = otherPersonRecordRepository.findAll().size();
        otherPersonRecord.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restOtherPersonRecordMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .with(csrf())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(otherPersonRecord))
            )
            .andExpect(status().isBadRequest());

        // Validate the OtherPersonRecord in the database
        List<OtherPersonRecord> otherPersonRecordList = otherPersonRecordRepository.findAll();
        assertThat(otherPersonRecordList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamOtherPersonRecord() throws Exception {
        int databaseSizeBeforeUpdate = otherPersonRecordRepository.findAll().size();
        otherPersonRecord.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restOtherPersonRecordMockMvc
            .perform(
                patch(ENTITY_API_URL)
                    .with(csrf())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(otherPersonRecord))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the OtherPersonRecord in the database
        List<OtherPersonRecord> otherPersonRecordList = otherPersonRecordRepository.findAll();
        assertThat(otherPersonRecordList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteOtherPersonRecord() throws Exception {
        // Initialize the database
        otherPersonRecordRepository.saveAndFlush(otherPersonRecord);

        int databaseSizeBeforeDelete = otherPersonRecordRepository.findAll().size();

        // Delete the otherPersonRecord
        restOtherPersonRecordMockMvc
            .perform(delete(ENTITY_API_URL_ID, otherPersonRecord.getId()).with(csrf()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<OtherPersonRecord> otherPersonRecordList = otherPersonRecordRepository.findAll();
        assertThat(otherPersonRecordList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
