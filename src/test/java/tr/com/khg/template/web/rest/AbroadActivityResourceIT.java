package tr.com.khg.template.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import tr.com.khg.template.IntegrationTest;
import tr.com.khg.template.domain.AbroadActivity;
import tr.com.khg.template.repository.AbroadActivityRepository;

/**
 * Integration tests for the {@link AbroadActivityResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class AbroadActivityResourceIT {

    private static final String DEFAULT_COUNTRY = "AAAAAAAAAA";
    private static final String UPDATED_COUNTRY = "BBBBBBBBBB";

    private static final Integer DEFAULT_START_YEAR = 1;
    private static final Integer UPDATED_START_YEAR = 2;

    private static final Integer DEFAULT_END_YEAR = 1;
    private static final Integer UPDATED_END_YEAR = 2;

    private static final String DEFAULT_POSITION = "AAAAAAAAAA";
    private static final String UPDATED_POSITION = "BBBBBBBBBB";

    private static final Boolean DEFAULT_DELETED = false;
    private static final Boolean UPDATED_DELETED = true;

    private static final String ENTITY_API_URL = "/api/abroad-activities";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private AbroadActivityRepository abroadActivityRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restAbroadActivityMockMvc;

    private AbroadActivity abroadActivity;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static AbroadActivity createEntity(EntityManager em) {
        AbroadActivity abroadActivity = new AbroadActivity()
            .country(DEFAULT_COUNTRY)
            .startYear(DEFAULT_START_YEAR)
            .endYear(DEFAULT_END_YEAR)
            .position(DEFAULT_POSITION)
            .deleted(DEFAULT_DELETED);
        return abroadActivity;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static AbroadActivity createUpdatedEntity(EntityManager em) {
        AbroadActivity abroadActivity = new AbroadActivity()
            .country(UPDATED_COUNTRY)
            .startYear(UPDATED_START_YEAR)
            .endYear(UPDATED_END_YEAR)
            .position(UPDATED_POSITION)
            .deleted(UPDATED_DELETED);
        return abroadActivity;
    }

    @BeforeEach
    public void initTest() {
        abroadActivity = createEntity(em);
    }

    @Test
    @Transactional
    void createAbroadActivity() throws Exception {
        int databaseSizeBeforeCreate = abroadActivityRepository.findAll().size();
        // Create the AbroadActivity
        restAbroadActivityMockMvc
            .perform(
                post(ENTITY_API_URL)
                    .with(csrf())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(abroadActivity))
            )
            .andExpect(status().isCreated());

        // Validate the AbroadActivity in the database
        List<AbroadActivity> abroadActivityList = abroadActivityRepository.findAll();
        assertThat(abroadActivityList).hasSize(databaseSizeBeforeCreate + 1);
        AbroadActivity testAbroadActivity = abroadActivityList.get(abroadActivityList.size() - 1);
        assertThat(testAbroadActivity.getCountry()).isEqualTo(DEFAULT_COUNTRY);
        assertThat(testAbroadActivity.getStartYear()).isEqualTo(DEFAULT_START_YEAR);
        assertThat(testAbroadActivity.getEndYear()).isEqualTo(DEFAULT_END_YEAR);
        assertThat(testAbroadActivity.getPosition()).isEqualTo(DEFAULT_POSITION);
        assertThat(testAbroadActivity.getDeleted()).isEqualTo(DEFAULT_DELETED);
    }

    @Test
    @Transactional
    void createAbroadActivityWithExistingId() throws Exception {
        // Create the AbroadActivity with an existing ID
        abroadActivity.setId(1L);

        int databaseSizeBeforeCreate = abroadActivityRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restAbroadActivityMockMvc
            .perform(
                post(ENTITY_API_URL)
                    .with(csrf())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(abroadActivity))
            )
            .andExpect(status().isBadRequest());

        // Validate the AbroadActivity in the database
        List<AbroadActivity> abroadActivityList = abroadActivityRepository.findAll();
        assertThat(abroadActivityList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void getAllAbroadActivities() throws Exception {
        // Initialize the database
        abroadActivityRepository.saveAndFlush(abroadActivity);

        // Get all the abroadActivityList
        restAbroadActivityMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(abroadActivity.getId().intValue())))
            .andExpect(jsonPath("$.[*].country").value(hasItem(DEFAULT_COUNTRY)))
            .andExpect(jsonPath("$.[*].startYear").value(hasItem(DEFAULT_START_YEAR)))
            .andExpect(jsonPath("$.[*].endYear").value(hasItem(DEFAULT_END_YEAR)))
            .andExpect(jsonPath("$.[*].position").value(hasItem(DEFAULT_POSITION)))
            .andExpect(jsonPath("$.[*].deleted").value(hasItem(DEFAULT_DELETED.booleanValue())));
    }

    @Test
    @Transactional
    void getAbroadActivity() throws Exception {
        // Initialize the database
        abroadActivityRepository.saveAndFlush(abroadActivity);

        // Get the abroadActivity
        restAbroadActivityMockMvc
            .perform(get(ENTITY_API_URL_ID, abroadActivity.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(abroadActivity.getId().intValue()))
            .andExpect(jsonPath("$.country").value(DEFAULT_COUNTRY))
            .andExpect(jsonPath("$.startYear").value(DEFAULT_START_YEAR))
            .andExpect(jsonPath("$.endYear").value(DEFAULT_END_YEAR))
            .andExpect(jsonPath("$.position").value(DEFAULT_POSITION))
            .andExpect(jsonPath("$.deleted").value(DEFAULT_DELETED.booleanValue()));
    }

    @Test
    @Transactional
    void getNonExistingAbroadActivity() throws Exception {
        // Get the abroadActivity
        restAbroadActivityMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putNewAbroadActivity() throws Exception {
        // Initialize the database
        abroadActivityRepository.saveAndFlush(abroadActivity);

        int databaseSizeBeforeUpdate = abroadActivityRepository.findAll().size();

        // Update the abroadActivity
        AbroadActivity updatedAbroadActivity = abroadActivityRepository.findById(abroadActivity.getId()).get();
        // Disconnect from session so that the updates on updatedAbroadActivity are not directly saved in db
        em.detach(updatedAbroadActivity);
        updatedAbroadActivity
            .country(UPDATED_COUNTRY)
            .startYear(UPDATED_START_YEAR)
            .endYear(UPDATED_END_YEAR)
            .position(UPDATED_POSITION)
            .deleted(UPDATED_DELETED);

        restAbroadActivityMockMvc
            .perform(
                put(ENTITY_API_URL_ID, updatedAbroadActivity.getId())
                    .with(csrf())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(updatedAbroadActivity))
            )
            .andExpect(status().isOk());

        // Validate the AbroadActivity in the database
        List<AbroadActivity> abroadActivityList = abroadActivityRepository.findAll();
        assertThat(abroadActivityList).hasSize(databaseSizeBeforeUpdate);
        AbroadActivity testAbroadActivity = abroadActivityList.get(abroadActivityList.size() - 1);
        assertThat(testAbroadActivity.getCountry()).isEqualTo(UPDATED_COUNTRY);
        assertThat(testAbroadActivity.getStartYear()).isEqualTo(UPDATED_START_YEAR);
        assertThat(testAbroadActivity.getEndYear()).isEqualTo(UPDATED_END_YEAR);
        assertThat(testAbroadActivity.getPosition()).isEqualTo(UPDATED_POSITION);
        assertThat(testAbroadActivity.getDeleted()).isEqualTo(UPDATED_DELETED);
    }

    @Test
    @Transactional
    void putNonExistingAbroadActivity() throws Exception {
        int databaseSizeBeforeUpdate = abroadActivityRepository.findAll().size();
        abroadActivity.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restAbroadActivityMockMvc
            .perform(
                put(ENTITY_API_URL_ID, abroadActivity.getId())
                    .with(csrf())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(abroadActivity))
            )
            .andExpect(status().isBadRequest());

        // Validate the AbroadActivity in the database
        List<AbroadActivity> abroadActivityList = abroadActivityRepository.findAll();
        assertThat(abroadActivityList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchAbroadActivity() throws Exception {
        int databaseSizeBeforeUpdate = abroadActivityRepository.findAll().size();
        abroadActivity.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restAbroadActivityMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .with(csrf())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(abroadActivity))
            )
            .andExpect(status().isBadRequest());

        // Validate the AbroadActivity in the database
        List<AbroadActivity> abroadActivityList = abroadActivityRepository.findAll();
        assertThat(abroadActivityList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamAbroadActivity() throws Exception {
        int databaseSizeBeforeUpdate = abroadActivityRepository.findAll().size();
        abroadActivity.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restAbroadActivityMockMvc
            .perform(
                put(ENTITY_API_URL)
                    .with(csrf())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(abroadActivity))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the AbroadActivity in the database
        List<AbroadActivity> abroadActivityList = abroadActivityRepository.findAll();
        assertThat(abroadActivityList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateAbroadActivityWithPatch() throws Exception {
        // Initialize the database
        abroadActivityRepository.saveAndFlush(abroadActivity);

        int databaseSizeBeforeUpdate = abroadActivityRepository.findAll().size();

        // Update the abroadActivity using partial update
        AbroadActivity partialUpdatedAbroadActivity = new AbroadActivity();
        partialUpdatedAbroadActivity.setId(abroadActivity.getId());

        partialUpdatedAbroadActivity.country(UPDATED_COUNTRY).position(UPDATED_POSITION).deleted(UPDATED_DELETED);

        restAbroadActivityMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedAbroadActivity.getId())
                    .with(csrf())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedAbroadActivity))
            )
            .andExpect(status().isOk());

        // Validate the AbroadActivity in the database
        List<AbroadActivity> abroadActivityList = abroadActivityRepository.findAll();
        assertThat(abroadActivityList).hasSize(databaseSizeBeforeUpdate);
        AbroadActivity testAbroadActivity = abroadActivityList.get(abroadActivityList.size() - 1);
        assertThat(testAbroadActivity.getCountry()).isEqualTo(UPDATED_COUNTRY);
        assertThat(testAbroadActivity.getStartYear()).isEqualTo(DEFAULT_START_YEAR);
        assertThat(testAbroadActivity.getEndYear()).isEqualTo(DEFAULT_END_YEAR);
        assertThat(testAbroadActivity.getPosition()).isEqualTo(UPDATED_POSITION);
        assertThat(testAbroadActivity.getDeleted()).isEqualTo(UPDATED_DELETED);
    }

    @Test
    @Transactional
    void fullUpdateAbroadActivityWithPatch() throws Exception {
        // Initialize the database
        abroadActivityRepository.saveAndFlush(abroadActivity);

        int databaseSizeBeforeUpdate = abroadActivityRepository.findAll().size();

        // Update the abroadActivity using partial update
        AbroadActivity partialUpdatedAbroadActivity = new AbroadActivity();
        partialUpdatedAbroadActivity.setId(abroadActivity.getId());

        partialUpdatedAbroadActivity
            .country(UPDATED_COUNTRY)
            .startYear(UPDATED_START_YEAR)
            .endYear(UPDATED_END_YEAR)
            .position(UPDATED_POSITION)
            .deleted(UPDATED_DELETED);

        restAbroadActivityMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedAbroadActivity.getId())
                    .with(csrf())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedAbroadActivity))
            )
            .andExpect(status().isOk());

        // Validate the AbroadActivity in the database
        List<AbroadActivity> abroadActivityList = abroadActivityRepository.findAll();
        assertThat(abroadActivityList).hasSize(databaseSizeBeforeUpdate);
        AbroadActivity testAbroadActivity = abroadActivityList.get(abroadActivityList.size() - 1);
        assertThat(testAbroadActivity.getCountry()).isEqualTo(UPDATED_COUNTRY);
        assertThat(testAbroadActivity.getStartYear()).isEqualTo(UPDATED_START_YEAR);
        assertThat(testAbroadActivity.getEndYear()).isEqualTo(UPDATED_END_YEAR);
        assertThat(testAbroadActivity.getPosition()).isEqualTo(UPDATED_POSITION);
        assertThat(testAbroadActivity.getDeleted()).isEqualTo(UPDATED_DELETED);
    }

    @Test
    @Transactional
    void patchNonExistingAbroadActivity() throws Exception {
        int databaseSizeBeforeUpdate = abroadActivityRepository.findAll().size();
        abroadActivity.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restAbroadActivityMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, abroadActivity.getId())
                    .with(csrf())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(abroadActivity))
            )
            .andExpect(status().isBadRequest());

        // Validate the AbroadActivity in the database
        List<AbroadActivity> abroadActivityList = abroadActivityRepository.findAll();
        assertThat(abroadActivityList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchAbroadActivity() throws Exception {
        int databaseSizeBeforeUpdate = abroadActivityRepository.findAll().size();
        abroadActivity.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restAbroadActivityMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .with(csrf())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(abroadActivity))
            )
            .andExpect(status().isBadRequest());

        // Validate the AbroadActivity in the database
        List<AbroadActivity> abroadActivityList = abroadActivityRepository.findAll();
        assertThat(abroadActivityList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamAbroadActivity() throws Exception {
        int databaseSizeBeforeUpdate = abroadActivityRepository.findAll().size();
        abroadActivity.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restAbroadActivityMockMvc
            .perform(
                patch(ENTITY_API_URL)
                    .with(csrf())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(abroadActivity))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the AbroadActivity in the database
        List<AbroadActivity> abroadActivityList = abroadActivityRepository.findAll();
        assertThat(abroadActivityList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteAbroadActivity() throws Exception {
        // Initialize the database
        abroadActivityRepository.saveAndFlush(abroadActivity);

        int databaseSizeBeforeDelete = abroadActivityRepository.findAll().size();

        // Delete the abroadActivity
        restAbroadActivityMockMvc
            .perform(delete(ENTITY_API_URL_ID, abroadActivity.getId()).with(csrf()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<AbroadActivity> abroadActivityList = abroadActivityRepository.findAll();
        assertThat(abroadActivityList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
