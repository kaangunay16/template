jest.mock('@angular/router');

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { of, Subject } from 'rxjs';

import { PastActivityService } from '../service/past-activity.service';
import { IPastActivity, PastActivity } from '../past-activity.model';
import { IPerson } from 'app/entities/person/person.model';
import { PersonService } from 'app/entities/person/service/person.service';

import { PastActivityUpdateComponent } from './past-activity-update.component';

describe('PastActivity Management Update Component', () => {
  let comp: PastActivityUpdateComponent;
  let fixture: ComponentFixture<PastActivityUpdateComponent>;
  let activatedRoute: ActivatedRoute;
  let pastActivityService: PastActivityService;
  let personService: PersonService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      declarations: [PastActivityUpdateComponent],
      providers: [FormBuilder, ActivatedRoute],
    })
      .overrideTemplate(PastActivityUpdateComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(PastActivityUpdateComponent);
    activatedRoute = TestBed.inject(ActivatedRoute);
    pastActivityService = TestBed.inject(PastActivityService);
    personService = TestBed.inject(PersonService);

    comp = fixture.componentInstance;
  });

  describe('ngOnInit', () => {
    it('Should call Person query and add missing value', () => {
      const pastActivity: IPastActivity = { id: 456 };
      const person: IPerson = { id: 31873 };
      pastActivity.person = person;

      const personCollection: IPerson[] = [{ id: 49142 }];
      jest.spyOn(personService, 'query').mockReturnValue(of(new HttpResponse({ body: personCollection })));
      const additionalPeople = [person];
      const expectedCollection: IPerson[] = [...additionalPeople, ...personCollection];
      jest.spyOn(personService, 'addPersonToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ pastActivity });
      comp.ngOnInit();

      expect(personService.query).toHaveBeenCalled();
      expect(personService.addPersonToCollectionIfMissing).toHaveBeenCalledWith(personCollection, ...additionalPeople);
      expect(comp.peopleSharedCollection).toEqual(expectedCollection);
    });

    it('Should update editForm', () => {
      const pastActivity: IPastActivity = { id: 456 };
      const person: IPerson = { id: 94691 };
      pastActivity.person = person;

      activatedRoute.data = of({ pastActivity });
      comp.ngOnInit();

      expect(comp.editForm.value).toEqual(expect.objectContaining(pastActivity));
      expect(comp.peopleSharedCollection).toContain(person);
    });
  });

  describe('save', () => {
    it('Should call update service on save for existing entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<PastActivity>>();
      const pastActivity = { id: 123 };
      jest.spyOn(pastActivityService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ pastActivity });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: pastActivity }));
      saveSubject.complete();

      // THEN
      expect(comp.previousState).toHaveBeenCalled();
      expect(pastActivityService.update).toHaveBeenCalledWith(pastActivity);
      expect(comp.isSaving).toEqual(false);
    });

    it('Should call create service on save for new entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<PastActivity>>();
      const pastActivity = new PastActivity();
      jest.spyOn(pastActivityService, 'create').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ pastActivity });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: pastActivity }));
      saveSubject.complete();

      // THEN
      expect(pastActivityService.create).toHaveBeenCalledWith(pastActivity);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).toHaveBeenCalled();
    });

    it('Should set isSaving to false on error', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<PastActivity>>();
      const pastActivity = { id: 123 };
      jest.spyOn(pastActivityService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ pastActivity });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.error('This is an error!');

      // THEN
      expect(pastActivityService.update).toHaveBeenCalledWith(pastActivity);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).not.toHaveBeenCalled();
    });
  });

  describe('Tracking relationships identifiers', () => {
    describe('trackPersonById', () => {
      it('Should return tracked Person primary key', () => {
        const entity = { id: 123 };
        const trackResult = comp.trackPersonById(0, entity);
        expect(trackResult).toEqual(entity.id);
      });
    });
  });
});
