package tr.com.khg.template.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.ResponseUtil;
import tr.com.khg.template.domain.OtherPerson;
import tr.com.khg.template.domain.request.OtherPersonCheck;
import tr.com.khg.template.domain.request.Parameter;
import tr.com.khg.template.domain.request.Query;
import tr.com.khg.template.repository.OtherPersonRepository;
import tr.com.khg.template.service.OtherPersonService;
import tr.com.khg.template.web.rest.errors.BadRequestAlertException;

/**
 * REST controller for managing {@link tr.com.khg.template.domain.OtherPerson}.
 */
@RestController
@RequestMapping("/api")
public class OtherPersonResource {

    private final Logger log = LoggerFactory.getLogger(OtherPersonResource.class);

    private static final String ENTITY_NAME = "otherPerson";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final OtherPersonService otherPersonService;

    private final OtherPersonRepository otherPersonRepository;

    public OtherPersonResource(OtherPersonService otherPersonService, OtherPersonRepository otherPersonRepository) {
        this.otherPersonService = otherPersonService;
        this.otherPersonRepository = otherPersonRepository;
    }

    /**
     * {@code POST  /other-people} : Create a new otherPerson.
     *
     * @param otherPerson the otherPerson to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new otherPerson, or with status {@code 400 (Bad Request)} if the otherPerson has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/other-people")
    public ResponseEntity<OtherPerson> createOtherPerson(@RequestBody OtherPerson otherPerson) throws URISyntaxException {
        log.debug("REST request to save OtherPerson : {}", otherPerson);
        if (otherPerson.getId() != null) {
            throw new BadRequestAlertException("A new otherPerson cannot already have an ID", ENTITY_NAME, "idexists");
        }
        OtherPerson result = otherPersonService.save(otherPerson);
        return ResponseEntity
            .created(new URI("/api/other-people/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /other-people/:id} : Updates an existing otherPerson.
     *
     * @param id the id of the otherPerson to save.
     * @param otherPerson the otherPerson to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated otherPerson,
     * or with status {@code 400 (Bad Request)} if the otherPerson is not valid,
     * or with status {@code 500 (Internal Server Error)} if the otherPerson couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/other-people/{id}")
    public ResponseEntity<OtherPerson> updateOtherPerson(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody OtherPerson otherPerson
    ) throws URISyntaxException {
        log.debug("REST request to update OtherPerson : {}, {}", id, otherPerson);
        if (otherPerson.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, otherPerson.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!otherPersonRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        OtherPerson result = otherPersonService.save(otherPerson);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, otherPerson.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /other-people/:id} : Partial updates given fields of an existing otherPerson, field will ignore if it is null
     *
     * @param id the id of the otherPerson to save.
     * @param otherPerson the otherPerson to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated otherPerson,
     * or with status {@code 400 (Bad Request)} if the otherPerson is not valid,
     * or with status {@code 404 (Not Found)} if the otherPerson is not found,
     * or with status {@code 500 (Internal Server Error)} if the otherPerson couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/other-people/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<OtherPerson> partialUpdateOtherPerson(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody OtherPerson otherPerson
    ) throws URISyntaxException {
        log.debug("REST request to partial update OtherPerson partially : {}, {}", id, otherPerson);
        if (otherPerson.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, otherPerson.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!otherPersonRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<OtherPerson> result = otherPersonService.partialUpdate(otherPerson);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, otherPerson.getId().toString())
        );
    }

    /**
     * {@code GET  /other-people} : get all the otherPeople.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of otherPeople in body.
     */
    @GetMapping("/other-people")
    public List<OtherPerson> getAllOtherPeople() {
        log.debug("REST request to get all OtherPeople");
        return otherPersonService.findAll();
    }

    /**
     * {@code GET  /other-people/:id} : get the "id" otherPerson.
     *
     * @param id the id of the otherPerson to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the otherPerson, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/other-people/{id}")
    public ResponseEntity<OtherPerson> getOtherPerson(@PathVariable Long id) {
        log.debug("REST request to get OtherPerson : {}", id);
        Optional<OtherPerson> otherPerson = otherPersonService.findOne(id);
        return ResponseUtil.wrapOrNotFound(otherPerson);
    }

    /**
     * {@code GET  /other-people/recordId=:id} : get the "id" otherPersonRecord.
     *
     * @param id the id of the otherPersonRecord
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the otherPerson, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/other-people/record-id={id}")
    public ResponseEntity<OtherPerson> getOtherPersonWithRecordId(@PathVariable Long id) {
        log.debug("REST request to get OtherPerson with record id : {}", id);
        Optional<OtherPerson> otherPerson = otherPersonService.findByRecordId(id);
        return ResponseUtil.wrapOrNotFound(otherPerson);
    }

    /**
     * {@code DELETE  /other-people/:id} : delete the "id" otherPerson.
     *
     * @param id the id of the otherPerson to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/other-people/{id}")
    public ResponseEntity<Void> deleteOtherPerson(@PathVariable Long id) {
        log.debug("REST request to delete OtherPerson : {}", id);
        otherPersonService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }

    /**
     * {@code GET  /other-people/identity=:identityNumber} : get the "identity" person.
     *
     * @param identityNumber the id of the other person to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the person, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/other-people/identity={identityNumber}")
    public ResponseEntity<OtherPerson> getOtherPerson(@PathVariable String identityNumber) {
        log.debug("REST request to get Other Person : {}", identityNumber);
        Optional<OtherPerson> otherPerson = otherPersonService.findByIdentityNumber(identityNumber);
        return ResponseUtil.wrapOrNotFound(otherPerson);
    }

    /**
     * Get other persons with filters
     *
     * @param query search parameters
     * @return filtered other persons
     */
    @PostMapping("/other-people/filter")
    public ResponseEntity<List<OtherPerson>> filterPersons(@RequestBody Query query) {
        log.debug("REST request to filter persons");
        List<OtherPerson> filteredPersons = otherPersonService.filterPerson(query);
        return ResponseEntity.ok().body(filteredPersons);
    }

    /**
     * Check if exists other person
     *
     * @param otherPersonCheck identity numbers will be checked
     * @return existing identity numbers list
     */
    @PostMapping("/other-people/check-if-exists")
    public ResponseEntity<List<String>> checkOtherPersonIfExists(@RequestBody OtherPersonCheck otherPersonCheck) {
        log.debug("REST request to checking if exists other person");
        return ResponseEntity
            .ok()
            .body(otherPersonService.checkOtherPersonIfExists(otherPersonCheck.getIdentityNumbers(), otherPersonCheck.getCity()));
    }
}
