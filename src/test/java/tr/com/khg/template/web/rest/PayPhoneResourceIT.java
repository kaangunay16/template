package tr.com.khg.template.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import tr.com.khg.template.IntegrationTest;
import tr.com.khg.template.domain.PayPhone;
import tr.com.khg.template.repository.PayPhoneRepository;

/**
 * Integration tests for the {@link PayPhoneResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class PayPhoneResourceIT {

    private static final String DEFAULT_CITY = "AAAAAAAAAA";
    private static final String UPDATED_CITY = "BBBBBBBBBB";

    private static final String DEFAULT_PHONE_NUMBER = "AAAAAAAAAA";
    private static final String UPDATED_PHONE_NUMBER = "BBBBBBBBBB";

    private static final String ENTITY_API_URL = "/api/pay-phones";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private PayPhoneRepository payPhoneRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restPayPhoneMockMvc;

    private PayPhone payPhone;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static PayPhone createEntity(EntityManager em) {
        PayPhone payPhone = new PayPhone().city(DEFAULT_CITY).phoneNumber(DEFAULT_PHONE_NUMBER);
        return payPhone;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static PayPhone createUpdatedEntity(EntityManager em) {
        PayPhone payPhone = new PayPhone().city(UPDATED_CITY).phoneNumber(UPDATED_PHONE_NUMBER);
        return payPhone;
    }

    @BeforeEach
    public void initTest() {
        payPhone = createEntity(em);
    }

    @Test
    @Transactional
    void createPayPhone() throws Exception {
        int databaseSizeBeforeCreate = payPhoneRepository.findAll().size();
        // Create the PayPhone
        restPayPhoneMockMvc
            .perform(
                post(ENTITY_API_URL)
                    .with(csrf())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(payPhone))
            )
            .andExpect(status().isCreated());

        // Validate the PayPhone in the database
        List<PayPhone> payPhoneList = payPhoneRepository.findAll();
        assertThat(payPhoneList).hasSize(databaseSizeBeforeCreate + 1);
        PayPhone testPayPhone = payPhoneList.get(payPhoneList.size() - 1);
        assertThat(testPayPhone.getCity()).isEqualTo(DEFAULT_CITY);
        assertThat(testPayPhone.getPhoneNumber()).isEqualTo(DEFAULT_PHONE_NUMBER);
    }

    @Test
    @Transactional
    void createPayPhoneWithExistingId() throws Exception {
        // Create the PayPhone with an existing ID
        payPhone.setId(1L);

        int databaseSizeBeforeCreate = payPhoneRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restPayPhoneMockMvc
            .perform(
                post(ENTITY_API_URL)
                    .with(csrf())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(payPhone))
            )
            .andExpect(status().isBadRequest());

        // Validate the PayPhone in the database
        List<PayPhone> payPhoneList = payPhoneRepository.findAll();
        assertThat(payPhoneList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void getAllPayPhones() throws Exception {
        // Initialize the database
        payPhoneRepository.saveAndFlush(payPhone);

        // Get all the payPhoneList
        restPayPhoneMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(payPhone.getId().intValue())))
            .andExpect(jsonPath("$.[*].city").value(hasItem(DEFAULT_CITY)))
            .andExpect(jsonPath("$.[*].phoneNumber").value(hasItem(DEFAULT_PHONE_NUMBER)));
    }

    @Test
    @Transactional
    void getPayPhone() throws Exception {
        // Initialize the database
        payPhoneRepository.saveAndFlush(payPhone);

        // Get the payPhone
        restPayPhoneMockMvc
            .perform(get(ENTITY_API_URL_ID, payPhone.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(payPhone.getId().intValue()))
            .andExpect(jsonPath("$.city").value(DEFAULT_CITY))
            .andExpect(jsonPath("$.phoneNumber").value(DEFAULT_PHONE_NUMBER));
    }

    @Test
    @Transactional
    void getNonExistingPayPhone() throws Exception {
        // Get the payPhone
        restPayPhoneMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putNewPayPhone() throws Exception {
        // Initialize the database
        payPhoneRepository.saveAndFlush(payPhone);

        int databaseSizeBeforeUpdate = payPhoneRepository.findAll().size();

        // Update the payPhone
        PayPhone updatedPayPhone = payPhoneRepository.findById(payPhone.getId()).get();
        // Disconnect from session so that the updates on updatedPayPhone are not directly saved in db
        em.detach(updatedPayPhone);
        updatedPayPhone.city(UPDATED_CITY).phoneNumber(UPDATED_PHONE_NUMBER);

        restPayPhoneMockMvc
            .perform(
                put(ENTITY_API_URL_ID, updatedPayPhone.getId())
                    .with(csrf())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(updatedPayPhone))
            )
            .andExpect(status().isOk());

        // Validate the PayPhone in the database
        List<PayPhone> payPhoneList = payPhoneRepository.findAll();
        assertThat(payPhoneList).hasSize(databaseSizeBeforeUpdate);
        PayPhone testPayPhone = payPhoneList.get(payPhoneList.size() - 1);
        assertThat(testPayPhone.getCity()).isEqualTo(UPDATED_CITY);
        assertThat(testPayPhone.getPhoneNumber()).isEqualTo(UPDATED_PHONE_NUMBER);
    }

    @Test
    @Transactional
    void putNonExistingPayPhone() throws Exception {
        int databaseSizeBeforeUpdate = payPhoneRepository.findAll().size();
        payPhone.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restPayPhoneMockMvc
            .perform(
                put(ENTITY_API_URL_ID, payPhone.getId())
                    .with(csrf())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(payPhone))
            )
            .andExpect(status().isBadRequest());

        // Validate the PayPhone in the database
        List<PayPhone> payPhoneList = payPhoneRepository.findAll();
        assertThat(payPhoneList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchPayPhone() throws Exception {
        int databaseSizeBeforeUpdate = payPhoneRepository.findAll().size();
        payPhone.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restPayPhoneMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .with(csrf())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(payPhone))
            )
            .andExpect(status().isBadRequest());

        // Validate the PayPhone in the database
        List<PayPhone> payPhoneList = payPhoneRepository.findAll();
        assertThat(payPhoneList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamPayPhone() throws Exception {
        int databaseSizeBeforeUpdate = payPhoneRepository.findAll().size();
        payPhone.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restPayPhoneMockMvc
            .perform(
                put(ENTITY_API_URL)
                    .with(csrf())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(payPhone))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the PayPhone in the database
        List<PayPhone> payPhoneList = payPhoneRepository.findAll();
        assertThat(payPhoneList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdatePayPhoneWithPatch() throws Exception {
        // Initialize the database
        payPhoneRepository.saveAndFlush(payPhone);

        int databaseSizeBeforeUpdate = payPhoneRepository.findAll().size();

        // Update the payPhone using partial update
        PayPhone partialUpdatedPayPhone = new PayPhone();
        partialUpdatedPayPhone.setId(payPhone.getId());

        partialUpdatedPayPhone.city(UPDATED_CITY).phoneNumber(UPDATED_PHONE_NUMBER);

        restPayPhoneMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedPayPhone.getId())
                    .with(csrf())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedPayPhone))
            )
            .andExpect(status().isOk());

        // Validate the PayPhone in the database
        List<PayPhone> payPhoneList = payPhoneRepository.findAll();
        assertThat(payPhoneList).hasSize(databaseSizeBeforeUpdate);
        PayPhone testPayPhone = payPhoneList.get(payPhoneList.size() - 1);
        assertThat(testPayPhone.getCity()).isEqualTo(UPDATED_CITY);
        assertThat(testPayPhone.getPhoneNumber()).isEqualTo(UPDATED_PHONE_NUMBER);
    }

    @Test
    @Transactional
    void fullUpdatePayPhoneWithPatch() throws Exception {
        // Initialize the database
        payPhoneRepository.saveAndFlush(payPhone);

        int databaseSizeBeforeUpdate = payPhoneRepository.findAll().size();

        // Update the payPhone using partial update
        PayPhone partialUpdatedPayPhone = new PayPhone();
        partialUpdatedPayPhone.setId(payPhone.getId());

        partialUpdatedPayPhone.city(UPDATED_CITY).phoneNumber(UPDATED_PHONE_NUMBER);

        restPayPhoneMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedPayPhone.getId())
                    .with(csrf())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedPayPhone))
            )
            .andExpect(status().isOk());

        // Validate the PayPhone in the database
        List<PayPhone> payPhoneList = payPhoneRepository.findAll();
        assertThat(payPhoneList).hasSize(databaseSizeBeforeUpdate);
        PayPhone testPayPhone = payPhoneList.get(payPhoneList.size() - 1);
        assertThat(testPayPhone.getCity()).isEqualTo(UPDATED_CITY);
        assertThat(testPayPhone.getPhoneNumber()).isEqualTo(UPDATED_PHONE_NUMBER);
    }

    @Test
    @Transactional
    void patchNonExistingPayPhone() throws Exception {
        int databaseSizeBeforeUpdate = payPhoneRepository.findAll().size();
        payPhone.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restPayPhoneMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, payPhone.getId())
                    .with(csrf())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(payPhone))
            )
            .andExpect(status().isBadRequest());

        // Validate the PayPhone in the database
        List<PayPhone> payPhoneList = payPhoneRepository.findAll();
        assertThat(payPhoneList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchPayPhone() throws Exception {
        int databaseSizeBeforeUpdate = payPhoneRepository.findAll().size();
        payPhone.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restPayPhoneMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .with(csrf())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(payPhone))
            )
            .andExpect(status().isBadRequest());

        // Validate the PayPhone in the database
        List<PayPhone> payPhoneList = payPhoneRepository.findAll();
        assertThat(payPhoneList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamPayPhone() throws Exception {
        int databaseSizeBeforeUpdate = payPhoneRepository.findAll().size();
        payPhone.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restPayPhoneMockMvc
            .perform(
                patch(ENTITY_API_URL)
                    .with(csrf())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(payPhone))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the PayPhone in the database
        List<PayPhone> payPhoneList = payPhoneRepository.findAll();
        assertThat(payPhoneList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deletePayPhone() throws Exception {
        // Initialize the database
        payPhoneRepository.saveAndFlush(payPhone);

        int databaseSizeBeforeDelete = payPhoneRepository.findAll().size();

        // Delete the payPhone
        restPayPhoneMockMvc
            .perform(delete(ENTITY_API_URL_ID, payPhone.getId()).with(csrf()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<PayPhone> payPhoneList = payPhoneRepository.findAll();
        assertThat(payPhoneList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
