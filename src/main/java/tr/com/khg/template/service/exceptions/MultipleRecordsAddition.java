package tr.com.khg.template.service.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.NOT_ACCEPTABLE, reason = "Çoklu Kayıt")
public class MultipleRecordsAddition extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public MultipleRecordsAddition() {
        super("Aynı anda birden fazla il faaliyeti eklenemez!");
    }
}
