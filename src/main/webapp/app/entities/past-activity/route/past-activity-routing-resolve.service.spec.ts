jest.mock('@angular/router');

import { TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ActivatedRouteSnapshot, Router } from '@angular/router';
import { of } from 'rxjs';

import { IPastActivity, PastActivity } from '../past-activity.model';
import { PastActivityService } from '../service/past-activity.service';

import { PastActivityRoutingResolveService } from './past-activity-routing-resolve.service';

describe('PastActivity routing resolve service', () => {
  let mockRouter: Router;
  let mockActivatedRouteSnapshot: ActivatedRouteSnapshot;
  let routingResolveService: PastActivityRoutingResolveService;
  let service: PastActivityService;
  let resultPastActivity: IPastActivity | undefined;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [Router, ActivatedRouteSnapshot],
    });
    mockRouter = TestBed.inject(Router);
    mockActivatedRouteSnapshot = TestBed.inject(ActivatedRouteSnapshot);
    routingResolveService = TestBed.inject(PastActivityRoutingResolveService);
    service = TestBed.inject(PastActivityService);
    resultPastActivity = undefined;
  });

  describe('resolve', () => {
    it('should return IPastActivity returned by find', () => {
      // GIVEN
      service.find = jest.fn(id => of(new HttpResponse({ body: { id } })));
      mockActivatedRouteSnapshot.params = { id: 123 };

      // WHEN
      routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
        resultPastActivity = result;
      });

      // THEN
      expect(service.find).toBeCalledWith(123);
      expect(resultPastActivity).toEqual({ id: 123 });
    });

    it('should return new IPastActivity if id is not provided', () => {
      // GIVEN
      service.find = jest.fn();
      mockActivatedRouteSnapshot.params = {};

      // WHEN
      routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
        resultPastActivity = result;
      });

      // THEN
      expect(service.find).not.toBeCalled();
      expect(resultPastActivity).toEqual(new PastActivity());
    });

    it('should route to 404 page if data not found in server', () => {
      // GIVEN
      jest.spyOn(service, 'find').mockReturnValue(of(new HttpResponse({ body: null as unknown as PastActivity })));
      mockActivatedRouteSnapshot.params = { id: 123 };

      // WHEN
      routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
        resultPastActivity = result;
      });

      // THEN
      expect(service.find).toBeCalledWith(123);
      expect(resultPastActivity).toEqual(undefined);
      expect(mockRouter.navigate).toHaveBeenCalledWith(['404']);
    });
  });
});
