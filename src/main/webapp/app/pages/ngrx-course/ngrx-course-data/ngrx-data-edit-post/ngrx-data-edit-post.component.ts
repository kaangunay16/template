import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

import { DataPostService } from '../data-post.service';

@Component({
  selector: 'jhi-ngrx-data-edit-post',
  templateUrl: './ngrx-data-edit-post.component.html',
  styleUrls: ['./ngrx-data-edit-post.component.scss'],
})
export class NgrxDataEditPostComponent implements OnInit {
  selectionForm!: FormGroup;
  id?: number;

  constructor(private postService: DataPostService, private route: ActivatedRoute, private router: Router) {}

  ngOnInit(): void {
    this.selectionForm = new FormGroup({
      id: new FormControl(null),
      key: new FormControl(null),
      value: new FormControl(null),
      priority: new FormControl(null),
    });

    this.id = +this.route.snapshot.params['id'];

    this.postService.entities$.subscribe(selections => {
      const selection = selections.find(sel => sel.id === this.id);

      if (selection) {
        this.selectionForm.patchValue({
          id: selection.id,
          key: selection.key,
          value: selection.value,
          priority: selection.priority,
        });
      }
    });
  }

  onSelectionEdit(): void {
    this.postService.update(this.selectionForm.value);
    this.router.navigate(['/ngrx', 'data']);
  }
}
