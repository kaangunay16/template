import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ActualActivityComponent } from './actual-activity.component';

describe('ActualActivityComponent', () => {
  let component: ActualActivityComponent;
  let fixture: ComponentFixture<ActualActivityComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ActualActivityComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ActualActivityComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
