import { Component } from '@angular/core';

@Component({
  selector: 'jhi-ngrx-loading-spinner',
  templateUrl: './ngrx-loading-spinner.component.html',
  styleUrls: ['./ngrx-loading-spinner.component.scss'],
})
export class NgrxLoadingSpinnerComponent {}
