import { Params, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

import { HasId } from 'app/entities/form-entity.model';

export class PageService<T extends HasId> {
  private _entities: T[] | null = null;

  constructor(public toastr: ToastrService, public router: Router) {}

  get entities(): T[] | null {
    return this._entities;
  }

  set entities(entities: T[] | null) {
    this._entities = entities;
  }

  addEntity(entity: T): void {
    if (!this.entities) {
      this.entities = [];
    }

    this.entities.push(entity);
  }

  addSingleEntity(entity: T): void {
    this.entities = [entity];
  }

  getSingleEntity(id?: number): T {
    if (!this.entities || this.entities.length === 0) {
      this.toastr.error('Kayıt bulunamadı!');
      this.router.navigate(['/404']);
      throw new Error('Kayıt Bulunamadı!');
    }

    if (!id) {
      return this.entities[0];
    }
    return this.entities.find(entity => entity.id === id)!;
  }

  entityCheck(params: Params): T {
    const id = params['id'];
    return this.getSingleEntity(Number(id));
  }

  clearEntities(): void {
    this.entities = null;
  }

  changeEntity(oldEntity: T, newEntity: T): void {
    const index = this.entities!.indexOf(oldEntity);
    this.entities!.splice(index, 1, newEntity);
  }
}
