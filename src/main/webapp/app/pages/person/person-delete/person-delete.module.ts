import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ComponentsModule } from 'app/components/components.module';
import { SharedModule } from 'app/shared/shared.module';
import { PersonDeleteRoutingModule } from './person-delete-routing.module';
import { PersonDeleteMainComponent } from './person-delete-main/person-delete-main.component';
import { PersonDeletionComponent } from './person-deletion/person-deletion.component';
import { PersonDeletionDemandsComponent } from './person-deletion-demands/person-deletion-demands.component';
import { PersonDeletionConfirmationComponent } from './person-deletion-confirmation/person-deletion-confirmation.component';

@NgModule({
  declarations: [PersonDeleteMainComponent, PersonDeletionComponent, PersonDeletionDemandsComponent, PersonDeletionConfirmationComponent],
  imports: [CommonModule, PersonDeleteRoutingModule, ComponentsModule, SharedModule],
})
export class PersonDeleteModule {}
