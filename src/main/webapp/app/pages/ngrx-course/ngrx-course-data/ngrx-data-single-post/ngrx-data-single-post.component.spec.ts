import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NgrxDataSinglePostComponent } from './ngrx-data-single-post.component';

describe('NgrxDataSinglePostComponent', () => {
  let component: NgrxDataSinglePostComponent;
  let fixture: ComponentFixture<NgrxDataSinglePostComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [NgrxDataSinglePostComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NgrxDataSinglePostComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
