import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NgrxDataPostListComponent } from './ngrx-data-post-list.component';

describe('NgrxDataPostListComponent', () => {
  let component: NgrxDataPostListComponent;
  let fixture: ComponentFixture<NgrxDataPostListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [NgrxDataPostListComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NgrxDataPostListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
