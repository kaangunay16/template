import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IOtherPerson } from '../other-person.model';

@Component({
  selector: 'jhi-other-person-detail',
  templateUrl: './other-person-detail.component.html',
})
export class OtherPersonDetailComponent implements OnInit {
  otherPerson: IOtherPerson | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ otherPerson }) => {
      this.otherPerson = otherPerson;
    });
  }

  previousState(): void {
    window.history.back();
  }
}
