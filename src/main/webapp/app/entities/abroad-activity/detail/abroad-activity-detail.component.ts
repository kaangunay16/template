import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IAbroadActivity } from '../abroad-activity.model';

@Component({
  selector: 'jhi-abroad-activity-detail',
  templateUrl: './abroad-activity-detail.component.html',
})
export class AbroadActivityDetailComponent implements OnInit {
  abroadActivity: IAbroadActivity | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ abroadActivity }) => {
      this.abroadActivity = abroadActivity;
    });
  }

  previousState(): void {
    window.history.back();
  }
}
