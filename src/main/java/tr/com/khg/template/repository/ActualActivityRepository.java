package tr.com.khg.template.repository;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;
import tr.com.khg.template.domain.ActualActivity;

/**
 * Spring Data SQL repository for the ActualActivity entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ActualActivityRepository extends JpaRepository<ActualActivity, Long> {}
