import { NgModule } from '@angular/core';
import { SharedModule } from 'app/shared/shared.module';
import { OtherPersonRecordComponent } from './list/other-person-record.component';
import { OtherPersonRecordDetailComponent } from './detail/other-person-record-detail.component';
import { OtherPersonRecordUpdateComponent } from './update/other-person-record-update.component';
import { OtherPersonRecordDeleteDialogComponent } from './delete/other-person-record-delete-dialog.component';
import { OtherPersonRecordRoutingModule } from './route/other-person-record-routing.module';

@NgModule({
  imports: [SharedModule, OtherPersonRecordRoutingModule],
  declarations: [
    OtherPersonRecordComponent,
    OtherPersonRecordDetailComponent,
    OtherPersonRecordUpdateComponent,
    OtherPersonRecordDeleteDialogComponent,
  ],
  entryComponents: [OtherPersonRecordDeleteDialogComponent],
})
export class OtherPersonRecordModule {}
