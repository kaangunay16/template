import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { IPayPhone, PayPhone } from '../pay-phone.model';
import { PayPhoneService } from '../service/pay-phone.service';

@Injectable({ providedIn: 'root' })
export class PayPhoneRoutingResolveService implements Resolve<IPayPhone> {
  constructor(protected service: PayPhoneService, protected router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IPayPhone> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        mergeMap((payPhone: HttpResponse<PayPhone>) => {
          if (payPhone.body) {
            return of(payPhone.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new PayPhone());
  }
}
