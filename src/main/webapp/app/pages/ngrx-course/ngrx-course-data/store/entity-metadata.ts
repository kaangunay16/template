import { EntityDataModuleConfig, EntityMetadataMap } from '@ngrx/data';

const entityMetadata: EntityMetadataMap = {
  Selection: {
    entityDispatcherOptions: {
      optimisticUpdate: true, // Önce store'u güncelle sonra güncelleme isteğini yap
      optimisticDelete: true,
    },
  },
};

export const entityConfig: EntityDataModuleConfig = {
  entityMetadata,
};
