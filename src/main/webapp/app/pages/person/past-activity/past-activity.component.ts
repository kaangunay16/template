import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';

import { IPerson } from 'app/entities/person/person.model';
import { PersonModuleService } from 'app/pages/person/person-module.service';
import { DynamicFormCreatorModel, IDynamicFormCreatorModel } from 'app/components/dynamic-form-creator/dynamic-form-field.model';
import { FormValueConverterService } from 'app/components/dynamic-form-creator/form-value-converter.service';
import { IPastActivity } from 'app/entities/past-activity/past-activity.model';

@Component({
  selector: 'jhi-past-activity',
  templateUrl: './past-activity.component.html',
  styleUrls: ['./past-activity.component.scss'],
})
export class PastActivityComponent implements OnInit, OnDestroy {
  person!: IPerson;
  formCreator!: IDynamicFormCreatorModel;
  routeSub!: Subscription;

  constructor(
    private personModuleService: PersonModuleService,
    private formValueService: FormValueConverterService,
    private activatedRoute: ActivatedRoute,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.routeSub = this.activatedRoute.queryParams.subscribe(params => {
      this.person = this.personModuleService.entityCheck(params);
      this.formCreator = new DynamicFormCreatorModel([
        {
          id: 'pastActivity',
          type: 'pastActivity',
          label: 'Geçmiş Faaliyet',
          multiple: true,
          value: this.person.pastActivities,
        },
      ]);
    });
  }

  ngOnDestroy(): void {
    this.routeSub.unsubscribe();
  }

  submit(): void {
    this.setFormValuesToEntity();
    this.router.navigate(['/person', 'actual-activities']);
  }

  updateSubmit(): void {
    this.setFormValuesToEntity();
    this.personModuleService.updatePerson(this.person);
  }

  private setFormValuesToEntity(): void {
    this.person.pastActivities = this.formValueService.getFormValue(this.formCreator, 'pastActivity') as IPastActivity[];
  }
}
