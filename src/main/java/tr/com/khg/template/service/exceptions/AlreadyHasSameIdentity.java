package tr.com.khg.template.service.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.NOT_ACCEPTABLE, reason = "Kimlik Numarası")
public class AlreadyHasSameIdentity extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public AlreadyHasSameIdentity(String identityNo) {
        super("Aynı kimlik numarasına sahip kayıt bulunmaktadır: " + identityNo);
    }
}
