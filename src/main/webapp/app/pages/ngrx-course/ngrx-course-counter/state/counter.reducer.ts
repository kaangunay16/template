import { Action, createReducer, on } from '@ngrx/store';

import { initialState } from './counter.state';
import { customIncrement, decrement, increment, reset, textChanged } from './counter.actions';

const _counterReducer = createReducer(
  initialState,
  on(increment, state => ({ ...state, counter: state.counter + 1 })),
  on(decrement, state => ({ ...state, counter: state.counter - 1 })),
  on(reset, state => ({ ...state, counter: 0 })),
  on(customIncrement, (state, action) => ({ ...state, counter: state.counter + action.value })),
  on(textChanged, (state, action) => ({ ...state, text: action.text }))
);

export function counterReducer(state: any, action: Action): any {
  return _counterReducer(state, action);
}
