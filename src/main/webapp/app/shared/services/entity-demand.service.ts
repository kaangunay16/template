import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Observable, of } from 'rxjs';

import { IDemand } from 'app/entities/demand/demand.model';
import { PageService } from 'app/shared/services/page-service';
import { DemandService } from 'app/entities/demand/service/demand.service';

@Injectable({
  providedIn: 'root',
})
export class EntityDemandService extends PageService<IDemand> {
  constructor(toastr: ToastrService, router: Router, private demandService: DemandService) {
    super(toastr, router);
  }

  getAllEntities(demandType: string): Observable<HttpResponse<IDemand[]>> {
    if (this.entities) {
      return of(new HttpResponse({ body: this.entities }));
    }

    return this.demandService.getAllWaitingDemands(demandType);
  }

  remove(id: number): void {
    if (this.entities) {
      this.entities = this.entities.filter(entity => entity.id !== id);
    }
  }
}
