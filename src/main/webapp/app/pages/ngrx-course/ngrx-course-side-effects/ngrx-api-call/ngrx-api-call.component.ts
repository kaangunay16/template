import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';

import { AppState } from '../../store/app.state';
import { ISelection } from 'app/entities/selection/selection.model';
import { selectionRequestStart } from '../state/side-effect.actions';
import { getSelections } from '../state/side-effect.selector';
import { setLoadingSpinner } from '../../store/shared/shared.action';

@Component({
  selector: 'jhi-ngrx-api-call',
  templateUrl: './ngrx-api-call.component.html',
  styleUrls: ['./ngrx-api-call.component.scss'],
})
export class NgrxApiCallComponent implements OnInit {
  selections$!: Observable<ISelection[] | null>;

  constructor(private store: Store<AppState>) {}

  ngOnInit(): void {
    this.store.dispatch(setLoadingSpinner({ status: true }));
    this.store.dispatch(selectionRequestStart({ selectionType: 'Bursa' }));
    this.selections$ = this.store.select(getSelections);
  }
}
