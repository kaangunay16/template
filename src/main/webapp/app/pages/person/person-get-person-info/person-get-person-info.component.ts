import { Component, OnDestroy, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';

import { PersonService } from 'app/entities/person/service/person.service';
import { ErrorSupportService } from 'app/shared/services/error-support.service';
import { PersonModuleService } from 'app/pages/person/person-module.service';
import { IdentityRetrieverComponent } from 'app/components/identity-retriever/identity-retriever.component';
@Component({
  selector: 'jhi-person-get-person-info',
  templateUrl: './person-get-person-info.component.html',
  styleUrls: ['./person-get-person-info.component.scss'],
})
export class PersonGetPersonInfoComponent implements OnDestroy {
  @ViewChild('retriever')
  retriever!: IdentityRetrieverComponent;

  personSub?: Subscription;

  constructor(
    private personService: PersonService,
    private errorService: ErrorSupportService,
    private personModuleService: PersonModuleService,
    private router: Router
  ) {}

  ngOnDestroy(): void {
    this.personSub?.unsubscribe();
  }

  identityInfoChanged(identity: string): void {
    this.personSub = this.personService.findByIdentityNumber(identity).subscribe(
      res => {
        if (res.body) {
          this.personModuleService.addSingleEntity(res.body);
          this.router.navigate(['/person', 'person-result'], { queryParams: { mode: 'update' } });
        }
      },
      err => this.errorService.errorHandler(err)
    );

    this.retriever.dialogRef?.close();
  }
}
