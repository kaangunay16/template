import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { of } from 'rxjs';

import { PayPhoneService } from '../service/pay-phone.service';

import { PayPhoneComponent } from './pay-phone.component';

describe('PayPhone Management Component', () => {
  let comp: PayPhoneComponent;
  let fixture: ComponentFixture<PayPhoneComponent>;
  let service: PayPhoneService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      declarations: [PayPhoneComponent],
    })
      .overrideTemplate(PayPhoneComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(PayPhoneComponent);
    comp = fixture.componentInstance;
    service = TestBed.inject(PayPhoneService);

    const headers = new HttpHeaders();
    jest.spyOn(service, 'query').mockReturnValue(
      of(
        new HttpResponse({
          body: [{ id: 123 }],
          headers,
        })
      )
    );
  });

  it('Should call load all on init', () => {
    // WHEN
    comp.ngOnInit();

    // THEN
    expect(service.query).toHaveBeenCalled();
    expect(comp.payPhones?.[0]).toEqual(expect.objectContaining({ id: 123 }));
  });
});
