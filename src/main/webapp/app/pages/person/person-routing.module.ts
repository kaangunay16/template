import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PersonMainComponent } from './person-main/person-main.component';
import { PersonGetIdentityComponent } from './person-get-identity/person-get-identity.component';
import { PersonGetPersonInfoComponent } from './person-get-person-info/person-get-person-info.component';
import { IdentityInfoComponent } from './identity-info/identity-info.component';
import { OrganizationalInfoComponent } from './organizational-info/organizational-info.component';
import { PastActivityComponent } from './past-activity/past-activity.component';
import { ActualActivityComponent } from './actual-activity/actual-activity.component';
import { AbroadActivityComponent } from './abroad-activity/abroad-activity.component';
import { PersonResultTableComponent } from './person-result-table/person-result-table.component';

const routes: Routes = [
  {
    path: '',
    component: PersonMainComponent,
    children: [
      {
        path: 'get-identity-info',
        component: PersonGetIdentityComponent,
      },
      {
        path: 'get-person-info',
        component: PersonGetPersonInfoComponent,
      },
      {
        path: 'identity-info',
        component: IdentityInfoComponent,
      },
      {
        path: 'organizational-info',
        component: OrganizationalInfoComponent,
      },
      {
        path: 'past-activities',
        component: PastActivityComponent,
      },
      {
        path: 'actual-activities',
        component: ActualActivityComponent,
      },
      {
        path: 'abroad-activities',
        component: AbroadActivityComponent,
      },
      {
        path: 'person-result',
        component: PersonResultTableComponent,
      },
      {
        path: 'person-deletion',
        loadChildren: () => import('./person-delete/person-delete.module').then(m => m.PersonDeleteModule),
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PersonRoutingModule {}
