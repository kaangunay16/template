package tr.com.khg.template.service.impl;

import java.time.LocalDate;
import java.time.Month;
import org.springframework.stereotype.Service;
import tr.com.khg.template.domain.Identity;
import tr.com.khg.template.domain.enumeration.Genders;
import tr.com.khg.template.domain.enumeration.MaritalStatus;
import tr.com.khg.template.service.IdentityRetrieverService;

@Service
public class IdentityRetrieverServiceImpl implements IdentityRetrieverService {

    @Override
    public Identity getIdentity(String identityNumber) {
        Identity information = new Identity();
        information.setIdentityNo(identityNumber);
        information.setName("Test");
        information.setSurname("Test");
        information.setAddress("City");
        information.setBirthday(LocalDate.of(2000, Month.JANUARY, 1));
        information.setFatherName("Father");
        information.setMotherName("Mother");
        information.setMaritalStatus(MaritalStatus.BEKAR);
        information.setEducation("University");
        information.setGender(Genders.ERKEK);
        information.setRegisteredCity("City");
        information.setRegisteredDistrict("District");
        information.setRegisteredVillage("Village");
        information.setPlaceOfBirth("District");
        information.setProfession("Profession");
        return information;
    }
}
