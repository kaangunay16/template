import { Component, Input, OnInit } from '@angular/core';
import { ControlValueAccessor, FormBuilder, FormGroup, NG_VALUE_ACCESSOR } from '@angular/forms';

import { DynamicFormFunctions } from 'app/components/dynamic-form-creator/dynamic-form-field.model';
import { DYNAMIC_FIELD } from 'app/components/dynamic-form-creator/dyanmic-form.token';
import { DynamicFormService } from 'app/components/dynamic-form-creator/dynamic-form.service';
import { AbroadActivity } from 'app/entities/abroad-activity/abroad-activity.model';
import { AbroadActivityService } from 'app/entities/abroad-activity/service/abroad-activity.service';
import { PersonModuleService } from 'app/pages/person/person-module.service';
import { FormFieldsService } from '../form-fields.service';

@Component({
  selector: 'jhi-abroad-activity-field',
  templateUrl: './abroad-activity-field.component.html',
  styleUrls: ['./abroad-activity-field.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: AbroadActivityFieldComponent,
      multi: true,
    },
    {
      provide: DYNAMIC_FIELD,
      useExisting: AbroadActivityFieldComponent,
    },
  ],
})
export class AbroadActivityFieldComponent implements OnInit, ControlValueAccessor, DynamicFormFunctions {
  form!: FormGroup;

  @Input()
  readOnly?: boolean = false;

  onTouched!: () => void;

  constructor(
    private formBuilder: FormBuilder,
    private formFieldsService: FormFieldsService,
    private service: AbroadActivityService,
    private dynamicFormService: DynamicFormService,
    private personModuleService: PersonModuleService
  ) {}

  ngOnInit(): void {
    this.form = this.formBuilder.group({
      id: [{ value: null }],
      country: [{ value: null, disabled: this.readOnly }],
      startYear: [{ value: null, disabled: this.readOnly }],
      endYear: [{ value: null, disabled: this.readOnly }],
      position: [{ value: null, disabled: this.readOnly }],
      deleted: [{ value: null }],
      person: [{ value: null }],
    });
  }

  registerOnChange(fn: (value: any) => void): void {
    this.form.valueChanges.subscribe(fn);
  }

  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

  writeValue(obj: AbroadActivity | null): void {
    this.form.patchValue({
      id: obj ? obj.id : null,
      country: obj ? obj.country : null,
      startYear: obj ? obj.startYear : null,
      endYear: obj ? obj.endYear : null,
      position: obj ? obj.position : null,
      deleted: obj ? obj.deleted : false,
      person: obj ? obj.person : null,
    });
  }

  remove(): void {
    const id = this.form.get('id')?.value;

    if (id) {
      this.formFieldsService.remove(id, this.service);
      this.personModuleService.removeAbroadActivity(this.dynamicFormService.getEntityId(), id);
    }
  }
}
