import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';

import { IMeeting } from 'app/entities/meeting/meeting.model';
import { MeetingModuleService } from '../meeting-module.service';
import { MeetingInfoComponent } from '../meeting-info/meeting-info.component';
import { MeetingAttendeesComponent } from '../meeting-attendees/meeting-attendees.component';

@Component({
  selector: 'jhi-meeting-finalize',
  templateUrl: './meeting-finalize.component.html',
  styleUrls: ['./meeting-finalize.component.scss'],
})
export class MeetingFinalizeComponent implements OnInit, OnDestroy {
  @ViewChild('meeting')
  meetingComponent!: MeetingInfoComponent;

  @ViewChild('attendees')
  attendeesComponent!: MeetingAttendeesComponent;

  meeting!: IMeeting;
  routeSub!: Subscription;

  constructor(private activatedRoute: ActivatedRoute, private meetingModuleService: MeetingModuleService) {}

  ngOnInit(): void {
    this.routeSub = this.activatedRoute.queryParams.subscribe(params => {
      this.meeting = this.meetingModuleService.entityCheck(params);
    });
  }

  ngOnDestroy(): void {
    this.meetingModuleService.clearToRemoveAttendees();
    this.routeSub.unsubscribe();
  }

  saveMeeting(): void {
    if (this.meetingModuleService.toRemoveAttendees) {
      this.meetingModuleService.toRemoveAttendees.forEach(attendee => (attendee.deleted = true));
    }

    this.meetingComponent.setFormValuesToEntity();
    this.attendeesComponent.createMeeting(this.meetingModuleService.toRemoveAttendees);
  }
}
