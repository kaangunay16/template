import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RxjsCourse4Component } from './rxjs-course4.component';

describe('RxjsCourse4Component', () => {
  let component: RxjsCourse4Component;
  let fixture: ComponentFixture<RxjsCourse4Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [RxjsCourse4Component],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RxjsCourse4Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
