import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { isPresent } from 'app/core/util/operators';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { IOtherName, getOtherNameIdentifier } from '../other-name.model';
import { IFormEntity } from '../../form-entity.model';

export type EntityResponseType = HttpResponse<IOtherName>;
export type EntityArrayResponseType = HttpResponse<IOtherName[]>;

@Injectable({ providedIn: 'root' })
export class OtherNameService implements IFormEntity {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/other-names');

  constructor(protected http: HttpClient, protected applicationConfigService: ApplicationConfigService) {}

  create(otherName: IOtherName): Observable<EntityResponseType> {
    return this.http.post<IOtherName>(this.resourceUrl, otherName, { observe: 'response' });
  }

  update(otherName: IOtherName): Observable<EntityResponseType> {
    return this.http.put<IOtherName>(`${this.resourceUrl}/${getOtherNameIdentifier(otherName) as number}`, otherName, {
      observe: 'response',
    });
  }

  partialUpdate(otherName: IOtherName): Observable<EntityResponseType> {
    return this.http.patch<IOtherName>(`${this.resourceUrl}/${getOtherNameIdentifier(otherName) as number}`, otherName, {
      observe: 'response',
    });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IOtherName>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IOtherName[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  addOtherNameToCollectionIfMissing(
    otherNameCollection: IOtherName[],
    ...otherNamesToCheck: (IOtherName | null | undefined)[]
  ): IOtherName[] {
    const otherNames: IOtherName[] = otherNamesToCheck.filter(isPresent);
    if (otherNames.length > 0) {
      const otherNameCollectionIdentifiers = otherNameCollection.map(otherNameItem => getOtherNameIdentifier(otherNameItem)!);
      const otherNamesToAdd = otherNames.filter(otherNameItem => {
        const otherNameIdentifier = getOtherNameIdentifier(otherNameItem);
        if (otherNameIdentifier == null || otherNameCollectionIdentifiers.includes(otherNameIdentifier)) {
          return false;
        }
        otherNameCollectionIdentifiers.push(otherNameIdentifier);
        return true;
      });
      return [...otherNamesToAdd, ...otherNameCollection];
    }
    return otherNameCollection;
  }
}
