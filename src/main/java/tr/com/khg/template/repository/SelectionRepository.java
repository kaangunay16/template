package tr.com.khg.template.repository;

import java.util.List;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;
import tr.com.khg.template.domain.Selection;

/**
 * Spring Data SQL repository for the Selection entity.
 */
@SuppressWarnings("unused")
@Repository
public interface SelectionRepository extends JpaRepository<Selection, Long> {
    List<Selection> findByKeyOrderByPriority(String key);
}
