package tr.com.khg.template.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.ResponseUtil;
import tr.com.khg.template.domain.MeetingAttendee;
import tr.com.khg.template.repository.MeetingAttendeeRepository;
import tr.com.khg.template.service.MeetingAttendeeService;
import tr.com.khg.template.web.rest.errors.BadRequestAlertException;

/**
 * REST controller for managing {@link tr.com.khg.template.domain.MeetingAttendee}.
 */
@RestController
@RequestMapping("/api")
public class MeetingAttendeeResource {

    private final Logger log = LoggerFactory.getLogger(MeetingAttendeeResource.class);

    private static final String ENTITY_NAME = "meetingAttendee";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final MeetingAttendeeService meetingAttendeeService;

    private final MeetingAttendeeRepository meetingAttendeeRepository;

    public MeetingAttendeeResource(MeetingAttendeeService meetingAttendeeService, MeetingAttendeeRepository meetingAttendeeRepository) {
        this.meetingAttendeeService = meetingAttendeeService;
        this.meetingAttendeeRepository = meetingAttendeeRepository;
    }

    /**
     * {@code POST  /meeting-attendees} : Create a new meetingAttendee.
     *
     * @param meetingAttendee the meetingAttendee to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new meetingAttendee, or with status {@code 400 (Bad Request)} if the meetingAttendee has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/meeting-attendees")
    public ResponseEntity<MeetingAttendee> createMeetingAttendee(@RequestBody MeetingAttendee meetingAttendee) throws URISyntaxException {
        log.debug("REST request to save MeetingAttendee : {}", meetingAttendee);
        if (meetingAttendee.getId() != null) {
            throw new BadRequestAlertException("A new meetingAttendee cannot already have an ID", ENTITY_NAME, "idexists");
        }
        MeetingAttendee result = meetingAttendeeService.save(meetingAttendee);
        return ResponseEntity
            .created(new URI("/api/meeting-attendees/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /meeting-attendees/:id} : Updates an existing meetingAttendee.
     *
     * @param id the id of the meetingAttendee to save.
     * @param meetingAttendee the meetingAttendee to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated meetingAttendee,
     * or with status {@code 400 (Bad Request)} if the meetingAttendee is not valid,
     * or with status {@code 500 (Internal Server Error)} if the meetingAttendee couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/meeting-attendees/{id}")
    public ResponseEntity<MeetingAttendee> updateMeetingAttendee(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody MeetingAttendee meetingAttendee
    ) throws URISyntaxException {
        log.debug("REST request to update MeetingAttendee : {}, {}", id, meetingAttendee);
        if (meetingAttendee.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, meetingAttendee.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!meetingAttendeeRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        MeetingAttendee result = meetingAttendeeService.save(meetingAttendee);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, meetingAttendee.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /meeting-attendees/:id} : Partial updates given fields of an existing meetingAttendee, field will ignore if it is null
     *
     * @param id the id of the meetingAttendee to save.
     * @param meetingAttendee the meetingAttendee to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated meetingAttendee,
     * or with status {@code 400 (Bad Request)} if the meetingAttendee is not valid,
     * or with status {@code 404 (Not Found)} if the meetingAttendee is not found,
     * or with status {@code 500 (Internal Server Error)} if the meetingAttendee couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/meeting-attendees/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<MeetingAttendee> partialUpdateMeetingAttendee(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody MeetingAttendee meetingAttendee
    ) throws URISyntaxException {
        log.debug("REST request to partial update MeetingAttendee partially : {}, {}", id, meetingAttendee);
        if (meetingAttendee.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, meetingAttendee.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!meetingAttendeeRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<MeetingAttendee> result = meetingAttendeeService.partialUpdate(meetingAttendee);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, meetingAttendee.getId().toString())
        );
    }

    /**
     * {@code GET  /meeting-attendees} : get all the meetingAttendees.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of meetingAttendees in body.
     */
    @GetMapping("/meeting-attendees")
    public List<MeetingAttendee> getAllMeetingAttendees() {
        log.debug("REST request to get all MeetingAttendees");
        return meetingAttendeeService.findAll();
    }

    /**
     * {@code GET  /meeting-attendees/:id} : get the "id" meetingAttendee.
     *
     * @param id the id of the meetingAttendee to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the meetingAttendee, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/meeting-attendees/{id}")
    public ResponseEntity<MeetingAttendee> getMeetingAttendee(@PathVariable Long id) {
        log.debug("REST request to get MeetingAttendee : {}", id);
        Optional<MeetingAttendee> meetingAttendee = meetingAttendeeService.findOne(id);
        return ResponseUtil.wrapOrNotFound(meetingAttendee);
    }

    /**
     * {@code DELETE  /meeting-attendees/:id} : delete the "id" meetingAttendee.
     *
     * @param id the id of the meetingAttendee to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/meeting-attendees/{id}")
    public ResponseEntity<Void> deleteMeetingAttendee(@PathVariable Long id) {
        log.debug("REST request to delete MeetingAttendee : {}", id);
        meetingAttendeeService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }
}
