import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

import { identityNumberRegex } from 'app/shared/patterns/patterns';

@Component({
  selector: 'jhi-quick-search',
  templateUrl: './quick-search.component.html',
  styleUrls: ['./quick-search.component.scss'],
})
export class QuickSearchComponent implements OnInit {
  form?: FormGroup;

  constructor(private router: Router) {}

  ngOnInit(): void {
    this.form = new FormGroup({
      tckNo: new FormControl(null, [Validators.required, Validators.pattern(identityNumberRegex)]),
    });
  }

  onSubmit(): void {
    this.router.navigate(['/sorgulama'], { queryParams: { tckNo: this.form?.get('tckNo')?.value } });
  }
}
