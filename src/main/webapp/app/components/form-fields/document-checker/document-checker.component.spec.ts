import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DocumentCheckerComponent } from './document-checker.component';

describe('DocumentCheckerComponent', () => {
  let component: DocumentCheckerComponent;
  let fixture: ComponentFixture<DocumentCheckerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [DocumentCheckerComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DocumentCheckerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
