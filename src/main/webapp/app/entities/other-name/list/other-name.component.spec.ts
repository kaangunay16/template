import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { of } from 'rxjs';

import { OtherNameService } from '../service/other-name.service';

import { OtherNameComponent } from './other-name.component';

describe('OtherName Management Component', () => {
  let comp: OtherNameComponent;
  let fixture: ComponentFixture<OtherNameComponent>;
  let service: OtherNameService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      declarations: [OtherNameComponent],
    })
      .overrideTemplate(OtherNameComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(OtherNameComponent);
    comp = fixture.componentInstance;
    service = TestBed.inject(OtherNameService);

    const headers = new HttpHeaders();
    jest.spyOn(service, 'query').mockReturnValue(
      of(
        new HttpResponse({
          body: [{ id: 123 }],
          headers,
        })
      )
    );
  });

  it('Should call load all on init', () => {
    // WHEN
    comp.ngOnInit();

    // THEN
    expect(service.query).toHaveBeenCalled();
    expect(comp.otherNames?.[0]).toEqual(expect.objectContaining({ id: 123 }));
  });
});
