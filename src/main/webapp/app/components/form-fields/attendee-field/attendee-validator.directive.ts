import { Directive } from '@angular/core';
import { AbstractControl, NG_VALIDATORS, ValidationErrors, Validator } from '@angular/forms';

import { AttendeeField } from 'app/components/models/attendee-field.model';

@Directive({
  selector: '[jhiAttendeeValidator]',
  providers: [
    {
      provide: NG_VALIDATORS,
      useExisting: AttendeeValidatorDirective,
      multi: true,
    },
  ],
})
export class AttendeeValidatorDirective implements Validator {
  validate(control: AbstractControl): ValidationErrors | null {
    const value = control.value as AttendeeField | null;

    if (value?.identityNumber && value.identityNumber.length > 0 && value.status) {
      return null;
    }

    return { invalidAttendee: true };
  }
}
