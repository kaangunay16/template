import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { RxjsCourseMainComponent } from './rxjs-course-main/rxjs-course-main.component';
import { RxjsCourse4Component } from './rxjs-course4/rxjs-course4.component';
import { RxjsCourse5Component } from './rxjs-course5/rxjs-course5.component';

const routes: Routes = [
  {
    path: '',
    component: RxjsCourseMainComponent,
    children: [
      {
        path: '4',
        component: RxjsCourse4Component,
      },
      {
        path: '5',
        component: RxjsCourse5Component,
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RxjsCourseRoutingModule {}
