import { Component } from '@angular/core';
import { Router } from '@angular/router';

import { IIdentity } from 'app/entities/identity/identity.model';
import { OtherPerson } from 'app/entities/other-person/other-person.model';
import { OtherPersonModuleService } from '../other-person-module.service';
import { OtherPersonRecord } from 'app/entities/other-person-record/other-person-record.model';

@Component({
  selector: 'jhi-other-person-get-identity',
  templateUrl: './other-person-get-identity.component.html',
  styleUrls: ['./other-person-get-identity.component.scss'],
})
export class OtherPersonGetIdentityComponent {
  constructor(private router: Router, private otherPersonModuleService: OtherPersonModuleService) {}

  identityInfoChanged(identity: IIdentity): void {
    const otherPerson = new OtherPerson();
    otherPerson.identity = identity;
    otherPerson.otherPersonRecords = [new OtherPersonRecord()];
    this.otherPersonModuleService.addSingleEntity(otherPerson);
    this.router.navigate(['/other-person', 'information']);
  }
}
