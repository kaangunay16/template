package tr.com.khg.template.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import tr.com.khg.template.IntegrationTest;
import tr.com.khg.template.domain.MeetingAttendee;
import tr.com.khg.template.repository.MeetingAttendeeRepository;

/**
 * Integration tests for the {@link MeetingAttendeeResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class MeetingAttendeeResourceIT {

    private static final String DEFAULT_STATUS = "AAAAAAAAAA";
    private static final String UPDATED_STATUS = "BBBBBBBBBB";

    private static final String ENTITY_API_URL = "/api/meeting-attendees";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private MeetingAttendeeRepository meetingAttendeeRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restMeetingAttendeeMockMvc;

    private MeetingAttendee meetingAttendee;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static MeetingAttendee createEntity(EntityManager em) {
        MeetingAttendee meetingAttendee = new MeetingAttendee().status(DEFAULT_STATUS);
        return meetingAttendee;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static MeetingAttendee createUpdatedEntity(EntityManager em) {
        MeetingAttendee meetingAttendee = new MeetingAttendee().status(UPDATED_STATUS);
        return meetingAttendee;
    }

    @BeforeEach
    public void initTest() {
        meetingAttendee = createEntity(em);
    }

    @Test
    @Transactional
    void createMeetingAttendee() throws Exception {
        int databaseSizeBeforeCreate = meetingAttendeeRepository.findAll().size();
        // Create the MeetingAttendee
        restMeetingAttendeeMockMvc
            .perform(
                post(ENTITY_API_URL)
                    .with(csrf())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(meetingAttendee))
            )
            .andExpect(status().isCreated());

        // Validate the MeetingAttendee in the database
        List<MeetingAttendee> meetingAttendeeList = meetingAttendeeRepository.findAll();
        assertThat(meetingAttendeeList).hasSize(databaseSizeBeforeCreate + 1);
        MeetingAttendee testMeetingAttendee = meetingAttendeeList.get(meetingAttendeeList.size() - 1);
        assertThat(testMeetingAttendee.getStatus()).isEqualTo(DEFAULT_STATUS);
    }

    @Test
    @Transactional
    void createMeetingAttendeeWithExistingId() throws Exception {
        // Create the MeetingAttendee with an existing ID
        meetingAttendee.setId(1L);

        int databaseSizeBeforeCreate = meetingAttendeeRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restMeetingAttendeeMockMvc
            .perform(
                post(ENTITY_API_URL)
                    .with(csrf())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(meetingAttendee))
            )
            .andExpect(status().isBadRequest());

        // Validate the MeetingAttendee in the database
        List<MeetingAttendee> meetingAttendeeList = meetingAttendeeRepository.findAll();
        assertThat(meetingAttendeeList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void getAllMeetingAttendees() throws Exception {
        // Initialize the database
        meetingAttendeeRepository.saveAndFlush(meetingAttendee);

        // Get all the meetingAttendeeList
        restMeetingAttendeeMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(meetingAttendee.getId().intValue())))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS)));
    }

    @Test
    @Transactional
    void getMeetingAttendee() throws Exception {
        // Initialize the database
        meetingAttendeeRepository.saveAndFlush(meetingAttendee);

        // Get the meetingAttendee
        restMeetingAttendeeMockMvc
            .perform(get(ENTITY_API_URL_ID, meetingAttendee.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(meetingAttendee.getId().intValue()))
            .andExpect(jsonPath("$.status").value(DEFAULT_STATUS));
    }

    @Test
    @Transactional
    void getNonExistingMeetingAttendee() throws Exception {
        // Get the meetingAttendee
        restMeetingAttendeeMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putNewMeetingAttendee() throws Exception {
        // Initialize the database
        meetingAttendeeRepository.saveAndFlush(meetingAttendee);

        int databaseSizeBeforeUpdate = meetingAttendeeRepository.findAll().size();

        // Update the meetingAttendee
        MeetingAttendee updatedMeetingAttendee = meetingAttendeeRepository.findById(meetingAttendee.getId()).get();
        // Disconnect from session so that the updates on updatedMeetingAttendee are not directly saved in db
        em.detach(updatedMeetingAttendee);
        updatedMeetingAttendee.status(UPDATED_STATUS);

        restMeetingAttendeeMockMvc
            .perform(
                put(ENTITY_API_URL_ID, updatedMeetingAttendee.getId())
                    .with(csrf())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(updatedMeetingAttendee))
            )
            .andExpect(status().isOk());

        // Validate the MeetingAttendee in the database
        List<MeetingAttendee> meetingAttendeeList = meetingAttendeeRepository.findAll();
        assertThat(meetingAttendeeList).hasSize(databaseSizeBeforeUpdate);
        MeetingAttendee testMeetingAttendee = meetingAttendeeList.get(meetingAttendeeList.size() - 1);
        assertThat(testMeetingAttendee.getStatus()).isEqualTo(UPDATED_STATUS);
    }

    @Test
    @Transactional
    void putNonExistingMeetingAttendee() throws Exception {
        int databaseSizeBeforeUpdate = meetingAttendeeRepository.findAll().size();
        meetingAttendee.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restMeetingAttendeeMockMvc
            .perform(
                put(ENTITY_API_URL_ID, meetingAttendee.getId())
                    .with(csrf())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(meetingAttendee))
            )
            .andExpect(status().isBadRequest());

        // Validate the MeetingAttendee in the database
        List<MeetingAttendee> meetingAttendeeList = meetingAttendeeRepository.findAll();
        assertThat(meetingAttendeeList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchMeetingAttendee() throws Exception {
        int databaseSizeBeforeUpdate = meetingAttendeeRepository.findAll().size();
        meetingAttendee.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restMeetingAttendeeMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .with(csrf())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(meetingAttendee))
            )
            .andExpect(status().isBadRequest());

        // Validate the MeetingAttendee in the database
        List<MeetingAttendee> meetingAttendeeList = meetingAttendeeRepository.findAll();
        assertThat(meetingAttendeeList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamMeetingAttendee() throws Exception {
        int databaseSizeBeforeUpdate = meetingAttendeeRepository.findAll().size();
        meetingAttendee.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restMeetingAttendeeMockMvc
            .perform(
                put(ENTITY_API_URL)
                    .with(csrf())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(meetingAttendee))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the MeetingAttendee in the database
        List<MeetingAttendee> meetingAttendeeList = meetingAttendeeRepository.findAll();
        assertThat(meetingAttendeeList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateMeetingAttendeeWithPatch() throws Exception {
        // Initialize the database
        meetingAttendeeRepository.saveAndFlush(meetingAttendee);

        int databaseSizeBeforeUpdate = meetingAttendeeRepository.findAll().size();

        // Update the meetingAttendee using partial update
        MeetingAttendee partialUpdatedMeetingAttendee = new MeetingAttendee();
        partialUpdatedMeetingAttendee.setId(meetingAttendee.getId());

        partialUpdatedMeetingAttendee.status(UPDATED_STATUS);

        restMeetingAttendeeMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedMeetingAttendee.getId())
                    .with(csrf())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedMeetingAttendee))
            )
            .andExpect(status().isOk());

        // Validate the MeetingAttendee in the database
        List<MeetingAttendee> meetingAttendeeList = meetingAttendeeRepository.findAll();
        assertThat(meetingAttendeeList).hasSize(databaseSizeBeforeUpdate);
        MeetingAttendee testMeetingAttendee = meetingAttendeeList.get(meetingAttendeeList.size() - 1);
        assertThat(testMeetingAttendee.getStatus()).isEqualTo(UPDATED_STATUS);
    }

    @Test
    @Transactional
    void fullUpdateMeetingAttendeeWithPatch() throws Exception {
        // Initialize the database
        meetingAttendeeRepository.saveAndFlush(meetingAttendee);

        int databaseSizeBeforeUpdate = meetingAttendeeRepository.findAll().size();

        // Update the meetingAttendee using partial update
        MeetingAttendee partialUpdatedMeetingAttendee = new MeetingAttendee();
        partialUpdatedMeetingAttendee.setId(meetingAttendee.getId());

        partialUpdatedMeetingAttendee.status(UPDATED_STATUS);

        restMeetingAttendeeMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedMeetingAttendee.getId())
                    .with(csrf())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedMeetingAttendee))
            )
            .andExpect(status().isOk());

        // Validate the MeetingAttendee in the database
        List<MeetingAttendee> meetingAttendeeList = meetingAttendeeRepository.findAll();
        assertThat(meetingAttendeeList).hasSize(databaseSizeBeforeUpdate);
        MeetingAttendee testMeetingAttendee = meetingAttendeeList.get(meetingAttendeeList.size() - 1);
        assertThat(testMeetingAttendee.getStatus()).isEqualTo(UPDATED_STATUS);
    }

    @Test
    @Transactional
    void patchNonExistingMeetingAttendee() throws Exception {
        int databaseSizeBeforeUpdate = meetingAttendeeRepository.findAll().size();
        meetingAttendee.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restMeetingAttendeeMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, meetingAttendee.getId())
                    .with(csrf())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(meetingAttendee))
            )
            .andExpect(status().isBadRequest());

        // Validate the MeetingAttendee in the database
        List<MeetingAttendee> meetingAttendeeList = meetingAttendeeRepository.findAll();
        assertThat(meetingAttendeeList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchMeetingAttendee() throws Exception {
        int databaseSizeBeforeUpdate = meetingAttendeeRepository.findAll().size();
        meetingAttendee.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restMeetingAttendeeMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .with(csrf())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(meetingAttendee))
            )
            .andExpect(status().isBadRequest());

        // Validate the MeetingAttendee in the database
        List<MeetingAttendee> meetingAttendeeList = meetingAttendeeRepository.findAll();
        assertThat(meetingAttendeeList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamMeetingAttendee() throws Exception {
        int databaseSizeBeforeUpdate = meetingAttendeeRepository.findAll().size();
        meetingAttendee.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restMeetingAttendeeMockMvc
            .perform(
                patch(ENTITY_API_URL)
                    .with(csrf())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(meetingAttendee))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the MeetingAttendee in the database
        List<MeetingAttendee> meetingAttendeeList = meetingAttendeeRepository.findAll();
        assertThat(meetingAttendeeList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteMeetingAttendee() throws Exception {
        // Initialize the database
        meetingAttendeeRepository.saveAndFlush(meetingAttendee);

        int databaseSizeBeforeDelete = meetingAttendeeRepository.findAll().size();

        // Delete the meetingAttendee
        restMeetingAttendeeMockMvc
            .perform(delete(ENTITY_API_URL_ID, meetingAttendee.getId()).with(csrf()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<MeetingAttendee> meetingAttendeeList = meetingAttendeeRepository.findAll();
        assertThat(meetingAttendeeList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
