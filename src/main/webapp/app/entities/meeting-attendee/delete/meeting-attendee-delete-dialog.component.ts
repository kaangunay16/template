import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { IMeetingAttendee } from '../meeting-attendee.model';
import { MeetingAttendeeService } from '../service/meeting-attendee.service';

@Component({
  templateUrl: './meeting-attendee-delete-dialog.component.html',
})
export class MeetingAttendeeDeleteDialogComponent {
  meetingAttendee?: IMeetingAttendee;

  constructor(protected meetingAttendeeService: MeetingAttendeeService, protected activeModal: NgbActiveModal) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.meetingAttendeeService.delete(id).subscribe(() => {
      this.activeModal.close('deleted');
    });
  }
}
