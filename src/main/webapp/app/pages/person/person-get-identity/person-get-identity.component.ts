import { Component } from '@angular/core';
import { Router } from '@angular/router';

import { IIdentity } from 'app/entities/identity/identity.model';
import { Person } from 'app/entities/person/person.model';
import { PersonModuleService } from 'app/pages/person/person-module.service';

@Component({
  selector: 'jhi-person-get-identity',
  templateUrl: './person-get-identity.component.html',
  styleUrls: ['./person-get-identity.component.scss'],
})
export class PersonGetIdentityComponent {
  constructor(private personModuleService: PersonModuleService, private router: Router) {}

  identityInfoChanged(identity: IIdentity): void {
    const person = new Person();
    person.identity = identity;
    this.personModuleService.addSingleEntity(person);
    this.router.navigate(['/person', 'identity-info']);
  }
}
