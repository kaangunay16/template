import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NgrxSingleCallComponent } from './ngrx-single-call.component';

describe('NgrxSingleCallComponent', () => {
  let component: NgrxSingleCallComponent;
  let fixture: ComponentFixture<NgrxSingleCallComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [NgrxSingleCallComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NgrxSingleCallComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
