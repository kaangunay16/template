import { PageTableConfigs } from 'app/components/models/page-table-config.model';
import { TableAction } from 'app/components/table/table-action.model';

export const meetingTableConfigs: PageTableConfigs = {
  columns: [{ column: 'city' }, { column: 'subject' }],
  actions: [],
  searchParameters: [],
};

export const unfinalizedActions: TableAction[] = [
  {
    link: ['/meeting', 'info'],
    icon: 'edit',
    tooltip: 'Toplantıyı Düzenle',
  },
  {
    link: ['/meeting', 'finalize-meeting'],
    icon: 'stop',
    tooltip: 'Toplantıyı Tamamla',
  },
];

export const finalizedActions: TableAction[] = [
  {
    link: ['/meeting', 'finalize-meeting'],
    icon: 'edit',
    tooltip: 'Toplantıyı Düzenle',
  },
];
