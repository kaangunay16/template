import { Phone } from '../../entities/phone/phone.model';
import { ActivityField } from './activity-field.model';

export type DynamicReturnType = string | string[] | Phone | Phone[] | ActivityField | ActivityField[] | null;
