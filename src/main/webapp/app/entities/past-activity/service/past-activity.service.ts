import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { isPresent } from 'app/core/util/operators';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { IPastActivity, getPastActivityIdentifier } from '../past-activity.model';
import { IFormEntity } from '../../form-entity.model';

export type EntityResponseType = HttpResponse<IPastActivity>;
export type EntityArrayResponseType = HttpResponse<IPastActivity[]>;

@Injectable({ providedIn: 'root' })
export class PastActivityService implements IFormEntity {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/past-activities');

  constructor(protected http: HttpClient, protected applicationConfigService: ApplicationConfigService) {}

  create(pastActivity: IPastActivity): Observable<EntityResponseType> {
    return this.http.post<IPastActivity>(this.resourceUrl, pastActivity, { observe: 'response' });
  }

  update(pastActivity: IPastActivity): Observable<EntityResponseType> {
    return this.http.put<IPastActivity>(`${this.resourceUrl}/${getPastActivityIdentifier(pastActivity) as number}`, pastActivity, {
      observe: 'response',
    });
  }

  partialUpdate(pastActivity: IPastActivity): Observable<EntityResponseType> {
    return this.http.patch<IPastActivity>(`${this.resourceUrl}/${getPastActivityIdentifier(pastActivity) as number}`, pastActivity, {
      observe: 'response',
    });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IPastActivity>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IPastActivity[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  addPastActivityToCollectionIfMissing(
    pastActivityCollection: IPastActivity[],
    ...pastActivitiesToCheck: (IPastActivity | null | undefined)[]
  ): IPastActivity[] {
    const pastActivities: IPastActivity[] = pastActivitiesToCheck.filter(isPresent);
    if (pastActivities.length > 0) {
      const pastActivityCollectionIdentifiers = pastActivityCollection.map(
        pastActivityItem => getPastActivityIdentifier(pastActivityItem)!
      );
      const pastActivitiesToAdd = pastActivities.filter(pastActivityItem => {
        const pastActivityIdentifier = getPastActivityIdentifier(pastActivityItem);
        if (pastActivityIdentifier == null || pastActivityCollectionIdentifiers.includes(pastActivityIdentifier)) {
          return false;
        }
        pastActivityCollectionIdentifiers.push(pastActivityIdentifier);
        return true;
      });
      return [...pastActivitiesToAdd, ...pastActivityCollection];
    }
    return pastActivityCollection;
  }
}
