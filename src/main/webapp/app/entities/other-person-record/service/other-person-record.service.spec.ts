import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { IOtherPersonRecord, OtherPersonRecord } from '../other-person-record.model';

import { OtherPersonRecordService } from './other-person-record.service';

describe('OtherPersonRecord Service', () => {
  let service: OtherPersonRecordService;
  let httpMock: HttpTestingController;
  let elemDefault: IOtherPersonRecord;
  let expectedResult: IOtherPersonRecord | IOtherPersonRecord[] | boolean | null;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    expectedResult = null;
    service = TestBed.inject(OtherPersonRecordService);
    httpMock = TestBed.inject(HttpTestingController);

    elemDefault = {
      id: 0,
      city: 'AAAAAAA',
      rank: 'AAAAAAA',
      section: 'AAAAAAA',
      meetingStatus: 'AAAAAAA',
      status: 'AAAAAAA',
      speaker: false,
      addedByUnit: 'AAAAAAA',
      profession: 'AAAAAAA',
      document: 'AAAAAAA',
      type: 'AAAAAAA',
    };
  });

  describe('Service methods', () => {
    it('should find an element', () => {
      const returnedFromService = Object.assign({}, elemDefault);

      service.find(123).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(elemDefault);
    });

    it('should create a OtherPersonRecord', () => {
      const returnedFromService = Object.assign(
        {
          id: 0,
        },
        elemDefault
      );

      const expected = Object.assign({}, returnedFromService);

      service.create(new OtherPersonRecord()).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'POST' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should update a OtherPersonRecord', () => {
      const returnedFromService = Object.assign(
        {
          id: 1,
          city: 'BBBBBB',
          rank: 'BBBBBB',
          section: 'BBBBBB',
          meetingStatus: 'BBBBBB',
          status: 'BBBBBB',
          speaker: true,
          addedByUnit: 'BBBBBB',
          profession: 'BBBBBB',
          document: 'BBBBBB',
          type: 'BBBBBB',
        },
        elemDefault
      );

      const expected = Object.assign({}, returnedFromService);

      service.update(expected).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PUT' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should partial update a OtherPersonRecord', () => {
      const patchObject = Object.assign(
        {
          section: 'BBBBBB',
          meetingStatus: 'BBBBBB',
          speaker: true,
          profession: 'BBBBBB',
        },
        new OtherPersonRecord()
      );

      const returnedFromService = Object.assign(patchObject, elemDefault);

      const expected = Object.assign({}, returnedFromService);

      service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PATCH' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should return a list of OtherPersonRecord', () => {
      const returnedFromService = Object.assign(
        {
          id: 1,
          city: 'BBBBBB',
          rank: 'BBBBBB',
          section: 'BBBBBB',
          meetingStatus: 'BBBBBB',
          status: 'BBBBBB',
          speaker: true,
          addedByUnit: 'BBBBBB',
          profession: 'BBBBBB',
          document: 'BBBBBB',
          type: 'BBBBBB',
        },
        elemDefault
      );

      const expected = Object.assign({}, returnedFromService);

      service.query().subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush([returnedFromService]);
      httpMock.verify();
      expect(expectedResult).toContainEqual(expected);
    });

    it('should delete a OtherPersonRecord', () => {
      service.delete(123).subscribe(resp => (expectedResult = resp.ok));

      const req = httpMock.expectOne({ method: 'DELETE' });
      req.flush({ status: 200 });
      expect(expectedResult);
    });

    describe('addOtherPersonRecordToCollectionIfMissing', () => {
      it('should add a OtherPersonRecord to an empty array', () => {
        const otherPersonRecord: IOtherPersonRecord = { id: 123 };
        expectedResult = service.addOtherPersonRecordToCollectionIfMissing([], otherPersonRecord);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(otherPersonRecord);
      });

      it('should not add a OtherPersonRecord to an array that contains it', () => {
        const otherPersonRecord: IOtherPersonRecord = { id: 123 };
        const otherPersonRecordCollection: IOtherPersonRecord[] = [
          {
            ...otherPersonRecord,
          },
          { id: 456 },
        ];
        expectedResult = service.addOtherPersonRecordToCollectionIfMissing(otherPersonRecordCollection, otherPersonRecord);
        expect(expectedResult).toHaveLength(2);
      });

      it("should add a OtherPersonRecord to an array that doesn't contain it", () => {
        const otherPersonRecord: IOtherPersonRecord = { id: 123 };
        const otherPersonRecordCollection: IOtherPersonRecord[] = [{ id: 456 }];
        expectedResult = service.addOtherPersonRecordToCollectionIfMissing(otherPersonRecordCollection, otherPersonRecord);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(otherPersonRecord);
      });

      it('should add only unique OtherPersonRecord to an array', () => {
        const otherPersonRecordArray: IOtherPersonRecord[] = [{ id: 123 }, { id: 456 }, { id: 57404 }];
        const otherPersonRecordCollection: IOtherPersonRecord[] = [{ id: 123 }];
        expectedResult = service.addOtherPersonRecordToCollectionIfMissing(otherPersonRecordCollection, ...otherPersonRecordArray);
        expect(expectedResult).toHaveLength(3);
      });

      it('should accept varargs', () => {
        const otherPersonRecord: IOtherPersonRecord = { id: 123 };
        const otherPersonRecord2: IOtherPersonRecord = { id: 456 };
        expectedResult = service.addOtherPersonRecordToCollectionIfMissing([], otherPersonRecord, otherPersonRecord2);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(otherPersonRecord);
        expect(expectedResult).toContain(otherPersonRecord2);
      });

      it('should accept null and undefined values', () => {
        const otherPersonRecord: IOtherPersonRecord = { id: 123 };
        expectedResult = service.addOtherPersonRecordToCollectionIfMissing([], null, otherPersonRecord, undefined);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(otherPersonRecord);
      });

      it('should return initial array if no OtherPersonRecord is added', () => {
        const otherPersonRecordCollection: IOtherPersonRecord[] = [{ id: 123 }];
        expectedResult = service.addOtherPersonRecordToCollectionIfMissing(otherPersonRecordCollection, undefined, null);
        expect(expectedResult).toEqual(otherPersonRecordCollection);
      });
    });
  });

  afterEach(() => {
    httpMock.verify();
  });
});
