import { Component } from '@angular/core';

import { MainService } from '../main.service';

@Component({
  selector: 'jhi-pages',
  templateUrl: './pages.component.html',
  styleUrls: ['./pages.component.scss'],
})
export class PagesComponent {
  constructor(private mainService: MainService) {}

  onPageClick(): void {
    this.mainService.changeSideNav(false);
  }
}
