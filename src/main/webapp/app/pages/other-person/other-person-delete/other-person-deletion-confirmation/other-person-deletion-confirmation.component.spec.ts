import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OtherPersonDeletionConfirmationComponent } from './other-person-deletion-confirmation.component';

describe('OtherPersonDeletionConfirmationComponent', () => {
  let component: OtherPersonDeletionConfirmationComponent;
  let fixture: ComponentFixture<OtherPersonDeletionConfirmationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [OtherPersonDeletionConfirmationComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OtherPersonDeletionConfirmationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
