jest.mock('@angular/router');

import { TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ActivatedRouteSnapshot, Router } from '@angular/router';
import { of } from 'rxjs';

import { IActualActivity, ActualActivity } from '../actual-activity.model';
import { ActualActivityService } from '../service/actual-activity.service';

import { ActualActivityRoutingResolveService } from './actual-activity-routing-resolve.service';

describe('ActualActivity routing resolve service', () => {
  let mockRouter: Router;
  let mockActivatedRouteSnapshot: ActivatedRouteSnapshot;
  let routingResolveService: ActualActivityRoutingResolveService;
  let service: ActualActivityService;
  let resultActualActivity: IActualActivity | undefined;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [Router, ActivatedRouteSnapshot],
    });
    mockRouter = TestBed.inject(Router);
    mockActivatedRouteSnapshot = TestBed.inject(ActivatedRouteSnapshot);
    routingResolveService = TestBed.inject(ActualActivityRoutingResolveService);
    service = TestBed.inject(ActualActivityService);
    resultActualActivity = undefined;
  });

  describe('resolve', () => {
    it('should return IActualActivity returned by find', () => {
      // GIVEN
      service.find = jest.fn(id => of(new HttpResponse({ body: { id } })));
      mockActivatedRouteSnapshot.params = { id: 123 };

      // WHEN
      routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
        resultActualActivity = result;
      });

      // THEN
      expect(service.find).toBeCalledWith(123);
      expect(resultActualActivity).toEqual({ id: 123 });
    });

    it('should return new IActualActivity if id is not provided', () => {
      // GIVEN
      service.find = jest.fn();
      mockActivatedRouteSnapshot.params = {};

      // WHEN
      routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
        resultActualActivity = result;
      });

      // THEN
      expect(service.find).not.toBeCalled();
      expect(resultActualActivity).toEqual(new ActualActivity());
    });

    it('should route to 404 page if data not found in server', () => {
      // GIVEN
      jest.spyOn(service, 'find').mockReturnValue(of(new HttpResponse({ body: null as unknown as ActualActivity })));
      mockActivatedRouteSnapshot.params = { id: 123 };

      // WHEN
      routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
        resultActualActivity = result;
      });

      // THEN
      expect(service.find).toBeCalledWith(123);
      expect(resultActualActivity).toEqual(undefined);
      expect(mockRouter.navigate).toHaveBeenCalledWith(['404']);
    });
  });
});
