package tr.com.khg.template.service.impl;

import java.util.List;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tr.com.khg.template.domain.OtherPersonRecord;
import tr.com.khg.template.repository.OtherPersonRecordRepository;
import tr.com.khg.template.service.OtherPersonRecordService;

/**
 * Service Implementation for managing {@link OtherPersonRecord}.
 */
@Service
@Transactional
public class OtherPersonRecordServiceImpl implements OtherPersonRecordService {

    private final Logger log = LoggerFactory.getLogger(OtherPersonRecordServiceImpl.class);

    private final OtherPersonRecordRepository otherPersonRecordRepository;

    public OtherPersonRecordServiceImpl(OtherPersonRecordRepository otherPersonRecordRepository) {
        this.otherPersonRecordRepository = otherPersonRecordRepository;
    }

    @Override
    public OtherPersonRecord save(OtherPersonRecord otherPersonRecord) {
        log.debug("Request to save OtherPersonRecord : {}", otherPersonRecord);
        return otherPersonRecordRepository.save(otherPersonRecord);
    }

    @Override
    public Optional<OtherPersonRecord> partialUpdate(OtherPersonRecord otherPersonRecord) {
        log.debug("Request to partially update OtherPersonRecord : {}", otherPersonRecord);

        return otherPersonRecordRepository
            .findById(otherPersonRecord.getId())
            .map(existingOtherPersonRecord -> {
                if (otherPersonRecord.getCity() != null) {
                    existingOtherPersonRecord.setCity(otherPersonRecord.getCity());
                }
                if (otherPersonRecord.getRank() != null) {
                    existingOtherPersonRecord.setRank(otherPersonRecord.getRank());
                }
                if (otherPersonRecord.getSection() != null) {
                    existingOtherPersonRecord.setSection(otherPersonRecord.getSection());
                }
                if (otherPersonRecord.getMeetingStatus() != null) {
                    existingOtherPersonRecord.setMeetingStatus(otherPersonRecord.getMeetingStatus());
                }
                if (otherPersonRecord.getStatus() != null) {
                    existingOtherPersonRecord.setStatus(otherPersonRecord.getStatus());
                }
                if (otherPersonRecord.getSpeaker() != null) {
                    existingOtherPersonRecord.setSpeaker(otherPersonRecord.getSpeaker());
                }
                if (otherPersonRecord.getAddedByUnit() != null) {
                    existingOtherPersonRecord.setAddedByUnit(otherPersonRecord.getAddedByUnit());
                }
                if (otherPersonRecord.getProfession() != null) {
                    existingOtherPersonRecord.setProfession(otherPersonRecord.getProfession());
                }
                if (otherPersonRecord.getDocument() != null) {
                    existingOtherPersonRecord.setDocument(otherPersonRecord.getDocument());
                }
                if (otherPersonRecord.getType() != null) {
                    existingOtherPersonRecord.setType(otherPersonRecord.getType());
                }

                return existingOtherPersonRecord;
            })
            .map(otherPersonRecordRepository::save);
    }

    @Override
    @Transactional(readOnly = true)
    public List<OtherPersonRecord> findAll() {
        log.debug("Request to get all OtherPersonRecords");
        return otherPersonRecordRepository.findAll();
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<OtherPersonRecord> findOne(Long id) {
        log.debug("Request to get OtherPersonRecord : {}", id);
        return otherPersonRecordRepository.findById(id);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete OtherPersonRecord : {}", id);
        otherPersonRecordRepository.deleteById(id);
    }

    @Override
    public List<OtherPersonRecord> updateAll(List<OtherPersonRecord> records) {
        log.debug("Request to update all OtherPersonRecords");
        return otherPersonRecordRepository.saveAll(records);
    }
}
