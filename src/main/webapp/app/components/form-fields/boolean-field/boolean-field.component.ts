import { Component, Input } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

import { BooleanField } from 'app/components/models/boolean-field.model';

@Component({
  selector: 'jhi-boolean-field',
  templateUrl: './boolean-field.component.html',
  styleUrls: ['./boolean-field.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: BooleanFieldComponent,
      multi: true,
    },
  ],
})
export class BooleanFieldComponent implements ControlValueAccessor {
  @Input()
  placeholder?: string = '--';

  @Input()
  readOnly?: boolean = false;

  @Input()
  value?: boolean | null = null;

  @Input()
  fieldsValues?: BooleanField = { trueField: 'true', falseField: 'false' };

  onChange!: (value: boolean | null) => void;

  onTouched!: () => void;

  registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

  setDisabledState(isDisabled: boolean): void {
    this.readOnly = isDisabled;
  }

  writeValue(obj: boolean | null): void {
    this.value = obj;
  }

  change(value: boolean | null): void {
    this.onChange(value);
  }
}
