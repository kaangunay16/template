import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AttendeeFieldComponent } from './attendee-field.component';

describe('AttendeeFieldComponent', () => {
  let component: AttendeeFieldComponent;
  let fixture: ComponentFixture<AttendeeFieldComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [AttendeeFieldComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AttendeeFieldComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
