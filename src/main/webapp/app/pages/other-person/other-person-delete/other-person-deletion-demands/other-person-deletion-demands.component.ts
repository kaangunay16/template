import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';

import { TableConfig } from 'app/components/models/table-config.model';
import { IDemand } from 'app/entities/demand/demand.model';
import { DemandService } from 'app/entities/demand/service/demand.service';
import { ErrorSupportService } from 'app/shared/services/error-support.service';
import { EntityDemandService } from 'app/shared/services/entity-demand.service';
import { OTHER_PERSON_DELETION } from 'app/shared/services/demands-types';

@Component({
  selector: 'jhi-other-person-deletion-demands',
  templateUrl: './other-person-deletion-demands.component.html',
  styleUrls: ['./other-person-deletion-demands.component.scss'],
})
export class OtherPersonDeletionDemandsComponent implements OnInit, OnDestroy {
  demands!: IDemand[];
  demandSub!: Subscription;
  tableConfig?: TableConfig;
  loading = true;

  constructor(
    private demandService: DemandService,
    private entityDemandService: EntityDemandService,
    private errorService: ErrorSupportService
  ) {}

  ngOnInit(): void {
    this.demandSub = this.entityDemandService.getAllEntities(OTHER_PERSON_DELETION).subscribe(
      res => {
        this.demands = res.body!;
        this.entityDemandService.entities = this.demands;
        this.tableConfig = {
          columns: [
            {
              column: 'document',
            },
            {
              column: 'demandedBy',
            },
            {
              column: 'description',
            },
          ],
          data: this.demands,
          actions: [
            {
              link: ['/other-person', 'other-person-deletion', 'confirmation'],
              icon: 'file-alt',
              tooltip: 'Ayrıntılar',
            },
          ],
        };
        this.loading = false;
      },
      err => this.errorService.errorHandler(err)
    );
  }

  ngOnDestroy(): void {
    this.demandSub.unsubscribe();
  }
}
