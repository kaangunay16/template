import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { of } from 'rxjs';

import { PastActivityService } from '../service/past-activity.service';

import { PastActivityComponent } from './past-activity.component';

describe('PastActivity Management Component', () => {
  let comp: PastActivityComponent;
  let fixture: ComponentFixture<PastActivityComponent>;
  let service: PastActivityService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      declarations: [PastActivityComponent],
    })
      .overrideTemplate(PastActivityComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(PastActivityComponent);
    comp = fixture.componentInstance;
    service = TestBed.inject(PastActivityService);

    const headers = new HttpHeaders();
    jest.spyOn(service, 'query').mockReturnValue(
      of(
        new HttpResponse({
          body: [{ id: 123 }],
          headers,
        })
      )
    );
  });

  it('Should call load all on init', () => {
    // WHEN
    comp.ngOnInit();

    // THEN
    expect(service.query).toHaveBeenCalled();
    expect(comp.pastActivities?.[0]).toEqual(expect.objectContaining({ id: 123 }));
  });
});
