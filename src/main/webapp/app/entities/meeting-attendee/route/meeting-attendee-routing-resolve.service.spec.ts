jest.mock('@angular/router');

import { TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ActivatedRouteSnapshot, Router } from '@angular/router';
import { of } from 'rxjs';

import { IMeetingAttendee, MeetingAttendee } from '../meeting-attendee.model';
import { MeetingAttendeeService } from '../service/meeting-attendee.service';

import { MeetingAttendeeRoutingResolveService } from './meeting-attendee-routing-resolve.service';

describe('MeetingAttendee routing resolve service', () => {
  let mockRouter: Router;
  let mockActivatedRouteSnapshot: ActivatedRouteSnapshot;
  let routingResolveService: MeetingAttendeeRoutingResolveService;
  let service: MeetingAttendeeService;
  let resultMeetingAttendee: IMeetingAttendee | undefined;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [Router, ActivatedRouteSnapshot],
    });
    mockRouter = TestBed.inject(Router);
    mockActivatedRouteSnapshot = TestBed.inject(ActivatedRouteSnapshot);
    routingResolveService = TestBed.inject(MeetingAttendeeRoutingResolveService);
    service = TestBed.inject(MeetingAttendeeService);
    resultMeetingAttendee = undefined;
  });

  describe('resolve', () => {
    it('should return IMeetingAttendee returned by find', () => {
      // GIVEN
      service.find = jest.fn(id => of(new HttpResponse({ body: { id } })));
      mockActivatedRouteSnapshot.params = { id: 123 };

      // WHEN
      routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
        resultMeetingAttendee = result;
      });

      // THEN
      expect(service.find).toBeCalledWith(123);
      expect(resultMeetingAttendee).toEqual({ id: 123 });
    });

    it('should return new IMeetingAttendee if id is not provided', () => {
      // GIVEN
      service.find = jest.fn();
      mockActivatedRouteSnapshot.params = {};

      // WHEN
      routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
        resultMeetingAttendee = result;
      });

      // THEN
      expect(service.find).not.toBeCalled();
      expect(resultMeetingAttendee).toEqual(new MeetingAttendee());
    });

    it('should route to 404 page if data not found in server', () => {
      // GIVEN
      jest.spyOn(service, 'find').mockReturnValue(of(new HttpResponse({ body: null as unknown as MeetingAttendee })));
      mockActivatedRouteSnapshot.params = { id: 123 };

      // WHEN
      routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
        resultMeetingAttendee = result;
      });

      // THEN
      expect(service.find).toBeCalledWith(123);
      expect(resultMeetingAttendee).toEqual(undefined);
      expect(mockRouter.navigate).toHaveBeenCalledWith(['404']);
    });
  });
});
