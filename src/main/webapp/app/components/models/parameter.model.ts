export interface Parameter {
  field: string;
  fieldType: string;
  value: string;
  sqlOperation: string;
}
