package tr.com.khg.template.service;

import java.util.List;
import java.util.Optional;
import tr.com.khg.template.domain.OtherName;

/**
 * Service Interface for managing {@link OtherName}.
 */
public interface OtherNameService {
    /**
     * Save a otherName.
     *
     * @param otherName the entity to save.
     * @return the persisted entity.
     */
    OtherName save(OtherName otherName);

    /**
     * Partially updates a otherName.
     *
     * @param otherName the entity to update partially.
     * @return the persisted entity.
     */
    Optional<OtherName> partialUpdate(OtherName otherName);

    /**
     * Get all the otherNames.
     *
     * @return the list of entities.
     */
    List<OtherName> findAll();

    /**
     * Get the "id" otherName.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<OtherName> findOne(Long id);

    /**
     * Delete the "id" otherName.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
