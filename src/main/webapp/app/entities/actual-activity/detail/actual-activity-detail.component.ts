import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IActualActivity } from '../actual-activity.model';

@Component({
  selector: 'jhi-actual-activity-detail',
  templateUrl: './actual-activity-detail.component.html',
})
export class ActualActivityDetailComponent implements OnInit {
  actualActivity: IActualActivity | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ actualActivity }) => {
      this.actualActivity = actualActivity;
    });
  }

  previousState(): void {
    window.history.back();
  }
}
