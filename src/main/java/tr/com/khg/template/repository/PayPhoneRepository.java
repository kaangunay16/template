package tr.com.khg.template.repository;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;
import tr.com.khg.template.domain.PayPhone;

/**
 * Spring Data SQL repository for the PayPhone entity.
 */
@SuppressWarnings("unused")
@Repository
public interface PayPhoneRepository extends JpaRepository<PayPhone, Long> {}
