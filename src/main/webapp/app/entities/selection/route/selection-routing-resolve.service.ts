import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { ISelection, Selection } from '../selection.model';
import { SelectionService } from '../service/selection.service';

@Injectable({ providedIn: 'root' })
export class SelectionRoutingResolveService implements Resolve<ISelection> {
  constructor(protected service: SelectionService, protected router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<ISelection> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        mergeMap((selection: HttpResponse<Selection>) => {
          if (selection.body) {
            return of(selection.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new Selection());
  }
}
