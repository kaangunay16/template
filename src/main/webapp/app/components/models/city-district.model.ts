export interface ICityDistrict {
  city?: string | null;
  district?: string | null;
}

export class CityDistrict implements ICityDistrict {
  constructor(public city?: string | null, public district?: string | null) {}
}
