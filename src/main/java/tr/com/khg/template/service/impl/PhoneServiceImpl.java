package tr.com.khg.template.service.impl;

import java.util.List;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tr.com.khg.template.domain.Phone;
import tr.com.khg.template.repository.PhoneRepository;
import tr.com.khg.template.service.PhoneService;

/**
 * Service Implementation for managing {@link Phone}.
 */
@Service
@Transactional
public class PhoneServiceImpl implements PhoneService {

    private final Logger log = LoggerFactory.getLogger(PhoneServiceImpl.class);

    private final PhoneRepository phoneRepository;

    public PhoneServiceImpl(PhoneRepository phoneRepository) {
        this.phoneRepository = phoneRepository;
    }

    @Override
    public Phone save(Phone phone) {
        log.debug("Request to save Phone : {}", phone);
        return phoneRepository.save(phone);
    }

    @Override
    public Optional<Phone> partialUpdate(Phone phone) {
        log.debug("Request to partially update Phone : {}", phone);

        return phoneRepository
            .findById(phone.getId())
            .map(existingPhone -> {
                if (phone.getPhoneNumber() != null) {
                    existingPhone.setPhoneNumber(phone.getPhoneNumber());
                }
                if (phone.getStartYear() != null) {
                    existingPhone.setStartYear(phone.getStartYear());
                }
                if (phone.getEndYear() != null) {
                    existingPhone.setEndYear(phone.getEndYear());
                }
                if (phone.getDeleted() != null) {
                    existingPhone.setDeleted(phone.getDeleted());
                }
                if (phone.getType() != null) {
                    existingPhone.setType(phone.getType());
                }

                return existingPhone;
            })
            .map(phoneRepository::save);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Phone> findAll() {
        log.debug("Request to get all Phones");
        return phoneRepository.findAll();
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<Phone> findOne(Long id) {
        log.debug("Request to get Phone : {}", id);
        return phoneRepository.findById(id);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete Phone : {}", id);
        phoneRepository.deleteById(id);
    }
}
