import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { IOtherPersonRecord } from '../other-person-record.model';
import { OtherPersonRecordService } from '../service/other-person-record.service';

@Component({
  templateUrl: './other-person-record-delete-dialog.component.html',
})
export class OtherPersonRecordDeleteDialogComponent {
  otherPersonRecord?: IOtherPersonRecord;

  constructor(protected otherPersonRecordService: OtherPersonRecordService, protected activeModal: NgbActiveModal) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.otherPersonRecordService.delete(id).subscribe(() => {
      this.activeModal.close('deleted');
    });
  }
}
