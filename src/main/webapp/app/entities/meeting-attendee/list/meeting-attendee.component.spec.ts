import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { of } from 'rxjs';

import { MeetingAttendeeService } from '../service/meeting-attendee.service';

import { MeetingAttendeeComponent } from './meeting-attendee.component';

describe('MeetingAttendee Management Component', () => {
  let comp: MeetingAttendeeComponent;
  let fixture: ComponentFixture<MeetingAttendeeComponent>;
  let service: MeetingAttendeeService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      declarations: [MeetingAttendeeComponent],
    })
      .overrideTemplate(MeetingAttendeeComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(MeetingAttendeeComponent);
    comp = fixture.componentInstance;
    service = TestBed.inject(MeetingAttendeeService);

    const headers = new HttpHeaders();
    jest.spyOn(service, 'query').mockReturnValue(
      of(
        new HttpResponse({
          body: [{ id: 123 }],
          headers,
        })
      )
    );
  });

  it('Should call load all on init', () => {
    // WHEN
    comp.ngOnInit();

    // THEN
    expect(service.query).toHaveBeenCalled();
    expect(comp.meetingAttendees?.[0]).toEqual(expect.objectContaining({ id: 123 }));
  });
});
