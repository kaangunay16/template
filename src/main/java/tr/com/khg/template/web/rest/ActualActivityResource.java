package tr.com.khg.template.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.ResponseUtil;
import tr.com.khg.template.domain.ActualActivity;
import tr.com.khg.template.repository.ActualActivityRepository;
import tr.com.khg.template.service.ActualActivityService;
import tr.com.khg.template.web.rest.errors.BadRequestAlertException;

/**
 * REST controller for managing {@link tr.com.khg.template.domain.ActualActivity}.
 */
@RestController
@RequestMapping("/api")
public class ActualActivityResource {

    private final Logger log = LoggerFactory.getLogger(ActualActivityResource.class);

    private static final String ENTITY_NAME = "actualActivity";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ActualActivityService actualActivityService;

    private final ActualActivityRepository actualActivityRepository;

    public ActualActivityResource(ActualActivityService actualActivityService, ActualActivityRepository actualActivityRepository) {
        this.actualActivityService = actualActivityService;
        this.actualActivityRepository = actualActivityRepository;
    }

    /**
     * {@code POST  /actual-activities} : Create a new actualActivity.
     *
     * @param actualActivity the actualActivity to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new actualActivity, or with status {@code 400 (Bad Request)} if the actualActivity has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/actual-activities")
    public ResponseEntity<ActualActivity> createActualActivity(@RequestBody ActualActivity actualActivity) throws URISyntaxException {
        log.debug("REST request to save ActualActivity : {}", actualActivity);
        if (actualActivity.getId() != null) {
            throw new BadRequestAlertException("A new actualActivity cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ActualActivity result = actualActivityService.save(actualActivity);
        return ResponseEntity
            .created(new URI("/api/actual-activities/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /actual-activities/:id} : Updates an existing actualActivity.
     *
     * @param id the id of the actualActivity to save.
     * @param actualActivity the actualActivity to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated actualActivity,
     * or with status {@code 400 (Bad Request)} if the actualActivity is not valid,
     * or with status {@code 500 (Internal Server Error)} if the actualActivity couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/actual-activities/{id}")
    public ResponseEntity<ActualActivity> updateActualActivity(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody ActualActivity actualActivity
    ) throws URISyntaxException {
        log.debug("REST request to update ActualActivity : {}, {}", id, actualActivity);
        if (actualActivity.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, actualActivity.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!actualActivityRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        ActualActivity result = actualActivityService.save(actualActivity);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, actualActivity.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /actual-activities/:id} : Partial updates given fields of an existing actualActivity, field will ignore if it is null
     *
     * @param id the id of the actualActivity to save.
     * @param actualActivity the actualActivity to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated actualActivity,
     * or with status {@code 400 (Bad Request)} if the actualActivity is not valid,
     * or with status {@code 404 (Not Found)} if the actualActivity is not found,
     * or with status {@code 500 (Internal Server Error)} if the actualActivity couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/actual-activities/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<ActualActivity> partialUpdateActualActivity(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody ActualActivity actualActivity
    ) throws URISyntaxException {
        log.debug("REST request to partial update ActualActivity partially : {}, {}", id, actualActivity);
        if (actualActivity.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, actualActivity.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!actualActivityRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<ActualActivity> result = actualActivityService.partialUpdate(actualActivity);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, actualActivity.getId().toString())
        );
    }

    /**
     * {@code GET  /actual-activities} : get all the actualActivities.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of actualActivities in body.
     */
    @GetMapping("/actual-activities")
    public List<ActualActivity> getAllActualActivities() {
        log.debug("REST request to get all ActualActivities");
        return actualActivityService.findAll();
    }

    /**
     * {@code GET  /actual-activities/:id} : get the "id" actualActivity.
     *
     * @param id the id of the actualActivity to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the actualActivity, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/actual-activities/{id}")
    public ResponseEntity<ActualActivity> getActualActivity(@PathVariable Long id) {
        log.debug("REST request to get ActualActivity : {}", id);
        Optional<ActualActivity> actualActivity = actualActivityService.findOne(id);
        return ResponseUtil.wrapOrNotFound(actualActivity);
    }

    /**
     * {@code DELETE  /actual-activities/:id} : delete the "id" actualActivity.
     *
     * @param id the id of the actualActivity to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/actual-activities/{id}")
    public ResponseEntity<Void> deleteActualActivity(@PathVariable Long id) {
        log.debug("REST request to delete ActualActivity : {}", id);
        actualActivityService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }
}
