import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { SelectionComponent } from '../list/selection.component';
import { SelectionDetailComponent } from '../detail/selection-detail.component';
import { SelectionUpdateComponent } from '../update/selection-update.component';
import { SelectionRoutingResolveService } from './selection-routing-resolve.service';

const selectionRoute: Routes = [
  {
    path: '',
    component: SelectionComponent,
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: SelectionDetailComponent,
    resolve: {
      selection: SelectionRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: SelectionUpdateComponent,
    resolve: {
      selection: SelectionRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: SelectionUpdateComponent,
    resolve: {
      selection: SelectionRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
];

@NgModule({
  imports: [RouterModule.forChild(selectionRoute)],
  exports: [RouterModule],
})
export class SelectionRoutingModule {}
