import { PageTableConfigs } from 'app/components/models/page-table-config.model';
import { IOtherPersonRecord } from 'app/entities/other-person-record/other-person-record.model';

export const otherPersonTableConfigs: PageTableConfigs = {
  columns: [
    {
      column: 'identity.name',
    },
    {
      column: 'identity.surname',
    },
    {
      column: 'otherPersonRecords',
      parser: otherPersonRecordsFieldParser,
    },
  ],
  actions: [
    {
      link: ['/other-person', 'information'],
      icon: 'user-edit',
      tooltip: 'Kişiyi Güncelle',
    },
  ],
  searchParameters: [
    {
      field: 'identity.name',
      type: 'text',
    },
    {
      field: 'identity.surname',
      type: 'text',
    },
    {
      field: 'otherPersonRecords.city',
      type: 'text',
    },
  ],
};

function otherPersonRecordsFieldParser(records: IOtherPersonRecord[]): string {
  const result: string[] = [];
  records.forEach(record => {
    result.push(`${record.city!}`);
  });

  return result.join(' - ');
}
