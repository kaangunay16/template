import { Component, EventEmitter, OnDestroy, Output, ViewChild } from '@angular/core';
import { Subscription } from 'rxjs';

import { IdentityService } from 'app/entities/identity/service/identity.service';
import { IIdentity } from 'app/entities/identity/identity.model';
import { ErrorSupportService } from 'app/shared/services/error-support.service';
import { IdentityRetrieverComponent } from 'app/components/identity-retriever/identity-retriever.component';

@Component({
  selector: 'jhi-identity-info-retriever',
  templateUrl: './identity-info-retriever.component.html',
  styleUrls: ['./identity-info-retriever.component.scss'],
})
export class IdentityInfoRetrieverComponent implements OnDestroy {
  @ViewChild('retriever')
  retriever!: IdentityRetrieverComponent;

  identitySub?: Subscription;

  @Output()
  identityChanged: EventEmitter<IIdentity> = new EventEmitter();

  constructor(private identityService: IdentityService, private errorService: ErrorSupportService) {}

  ngOnDestroy(): void {
    this.identitySub?.unsubscribe();
  }

  identityInfoChanged(identity: string): void {
    this.identitySub = this.identityService.findByIdentityNumber(identity).subscribe(
      res => {
        if (res.body) {
          this.identityChanged.emit(res.body);
        }
      },
      err => this.errorService.errorHandler(err)
    );

    this.retriever.dialogRef?.close();
  }
}
