jest.mock('@ng-bootstrap/ng-bootstrap');

import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { of } from 'rxjs';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { OtherPersonService } from '../service/other-person.service';

import { OtherPersonDeleteDialogComponent } from './other-person-delete-dialog.component';

describe('OtherPerson Management Delete Component', () => {
  let comp: OtherPersonDeleteDialogComponent;
  let fixture: ComponentFixture<OtherPersonDeleteDialogComponent>;
  let service: OtherPersonService;
  let mockActiveModal: NgbActiveModal;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      declarations: [OtherPersonDeleteDialogComponent],
      providers: [NgbActiveModal],
    })
      .overrideTemplate(OtherPersonDeleteDialogComponent, '')
      .compileComponents();
    fixture = TestBed.createComponent(OtherPersonDeleteDialogComponent);
    comp = fixture.componentInstance;
    service = TestBed.inject(OtherPersonService);
    mockActiveModal = TestBed.inject(NgbActiveModal);
  });

  describe('confirmDelete', () => {
    it('Should call delete service on confirmDelete', inject(
      [],
      fakeAsync(() => {
        // GIVEN
        jest.spyOn(service, 'delete').mockReturnValue(of(new HttpResponse({})));

        // WHEN
        comp.confirmDelete(123);
        tick();

        // THEN
        expect(service.delete).toHaveBeenCalledWith(123);
        expect(mockActiveModal.close).toHaveBeenCalledWith('deleted');
      })
    ));

    it('Should not call delete service on clear', () => {
      // GIVEN
      jest.spyOn(service, 'delete');

      // WHEN
      comp.cancel();

      // THEN
      expect(service.delete).not.toHaveBeenCalled();
      expect(mockActiveModal.close).not.toHaveBeenCalled();
      expect(mockActiveModal.dismiss).toHaveBeenCalled();
    });
  });
});
