package tr.com.khg.template.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.ResponseUtil;
import tr.com.khg.template.domain.OtherName;
import tr.com.khg.template.repository.OtherNameRepository;
import tr.com.khg.template.service.OtherNameService;
import tr.com.khg.template.web.rest.errors.BadRequestAlertException;

/**
 * REST controller for managing {@link tr.com.khg.template.domain.OtherName}.
 */
@RestController
@RequestMapping("/api")
public class OtherNameResource {

    private final Logger log = LoggerFactory.getLogger(OtherNameResource.class);

    private static final String ENTITY_NAME = "otherName";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final OtherNameService otherNameService;

    private final OtherNameRepository otherNameRepository;

    public OtherNameResource(OtherNameService otherNameService, OtherNameRepository otherNameRepository) {
        this.otherNameService = otherNameService;
        this.otherNameRepository = otherNameRepository;
    }

    /**
     * {@code POST  /other-names} : Create a new otherName.
     *
     * @param otherName the otherName to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new otherName, or with status {@code 400 (Bad Request)} if the otherName has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/other-names")
    public ResponseEntity<OtherName> createOtherName(@RequestBody OtherName otherName) throws URISyntaxException {
        log.debug("REST request to save OtherName : {}", otherName);
        if (otherName.getId() != null) {
            throw new BadRequestAlertException("A new otherName cannot already have an ID", ENTITY_NAME, "idexists");
        }
        OtherName result = otherNameService.save(otherName);
        return ResponseEntity
            .created(new URI("/api/other-names/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /other-names/:id} : Updates an existing otherName.
     *
     * @param id the id of the otherName to save.
     * @param otherName the otherName to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated otherName,
     * or with status {@code 400 (Bad Request)} if the otherName is not valid,
     * or with status {@code 500 (Internal Server Error)} if the otherName couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/other-names/{id}")
    public ResponseEntity<OtherName> updateOtherName(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody OtherName otherName
    ) throws URISyntaxException {
        log.debug("REST request to update OtherName : {}, {}", id, otherName);
        if (otherName.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, otherName.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!otherNameRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        OtherName result = otherNameService.save(otherName);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, otherName.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /other-names/:id} : Partial updates given fields of an existing otherName, field will ignore if it is null
     *
     * @param id the id of the otherName to save.
     * @param otherName the otherName to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated otherName,
     * or with status {@code 400 (Bad Request)} if the otherName is not valid,
     * or with status {@code 404 (Not Found)} if the otherName is not found,
     * or with status {@code 500 (Internal Server Error)} if the otherName couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/other-names/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<OtherName> partialUpdateOtherName(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody OtherName otherName
    ) throws URISyntaxException {
        log.debug("REST request to partial update OtherName partially : {}, {}", id, otherName);
        if (otherName.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, otherName.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!otherNameRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<OtherName> result = otherNameService.partialUpdate(otherName);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, otherName.getId().toString())
        );
    }

    /**
     * {@code GET  /other-names} : get all the otherNames.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of otherNames in body.
     */
    @GetMapping("/other-names")
    public List<OtherName> getAllOtherNames() {
        log.debug("REST request to get all OtherNames");
        return otherNameService.findAll();
    }

    /**
     * {@code GET  /other-names/:id} : get the "id" otherName.
     *
     * @param id the id of the otherName to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the otherName, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/other-names/{id}")
    public ResponseEntity<OtherName> getOtherName(@PathVariable Long id) {
        log.debug("REST request to get OtherName : {}", id);
        Optional<OtherName> otherName = otherNameService.findOne(id);
        return ResponseUtil.wrapOrNotFound(otherName);
    }

    /**
     * {@code DELETE  /other-names/:id} : delete the "id" otherName.
     *
     * @param id the id of the otherName to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/other-names/{id}")
    public ResponseEntity<Void> deleteOtherName(@PathVariable Long id) {
        log.debug("REST request to delete OtherName : {}", id);
        otherNameService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }
}
