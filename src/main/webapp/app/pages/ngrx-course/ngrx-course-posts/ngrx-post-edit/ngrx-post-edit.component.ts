import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Store } from '@ngrx/store';
import { Subscription } from 'rxjs';

import { AppState } from '../../store/app.state';
import { getPostById } from '../state/posts.selector';
import { Post } from '../models/post.model';
import { updatePost } from '../state/posts.actions';
import { Update } from '@ngrx/entity';

@Component({
  selector: 'jhi-ngrx-post-edit',
  templateUrl: './ngrx-post-edit.component.html',
  styleUrls: ['./ngrx-post-edit.component.scss'],
})
export class NgrxPostEditComponent implements OnInit, OnDestroy {
  updateForm!: FormGroup;
  post!: Post;
  dataSubscription!: Subscription;

  constructor(private store: Store<AppState>) {}

  ngOnInit(): void {
    this.createForm();
    this.dataSubscription = this.store.select(getPostById).subscribe(post => {
      if (post) {
        this.post = post;
        this.updateForm.patchValue({
          title: post.title,
          description: post.description,
        });
      }
    });
  }

  ngOnDestroy(): void {
    this.dataSubscription.unsubscribe();
  }

  createForm(): void {
    this.updateForm = new FormGroup({
      title: new FormControl(null, [Validators.required, Validators.minLength(6)]),
      description: new FormControl(null, [Validators.required, Validators.minLength(10)]),
    });
  }

  onSubmit(): void {
    if (!this.updateForm.valid) {
      return;
    }

    const newPost: Post = {
      ...this.post,
      title: this.updateForm.get('title')?.value,
      description: this.updateForm.get('description')?.value,
    };

    const updatedPost: Update<Post> = {
      id: newPost.id!,
      changes: {
        ...newPost,
      },
    };

    this.store.dispatch(updatePost({ post: updatedPost }));
  }
}
