import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { IOtherPersonRecord, OtherPersonRecord } from '../other-person-record.model';
import { OtherPersonRecordService } from '../service/other-person-record.service';

@Injectable({ providedIn: 'root' })
export class OtherPersonRecordRoutingResolveService implements Resolve<IOtherPersonRecord> {
  constructor(protected service: OtherPersonRecordService, protected router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IOtherPersonRecord> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        mergeMap((otherPersonRecord: HttpResponse<OtherPersonRecord>) => {
          if (otherPersonRecord.body) {
            return of(otherPersonRecord.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new OtherPersonRecord());
  }
}
