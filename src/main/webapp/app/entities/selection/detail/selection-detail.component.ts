import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ISelection } from '../selection.model';

@Component({
  selector: 'jhi-selection-detail',
  templateUrl: './selection-detail.component.html',
})
export class SelectionDetailComponent implements OnInit {
  selection: ISelection | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ selection }) => {
      this.selection = selection;
    });
  }

  previousState(): void {
    window.history.back();
  }
}
