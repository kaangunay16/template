import { IPerson } from 'app/entities/person/person.model';
import { HasId } from '../form-entity.model';

export interface IOtherName extends HasId {
  id?: number;
  name?: string | null;
  person?: IPerson | null;
}

export class OtherName implements IOtherName {
  constructor(public id?: number, public name?: string | null, public person?: IPerson | null) {}
}

export function getOtherNameIdentifier(otherName: IOtherName): number | undefined {
  return otherName.id;
}
