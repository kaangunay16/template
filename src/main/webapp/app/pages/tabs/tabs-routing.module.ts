import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { TabMainComponent } from './tab-main/tab-main.component';

const routes: Routes = [
  {
    path: '',
    component: TabMainComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TabsRoutingModule {}
