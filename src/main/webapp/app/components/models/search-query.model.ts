import { HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { Query } from './query.model';

export interface SearchQuery<T> {
  search: (query: Query) => Observable<HttpResponse<T[]>>;
}
