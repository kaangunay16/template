package tr.com.khg.template.domain;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import tr.com.khg.template.web.rest.TestUtil;

class SelectionTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Selection.class);
        Selection selection1 = new Selection();
        selection1.setId(1L);
        Selection selection2 = new Selection();
        selection2.setId(selection1.getId());
        assertThat(selection1).isEqualTo(selection2);
        selection2.setId(2L);
        assertThat(selection1).isNotEqualTo(selection2);
        selection1.setId(null);
        assertThat(selection1).isNotEqualTo(selection2);
    }
}
