import { DemandType } from 'app/entities/enumerations/demand-type.model';

export interface IDemand {
  id?: number;
  document?: string | null;
  demandedBy?: string | null;
  description?: string | null;
  demandType?: string | null;
  status?: DemandType | null;
  processedBy?: string | null;
  entityId?: number | null;
  processDescription?: string | null;
}

export class Demand implements IDemand {
  constructor(
    public id?: number,
    public document?: string | null,
    public demandedBy?: string | null,
    public description?: string | null,
    public demandType?: string | null,
    public status?: DemandType | null,
    public processedBy?: string | null,
    public entityId?: number | null,
    public processDescription?: string | null
  ) {}
}

export function getDemandIdentifier(demand: IDemand): number | undefined {
  return demand.id;
}
