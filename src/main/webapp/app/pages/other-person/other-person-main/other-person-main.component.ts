import { Component, OnDestroy } from '@angular/core';

import { QueryBuilderService } from 'app/components/query-builder/query-builder.service';
import { OtherPersonModuleService } from '../other-person-module.service';

@Component({
  selector: 'jhi-other-person-main',
  templateUrl: './other-person-main.component.html',
  styleUrls: ['./other-person-main.component.scss'],
})
export class OtherPersonMainComponent implements OnDestroy {
  constructor(private queryBuilderService: QueryBuilderService, private otherPersonModuleService: OtherPersonModuleService) {}

  ngOnDestroy(): void {
    this.queryBuilderService.clearParameters();
    this.otherPersonModuleService.clearEntities();
  }
}
