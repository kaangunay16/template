import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';

import { IPerson } from 'app/entities/person/person.model';
import { DynamicFormCreatorModel, IDynamicFormCreatorModel } from 'app/components/dynamic-form-creator/dynamic-form-field.model';
import { PersonModuleService } from 'app/pages/person/person-module.service';
import { FormValueConverterService } from 'app/components/dynamic-form-creator/form-value-converter.service';
import { IActualActivity } from 'app/entities/actual-activity/actual-activity.model';

@Component({
  selector: 'jhi-actual-activity',
  templateUrl: './actual-activity.component.html',
  styleUrls: ['./actual-activity.component.scss'],
})
export class ActualActivityComponent implements OnInit, OnDestroy {
  person!: IPerson;
  formCreator!: IDynamicFormCreatorModel;
  routeSub!: Subscription;

  constructor(
    private personModuleService: PersonModuleService,
    private formValueService: FormValueConverterService,
    private activatedRoute: ActivatedRoute,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.routeSub = this.activatedRoute.queryParams.subscribe(params => {
      this.person = this.personModuleService.entityCheck(params);
      this.formCreator = new DynamicFormCreatorModel([
        {
          id: 'actualActivity',
          type: 'actualActivity',
          label: 'Güncel Faaliyet',
          multiple: true,
          value: this.person.actualActivities,
        },
      ]);
    });
  }

  ngOnDestroy(): void {
    this.routeSub.unsubscribe();
  }

  submit(): void {
    this.setFormValuesToEntity();
    this.router.navigate(['/person', 'abroad-activities']);
  }

  updateSubmit(): void {
    this.setFormValuesToEntity();
    this.personModuleService.updatePerson(this.person);
  }

  private setFormValuesToEntity(): void {
    this.person.actualActivities = this.formValueService.getFormValue(this.formCreator, 'actualActivity') as IActualActivity[];
  }
}
