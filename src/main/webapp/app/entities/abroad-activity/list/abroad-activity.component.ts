import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IAbroadActivity } from '../abroad-activity.model';
import { AbroadActivityService } from '../service/abroad-activity.service';
import { AbroadActivityDeleteDialogComponent } from '../delete/abroad-activity-delete-dialog.component';

@Component({
  selector: 'jhi-abroad-activity',
  templateUrl: './abroad-activity.component.html',
})
export class AbroadActivityComponent implements OnInit {
  abroadActivities?: IAbroadActivity[];
  isLoading = false;

  constructor(protected abroadActivityService: AbroadActivityService, protected modalService: NgbModal) {}

  loadAll(): void {
    this.isLoading = true;

    this.abroadActivityService.query().subscribe(
      (res: HttpResponse<IAbroadActivity[]>) => {
        this.isLoading = false;
        this.abroadActivities = res.body ?? [];
      },
      () => {
        this.isLoading = false;
      }
    );
  }

  ngOnInit(): void {
    this.loadAll();
  }

  trackId(index: number, item: IAbroadActivity): number {
    return item.id!;
  }

  delete(abroadActivity: IAbroadActivity): void {
    const modalRef = this.modalService.open(AbroadActivityDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.abroadActivity = abroadActivity;
    // unsubscribe not needed because closed completes on modal close
    modalRef.closed.subscribe(reason => {
      if (reason === 'deleted') {
        this.loadAll();
      }
    });
  }
}
