import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { OtherPersonDetailComponent } from './other-person-detail.component';

describe('OtherPerson Management Detail Component', () => {
  let comp: OtherPersonDetailComponent;
  let fixture: ComponentFixture<OtherPersonDetailComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [OtherPersonDetailComponent],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: { data: of({ otherPerson: { id: 123 } }) },
        },
      ],
    })
      .overrideTemplate(OtherPersonDetailComponent, '')
      .compileComponents();
    fixture = TestBed.createComponent(OtherPersonDetailComponent);
    comp = fixture.componentInstance;
  });

  describe('OnInit', () => {
    it('Should load otherPerson on init', () => {
      // WHEN
      comp.ngOnInit();

      // THEN
      expect(comp.otherPerson).toEqual(expect.objectContaining({ id: 123 }));
    });
  });
});
