import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import { IAbroadActivity, AbroadActivity } from '../abroad-activity.model';
import { AbroadActivityService } from '../service/abroad-activity.service';
import { IPerson } from 'app/entities/person/person.model';
import { PersonService } from 'app/entities/person/service/person.service';

@Component({
  selector: 'jhi-abroad-activity-update',
  templateUrl: './abroad-activity-update.component.html',
})
export class AbroadActivityUpdateComponent implements OnInit {
  isSaving = false;

  peopleSharedCollection: IPerson[] = [];

  editForm = this.fb.group({
    id: [],
    country: [],
    startYear: [],
    endYear: [],
    position: [],
    deleted: [],
    person: [],
  });

  constructor(
    protected abroadActivityService: AbroadActivityService,
    protected personService: PersonService,
    protected activatedRoute: ActivatedRoute,
    protected fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ abroadActivity }) => {
      this.updateForm(abroadActivity);

      this.loadRelationshipsOptions();
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const abroadActivity = this.createFromForm();
    if (abroadActivity.id !== undefined) {
      this.subscribeToSaveResponse(this.abroadActivityService.update(abroadActivity));
    } else {
      this.subscribeToSaveResponse(this.abroadActivityService.create(abroadActivity));
    }
  }

  trackPersonById(index: number, item: IPerson): number {
    return item.id!;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IAbroadActivity>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(abroadActivity: IAbroadActivity): void {
    this.editForm.patchValue({
      id: abroadActivity.id,
      country: abroadActivity.country,
      startYear: abroadActivity.startYear,
      endYear: abroadActivity.endYear,
      position: abroadActivity.position,
      deleted: abroadActivity.deleted,
      person: abroadActivity.person,
    });

    this.peopleSharedCollection = this.personService.addPersonToCollectionIfMissing(this.peopleSharedCollection, abroadActivity.person);
  }

  protected loadRelationshipsOptions(): void {
    this.personService
      .query()
      .pipe(map((res: HttpResponse<IPerson[]>) => res.body ?? []))
      .pipe(map((people: IPerson[]) => this.personService.addPersonToCollectionIfMissing(people, this.editForm.get('person')!.value)))
      .subscribe((people: IPerson[]) => (this.peopleSharedCollection = people));
  }

  protected createFromForm(): IAbroadActivity {
    return {
      ...new AbroadActivity(),
      id: this.editForm.get(['id'])!.value,
      country: this.editForm.get(['country'])!.value,
      startYear: this.editForm.get(['startYear'])!.value,
      endYear: this.editForm.get(['endYear'])!.value,
      position: this.editForm.get(['position'])!.value,
      deleted: this.editForm.get(['deleted'])!.value,
      person: this.editForm.get(['person'])!.value,
    };
  }
}
