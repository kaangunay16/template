import { NgModule } from '@angular/core';
import { SharedModule } from 'app/shared/shared.module';
import { PayPhoneComponent } from './list/pay-phone.component';
import { PayPhoneDetailComponent } from './detail/pay-phone-detail.component';
import { PayPhoneUpdateComponent } from './update/pay-phone-update.component';
import { PayPhoneDeleteDialogComponent } from './delete/pay-phone-delete-dialog.component';
import { PayPhoneRoutingModule } from './route/pay-phone-routing.module';

@NgModule({
  imports: [SharedModule, PayPhoneRoutingModule],
  declarations: [PayPhoneComponent, PayPhoneDetailComponent, PayPhoneUpdateComponent, PayPhoneDeleteDialogComponent],
  entryComponents: [PayPhoneDeleteDialogComponent],
})
export class PayPhoneModule {}
