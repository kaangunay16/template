package tr.com.khg.template.domain;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import tr.com.khg.template.web.rest.TestUtil;

class OtherPersonTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(OtherPerson.class);
        OtherPerson otherPerson1 = new OtherPerson();
        otherPerson1.setId(1L);
        OtherPerson otherPerson2 = new OtherPerson();
        otherPerson2.setId(otherPerson1.getId());
        assertThat(otherPerson1).isEqualTo(otherPerson2);
        otherPerson2.setId(2L);
        assertThat(otherPerson1).isNotEqualTo(otherPerson2);
        otherPerson1.setId(null);
        assertThat(otherPerson1).isNotEqualTo(otherPerson2);
    }
}
