import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { ToastrService } from 'ngx-toastr';

import { IMeeting } from 'app/entities/meeting/meeting.model';
import { MeetingModuleService } from '../meeting-module.service';
import { DynamicFormCreatorModel, IDynamicFormCreatorModel } from 'app/components/dynamic-form-creator/dynamic-form-field.model';
import { IMeetingAttendee } from 'app/entities/meeting-attendee/meeting-attendee.model';
import { AttendeeField } from 'app/components/models/attendee-field.model';
import { MeetingService } from 'app/entities/meeting/service/meeting.service';
import { ErrorSupportService } from 'app/shared/services/error-support.service';
import { OtherPersonModuleService } from 'app/pages/other-person/other-person-module.service';
import { OtherPersonService } from 'app/entities/other-person/service/other-person.service';

@Component({
  selector: 'jhi-meeting-attendees',
  templateUrl: './meeting-attendees.component.html',
  styleUrls: ['./meeting-attendees.component.scss'],
})
export class MeetingAttendeesComponent implements OnInit, OnDestroy {
  routeSub!: Subscription;
  meeting!: IMeeting;
  formCreator!: IDynamicFormCreatorModel;
  meetingSub?: Subscription;
  otherPersonSub?: Subscription;
  attendees: AttendeeField[] = [];

  @Input()
  size?: 'full' | 'medium' = 'medium';

  @Input()
  actions?: boolean = true;

  constructor(
    private activatedRoute: ActivatedRoute,
    private meetingModuleService: MeetingModuleService,
    private otherPersonModuleService: OtherPersonModuleService,
    private meetingService: MeetingService,
    private otherPersonService: OtherPersonService,
    private errorService: ErrorSupportService,
    private toastr: ToastrService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.routeSub = this.activatedRoute.queryParams.subscribe(params => {
      this.meeting = this.meetingModuleService.entityCheck(params);
    });

    this.meeting.meetingAttendees?.forEach(attendee => {
      this.attendees.push(this.meetingModuleService.convertMeetingAttendee(attendee));
    });

    this.formCreator = new DynamicFormCreatorModel([
      {
        id: 'attendee',
        type: 'attendee',
        label: 'Katılımcı Ekle',
        multiple: true,
        value: this.attendees,
        validators: [Validators.required],
      },
    ]);
  }

  ngOnDestroy(): void {
    this.routeSub.unsubscribe();
    this.meetingSub?.unsubscribe();
  }

  createMeeting(deletedAttendees?: IMeetingAttendee[]): void {
    const attendees: IMeetingAttendee[] = [];
    Object.keys(this.formCreator.form.controls).forEach(key => {
      const value = this.formCreator.form.get(key)!.value as AttendeeField;
      attendees.push(this.meetingModuleService.convertAttendeeField(value));
    });

    for (const attendee of attendees) {
      if (!attendee.identity || !attendee.status) {
        this.toastr.error('Katılımcıların tüm bilgilerini doldurun', 'Eksik Bilgi');
        return;
      }
    }

    if (this.meeting.totalAttendee !== attendees.length) {
      this.toastr.error('Toplam katılımcı sayısı kadar kayıt eklenmelidir.', 'Hatalı Katılımcı Sayısı');
      return;
    }

    if (this.meeting.id || attendees.length > 0) {
      this.meeting.finalized = true;
      this.meeting.attendee = attendees.filter(attendee => attendee.status === 'AttendeeStatus1').length;
    }

    this.meeting.meetingAttendees = attendees;

    if (deletedAttendees) {
      this.meeting.meetingAttendees.push(...deletedAttendees);
    }

    if (this.meeting.subject === 'MeetingSubject3') {
      const identityNumbers = this.meeting.meetingAttendees.map(attendee => attendee.identity!.identityNo!);

      this.otherPersonSub = this.otherPersonService.checkIfExists(identityNumbers, this.meeting.city!).subscribe(
        res => {
          const notExistsAttendees: string[] = [];
          attendees.forEach(attendee => {
            const identityNo = attendee.identity!.identityNo!;
            if (!res.body?.includes(identityNo)) {
              notExistsAttendees.push(identityNo);
            }
          });

          if (notExistsAttendees.length > 0) {
            this.otherPersonModuleService.setSubject3AndData({ meeting: this.meeting, notExistsAttendees });
            this.router.navigate(['/other-person', 'other-person-result'], { queryParams: { mode: 'meeting' } });
          } else {
            this.createRequest();
          }
        },
        err => this.errorService.errorHandler(err)
      );
    } else {
      this.createRequest();
    }
  }

  createRequest(): void {
    this.meetingSub = this.meetingService.create(this.meeting).subscribe(
      res => {
        if (res.body) {
          this.toastr.success('Toplantı başarıyla eklendi');
          this.router.navigate(['/meeting', 'info'], { queryParams: { mode: 'create' } });
        }
      },
      err => this.errorService.errorHandler(err)
    );
  }
}
