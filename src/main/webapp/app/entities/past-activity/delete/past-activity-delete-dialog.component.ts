import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { IPastActivity } from '../past-activity.model';
import { PastActivityService } from '../service/past-activity.service';

@Component({
  templateUrl: './past-activity-delete-dialog.component.html',
})
export class PastActivityDeleteDialogComponent {
  pastActivity?: IPastActivity;

  constructor(protected pastActivityService: PastActivityService, protected activeModal: NgbActiveModal) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.pastActivityService.delete(id).subscribe(() => {
      this.activeModal.close('deleted');
    });
  }
}
