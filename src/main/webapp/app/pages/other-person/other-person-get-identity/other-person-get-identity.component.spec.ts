import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OtherPersonGetIdentityComponent } from './other-person-get-identity.component';

describe('OtherPersonGetIdentityComponent', () => {
  let component: OtherPersonGetIdentityComponent;
  let fixture: ComponentFixture<OtherPersonGetIdentityComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [OtherPersonGetIdentityComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OtherPersonGetIdentityComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
