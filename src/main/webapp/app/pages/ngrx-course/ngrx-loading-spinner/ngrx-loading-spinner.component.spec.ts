import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NgrxLoadingSpinnerComponent } from './ngrx-loading-spinner.component';

describe('NgrxLoadingSpinnerComponent', () => {
  let component: NgrxLoadingSpinnerComponent;
  let fixture: ComponentFixture<NgrxLoadingSpinnerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [NgrxLoadingSpinnerComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NgrxLoadingSpinnerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
