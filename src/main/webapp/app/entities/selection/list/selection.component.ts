import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { ISelection } from '../selection.model';
import { SelectionService } from '../service/selection.service';
import { SelectionDeleteDialogComponent } from '../delete/selection-delete-dialog.component';

@Component({
  selector: 'jhi-selection',
  templateUrl: './selection.component.html',
})
export class SelectionComponent implements OnInit {
  selections?: ISelection[];
  isLoading = false;

  constructor(protected selectionService: SelectionService, protected modalService: NgbModal) {}

  loadAll(): void {
    this.isLoading = true;

    this.selectionService.query().subscribe(
      (res: HttpResponse<ISelection[]>) => {
        this.isLoading = false;
        this.selections = res.body ?? [];
      },
      () => {
        this.isLoading = false;
      }
    );
  }

  ngOnInit(): void {
    this.loadAll();
  }

  trackId(index: number, item: ISelection): number {
    return item.id!;
  }

  delete(selection: ISelection): void {
    const modalRef = this.modalService.open(SelectionDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.selection = selection;
    // unsubscribe not needed because closed completes on modal close
    modalRef.closed.subscribe(reason => {
      if (reason === 'deleted') {
        this.loadAll();
      }
    });
  }
}
