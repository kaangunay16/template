import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OtherPersonDeleteMainComponent } from './other-person-delete-main.component';

describe('OtherPersonDeleteMainComponent', () => {
  let component: OtherPersonDeleteMainComponent;
  let fixture: ComponentFixture<OtherPersonDeleteMainComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [OtherPersonDeleteMainComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OtherPersonDeleteMainComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
