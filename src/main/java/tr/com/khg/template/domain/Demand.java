package tr.com.khg.template.domain;

import java.io.Serializable;
import javax.persistence.*;
import tr.com.khg.template.domain.enumeration.DemandType;

/**
 * A Demand.
 */
@Entity
@Table(name = "demand")
public class Demand implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @Column(name = "document")
    private String document;

    @Column(name = "demanded_by")
    private String demandedBy;

    @Column(name = "description")
    private String description;

    @Column(name = "demand_type")
    private String demandType;

    @Enumerated(EnumType.STRING)
    @Column(name = "status")
    private DemandType status;

    @Column(name = "processed_by")
    private String processedBy;

    @Column(name = "entity_id")
    private Long entityId;

    @Column(name = "process_description")
    private String processDescription;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public Demand id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDocument() {
        return this.document;
    }

    public Demand document(String document) {
        this.setDocument(document);
        return this;
    }

    public void setDocument(String document) {
        this.document = document;
    }

    public String getDemandedBy() {
        return this.demandedBy;
    }

    public Demand demandedBy(String demandedBy) {
        this.setDemandedBy(demandedBy);
        return this;
    }

    public void setDemandedBy(String demandedBy) {
        this.demandedBy = demandedBy;
    }

    public String getDescription() {
        return this.description;
    }

    public Demand description(String description) {
        this.setDescription(description);
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDemandType() {
        return this.demandType;
    }

    public Demand demandType(String demandType) {
        this.setDemandType(demandType);
        return this;
    }

    public void setDemandType(String demandType) {
        this.demandType = demandType;
    }

    public DemandType getStatus() {
        return this.status;
    }

    public Demand status(DemandType status) {
        this.setStatus(status);
        return this;
    }

    public void setStatus(DemandType status) {
        this.status = status;
    }

    public String getProcessedBy() {
        return this.processedBy;
    }

    public Demand processedBy(String processedBy) {
        this.setProcessedBy(processedBy);
        return this;
    }

    public void setProcessedBy(String processedBy) {
        this.processedBy = processedBy;
    }

    public Long getEntityId() {
        return this.entityId;
    }

    public Demand entityId(Long entityId) {
        this.setEntityId(entityId);
        return this;
    }

    public void setEntityId(Long entityId) {
        this.entityId = entityId;
    }

    public String getProcessDescription() {
        return this.processDescription;
    }

    public Demand processDescription(String processDescription) {
        this.setProcessDescription(processDescription);
        return this;
    }

    public void setProcessDescription(String processDescription) {
        this.processDescription = processDescription;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Demand)) {
            return false;
        }
        return id != null && id.equals(((Demand) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Demand{" +
            "id=" + getId() +
            ", document='" + getDocument() + "'" +
            ", demandedBy='" + getDemandedBy() + "'" +
            ", description='" + getDescription() + "'" +
            ", demandType='" + getDemandType() + "'" +
            ", status='" + getStatus() + "'" +
            ", processedBy='" + getProcessedBy() + "'" +
            ", entityId=" + getEntityId() +
            ", processDescription='" + getProcessDescription() + "'" +
            "}";
    }
}
