package tr.com.khg.template.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import tr.com.khg.template.IntegrationTest;
import tr.com.khg.template.domain.Identity;
import tr.com.khg.template.domain.enumeration.Genders;
import tr.com.khg.template.domain.enumeration.MaritalStatus;
import tr.com.khg.template.repository.IdentityRepository;

/**
 * Integration tests for the {@link IdentityResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class IdentityResourceIT {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_SURNAME = "AAAAAAAAAA";
    private static final String UPDATED_SURNAME = "BBBBBBBBBB";

    private static final Genders DEFAULT_GENDER = Genders.ERKEK;
    private static final Genders UPDATED_GENDER = Genders.KADIN;

    private static final String DEFAULT_PLACE_OF_BIRTH = "AAAAAAAAAA";
    private static final String UPDATED_PLACE_OF_BIRTH = "BBBBBBBBBB";

    private static final String DEFAULT_REGISTERED_CITY = "AAAAAAAAAA";
    private static final String UPDATED_REGISTERED_CITY = "BBBBBBBBBB";

    private static final String DEFAULT_REGISTERED_DISTRICT = "AAAAAAAAAA";
    private static final String UPDATED_REGISTERED_DISTRICT = "BBBBBBBBBB";

    private static final String DEFAULT_REGISTERED_VILLAGE = "AAAAAAAAAA";
    private static final String UPDATED_REGISTERED_VILLAGE = "BBBBBBBBBB";

    private static final String DEFAULT_MOTHER_NAME = "AAAAAAAAAA";
    private static final String UPDATED_MOTHER_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_FATHER_NAME = "AAAAAAAAAA";
    private static final String UPDATED_FATHER_NAME = "BBBBBBBBBB";

    private static final MaritalStatus DEFAULT_MARITAL_STATUS = MaritalStatus.BEKAR;
    private static final MaritalStatus UPDATED_MARITAL_STATUS = MaritalStatus.EVLI;

    private static final String DEFAULT_EDUCATION = "AAAAAAAAAA";
    private static final String UPDATED_EDUCATION = "BBBBBBBBBB";

    private static final String DEFAULT_ADDRESS = "AAAAAAAAAA";
    private static final String UPDATED_ADDRESS = "BBBBBBBBBB";

    private static final String DEFAULT_PROFESSION = "AAAAAAAAAA";
    private static final String UPDATED_PROFESSION = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_BIRTHDAY = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_BIRTHDAY = LocalDate.now(ZoneId.systemDefault());

    private static final String DEFAULT_IDENTITY_NO = "AAAAAAAAAA";
    private static final String UPDATED_IDENTITY_NO = "BBBBBBBBBB";

    private static final String ENTITY_API_URL = "/api/identities";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private IdentityRepository identityRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restIdentityMockMvc;

    private Identity identity;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Identity createEntity(EntityManager em) {
        Identity identity = new Identity()
            .name(DEFAULT_NAME)
            .surname(DEFAULT_SURNAME)
            .gender(DEFAULT_GENDER)
            .placeOfBirth(DEFAULT_PLACE_OF_BIRTH)
            .registeredCity(DEFAULT_REGISTERED_CITY)
            .registeredDistrict(DEFAULT_REGISTERED_DISTRICT)
            .registeredVillage(DEFAULT_REGISTERED_VILLAGE)
            .motherName(DEFAULT_MOTHER_NAME)
            .fatherName(DEFAULT_FATHER_NAME)
            .maritalStatus(DEFAULT_MARITAL_STATUS)
            .education(DEFAULT_EDUCATION)
            .address(DEFAULT_ADDRESS)
            .profession(DEFAULT_PROFESSION)
            .birthday(DEFAULT_BIRTHDAY)
            .identityNo(DEFAULT_IDENTITY_NO);
        return identity;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Identity createUpdatedEntity(EntityManager em) {
        Identity identity = new Identity()
            .name(UPDATED_NAME)
            .surname(UPDATED_SURNAME)
            .gender(UPDATED_GENDER)
            .placeOfBirth(UPDATED_PLACE_OF_BIRTH)
            .registeredCity(UPDATED_REGISTERED_CITY)
            .registeredDistrict(UPDATED_REGISTERED_DISTRICT)
            .registeredVillage(UPDATED_REGISTERED_VILLAGE)
            .motherName(UPDATED_MOTHER_NAME)
            .fatherName(UPDATED_FATHER_NAME)
            .maritalStatus(UPDATED_MARITAL_STATUS)
            .education(UPDATED_EDUCATION)
            .address(UPDATED_ADDRESS)
            .profession(UPDATED_PROFESSION)
            .birthday(UPDATED_BIRTHDAY)
            .identityNo(UPDATED_IDENTITY_NO);
        return identity;
    }

    @BeforeEach
    public void initTest() {
        identity = createEntity(em);
    }

    @Test
    @Transactional
    void createIdentity() throws Exception {
        int databaseSizeBeforeCreate = identityRepository.findAll().size();
        // Create the Identity
        restIdentityMockMvc
            .perform(
                post(ENTITY_API_URL)
                    .with(csrf())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(identity))
            )
            .andExpect(status().isCreated());

        // Validate the Identity in the database
        List<Identity> identityList = identityRepository.findAll();
        assertThat(identityList).hasSize(databaseSizeBeforeCreate + 1);
        Identity testIdentity = identityList.get(identityList.size() - 1);
        assertThat(testIdentity.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testIdentity.getSurname()).isEqualTo(DEFAULT_SURNAME);
        assertThat(testIdentity.getGender()).isEqualTo(DEFAULT_GENDER);
        assertThat(testIdentity.getPlaceOfBirth()).isEqualTo(DEFAULT_PLACE_OF_BIRTH);
        assertThat(testIdentity.getRegisteredCity()).isEqualTo(DEFAULT_REGISTERED_CITY);
        assertThat(testIdentity.getRegisteredDistrict()).isEqualTo(DEFAULT_REGISTERED_DISTRICT);
        assertThat(testIdentity.getRegisteredVillage()).isEqualTo(DEFAULT_REGISTERED_VILLAGE);
        assertThat(testIdentity.getMotherName()).isEqualTo(DEFAULT_MOTHER_NAME);
        assertThat(testIdentity.getFatherName()).isEqualTo(DEFAULT_FATHER_NAME);
        assertThat(testIdentity.getMaritalStatus()).isEqualTo(DEFAULT_MARITAL_STATUS);
        assertThat(testIdentity.getEducation()).isEqualTo(DEFAULT_EDUCATION);
        assertThat(testIdentity.getAddress()).isEqualTo(DEFAULT_ADDRESS);
        assertThat(testIdentity.getProfession()).isEqualTo(DEFAULT_PROFESSION);
        assertThat(testIdentity.getBirthday()).isEqualTo(DEFAULT_BIRTHDAY);
        assertThat(testIdentity.getIdentityNo()).isEqualTo(DEFAULT_IDENTITY_NO);
    }

    @Test
    @Transactional
    void createIdentityWithExistingId() throws Exception {
        // Create the Identity with an existing ID
        identity.setId(1L);

        int databaseSizeBeforeCreate = identityRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restIdentityMockMvc
            .perform(
                post(ENTITY_API_URL)
                    .with(csrf())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(identity))
            )
            .andExpect(status().isBadRequest());

        // Validate the Identity in the database
        List<Identity> identityList = identityRepository.findAll();
        assertThat(identityList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void getAllIdentities() throws Exception {
        // Initialize the database
        identityRepository.saveAndFlush(identity);

        // Get all the identityList
        restIdentityMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(identity.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)))
            .andExpect(jsonPath("$.[*].surname").value(hasItem(DEFAULT_SURNAME)))
            .andExpect(jsonPath("$.[*].gender").value(hasItem(DEFAULT_GENDER.toString())))
            .andExpect(jsonPath("$.[*].placeOfBirth").value(hasItem(DEFAULT_PLACE_OF_BIRTH)))
            .andExpect(jsonPath("$.[*].registeredCity").value(hasItem(DEFAULT_REGISTERED_CITY)))
            .andExpect(jsonPath("$.[*].registeredDistrict").value(hasItem(DEFAULT_REGISTERED_DISTRICT)))
            .andExpect(jsonPath("$.[*].registeredVillage").value(hasItem(DEFAULT_REGISTERED_VILLAGE)))
            .andExpect(jsonPath("$.[*].motherName").value(hasItem(DEFAULT_MOTHER_NAME)))
            .andExpect(jsonPath("$.[*].fatherName").value(hasItem(DEFAULT_FATHER_NAME)))
            .andExpect(jsonPath("$.[*].maritalStatus").value(hasItem(DEFAULT_MARITAL_STATUS.toString())))
            .andExpect(jsonPath("$.[*].education").value(hasItem(DEFAULT_EDUCATION)))
            .andExpect(jsonPath("$.[*].address").value(hasItem(DEFAULT_ADDRESS)))
            .andExpect(jsonPath("$.[*].profession").value(hasItem(DEFAULT_PROFESSION)))
            .andExpect(jsonPath("$.[*].birthday").value(hasItem(DEFAULT_BIRTHDAY.toString())))
            .andExpect(jsonPath("$.[*].identityNo").value(hasItem(DEFAULT_IDENTITY_NO)));
    }

    @Test
    @Transactional
    void getIdentity() throws Exception {
        // Initialize the database
        identityRepository.saveAndFlush(identity);

        // Get the identity
        restIdentityMockMvc
            .perform(get(ENTITY_API_URL_ID, identity.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(identity.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME))
            .andExpect(jsonPath("$.surname").value(DEFAULT_SURNAME))
            .andExpect(jsonPath("$.gender").value(DEFAULT_GENDER.toString()))
            .andExpect(jsonPath("$.placeOfBirth").value(DEFAULT_PLACE_OF_BIRTH))
            .andExpect(jsonPath("$.registeredCity").value(DEFAULT_REGISTERED_CITY))
            .andExpect(jsonPath("$.registeredDistrict").value(DEFAULT_REGISTERED_DISTRICT))
            .andExpect(jsonPath("$.registeredVillage").value(DEFAULT_REGISTERED_VILLAGE))
            .andExpect(jsonPath("$.motherName").value(DEFAULT_MOTHER_NAME))
            .andExpect(jsonPath("$.fatherName").value(DEFAULT_FATHER_NAME))
            .andExpect(jsonPath("$.maritalStatus").value(DEFAULT_MARITAL_STATUS.toString()))
            .andExpect(jsonPath("$.education").value(DEFAULT_EDUCATION))
            .andExpect(jsonPath("$.address").value(DEFAULT_ADDRESS))
            .andExpect(jsonPath("$.profession").value(DEFAULT_PROFESSION))
            .andExpect(jsonPath("$.birthday").value(DEFAULT_BIRTHDAY.toString()))
            .andExpect(jsonPath("$.identityNo").value(DEFAULT_IDENTITY_NO));
    }

    @Test
    @Transactional
    void getNonExistingIdentity() throws Exception {
        // Get the identity
        restIdentityMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putNewIdentity() throws Exception {
        // Initialize the database
        identityRepository.saveAndFlush(identity);

        int databaseSizeBeforeUpdate = identityRepository.findAll().size();

        // Update the identity
        Identity updatedIdentity = identityRepository.findById(identity.getId()).get();
        // Disconnect from session so that the updates on updatedIdentity are not directly saved in db
        em.detach(updatedIdentity);
        updatedIdentity
            .name(UPDATED_NAME)
            .surname(UPDATED_SURNAME)
            .gender(UPDATED_GENDER)
            .placeOfBirth(UPDATED_PLACE_OF_BIRTH)
            .registeredCity(UPDATED_REGISTERED_CITY)
            .registeredDistrict(UPDATED_REGISTERED_DISTRICT)
            .registeredVillage(UPDATED_REGISTERED_VILLAGE)
            .motherName(UPDATED_MOTHER_NAME)
            .fatherName(UPDATED_FATHER_NAME)
            .maritalStatus(UPDATED_MARITAL_STATUS)
            .education(UPDATED_EDUCATION)
            .address(UPDATED_ADDRESS)
            .profession(UPDATED_PROFESSION)
            .birthday(UPDATED_BIRTHDAY)
            .identityNo(UPDATED_IDENTITY_NO);

        restIdentityMockMvc
            .perform(
                put(ENTITY_API_URL_ID, updatedIdentity.getId())
                    .with(csrf())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(updatedIdentity))
            )
            .andExpect(status().isOk());

        // Validate the Identity in the database
        List<Identity> identityList = identityRepository.findAll();
        assertThat(identityList).hasSize(databaseSizeBeforeUpdate);
        Identity testIdentity = identityList.get(identityList.size() - 1);
        assertThat(testIdentity.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testIdentity.getSurname()).isEqualTo(UPDATED_SURNAME);
        assertThat(testIdentity.getGender()).isEqualTo(UPDATED_GENDER);
        assertThat(testIdentity.getPlaceOfBirth()).isEqualTo(UPDATED_PLACE_OF_BIRTH);
        assertThat(testIdentity.getRegisteredCity()).isEqualTo(UPDATED_REGISTERED_CITY);
        assertThat(testIdentity.getRegisteredDistrict()).isEqualTo(UPDATED_REGISTERED_DISTRICT);
        assertThat(testIdentity.getRegisteredVillage()).isEqualTo(UPDATED_REGISTERED_VILLAGE);
        assertThat(testIdentity.getMotherName()).isEqualTo(UPDATED_MOTHER_NAME);
        assertThat(testIdentity.getFatherName()).isEqualTo(UPDATED_FATHER_NAME);
        assertThat(testIdentity.getMaritalStatus()).isEqualTo(UPDATED_MARITAL_STATUS);
        assertThat(testIdentity.getEducation()).isEqualTo(UPDATED_EDUCATION);
        assertThat(testIdentity.getAddress()).isEqualTo(UPDATED_ADDRESS);
        assertThat(testIdentity.getProfession()).isEqualTo(UPDATED_PROFESSION);
        assertThat(testIdentity.getBirthday()).isEqualTo(UPDATED_BIRTHDAY);
        assertThat(testIdentity.getIdentityNo()).isEqualTo(UPDATED_IDENTITY_NO);
    }

    @Test
    @Transactional
    void putNonExistingIdentity() throws Exception {
        int databaseSizeBeforeUpdate = identityRepository.findAll().size();
        identity.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restIdentityMockMvc
            .perform(
                put(ENTITY_API_URL_ID, identity.getId())
                    .with(csrf())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(identity))
            )
            .andExpect(status().isBadRequest());

        // Validate the Identity in the database
        List<Identity> identityList = identityRepository.findAll();
        assertThat(identityList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchIdentity() throws Exception {
        int databaseSizeBeforeUpdate = identityRepository.findAll().size();
        identity.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restIdentityMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .with(csrf())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(identity))
            )
            .andExpect(status().isBadRequest());

        // Validate the Identity in the database
        List<Identity> identityList = identityRepository.findAll();
        assertThat(identityList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamIdentity() throws Exception {
        int databaseSizeBeforeUpdate = identityRepository.findAll().size();
        identity.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restIdentityMockMvc
            .perform(
                put(ENTITY_API_URL)
                    .with(csrf())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(identity))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the Identity in the database
        List<Identity> identityList = identityRepository.findAll();
        assertThat(identityList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateIdentityWithPatch() throws Exception {
        // Initialize the database
        identityRepository.saveAndFlush(identity);

        int databaseSizeBeforeUpdate = identityRepository.findAll().size();

        // Update the identity using partial update
        Identity partialUpdatedIdentity = new Identity();
        partialUpdatedIdentity.setId(identity.getId());

        partialUpdatedIdentity
            .placeOfBirth(UPDATED_PLACE_OF_BIRTH)
            .registeredDistrict(UPDATED_REGISTERED_DISTRICT)
            .registeredVillage(UPDATED_REGISTERED_VILLAGE)
            .motherName(UPDATED_MOTHER_NAME);

        restIdentityMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedIdentity.getId())
                    .with(csrf())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedIdentity))
            )
            .andExpect(status().isOk());

        // Validate the Identity in the database
        List<Identity> identityList = identityRepository.findAll();
        assertThat(identityList).hasSize(databaseSizeBeforeUpdate);
        Identity testIdentity = identityList.get(identityList.size() - 1);
        assertThat(testIdentity.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testIdentity.getSurname()).isEqualTo(DEFAULT_SURNAME);
        assertThat(testIdentity.getGender()).isEqualTo(DEFAULT_GENDER);
        assertThat(testIdentity.getPlaceOfBirth()).isEqualTo(UPDATED_PLACE_OF_BIRTH);
        assertThat(testIdentity.getRegisteredCity()).isEqualTo(DEFAULT_REGISTERED_CITY);
        assertThat(testIdentity.getRegisteredDistrict()).isEqualTo(UPDATED_REGISTERED_DISTRICT);
        assertThat(testIdentity.getRegisteredVillage()).isEqualTo(UPDATED_REGISTERED_VILLAGE);
        assertThat(testIdentity.getMotherName()).isEqualTo(UPDATED_MOTHER_NAME);
        assertThat(testIdentity.getFatherName()).isEqualTo(DEFAULT_FATHER_NAME);
        assertThat(testIdentity.getMaritalStatus()).isEqualTo(DEFAULT_MARITAL_STATUS);
        assertThat(testIdentity.getEducation()).isEqualTo(DEFAULT_EDUCATION);
        assertThat(testIdentity.getAddress()).isEqualTo(DEFAULT_ADDRESS);
        assertThat(testIdentity.getProfession()).isEqualTo(DEFAULT_PROFESSION);
        assertThat(testIdentity.getBirthday()).isEqualTo(DEFAULT_BIRTHDAY);
        assertThat(testIdentity.getIdentityNo()).isEqualTo(DEFAULT_IDENTITY_NO);
    }

    @Test
    @Transactional
    void fullUpdateIdentityWithPatch() throws Exception {
        // Initialize the database
        identityRepository.saveAndFlush(identity);

        int databaseSizeBeforeUpdate = identityRepository.findAll().size();

        // Update the identity using partial update
        Identity partialUpdatedIdentity = new Identity();
        partialUpdatedIdentity.setId(identity.getId());

        partialUpdatedIdentity
            .name(UPDATED_NAME)
            .surname(UPDATED_SURNAME)
            .gender(UPDATED_GENDER)
            .placeOfBirth(UPDATED_PLACE_OF_BIRTH)
            .registeredCity(UPDATED_REGISTERED_CITY)
            .registeredDistrict(UPDATED_REGISTERED_DISTRICT)
            .registeredVillage(UPDATED_REGISTERED_VILLAGE)
            .motherName(UPDATED_MOTHER_NAME)
            .fatherName(UPDATED_FATHER_NAME)
            .maritalStatus(UPDATED_MARITAL_STATUS)
            .education(UPDATED_EDUCATION)
            .address(UPDATED_ADDRESS)
            .profession(UPDATED_PROFESSION)
            .birthday(UPDATED_BIRTHDAY)
            .identityNo(UPDATED_IDENTITY_NO);

        restIdentityMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedIdentity.getId())
                    .with(csrf())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedIdentity))
            )
            .andExpect(status().isOk());

        // Validate the Identity in the database
        List<Identity> identityList = identityRepository.findAll();
        assertThat(identityList).hasSize(databaseSizeBeforeUpdate);
        Identity testIdentity = identityList.get(identityList.size() - 1);
        assertThat(testIdentity.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testIdentity.getSurname()).isEqualTo(UPDATED_SURNAME);
        assertThat(testIdentity.getGender()).isEqualTo(UPDATED_GENDER);
        assertThat(testIdentity.getPlaceOfBirth()).isEqualTo(UPDATED_PLACE_OF_BIRTH);
        assertThat(testIdentity.getRegisteredCity()).isEqualTo(UPDATED_REGISTERED_CITY);
        assertThat(testIdentity.getRegisteredDistrict()).isEqualTo(UPDATED_REGISTERED_DISTRICT);
        assertThat(testIdentity.getRegisteredVillage()).isEqualTo(UPDATED_REGISTERED_VILLAGE);
        assertThat(testIdentity.getMotherName()).isEqualTo(UPDATED_MOTHER_NAME);
        assertThat(testIdentity.getFatherName()).isEqualTo(UPDATED_FATHER_NAME);
        assertThat(testIdentity.getMaritalStatus()).isEqualTo(UPDATED_MARITAL_STATUS);
        assertThat(testIdentity.getEducation()).isEqualTo(UPDATED_EDUCATION);
        assertThat(testIdentity.getAddress()).isEqualTo(UPDATED_ADDRESS);
        assertThat(testIdentity.getProfession()).isEqualTo(UPDATED_PROFESSION);
        assertThat(testIdentity.getBirthday()).isEqualTo(UPDATED_BIRTHDAY);
        assertThat(testIdentity.getIdentityNo()).isEqualTo(UPDATED_IDENTITY_NO);
    }

    @Test
    @Transactional
    void patchNonExistingIdentity() throws Exception {
        int databaseSizeBeforeUpdate = identityRepository.findAll().size();
        identity.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restIdentityMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, identity.getId())
                    .with(csrf())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(identity))
            )
            .andExpect(status().isBadRequest());

        // Validate the Identity in the database
        List<Identity> identityList = identityRepository.findAll();
        assertThat(identityList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchIdentity() throws Exception {
        int databaseSizeBeforeUpdate = identityRepository.findAll().size();
        identity.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restIdentityMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .with(csrf())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(identity))
            )
            .andExpect(status().isBadRequest());

        // Validate the Identity in the database
        List<Identity> identityList = identityRepository.findAll();
        assertThat(identityList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamIdentity() throws Exception {
        int databaseSizeBeforeUpdate = identityRepository.findAll().size();
        identity.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restIdentityMockMvc
            .perform(
                patch(ENTITY_API_URL)
                    .with(csrf())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(identity))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the Identity in the database
        List<Identity> identityList = identityRepository.findAll();
        assertThat(identityList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteIdentity() throws Exception {
        // Initialize the database
        identityRepository.saveAndFlush(identity);

        int databaseSizeBeforeDelete = identityRepository.findAll().size();

        // Delete the identity
        restIdentityMockMvc
            .perform(delete(ENTITY_API_URL_ID, identity.getId()).with(csrf()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Identity> identityList = identityRepository.findAll();
        assertThat(identityList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
