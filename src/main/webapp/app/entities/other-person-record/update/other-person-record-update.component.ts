import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import { IOtherPersonRecord, OtherPersonRecord } from '../other-person-record.model';
import { OtherPersonRecordService } from '../service/other-person-record.service';
import { IOtherPerson } from 'app/entities/other-person/other-person.model';
import { OtherPersonService } from 'app/entities/other-person/service/other-person.service';

@Component({
  selector: 'jhi-other-person-record-update',
  templateUrl: './other-person-record-update.component.html',
})
export class OtherPersonRecordUpdateComponent implements OnInit {
  isSaving = false;

  otherPeopleSharedCollection: IOtherPerson[] = [];

  editForm = this.fb.group({
    id: [],
    city: [],
    rank: [],
    section: [],
    meetingStatus: [],
    status: [],
    speaker: [],
    addedByUnit: [],
    profession: [],
    document: [],
    type: [],
    otherPerson: [],
  });

  constructor(
    protected otherPersonRecordService: OtherPersonRecordService,
    protected otherPersonService: OtherPersonService,
    protected activatedRoute: ActivatedRoute,
    protected fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ otherPersonRecord }) => {
      this.updateForm(otherPersonRecord);

      this.loadRelationshipsOptions();
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const otherPersonRecord = this.createFromForm();
    if (otherPersonRecord.id !== undefined) {
      this.subscribeToSaveResponse(this.otherPersonRecordService.update(otherPersonRecord));
    } else {
      this.subscribeToSaveResponse(this.otherPersonRecordService.create(otherPersonRecord));
    }
  }

  trackOtherPersonById(index: number, item: IOtherPerson): number {
    return item.id!;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IOtherPersonRecord>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(otherPersonRecord: IOtherPersonRecord): void {
    this.editForm.patchValue({
      id: otherPersonRecord.id,
      city: otherPersonRecord.city,
      rank: otherPersonRecord.rank,
      section: otherPersonRecord.section,
      meetingStatus: otherPersonRecord.meetingStatus,
      status: otherPersonRecord.status,
      speaker: otherPersonRecord.speaker,
      addedByUnit: otherPersonRecord.addedByUnit,
      profession: otherPersonRecord.profession,
      document: otherPersonRecord.document,
      type: otherPersonRecord.type,
      otherPerson: otherPersonRecord.otherPerson,
    });

    this.otherPeopleSharedCollection = this.otherPersonService.addOtherPersonToCollectionIfMissing(
      this.otherPeopleSharedCollection,
      otherPersonRecord.otherPerson
    );
  }

  protected loadRelationshipsOptions(): void {
    this.otherPersonService
      .query()
      .pipe(map((res: HttpResponse<IOtherPerson[]>) => res.body ?? []))
      .pipe(
        map((otherPeople: IOtherPerson[]) =>
          this.otherPersonService.addOtherPersonToCollectionIfMissing(otherPeople, this.editForm.get('otherPerson')!.value)
        )
      )
      .subscribe((otherPeople: IOtherPerson[]) => (this.otherPeopleSharedCollection = otherPeople));
  }

  protected createFromForm(): IOtherPersonRecord {
    return {
      ...new OtherPersonRecord(),
      id: this.editForm.get(['id'])!.value,
      city: this.editForm.get(['city'])!.value,
      rank: this.editForm.get(['rank'])!.value,
      section: this.editForm.get(['section'])!.value,
      meetingStatus: this.editForm.get(['meetingStatus'])!.value,
      status: this.editForm.get(['status'])!.value,
      speaker: this.editForm.get(['speaker'])!.value,
      addedByUnit: this.editForm.get(['addedByUnit'])!.value,
      profession: this.editForm.get(['profession'])!.value,
      document: this.editForm.get(['document'])!.value,
      type: this.editForm.get(['type'])!.value,
      otherPerson: this.editForm.get(['otherPerson'])!.value,
    };
  }
}
