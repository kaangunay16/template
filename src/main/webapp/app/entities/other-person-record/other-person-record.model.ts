import { IOtherPerson } from 'app/entities/other-person/other-person.model';
import { HasId } from '../form-entity.model';

export interface IOtherPersonRecord extends HasId {
  id?: number;
  city?: string | null;
  rank?: string | null;
  section?: string | null;
  meetingStatus?: string | null;
  status?: string | null;
  speaker?: boolean | null;
  addedByUnit?: string | null;
  profession?: string | null;
  document?: string | null;
  type?: string | null;
  otherPerson?: IOtherPerson | null;
}

export class OtherPersonRecord implements IOtherPersonRecord {
  constructor(
    public id?: number,
    public city?: string | null,
    public rank?: string | null,
    public section?: string | null,
    public meetingStatus?: string | null,
    public status?: string | null,
    public speaker?: boolean | null,
    public addedByUnit?: string | null,
    public profession?: string | null,
    public document?: string | null,
    public type?: string | null,
    public otherPerson?: IOtherPerson | null
  ) {}
}

export function getOtherPersonRecordIdentifier(otherPersonRecord: IOtherPersonRecord): number | undefined {
  return otherPersonRecord.id;
}
