package tr.com.khg.template.domain;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import tr.com.khg.template.web.rest.TestUtil;

class ActualActivityTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ActualActivity.class);
        ActualActivity actualActivity1 = new ActualActivity();
        actualActivity1.setId(1L);
        ActualActivity actualActivity2 = new ActualActivity();
        actualActivity2.setId(actualActivity1.getId());
        assertThat(actualActivity1).isEqualTo(actualActivity2);
        actualActivity2.setId(2L);
        assertThat(actualActivity1).isNotEqualTo(actualActivity2);
        actualActivity1.setId(null);
        assertThat(actualActivity1).isNotEqualTo(actualActivity2);
    }
}
