import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { IPayPhone } from '../pay-phone.model';
import { PayPhoneService } from '../service/pay-phone.service';

@Component({
  templateUrl: './pay-phone-delete-dialog.component.html',
})
export class PayPhoneDeleteDialogComponent {
  payPhone?: IPayPhone;

  constructor(protected payPhoneService: PayPhoneService, protected activeModal: NgbActiveModal) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.payPhoneService.delete(id).subscribe(() => {
      this.activeModal.close('deleted');
    });
  }
}
