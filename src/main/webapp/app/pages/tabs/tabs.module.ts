import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedModule } from 'app/shared/shared.module';
import { TabsRoutingModule } from './tabs-routing.module';
import { TabsService } from './tabs.service';
import { TabMainComponent } from './tab-main/tab-main.component';
import { Tab1Component } from './tab1/tab1.component';
import { ContentContainerDirective } from './content-container.directive';
import { TabContentComponent } from './tab-content/tab-content.component';

@NgModule({
  declarations: [TabMainComponent, Tab1Component, ContentContainerDirective, TabContentComponent],
  imports: [CommonModule, TabsRoutingModule, SharedModule],
  providers: [TabsService],
})
export class TabsModule {}
