import { Action, createReducer, on } from '@ngrx/store';

import { initialState } from './side-effect.state';
import { selectionRequestSuccess } from './side-effect.actions';

const _sideEffectReducer = createReducer(
  initialState,
  on(selectionRequestSuccess, (state, action) => ({ ...state, selections: action.selections }))
);

export function sideEffectReducer(state: any, action: Action): any {
  return _sideEffectReducer(state, action);
}
