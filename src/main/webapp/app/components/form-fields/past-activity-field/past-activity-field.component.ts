import { Component, Input, OnInit } from '@angular/core';
import { ControlValueAccessor, FormBuilder, FormGroup, NG_VALUE_ACCESSOR } from '@angular/forms';

import { PastActivityService } from 'app/entities/past-activity/service/past-activity.service';
import { PastActivity } from 'app/entities/past-activity/past-activity.model';
import { DynamicFormService } from 'app/components/dynamic-form-creator/dynamic-form.service';
import { DYNAMIC_FIELD } from 'app/components/dynamic-form-creator/dyanmic-form.token';
import { DynamicFormFunctions } from 'app/components/dynamic-form-creator/dynamic-form-field.model';
import { PersonModuleService } from 'app/pages/person/person-module.service';
import { FormFieldsService } from '../form-fields.service';

@Component({
  selector: 'jhi-past-activity-field',
  templateUrl: './past-activity-field.component.html',
  styleUrls: ['./past-activity-field.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: PastActivityFieldComponent,
      multi: true,
    },
    {
      provide: DYNAMIC_FIELD,
      useExisting: PastActivityFieldComponent,
    },
  ],
})
export class PastActivityFieldComponent implements OnInit, ControlValueAccessor, DynamicFormFunctions {
  form!: FormGroup;

  @Input()
  readOnly?: boolean = false;

  onTouched!: () => void;

  constructor(
    private formBuilder: FormBuilder,
    private formFieldsService: FormFieldsService,
    private service: PastActivityService,
    private personModuleService: PersonModuleService,
    private dynamicFormService: DynamicFormService
  ) {}

  ngOnInit(): void {
    this.form = this.formBuilder.group({
      id: [{ value: null }],
      city: [{ value: null, disabled: this.readOnly }],
      startYear: [{ value: null, disabled: this.readOnly }],
      endYear: [{ value: null, disabled: this.readOnly }],
      deleted: [{ value: null }],
      person: [{ value: null }],
    });
  }

  registerOnChange(fn: (value: any) => void): void {
    this.form.valueChanges.subscribe(fn);
  }

  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

  writeValue(obj: PastActivity | null): void {
    this.form.patchValue({
      id: obj ? obj.id : null,
      city: obj ? obj.city : null,
      startYear: obj ? obj.startYear : null,
      endYear: obj ? obj.endYear : null,
      deleted: obj ? obj.deleted : false,
      person: obj ? obj.person : null,
    });
  }

  remove(): void {
    const id = this.form.get('id')?.value;

    if (id) {
      this.personModuleService.removePastActivity(this.dynamicFormService.getEntityId(), id);
      this.formFieldsService.remove(id, this.service);
    }
  }
}
