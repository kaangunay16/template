import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';

import { ISelection, Selection } from '../selection.model';
import { SelectionService } from '../service/selection.service';

@Component({
  selector: 'jhi-selection-update',
  templateUrl: './selection-update.component.html',
})
export class SelectionUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [],
    key: [],
    value: [],
    priority: [],
  });

  constructor(protected selectionService: SelectionService, protected activatedRoute: ActivatedRoute, protected fb: FormBuilder) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ selection }) => {
      this.updateForm(selection);
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const selection = this.createFromForm();
    if (selection.id !== undefined) {
      this.subscribeToSaveResponse(this.selectionService.update(selection));
    } else {
      this.subscribeToSaveResponse(this.selectionService.create(selection));
    }
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ISelection>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(selection: ISelection): void {
    this.editForm.patchValue({
      id: selection.id,
      key: selection.key,
      value: selection.value,
      priority: selection.priority,
    });
  }

  protected createFromForm(): ISelection {
    return {
      ...new Selection(),
      id: this.editForm.get(['id'])!.value,
      key: this.editForm.get(['key'])!.value,
      value: this.editForm.get(['value'])!.value,
      priority: this.editForm.get(['priority'])!.value,
    };
  }
}
