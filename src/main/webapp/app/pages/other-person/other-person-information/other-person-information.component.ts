import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';

import { DynamicFormCreatorModel, IDynamicFormCreatorModel } from 'app/components/dynamic-form-creator/dynamic-form-field.model';
import { IOtherPerson } from 'app/entities/other-person/other-person.model';
import { OtherPersonModuleService } from 'app/pages/other-person/other-person-module.service';

@Component({
  selector: 'jhi-other-person-information',
  templateUrl: './other-person-information.component.html',
  styleUrls: ['./other-person-information.component.scss'],
})
export class OtherPersonInformationComponent implements OnInit, OnDestroy {
  otherPerson!: IOtherPerson;
  formCreator!: IDynamicFormCreatorModel;
  formCreatorsOfRecords!: IDynamicFormCreatorModel[];
  routeSub!: Subscription;
  formSubscriptions: Subscription[] = [];
  isButtonActive = false;
  formsStatus: boolean[] = [];
  isSubject3?: boolean;

  constructor(private activatedRoute: ActivatedRoute, private otherPersonModuleService: OtherPersonModuleService, private router: Router) {}

  ngOnInit(): void {
    this.routeSub = this.activatedRoute.queryParams.subscribe(params => {
      this.otherPerson = this.otherPersonModuleService.entityCheck(params);
      this.isSubject3 = !!this.otherPersonModuleService.subject3;
      this.formCreatorsOfRecords = this.otherPersonModuleService.createRecordsForms(this.otherPerson);
      this.subscribeAllForms(this.formCreatorsOfRecords);
      this.formCreator = new DynamicFormCreatorModel([
        {
          id: 'identityNo',
          label: 'Kimlik Numarası',
          type: 'text',
          value: this.otherPerson.identity?.identityNo,
          readOnly: true,
        },
        {
          id: 'name',
          label: 'İsim Soyisim',
          type: 'text',
          value: `${this.otherPerson.identity!.name!} ${this.otherPerson.identity!.surname!}`,
          readOnly: true,
        },
      ]);
    });
  }

  subscribeAllForms(forms: IDynamicFormCreatorModel[]): void {
    this.formsStatus = Array<boolean>(forms.length).fill(false, 0, forms.length);
    forms.forEach((form, index) => {
      const sub = form.form.statusChanges.subscribe(status => {
        this.formsStatus[index] = status === 'VALID';
        this.checkValidationOfButton();
      });
      this.formSubscriptions.push(sub);
    });
  }

  checkValidationOfButton(): void {
    let status = this.formsStatus[0];
    this.formsStatus.forEach(st => {
      status = status && st;
    });

    this.isButtonActive = status;
  }

  ngOnDestroy(): void {
    this.routeSub.unsubscribe();
    this.formSubscriptions.forEach(sub => sub.unsubscribe());
  }

  createSubmit(): void {
    this.otherPerson.otherPersonRecords = this.otherPersonModuleService.getAllRecordsFromFormCreators(this.formCreatorsOfRecords);
    this.otherPersonModuleService.createOtherPerson(this.otherPerson);
  }

  updateSubmit(): void {
    this.otherPersonModuleService.updateAllRecords(this.otherPerson, this.formCreatorsOfRecords);
  }

  meetingSubmit(): void {
    this.otherPerson.otherPersonRecords = this.otherPersonModuleService.getAllRecordsFromFormCreators(this.formCreatorsOfRecords);
    window.history.back();
  }

  createDeletionDemand(recordId: number): void {
    this.router.navigate(['/other-person', 'other-person-deletion'], { queryParams: { id: this.otherPerson.id, recordId } });
  }
}
