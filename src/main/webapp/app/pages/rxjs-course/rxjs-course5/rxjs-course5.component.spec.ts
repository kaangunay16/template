import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RxjsCourse5Component } from './rxjs-course5.component';

describe('RxjsCourse5Component', () => {
  let component: RxjsCourse5Component;
  let fixture: ComponentFixture<RxjsCourse5Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [RxjsCourse5Component],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RxjsCourse5Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
