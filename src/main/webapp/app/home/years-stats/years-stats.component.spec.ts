import { ComponentFixture, TestBed } from '@angular/core/testing';

import { YearsStatsComponent } from './years-stats.component';

describe('YearsStatsComponent', () => {
  let component: YearsStatsComponent;
  let fixture: ComponentFixture<YearsStatsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [YearsStatsComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(YearsStatsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
