import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IdentityInfoRetrieverComponent } from './identity-info-retriever.component';

describe('IdentityInfoRetrieverComponent', () => {
  let component: IdentityInfoRetrieverComponent;
  let fixture: ComponentFixture<IdentityInfoRetrieverComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [IdentityInfoRetrieverComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(IdentityInfoRetrieverComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
