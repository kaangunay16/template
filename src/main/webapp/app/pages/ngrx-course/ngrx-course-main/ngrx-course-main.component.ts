import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';

import { AppState } from '../store/app.state';
import { getErrorMessage, getLoading } from '../store/shared/shared.selector';

@Component({
  selector: 'jhi-ngrx-course-main',
  templateUrl: './ngrx-course-main.component.html',
  styleUrls: ['./ngrx-course-main.component.scss'],
})
export class NgrxCourseMainComponent implements OnInit {
  showLoading$!: Observable<boolean>;
  errorMessage$!: Observable<string>;

  constructor(private store: Store<AppState>) {}

  ngOnInit(): void {
    this.showLoading$ = this.store.select(getLoading);
    this.errorMessage$ = this.store.select(getErrorMessage);
  }
}
