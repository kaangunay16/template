export interface BooleanField {
  trueField: string;
  falseField: string;
}
