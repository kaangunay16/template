import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { IOtherName } from '../other-name.model';
import { OtherNameService } from '../service/other-name.service';

@Component({
  templateUrl: './other-name-delete-dialog.component.html',
})
export class OtherNameDeleteDialogComponent {
  otherName?: IOtherName;

  constructor(protected otherNameService: OtherNameService, protected activeModal: NgbActiveModal) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.otherNameService.delete(id).subscribe(() => {
      this.activeModal.close('deleted');
    });
  }
}
