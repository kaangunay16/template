export interface IStatistic {
  key: string;
  value: number;
}

export class Statistic implements IStatistic {
  constructor(public key: string, public value: number) {}
}
