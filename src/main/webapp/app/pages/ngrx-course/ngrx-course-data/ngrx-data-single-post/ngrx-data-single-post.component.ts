import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

import { DataPostService } from '../data-post.service';

@Component({
  selector: 'jhi-ngrx-data-single-post',
  templateUrl: './ngrx-data-single-post.component.html',
  styleUrls: ['./ngrx-data-single-post.component.scss'],
})
export class NgrxDataSinglePostComponent implements OnInit {
  selectionForm!: FormGroup;

  constructor(private postService: DataPostService, private route: ActivatedRoute, private router: Router) {}

  ngOnInit(): void {
    this.selectionForm = new FormGroup({
      id: new FormControl(null),
      key: new FormControl({ readOnly: true, value: null }),
      value: new FormControl({ readOnly: true, value: null }),
      priority: new FormControl({ readOnly: true, value: null }),
    });

    const id = +this.route.snapshot.params['id'];

    this.postService.entities$.subscribe(selections => {
      const selection = selections.find(sel => sel.id === id);

      if (selection) {
        this.selectionForm.patchValue({
          id: selection.id,
          key: selection.key,
          value: selection.value,
          priority: selection.priority,
        });
      }
    });
  }
}
