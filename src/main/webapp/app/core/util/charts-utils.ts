export function generateRandomColors(num: number): string[] {
  const colors: string[] = [];
  for (let i = 0; i < num; i++) {
    colors.push(`rgba(${getRandomNumber()}, ${getRandomNumber()}, ${getRandomNumber()}, 0.5)`);
  }

  return colors;
}

export function generateRandomColor(): string {
  return generateRandomColors(1)[0];
}

function getRandomNumber(): number {
  return Math.floor(Math.random() * 255);
}
