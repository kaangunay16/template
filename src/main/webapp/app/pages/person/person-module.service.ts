import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import * as dayjs from 'dayjs';

import { IPerson } from 'app/entities/person/person.model';
import { PastActivity } from 'app/entities/past-activity/past-activity.model';
import { ActivityField } from 'app/components/models/activity-field.model';
import { PersonService } from 'app/entities/person/service/person.service';
import { ErrorSupportService } from 'app/shared/services/error-support.service';
import { PageService } from 'app/shared/services/page-service';

@Injectable({
  providedIn: 'root',
})
export class PersonModuleService extends PageService<IPerson> {
  constructor(toastr: ToastrService, router: Router, private personService: PersonService, private errorService: ErrorSupportService) {
    super(toastr, router);
  }

  stopActualActivities(actualActivityId: number, personId?: number): void {
    let person = this.getSingleEntity(personId);
    const actualActivity = person.actualActivities?.find(activity => activity.id === actualActivityId);
    if (actualActivity) {
      const pastActivity = new PastActivity();
      pastActivity.city = actualActivity.city;
      pastActivity.startYear = actualActivity.startYear;
      pastActivity.endYear = dayjs().year();
      pastActivity.deleted = false;
      person.actualActivities = person.actualActivities?.filter(activity => activity.id !== actualActivityId);
      person.pastActivities?.push(pastActivity);
      this.personService.convertActualToPastActivity(actualActivityId, person).subscribe(
        result => {
          if (result.body) {
            person = result.body;
            this.toastr.success('Faaliyet Geçmiş Faaliyetlere Aktarıldı.');
            this.router.navigate(['/person', 'person-result']);
          }
        },
        err => this.errorService.errorHandler(err)
      );
    }
  }

  setActivityField(person: IPerson, aField?: ActivityField | null): void {
    if (!aField) {
      return;
    }

    person.organization = aField.organization;
    person.section = aField.section;
    person.position = aField.status;
    person.institution = aField.institution;
    person.city = aField.city;
    person.district = aField.district;
  }

  getActivityField(person: IPerson): ActivityField | null {
    const activityField = new ActivityField();
    activityField.section = person.section;
    activityField.status = person.position;
    activityField.organization = person.organization;
    activityField.city = person.city;
    activityField.district = person.district;

    return activityField;
  }

  updatePerson(person: IPerson): void {
    this.personService.update(person).subscribe(
      res => {
        if (res.body) {
          this.toastr.success('Kayıt başarıyla güncellendi.');
          this.changeEntity(person, res.body);
          window.history.back();
        }
      },
      err => this.errorService.errorHandler(err)
    );
  }

  removeAbroadActivity(personId: number | undefined, abroadActivityId: number): void {
    const person = this.getSingleEntity(personId);
    person.abroadActivities = person.abroadActivities?.filter(activity => activity.id !== abroadActivityId);
  }

  removeActualActivity(personId: number | undefined, actualActivityId: number): void {
    const person = this.getSingleEntity(personId);
    person.actualActivities = person.actualActivities?.filter(activity => activity.id !== actualActivityId);
  }

  removeOtherName(personId: number | undefined, otherNameId: number): void {
    const person = this.getSingleEntity(personId);
    person.otherNames = person.otherNames?.filter(name => name.id !== otherNameId);
  }

  removePastActivity(personId: number | undefined, pastActivityId: number): void {
    const person = this.getSingleEntity(personId);
    person.pastActivities = person.pastActivities?.filter(activity => activity.id !== pastActivityId);
  }

  removePhone(personId: number | undefined, phoneId: number): void {
    const person = this.getSingleEntity(personId);
    person.phones = person.phones?.filter(phone => phone.id !== phoneId);
  }
}
