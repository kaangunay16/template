package tr.com.khg.template.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import tr.com.khg.template.IntegrationTest;
import tr.com.khg.template.domain.Selection;
import tr.com.khg.template.repository.SelectionRepository;

/**
 * Integration tests for the {@link SelectionResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class SelectionResourceIT {

    private static final String DEFAULT_KEY = "AAAAAAAAAA";
    private static final String UPDATED_KEY = "BBBBBBBBBB";

    private static final String DEFAULT_VALUE = "AAAAAAAAAA";
    private static final String UPDATED_VALUE = "BBBBBBBBBB";

    private static final Integer DEFAULT_PRIORITY = 1;
    private static final Integer UPDATED_PRIORITY = 2;

    private static final String ENTITY_API_URL = "/api/selections";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private SelectionRepository selectionRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restSelectionMockMvc;

    private Selection selection;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Selection createEntity(EntityManager em) {
        Selection selection = new Selection().key(DEFAULT_KEY).value(DEFAULT_VALUE).priority(DEFAULT_PRIORITY);
        return selection;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Selection createUpdatedEntity(EntityManager em) {
        Selection selection = new Selection().key(UPDATED_KEY).value(UPDATED_VALUE).priority(UPDATED_PRIORITY);
        return selection;
    }

    @BeforeEach
    public void initTest() {
        selection = createEntity(em);
    }

    @Test
    @Transactional
    void createSelection() throws Exception {
        int databaseSizeBeforeCreate = selectionRepository.findAll().size();
        // Create the Selection
        restSelectionMockMvc
            .perform(
                post(ENTITY_API_URL)
                    .with(csrf())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(selection))
            )
            .andExpect(status().isCreated());

        // Validate the Selection in the database
        List<Selection> selectionList = selectionRepository.findAll();
        assertThat(selectionList).hasSize(databaseSizeBeforeCreate + 1);
        Selection testSelection = selectionList.get(selectionList.size() - 1);
        assertThat(testSelection.getKey()).isEqualTo(DEFAULT_KEY);
        assertThat(testSelection.getValue()).isEqualTo(DEFAULT_VALUE);
        assertThat(testSelection.getPriority()).isEqualTo(DEFAULT_PRIORITY);
    }

    @Test
    @Transactional
    void createSelectionWithExistingId() throws Exception {
        // Create the Selection with an existing ID
        selection.setId(1L);

        int databaseSizeBeforeCreate = selectionRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restSelectionMockMvc
            .perform(
                post(ENTITY_API_URL)
                    .with(csrf())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(selection))
            )
            .andExpect(status().isBadRequest());

        // Validate the Selection in the database
        List<Selection> selectionList = selectionRepository.findAll();
        assertThat(selectionList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void getAllSelections() throws Exception {
        // Initialize the database
        selectionRepository.saveAndFlush(selection);

        // Get all the selectionList
        restSelectionMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(selection.getId().intValue())))
            .andExpect(jsonPath("$.[*].key").value(hasItem(DEFAULT_KEY)))
            .andExpect(jsonPath("$.[*].value").value(hasItem(DEFAULT_VALUE)))
            .andExpect(jsonPath("$.[*].priority").value(hasItem(DEFAULT_PRIORITY)));
    }

    @Test
    @Transactional
    void getSelection() throws Exception {
        // Initialize the database
        selectionRepository.saveAndFlush(selection);

        // Get the selection
        restSelectionMockMvc
            .perform(get(ENTITY_API_URL_ID, selection.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(selection.getId().intValue()))
            .andExpect(jsonPath("$.key").value(DEFAULT_KEY))
            .andExpect(jsonPath("$.value").value(DEFAULT_VALUE))
            .andExpect(jsonPath("$.priority").value(DEFAULT_PRIORITY));
    }

    @Test
    @Transactional
    void getNonExistingSelection() throws Exception {
        // Get the selection
        restSelectionMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putNewSelection() throws Exception {
        // Initialize the database
        selectionRepository.saveAndFlush(selection);

        int databaseSizeBeforeUpdate = selectionRepository.findAll().size();

        // Update the selection
        Selection updatedSelection = selectionRepository.findById(selection.getId()).get();
        // Disconnect from session so that the updates on updatedSelection are not directly saved in db
        em.detach(updatedSelection);
        updatedSelection.key(UPDATED_KEY).value(UPDATED_VALUE).priority(UPDATED_PRIORITY);

        restSelectionMockMvc
            .perform(
                put(ENTITY_API_URL_ID, updatedSelection.getId())
                    .with(csrf())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(updatedSelection))
            )
            .andExpect(status().isOk());

        // Validate the Selection in the database
        List<Selection> selectionList = selectionRepository.findAll();
        assertThat(selectionList).hasSize(databaseSizeBeforeUpdate);
        Selection testSelection = selectionList.get(selectionList.size() - 1);
        assertThat(testSelection.getKey()).isEqualTo(UPDATED_KEY);
        assertThat(testSelection.getValue()).isEqualTo(UPDATED_VALUE);
        assertThat(testSelection.getPriority()).isEqualTo(UPDATED_PRIORITY);
    }

    @Test
    @Transactional
    void putNonExistingSelection() throws Exception {
        int databaseSizeBeforeUpdate = selectionRepository.findAll().size();
        selection.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restSelectionMockMvc
            .perform(
                put(ENTITY_API_URL_ID, selection.getId())
                    .with(csrf())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(selection))
            )
            .andExpect(status().isBadRequest());

        // Validate the Selection in the database
        List<Selection> selectionList = selectionRepository.findAll();
        assertThat(selectionList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchSelection() throws Exception {
        int databaseSizeBeforeUpdate = selectionRepository.findAll().size();
        selection.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restSelectionMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .with(csrf())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(selection))
            )
            .andExpect(status().isBadRequest());

        // Validate the Selection in the database
        List<Selection> selectionList = selectionRepository.findAll();
        assertThat(selectionList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamSelection() throws Exception {
        int databaseSizeBeforeUpdate = selectionRepository.findAll().size();
        selection.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restSelectionMockMvc
            .perform(
                put(ENTITY_API_URL)
                    .with(csrf())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(selection))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the Selection in the database
        List<Selection> selectionList = selectionRepository.findAll();
        assertThat(selectionList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateSelectionWithPatch() throws Exception {
        // Initialize the database
        selectionRepository.saveAndFlush(selection);

        int databaseSizeBeforeUpdate = selectionRepository.findAll().size();

        // Update the selection using partial update
        Selection partialUpdatedSelection = new Selection();
        partialUpdatedSelection.setId(selection.getId());

        partialUpdatedSelection.value(UPDATED_VALUE).priority(UPDATED_PRIORITY);

        restSelectionMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedSelection.getId())
                    .with(csrf())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedSelection))
            )
            .andExpect(status().isOk());

        // Validate the Selection in the database
        List<Selection> selectionList = selectionRepository.findAll();
        assertThat(selectionList).hasSize(databaseSizeBeforeUpdate);
        Selection testSelection = selectionList.get(selectionList.size() - 1);
        assertThat(testSelection.getKey()).isEqualTo(DEFAULT_KEY);
        assertThat(testSelection.getValue()).isEqualTo(UPDATED_VALUE);
        assertThat(testSelection.getPriority()).isEqualTo(UPDATED_PRIORITY);
    }

    @Test
    @Transactional
    void fullUpdateSelectionWithPatch() throws Exception {
        // Initialize the database
        selectionRepository.saveAndFlush(selection);

        int databaseSizeBeforeUpdate = selectionRepository.findAll().size();

        // Update the selection using partial update
        Selection partialUpdatedSelection = new Selection();
        partialUpdatedSelection.setId(selection.getId());

        partialUpdatedSelection.key(UPDATED_KEY).value(UPDATED_VALUE).priority(UPDATED_PRIORITY);

        restSelectionMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedSelection.getId())
                    .with(csrf())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedSelection))
            )
            .andExpect(status().isOk());

        // Validate the Selection in the database
        List<Selection> selectionList = selectionRepository.findAll();
        assertThat(selectionList).hasSize(databaseSizeBeforeUpdate);
        Selection testSelection = selectionList.get(selectionList.size() - 1);
        assertThat(testSelection.getKey()).isEqualTo(UPDATED_KEY);
        assertThat(testSelection.getValue()).isEqualTo(UPDATED_VALUE);
        assertThat(testSelection.getPriority()).isEqualTo(UPDATED_PRIORITY);
    }

    @Test
    @Transactional
    void patchNonExistingSelection() throws Exception {
        int databaseSizeBeforeUpdate = selectionRepository.findAll().size();
        selection.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restSelectionMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, selection.getId())
                    .with(csrf())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(selection))
            )
            .andExpect(status().isBadRequest());

        // Validate the Selection in the database
        List<Selection> selectionList = selectionRepository.findAll();
        assertThat(selectionList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchSelection() throws Exception {
        int databaseSizeBeforeUpdate = selectionRepository.findAll().size();
        selection.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restSelectionMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .with(csrf())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(selection))
            )
            .andExpect(status().isBadRequest());

        // Validate the Selection in the database
        List<Selection> selectionList = selectionRepository.findAll();
        assertThat(selectionList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamSelection() throws Exception {
        int databaseSizeBeforeUpdate = selectionRepository.findAll().size();
        selection.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restSelectionMockMvc
            .perform(
                patch(ENTITY_API_URL)
                    .with(csrf())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(selection))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the Selection in the database
        List<Selection> selectionList = selectionRepository.findAll();
        assertThat(selectionList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteSelection() throws Exception {
        // Initialize the database
        selectionRepository.saveAndFlush(selection);

        int databaseSizeBeforeDelete = selectionRepository.findAll().size();

        // Delete the selection
        restSelectionMockMvc
            .perform(delete(ENTITY_API_URL_ID, selection.getId()).with(csrf()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Selection> selectionList = selectionRepository.findAll();
        assertThat(selectionList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
