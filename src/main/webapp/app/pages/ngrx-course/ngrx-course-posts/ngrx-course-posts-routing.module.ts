import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { NgrxPostListComponent } from './ngrx-post-list/ngrx-post-list.component';
import { NgrxPostAddComponent } from './ngrx-post-add/ngrx-post-add.component';
import { NgrxPostEditComponent } from './ngrx-post-edit/ngrx-post-edit.component';

const routes: Routes = [
  {
    path: '',
    component: NgrxPostListComponent,
    children: [
      {
        path: 'add',
        component: NgrxPostAddComponent,
      },
      {
        path: 'edit/:id',
        component: NgrxPostEditComponent,
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class NgrxCoursePostsRoutingModule {}
