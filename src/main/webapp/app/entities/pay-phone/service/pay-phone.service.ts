import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { isPresent } from 'app/core/util/operators';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { IPayPhone, getPayPhoneIdentifier } from '../pay-phone.model';

export type EntityResponseType = HttpResponse<IPayPhone>;
export type EntityArrayResponseType = HttpResponse<IPayPhone[]>;

@Injectable({ providedIn: 'root' })
export class PayPhoneService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/pay-phones');

  constructor(protected http: HttpClient, protected applicationConfigService: ApplicationConfigService) {}

  create(payPhone: IPayPhone): Observable<EntityResponseType> {
    return this.http.post<IPayPhone>(this.resourceUrl, payPhone, { observe: 'response' });
  }

  update(payPhone: IPayPhone): Observable<EntityResponseType> {
    return this.http.put<IPayPhone>(`${this.resourceUrl}/${getPayPhoneIdentifier(payPhone) as number}`, payPhone, { observe: 'response' });
  }

  partialUpdate(payPhone: IPayPhone): Observable<EntityResponseType> {
    return this.http.patch<IPayPhone>(`${this.resourceUrl}/${getPayPhoneIdentifier(payPhone) as number}`, payPhone, {
      observe: 'response',
    });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IPayPhone>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IPayPhone[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  addPayPhoneToCollectionIfMissing(payPhoneCollection: IPayPhone[], ...payPhonesToCheck: (IPayPhone | null | undefined)[]): IPayPhone[] {
    const payPhones: IPayPhone[] = payPhonesToCheck.filter(isPresent);
    if (payPhones.length > 0) {
      const payPhoneCollectionIdentifiers = payPhoneCollection.map(payPhoneItem => getPayPhoneIdentifier(payPhoneItem)!);
      const payPhonesToAdd = payPhones.filter(payPhoneItem => {
        const payPhoneIdentifier = getPayPhoneIdentifier(payPhoneItem);
        if (payPhoneIdentifier == null || payPhoneCollectionIdentifiers.includes(payPhoneIdentifier)) {
          return false;
        }
        payPhoneCollectionIdentifiers.push(payPhoneIdentifier);
        return true;
      });
      return [...payPhonesToAdd, ...payPhoneCollection];
    }
    return payPhoneCollection;
  }
}
