import { ISelection } from 'app/entities/selection/selection.model';

export interface SideEffectState {
  selections: ISelection[] | null;
}

export const initialState: SideEffectState = {
  selections: null,
};
