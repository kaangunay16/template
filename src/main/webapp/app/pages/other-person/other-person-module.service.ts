import { Injectable } from '@angular/core';
import { Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

import { DynamicFormCreatorModel, IDynamicFormCreatorModel } from 'app/components/dynamic-form-creator/dynamic-form-field.model';
import { FormValueConverterService } from 'app/components/dynamic-form-creator/form-value-converter.service';
import { IOtherPerson, OtherPerson } from 'app/entities/other-person/other-person.model';
import { IOtherPersonRecord, OtherPersonRecord } from 'app/entities/other-person-record/other-person-record.model';
import { OtherPersonService } from 'app/entities/other-person/service/other-person.service';
import { OtherPersonRecordService } from 'app/entities/other-person-record/service/other-person-record.service';
import { ErrorSupportService } from 'app/shared/services/error-support.service';
import { PageService } from 'app/shared/services/page-service';
import { MeetingSubject3Meeting } from 'app/shared/model/meeting-subject-3-meeting';

@Injectable({
  providedIn: 'root',
})
export class OtherPersonModuleService extends PageService<IOtherPerson> {
  private _subject3?: MeetingSubject3Meeting;

  constructor(
    toastr: ToastrService,
    router: Router,
    private otherPersonService: OtherPersonService,
    private otherPersonRecordService: OtherPersonRecordService,
    private errorService: ErrorSupportService,
    private formValueService: FormValueConverterService
  ) {
    super(toastr, router);
  }

  getAllRecordsFromFormCreators(formCreators: IDynamicFormCreatorModel[]): IOtherPersonRecord[] {
    const records: IOtherPersonRecord[] = [];
    formCreators.forEach(formCreator => records.push(this.getRecordFromFormCreator(formCreator)));

    return records;
  }

  getRecordFromFormCreator(formCreator: IDynamicFormCreatorModel): IOtherPersonRecord {
    const record = new OtherPersonRecord();
    record.document = this.getFieldValue(formCreator, 'document');
    record.city = this.getFieldValue(formCreator, 'city');
    record.rank = this.getFieldValue(formCreator, 'rank');
    record.section = this.getFieldValue(formCreator, 'section');
    record.meetingStatus = this.getFieldValue(formCreator, 'meeting');
    record.status = this.getFieldValue(formCreator, 'status');
    record.profession = this.getFieldValue(formCreator, 'profession');
    record.addedByUnit = this.getFieldValue(formCreator, 'unit');
    record.type = this.getFieldValue(formCreator, 'type');
    record.speaker = this.formValueService.getFormValue(formCreator, 'speaker') as boolean | null;

    return record;
  }

  getFieldValue(formCreator: IDynamicFormCreatorModel, fieldId: string): string {
    return this.formValueService.getFormValue(formCreator, fieldId) as string;
  }

  createRecordsForms(otherPerson: IOtherPerson): IDynamicFormCreatorModel[] {
    if (!otherPerson.otherPersonRecords) {
      return [];
    }

    const isSubject3 = !!this.subject3;

    const forms: IDynamicFormCreatorModel[] = [];
    otherPerson.otherPersonRecords.forEach(record => {
      forms.push(
        new DynamicFormCreatorModel([
          {
            id: 'document',
            label: 'Evrak Numarası',
            type: record.id ? 'text' : 'document',
            value: record.document,
            readOnly: !!record.id,
            validators: [Validators.required],
          },
          {
            id: 'type',
            type: 'selections',
            label: 'Tip',
            selectionType: 'other-person-type',
            placeholder: 'Tip Seç',
            value: record.type,
          },
          {
            id: 'city',
            type: 'selections',
            label: 'İl',
            selectionType: 'city',
            placeholder: 'İl Seç',
            value: record.city,
            readOnly: !!record.id || isSubject3,
          },
          {
            id: 'rank',
            type: 'selections',
            label: 'Rütbe',
            selectionType: 'rank',
            placeholder: 'Rütbe Seç',
            value: record.rank,
          },
          {
            id: 'section',
            type: 'selections',
            label: 'Kısım',
            selectionType: 'section',
            placeholder: 'Kısım Seç',
            value: record.section,
          },
          {
            id: 'meeting',
            type: 'selections',
            label: 'Toplantı',
            selectionType: 'meeting-status',
            placeholder: 'Toplantı Durumu Seç',
            readOnly: isSubject3,
            value: record.meetingStatus,
          },
          {
            id: 'status',
            type: 'selections',
            label: 'Durum',
            selectionType: 'status',
            placeholder: 'Durum Seç',
            value: record.status,
          },
          {
            id: 'profession',
            label: 'Meslek',
            type: 'text',
            value: record.profession,
          },
          {
            id: 'speaker',
            type: 'booleanField',
            label: 'Konuşmacı Durumu',
            placeholder: 'Konuşmacı Durumu Seç',
            value: record.speaker,
            fieldsValues: {
              trueField: 'Konuşmacı',
              falseField: 'Konuşmacı Değil',
            },
          },
          {
            id: 'unit',
            type: 'selections',
            label: 'Birim',
            selectionType: 'unit',
            placeholder: 'Birim Seç',
            value: record.addedByUnit,
          },
        ])
      );
    });

    return forms;
  }

  createOtherPerson(otherPerson: IOtherPerson): void {
    this.otherPersonService.create(otherPerson).subscribe(
      res => {
        if (res.body) {
          this.toastr.success('Kayıt Başarıyla Oluşturuldu');
          this.router.navigate(['/other-person']);
        }
      },
      err => this.errorService.errorHandler(err)
    );
  }

  updateAllRecords(otherPerson: IOtherPerson, formCreators: IDynamicFormCreatorModel[]): void {
    const records: IOtherPersonRecord[] = [];
    this.getAllRecordsFromFormCreators(formCreators).forEach((record, index) => {
      records.push({ ...record, id: otherPerson.otherPersonRecords![index].id, otherPerson });
    });
    this.otherPersonRecordService.updateAll(records).subscribe(
      res => {
        if (res.body) {
          this.toastr.success('Kayıt Başarıyla Güncellendi');
          otherPerson.otherPersonRecords = res.body;
          window.history.back();
        }
      },
      err => this.errorService.errorHandler(err)
    );
  }

  set subject3(data: MeetingSubject3Meeting | undefined) {
    this._subject3 = data;
  }

  get subject3(): MeetingSubject3Meeting | undefined {
    return this._subject3;
  }

  setSubject3AndData(data: MeetingSubject3Meeting): void {
    this.subject3 = data;
    this.entities = this.convertSubject3ToOtherPerson(this.subject3);
  }

  clearSubject3(): void {
    this._subject3 = undefined;
  }

  convertSubject3ToOtherPerson(subject3: MeetingSubject3Meeting): IOtherPerson[] {
    const record = new OtherPersonRecord();
    record.city = subject3.meeting.city;
    record.meetingStatus = 'MeetingStatus1';
    const otherPersonList: IOtherPerson[] = [];

    subject3.notExistsAttendees.forEach((identityNo, fakeId) => {
      const otherPerson = new OtherPerson();
      otherPerson.id = fakeId;
      otherPerson.identity = subject3.meeting.meetingAttendees?.find(attendee => attendee.identity?.identityNo === identityNo)?.identity;
      otherPerson.otherPersonRecords = [{ ...record }];
      otherPersonList.push(otherPerson);
    });

    return otherPersonList;
  }
}
