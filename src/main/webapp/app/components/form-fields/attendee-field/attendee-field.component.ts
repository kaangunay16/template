import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { ControlValueAccessor, FormBuilder, FormGroup, NG_VALUE_ACCESSOR, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';

import { DynamicFormFunctions } from 'app/components/dynamic-form-creator/dynamic-form-field.model';
import { DYNAMIC_FIELD } from 'app/components/dynamic-form-creator/dyanmic-form.token';
import { AttendeeField } from 'app/components/models/attendee-field.model';
import { IdentityService } from 'app/entities/identity/service/identity.service';
import { MeetingAttendeeService } from 'app/entities/meeting-attendee/service/meeting-attendee.service';
import { identityNumberRegex } from 'app/shared/patterns/patterns';
import { ErrorSupportService } from 'app/shared/services/error-support.service';
import { FormFieldsService } from '../form-fields.service';
import { MeetingModuleService } from 'app/pages/meeting/meeting-module.service';

@Component({
  selector: 'jhi-attendee-field',
  templateUrl: './attendee-field.component.html',
  styleUrls: ['./attendee-field.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: AttendeeFieldComponent,
      multi: true,
    },
    {
      provide: DYNAMIC_FIELD,
      useExisting: AttendeeFieldComponent,
    },
  ],
})
export class AttendeeFieldComponent implements OnInit, OnDestroy, ControlValueAccessor, DynamicFormFunctions {
  form!: FormGroup;
  identitySub?: Subscription;

  @Input()
  readOnly?: boolean = false;

  onTouched!: () => void;

  constructor(
    private formBuilder: FormBuilder,
    private identityService: IdentityService,
    private formFieldsService: FormFieldsService,
    private errorService: ErrorSupportService,
    private service: MeetingAttendeeService,
    private meetingModuleService: MeetingModuleService
  ) {}

  ngOnInit(): void {
    this.form = this.formBuilder.group({
      id: [{ value: null }],
      identityNumber: [{ value: null, disabled: this.readOnly }, [Validators.required, Validators.pattern(identityNumberRegex)]],
      nameAndSurname: [{ value: null, disabled: true }],
      status: [{ value: null, disabled: this.readOnly }, Validators.required],
      identity: [{ value: null }],
    });
  }

  ngOnDestroy(): void {
    this.identitySub?.unsubscribe();
  }

  registerOnChange(fn: (value: any) => void): void {
    this.form.valueChanges.subscribe(fn);
  }

  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

  writeValue(obj: AttendeeField | null): void {
    this.form.patchValue({
      id: obj ? obj.id : null,
      identityNumber: obj ? obj.identityNumber : null,
      nameAndSurname: obj ? obj.nameAndSurname : null,
      status: obj ? obj.status : null,
      identity: obj ? obj.identity : null,
    });

    const idNum = this.form.get('identityNumber')!;

    if (!obj) {
      idNum.enable();
    }

    if (obj?.id) {
      idNum.disable();
    }
  }

  inputChanged(): void {
    const identityNumber = this.form.get('identityNumber')!.value as string;
    if (identityNumber.length === 11) {
      this.identitySub = this.identityService.findByIdentityNumber(identityNumber).subscribe(
        res => {
          if (res.body) {
            const identity = res.body;
            this.form.patchValue({ identity, nameAndSurname: `${identity.name!} ${identity.surname!}` });
          }
        },
        err => this.errorService.errorHandler(err)
      );
    }
  }

  remove(): void {
    const id = this.form.get('id')?.value;

    if (id) {
      const value = this.form.value as AttendeeField;
      this.meetingModuleService.addAttendeeFieldToRemoveAttendees(value);
    }
  }
}
