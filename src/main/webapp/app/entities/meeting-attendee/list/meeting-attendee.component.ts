import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IMeetingAttendee } from '../meeting-attendee.model';
import { MeetingAttendeeService } from '../service/meeting-attendee.service';
import { MeetingAttendeeDeleteDialogComponent } from '../delete/meeting-attendee-delete-dialog.component';

@Component({
  selector: 'jhi-meeting-attendee',
  templateUrl: './meeting-attendee.component.html',
})
export class MeetingAttendeeComponent implements OnInit {
  meetingAttendees?: IMeetingAttendee[];
  isLoading = false;

  constructor(protected meetingAttendeeService: MeetingAttendeeService, protected modalService: NgbModal) {}

  loadAll(): void {
    this.isLoading = true;

    this.meetingAttendeeService.query().subscribe(
      (res: HttpResponse<IMeetingAttendee[]>) => {
        this.isLoading = false;
        this.meetingAttendees = res.body ?? [];
      },
      () => {
        this.isLoading = false;
      }
    );
  }

  ngOnInit(): void {
    this.loadAll();
  }

  trackId(index: number, item: IMeetingAttendee): number {
    return item.id!;
  }

  delete(meetingAttendee: IMeetingAttendee): void {
    const modalRef = this.modalService.open(MeetingAttendeeDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.meetingAttendee = meetingAttendee;
    // unsubscribe not needed because closed completes on modal close
    modalRef.closed.subscribe(reason => {
      if (reason === 'deleted') {
        this.loadAll();
      }
    });
  }
}
