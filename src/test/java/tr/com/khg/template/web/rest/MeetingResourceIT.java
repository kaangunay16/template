package tr.com.khg.template.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import tr.com.khg.template.IntegrationTest;
import tr.com.khg.template.domain.Meeting;
import tr.com.khg.template.repository.MeetingRepository;

/**
 * Integration tests for the {@link MeetingResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class MeetingResourceIT {

    private static final String DEFAULT_CITY = "AAAAAAAAAA";
    private static final String UPDATED_CITY = "BBBBBBBBBB";

    private static final Integer DEFAULT_TOTAL_PLACES = 1;
    private static final Integer UPDATED_TOTAL_PLACES = 2;

    private static final LocalDate DEFAULT_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final String DEFAULT_SUBJECT = "AAAAAAAAAA";
    private static final String UPDATED_SUBJECT = "BBBBBBBBBB";

    private static final String DEFAULT_UNIT = "AAAAAAAAAA";
    private static final String UPDATED_UNIT = "BBBBBBBBBB";

    private static final Integer DEFAULT_TOTAL_ATTENDEE = 1;
    private static final Integer UPDATED_TOTAL_ATTENDEE = 2;

    private static final Integer DEFAULT_ATTENDEE = 1;
    private static final Integer UPDATED_ATTENDEE = 2;

    private static final String DEFAULT_TYPE = "AAAAAAAAAA";
    private static final String UPDATED_TYPE = "BBBBBBBBBB";

    private static final Boolean DEFAULT_FINALIZED = false;
    private static final Boolean UPDATED_FINALIZED = true;

    private static final String ENTITY_API_URL = "/api/meetings";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private MeetingRepository meetingRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restMeetingMockMvc;

    private Meeting meeting;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Meeting createEntity(EntityManager em) {
        Meeting meeting = new Meeting()
            .city(DEFAULT_CITY)
            .totalPlaces(DEFAULT_TOTAL_PLACES)
            .date(DEFAULT_DATE)
            .subject(DEFAULT_SUBJECT)
            .unit(DEFAULT_UNIT)
            .totalAttendee(DEFAULT_TOTAL_ATTENDEE)
            .attendee(DEFAULT_ATTENDEE)
            .type(DEFAULT_TYPE)
            .finalized(DEFAULT_FINALIZED);
        return meeting;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Meeting createUpdatedEntity(EntityManager em) {
        Meeting meeting = new Meeting()
            .city(UPDATED_CITY)
            .totalPlaces(UPDATED_TOTAL_PLACES)
            .date(UPDATED_DATE)
            .subject(UPDATED_SUBJECT)
            .unit(UPDATED_UNIT)
            .totalAttendee(UPDATED_TOTAL_ATTENDEE)
            .attendee(UPDATED_ATTENDEE)
            .type(UPDATED_TYPE)
            .finalized(UPDATED_FINALIZED);
        return meeting;
    }

    @BeforeEach
    public void initTest() {
        meeting = createEntity(em);
    }

    @Test
    @Transactional
    void createMeeting() throws Exception {
        int databaseSizeBeforeCreate = meetingRepository.findAll().size();
        // Create the Meeting
        restMeetingMockMvc
            .perform(
                post(ENTITY_API_URL)
                    .with(csrf())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(meeting))
            )
            .andExpect(status().isCreated());

        // Validate the Meeting in the database
        List<Meeting> meetingList = meetingRepository.findAll();
        assertThat(meetingList).hasSize(databaseSizeBeforeCreate + 1);
        Meeting testMeeting = meetingList.get(meetingList.size() - 1);
        assertThat(testMeeting.getCity()).isEqualTo(DEFAULT_CITY);
        assertThat(testMeeting.getTotalPlaces()).isEqualTo(DEFAULT_TOTAL_PLACES);
        assertThat(testMeeting.getDate()).isEqualTo(DEFAULT_DATE);
        assertThat(testMeeting.getSubject()).isEqualTo(DEFAULT_SUBJECT);
        assertThat(testMeeting.getUnit()).isEqualTo(DEFAULT_UNIT);
        assertThat(testMeeting.getTotalAttendee()).isEqualTo(DEFAULT_TOTAL_ATTENDEE);
        assertThat(testMeeting.getAttendee()).isEqualTo(DEFAULT_ATTENDEE);
        assertThat(testMeeting.getType()).isEqualTo(DEFAULT_TYPE);
        assertThat(testMeeting.getFinalized()).isEqualTo(DEFAULT_FINALIZED);
    }

    @Test
    @Transactional
    void createMeetingWithExistingId() throws Exception {
        // Create the Meeting with an existing ID
        meeting.setId(1L);

        int databaseSizeBeforeCreate = meetingRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restMeetingMockMvc
            .perform(
                post(ENTITY_API_URL)
                    .with(csrf())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(meeting))
            )
            .andExpect(status().isBadRequest());

        // Validate the Meeting in the database
        List<Meeting> meetingList = meetingRepository.findAll();
        assertThat(meetingList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void getAllMeetings() throws Exception {
        // Initialize the database
        meetingRepository.saveAndFlush(meeting);

        // Get all the meetingList
        restMeetingMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(meeting.getId().intValue())))
            .andExpect(jsonPath("$.[*].city").value(hasItem(DEFAULT_CITY)))
            .andExpect(jsonPath("$.[*].totalPlaces").value(hasItem(DEFAULT_TOTAL_PLACES)))
            .andExpect(jsonPath("$.[*].date").value(hasItem(DEFAULT_DATE.toString())))
            .andExpect(jsonPath("$.[*].subject").value(hasItem(DEFAULT_SUBJECT)))
            .andExpect(jsonPath("$.[*].unit").value(hasItem(DEFAULT_UNIT)))
            .andExpect(jsonPath("$.[*].totalAttendee").value(hasItem(DEFAULT_TOTAL_ATTENDEE)))
            .andExpect(jsonPath("$.[*].attendee").value(hasItem(DEFAULT_ATTENDEE)))
            .andExpect(jsonPath("$.[*].type").value(hasItem(DEFAULT_TYPE)))
            .andExpect(jsonPath("$.[*].finalized").value(hasItem(DEFAULT_FINALIZED.booleanValue())));
    }

    @Test
    @Transactional
    void getMeeting() throws Exception {
        // Initialize the database
        meetingRepository.saveAndFlush(meeting);

        // Get the meeting
        restMeetingMockMvc
            .perform(get(ENTITY_API_URL_ID, meeting.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(meeting.getId().intValue()))
            .andExpect(jsonPath("$.city").value(DEFAULT_CITY))
            .andExpect(jsonPath("$.totalPlaces").value(DEFAULT_TOTAL_PLACES))
            .andExpect(jsonPath("$.date").value(DEFAULT_DATE.toString()))
            .andExpect(jsonPath("$.subject").value(DEFAULT_SUBJECT))
            .andExpect(jsonPath("$.unit").value(DEFAULT_UNIT))
            .andExpect(jsonPath("$.totalAttendee").value(DEFAULT_TOTAL_ATTENDEE))
            .andExpect(jsonPath("$.attendee").value(DEFAULT_ATTENDEE))
            .andExpect(jsonPath("$.type").value(DEFAULT_TYPE))
            .andExpect(jsonPath("$.finalized").value(DEFAULT_FINALIZED.booleanValue()));
    }

    @Test
    @Transactional
    void getNonExistingMeeting() throws Exception {
        // Get the meeting
        restMeetingMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putNewMeeting() throws Exception {
        // Initialize the database
        meetingRepository.saveAndFlush(meeting);

        int databaseSizeBeforeUpdate = meetingRepository.findAll().size();

        // Update the meeting
        Meeting updatedMeeting = meetingRepository.findById(meeting.getId()).get();
        // Disconnect from session so that the updates on updatedMeeting are not directly saved in db
        em.detach(updatedMeeting);
        updatedMeeting
            .city(UPDATED_CITY)
            .totalPlaces(UPDATED_TOTAL_PLACES)
            .date(UPDATED_DATE)
            .subject(UPDATED_SUBJECT)
            .unit(UPDATED_UNIT)
            .totalAttendee(UPDATED_TOTAL_ATTENDEE)
            .attendee(UPDATED_ATTENDEE)
            .type(UPDATED_TYPE)
            .finalized(UPDATED_FINALIZED);

        restMeetingMockMvc
            .perform(
                put(ENTITY_API_URL_ID, updatedMeeting.getId())
                    .with(csrf())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(updatedMeeting))
            )
            .andExpect(status().isOk());

        // Validate the Meeting in the database
        List<Meeting> meetingList = meetingRepository.findAll();
        assertThat(meetingList).hasSize(databaseSizeBeforeUpdate);
        Meeting testMeeting = meetingList.get(meetingList.size() - 1);
        assertThat(testMeeting.getCity()).isEqualTo(UPDATED_CITY);
        assertThat(testMeeting.getTotalPlaces()).isEqualTo(UPDATED_TOTAL_PLACES);
        assertThat(testMeeting.getDate()).isEqualTo(UPDATED_DATE);
        assertThat(testMeeting.getSubject()).isEqualTo(UPDATED_SUBJECT);
        assertThat(testMeeting.getUnit()).isEqualTo(UPDATED_UNIT);
        assertThat(testMeeting.getTotalAttendee()).isEqualTo(UPDATED_TOTAL_ATTENDEE);
        assertThat(testMeeting.getAttendee()).isEqualTo(UPDATED_ATTENDEE);
        assertThat(testMeeting.getType()).isEqualTo(UPDATED_TYPE);
        assertThat(testMeeting.getFinalized()).isEqualTo(UPDATED_FINALIZED);
    }

    @Test
    @Transactional
    void putNonExistingMeeting() throws Exception {
        int databaseSizeBeforeUpdate = meetingRepository.findAll().size();
        meeting.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restMeetingMockMvc
            .perform(
                put(ENTITY_API_URL_ID, meeting.getId())
                    .with(csrf())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(meeting))
            )
            .andExpect(status().isBadRequest());

        // Validate the Meeting in the database
        List<Meeting> meetingList = meetingRepository.findAll();
        assertThat(meetingList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchMeeting() throws Exception {
        int databaseSizeBeforeUpdate = meetingRepository.findAll().size();
        meeting.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restMeetingMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .with(csrf())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(meeting))
            )
            .andExpect(status().isBadRequest());

        // Validate the Meeting in the database
        List<Meeting> meetingList = meetingRepository.findAll();
        assertThat(meetingList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamMeeting() throws Exception {
        int databaseSizeBeforeUpdate = meetingRepository.findAll().size();
        meeting.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restMeetingMockMvc
            .perform(
                put(ENTITY_API_URL).with(csrf()).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(meeting))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the Meeting in the database
        List<Meeting> meetingList = meetingRepository.findAll();
        assertThat(meetingList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateMeetingWithPatch() throws Exception {
        // Initialize the database
        meetingRepository.saveAndFlush(meeting);

        int databaseSizeBeforeUpdate = meetingRepository.findAll().size();

        // Update the meeting using partial update
        Meeting partialUpdatedMeeting = new Meeting();
        partialUpdatedMeeting.setId(meeting.getId());

        partialUpdatedMeeting.attendee(UPDATED_ATTENDEE).type(UPDATED_TYPE);

        restMeetingMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedMeeting.getId())
                    .with(csrf())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedMeeting))
            )
            .andExpect(status().isOk());

        // Validate the Meeting in the database
        List<Meeting> meetingList = meetingRepository.findAll();
        assertThat(meetingList).hasSize(databaseSizeBeforeUpdate);
        Meeting testMeeting = meetingList.get(meetingList.size() - 1);
        assertThat(testMeeting.getCity()).isEqualTo(DEFAULT_CITY);
        assertThat(testMeeting.getTotalPlaces()).isEqualTo(DEFAULT_TOTAL_PLACES);
        assertThat(testMeeting.getDate()).isEqualTo(DEFAULT_DATE);
        assertThat(testMeeting.getSubject()).isEqualTo(DEFAULT_SUBJECT);
        assertThat(testMeeting.getUnit()).isEqualTo(DEFAULT_UNIT);
        assertThat(testMeeting.getTotalAttendee()).isEqualTo(DEFAULT_TOTAL_ATTENDEE);
        assertThat(testMeeting.getAttendee()).isEqualTo(UPDATED_ATTENDEE);
        assertThat(testMeeting.getType()).isEqualTo(UPDATED_TYPE);
        assertThat(testMeeting.getFinalized()).isEqualTo(DEFAULT_FINALIZED);
    }

    @Test
    @Transactional
    void fullUpdateMeetingWithPatch() throws Exception {
        // Initialize the database
        meetingRepository.saveAndFlush(meeting);

        int databaseSizeBeforeUpdate = meetingRepository.findAll().size();

        // Update the meeting using partial update
        Meeting partialUpdatedMeeting = new Meeting();
        partialUpdatedMeeting.setId(meeting.getId());

        partialUpdatedMeeting
            .city(UPDATED_CITY)
            .totalPlaces(UPDATED_TOTAL_PLACES)
            .date(UPDATED_DATE)
            .subject(UPDATED_SUBJECT)
            .unit(UPDATED_UNIT)
            .totalAttendee(UPDATED_TOTAL_ATTENDEE)
            .attendee(UPDATED_ATTENDEE)
            .type(UPDATED_TYPE)
            .finalized(UPDATED_FINALIZED);

        restMeetingMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedMeeting.getId())
                    .with(csrf())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedMeeting))
            )
            .andExpect(status().isOk());

        // Validate the Meeting in the database
        List<Meeting> meetingList = meetingRepository.findAll();
        assertThat(meetingList).hasSize(databaseSizeBeforeUpdate);
        Meeting testMeeting = meetingList.get(meetingList.size() - 1);
        assertThat(testMeeting.getCity()).isEqualTo(UPDATED_CITY);
        assertThat(testMeeting.getTotalPlaces()).isEqualTo(UPDATED_TOTAL_PLACES);
        assertThat(testMeeting.getDate()).isEqualTo(UPDATED_DATE);
        assertThat(testMeeting.getSubject()).isEqualTo(UPDATED_SUBJECT);
        assertThat(testMeeting.getUnit()).isEqualTo(UPDATED_UNIT);
        assertThat(testMeeting.getTotalAttendee()).isEqualTo(UPDATED_TOTAL_ATTENDEE);
        assertThat(testMeeting.getAttendee()).isEqualTo(UPDATED_ATTENDEE);
        assertThat(testMeeting.getType()).isEqualTo(UPDATED_TYPE);
        assertThat(testMeeting.getFinalized()).isEqualTo(UPDATED_FINALIZED);
    }

    @Test
    @Transactional
    void patchNonExistingMeeting() throws Exception {
        int databaseSizeBeforeUpdate = meetingRepository.findAll().size();
        meeting.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restMeetingMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, meeting.getId())
                    .with(csrf())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(meeting))
            )
            .andExpect(status().isBadRequest());

        // Validate the Meeting in the database
        List<Meeting> meetingList = meetingRepository.findAll();
        assertThat(meetingList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchMeeting() throws Exception {
        int databaseSizeBeforeUpdate = meetingRepository.findAll().size();
        meeting.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restMeetingMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .with(csrf())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(meeting))
            )
            .andExpect(status().isBadRequest());

        // Validate the Meeting in the database
        List<Meeting> meetingList = meetingRepository.findAll();
        assertThat(meetingList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamMeeting() throws Exception {
        int databaseSizeBeforeUpdate = meetingRepository.findAll().size();
        meeting.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restMeetingMockMvc
            .perform(
                patch(ENTITY_API_URL)
                    .with(csrf())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(meeting))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the Meeting in the database
        List<Meeting> meetingList = meetingRepository.findAll();
        assertThat(meetingList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteMeeting() throws Exception {
        // Initialize the database
        meetingRepository.saveAndFlush(meeting);

        int databaseSizeBeforeDelete = meetingRepository.findAll().size();

        // Delete the meeting
        restMeetingMockMvc
            .perform(delete(ENTITY_API_URL_ID, meeting.getId()).with(csrf()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Meeting> meetingList = meetingRepository.findAll();
        assertThat(meetingList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
