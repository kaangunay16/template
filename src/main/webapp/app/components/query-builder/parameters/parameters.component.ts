import { Component, Input } from '@angular/core';

import { Parameter } from '../../models/parameter.model';
import { QueryBuilderService } from '../query-builder.service';

@Component({
  selector: 'jhi-parameters',
  templateUrl: './parameters.component.html',
  styleUrls: ['./parameters.component.scss'],
})
export class ParametersComponent {
  @Input()
  param!: Parameter;

  constructor(private queryBuilderService: QueryBuilderService) {}

  removeParam(): void {
    this.queryBuilderService.removeParameter(this.param);
  }
}
