package tr.com.khg.template.service.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.NOT_ACCEPTABLE, reason = "Diğer Şahıs Bulunamadı!")
public class OtherPersonNotExistsRecordId extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public OtherPersonNotExistsRecordId() {
        super("Belirtilen il kaydına sahip diğer şahıs bulunamadı!");
    }
}
