import { Component } from '@angular/core';

@Component({
  selector: 'jhi-ngrx-course-data-main',
  templateUrl: './ngrx-course-data-main.component.html',
  styleUrls: ['./ngrx-course-data-main.component.scss'],
})
export class NgrxCourseDataMainComponent {}
