import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

import { Parameter } from '../models/parameter.model';
import { HasId } from '../../entities/form-entity.model';

@Injectable({
  providedIn: 'root',
})
export class QueryBuilderService {
  parameters: Parameter[] = [];
  parametersSub = new BehaviorSubject<Parameter[]>(this.parameters);

  result: HasId[] | null = null;
  resultSub = new BehaviorSubject<HasId[] | null>(this.result);

  addNewParameter(parameter: Parameter): void {
    this.parameters.push(parameter);
    this.parametersSub.next(this.parameters);
  }

  clearParameters(): void {
    this.parameters = [];
    this.parametersSub.next(this.parameters);
  }

  removeParameter(parameter: Parameter): void {
    this.parameters.splice(this.parameters.indexOf(parameter), 1);
    this.parametersSub.next(this.parameters);
  }

  setResult(result: HasId[]): void {
    this.result = result;
    this.resultSub.next(this.result);
  }

  clearResult(): void {
    this.result = null;
    this.resultSub.next(this.result);
  }
}
