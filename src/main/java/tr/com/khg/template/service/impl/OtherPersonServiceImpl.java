package tr.com.khg.template.service.impl;

import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.persistence.EntityManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tr.com.khg.template.domain.Identity;
import tr.com.khg.template.domain.OtherPerson;
import tr.com.khg.template.domain.OtherPersonRecord;
import tr.com.khg.template.domain.request.Query;
import tr.com.khg.template.repository.IdentityRepository;
import tr.com.khg.template.repository.OtherPersonRecordRepository;
import tr.com.khg.template.repository.OtherPersonRepository;
import tr.com.khg.template.service.OtherPersonService;
import tr.com.khg.template.service.exceptions.AlreadyExistSameOtherPersonRecord;
import tr.com.khg.template.service.exceptions.MultipleRecordsAddition;
import tr.com.khg.template.service.exceptions.OtherPersonNotExistsRecordId;
import tr.com.khg.template.service.utils.DynamicQueryBuilder;

/**
 * Service Implementation for managing {@link OtherPerson}.
 */
@Service
@Transactional
public class OtherPersonServiceImpl implements OtherPersonService {

    private final Logger log = LoggerFactory.getLogger(OtherPersonServiceImpl.class);

    private final OtherPersonRepository otherPersonRepository;

    private final OtherPersonRecordRepository otherPersonRecordRepository;

    private final IdentityRepository identityRepository;

    private final EntityManager entityManager;

    public OtherPersonServiceImpl(
        OtherPersonRepository otherPersonRepository,
        OtherPersonRecordRepository otherPersonRecordRepository,
        IdentityRepository identityRepository,
        EntityManager entityManager
    ) {
        this.otherPersonRepository = otherPersonRepository;
        this.otherPersonRecordRepository = otherPersonRecordRepository;
        this.identityRepository = identityRepository;
        this.entityManager = entityManager;
    }

    @Override
    @Transactional
    public OtherPerson save(OtherPerson otherPerson) {
        log.debug("Request to save OtherPerson : {}", otherPerson);

        if (otherPerson.getId() == null) {
            if (otherPerson.getOtherPersonRecords().size() > 1) {
                throw new MultipleRecordsAddition();
            }

            Optional<OtherPerson> otPerson = otherPersonRepository.findByIdentityNoFetchAllRelations(
                otherPerson.getIdentity().getIdentityNo()
            );
            otPerson.ifPresent(o ->
                o
                    .getOtherPersonRecords()
                    .forEach(record -> {
                        if (record.getCity().equals(otherPerson.getOtherPersonRecords().iterator().next().getCity())) {
                            throw new AlreadyExistSameOtherPersonRecord();
                        }

                        otherPerson.setId(o.getId());
                        otherPerson.setIdentity(o.getIdentity());
                    })
            );
        }

        if (otherPerson.getIdentity().getId() == null) {
            Optional<Identity> identity = identityRepository.findByIdentityNo(otherPerson.getIdentity().getIdentityNo());
            identity.ifPresent(otherPerson::setIdentity);
        }

        if (otherPerson.getOtherPersonRecords() != null) otherPerson
            .getOtherPersonRecords()
            .forEach(record -> record.setOtherPerson(otherPerson));
        return otherPersonRepository.save(otherPerson);
    }

    @Override
    public Optional<OtherPerson> partialUpdate(OtherPerson otherPerson) {
        log.debug("Request to partially update OtherPerson : {}", otherPerson);

        return otherPersonRepository
            .findById(otherPerson.getId())
            .map(existingOtherPerson -> {
                return existingOtherPerson;
            })
            .map(otherPersonRepository::save);
    }

    @Override
    @Transactional(readOnly = true)
    public List<OtherPerson> findAll() {
        log.debug("Request to get all OtherPeople");
        return otherPersonRepository.findAll();
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<OtherPerson> findOne(Long id) {
        log.debug("Request to get OtherPerson : {}", id);
        return otherPersonRepository.findById(id);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete OtherPerson : {}", id);
        otherPersonRepository.deleteById(id);
    }

    @Override
    public Optional<OtherPerson> findByIdentityNumber(String identityNo) {
        return otherPersonRepository.findByIdentityNoFetchAllRelations(identityNo);
    }

    @Override
    public List<OtherPerson> filterPerson(Query query) {
        log.debug("Request to filter person");
        DynamicQueryBuilder<OtherPerson> q = new DynamicQueryBuilder<>(entityManager, OtherPerson.class);
        return q.createQuery(query);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<OtherPerson> findByRecordId(Long recordId) {
        log.debug("Request to find other person with record id : {}", recordId);
        Optional<OtherPerson> otherPerson = otherPersonRepository.findByOtherPersonRecordsId(recordId);

        if (otherPerson.isEmpty()) {
            throw new OtherPersonNotExistsRecordId();
        }

        otherPerson.ifPresent(person ->
            person.setOtherPersonRecords(
                person
                    .getOtherPersonRecords()
                    .stream()
                    .filter(record -> Objects.equals(record.getId(), recordId))
                    .collect(Collectors.toSet())
            )
        );

        return otherPerson;
    }

    @Override
    public void deleteWithRecordId(Long recordId) {
        log.debug("Request to remove other person with record id : {}", recordId);
        Optional<OtherPerson> otherPerson = otherPersonRepository.findByOtherPersonRecordsId(recordId);

        if (otherPerson.isEmpty()) {
            throw new OtherPersonNotExistsRecordId();
        }

        if (otherPerson.get().getOtherPersonRecords().size() == 1) {
            otherPersonRepository.deleteById(otherPerson.get().getId());
        } else {
            OtherPersonRecord otherPersonRecord = otherPerson
                .get()
                .getOtherPersonRecords()
                .stream()
                .filter(record -> Objects.equals(record.getId(), recordId))
                .collect(Collectors.toList())
                .get(0);
            otherPerson.get().removeOtherPersonRecord(otherPersonRecord);
            otherPersonRecordRepository.deleteById(recordId);
        }
    }

    @Override
    public List<String> checkOtherPersonIfExists(List<String> identityNumbers, String city) {
        log.debug("Request to checking if exist other persons");

        return otherPersonRepository.findExistingOtherPersons(identityNumbers, city);
    }
}
