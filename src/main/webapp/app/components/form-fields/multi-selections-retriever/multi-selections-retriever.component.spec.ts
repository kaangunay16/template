import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MultiSelectionsRetrieverComponent } from './multi-selections-retriever.component';

describe('MultiSelectionsRetrieverComponent', () => {
  let component: MultiSelectionsRetrieverComponent;
  let fixture: ComponentFixture<MultiSelectionsRetrieverComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [MultiSelectionsRetrieverComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MultiSelectionsRetrieverComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
