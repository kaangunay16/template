import { Component } from '@angular/core';
import * as XLSX from 'xlsx';

@Component({
  selector: 'jhi-excel-exporter',
  templateUrl: './excel-exporter.component.html',
  styleUrls: ['./excel-exporter.component.scss'],
})
export class ExcelExporterComponent {
  exportExcel(): void {
    const fileName = 'export.xlsx';
    const element = document.getElementById('result-table');
    const ws = XLSX.utils.table_to_sheet(element);
    const wb = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(wb, ws, 'Page1');
    XLSX.writeFile(wb, fileName);
  }
}
