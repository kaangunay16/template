package tr.com.khg.template.service.impl;

import java.util.List;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tr.com.khg.template.domain.Demand;
import tr.com.khg.template.domain.enumeration.DemandType;
import tr.com.khg.template.repository.DemandRepository;
import tr.com.khg.template.service.DemandService;
import tr.com.khg.template.service.utils.DemandHelper;

/**
 * Service Implementation for managing {@link Demand}.
 */
@Service
@Transactional
public class DemandServiceImpl implements DemandService {

    private final Logger log = LoggerFactory.getLogger(DemandServiceImpl.class);

    private final DemandRepository demandRepository;

    private final DemandHelper demandHelper;

    public DemandServiceImpl(DemandRepository demandRepository, DemandHelper demandHelper) {
        this.demandRepository = demandRepository;
        this.demandHelper = demandHelper;
    }

    @Override
    @Transactional
    public Demand save(Demand demand) {
        log.debug("Request to save Demand : {}", demand);
        User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        if (demand.getId() == null) {
            demand.setDemandedBy(user.getUsername());
        } else {
            demand.setProcessedBy(user.getUsername());

            if (demand.getStatus() == DemandType.CONFIRMED) {
                demandHelper.deleteDemandedEntity(demand);
            }
        }

        return demandRepository.save(demand);
    }

    @Override
    public Optional<Demand> partialUpdate(Demand demand) {
        log.debug("Request to partially update Demand : {}", demand);

        return demandRepository
            .findById(demand.getId())
            .map(existingDemand -> {
                if (demand.getDocument() != null) {
                    existingDemand.setDocument(demand.getDocument());
                }
                if (demand.getDemandedBy() != null) {
                    existingDemand.setDemandedBy(demand.getDemandedBy());
                }
                if (demand.getDescription() != null) {
                    existingDemand.setDescription(demand.getDescription());
                }
                if (demand.getDemandType() != null) {
                    existingDemand.setDemandType(demand.getDemandType());
                }
                if (demand.getStatus() != null) {
                    existingDemand.setStatus(demand.getStatus());
                }
                if (demand.getProcessedBy() != null) {
                    existingDemand.setProcessedBy(demand.getProcessedBy());
                }
                if (demand.getEntityId() != null) {
                    existingDemand.setEntityId(demand.getEntityId());
                }
                if (demand.getProcessDescription() != null) {
                    existingDemand.setProcessDescription(demand.getProcessDescription());
                }

                return existingDemand;
            })
            .map(demandRepository::save);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Demand> findAll() {
        log.debug("Request to get all Demands");
        return demandRepository.findAll();
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<Demand> findOne(Long id) {
        log.debug("Request to get Demand : {}", id);
        return demandRepository.findById(id);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete Demand : {}", id);
        demandRepository.deleteById(id);
    }

    @Override
    public List<Demand> findAllWaitingDemands(String demandType) {
        log.debug("Request to all waiting demands");
        return demandRepository.findByStatusAndDemandType(DemandType.WAITING, demandType);
    }
}
