import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ActualActivityFieldComponent } from './actual-activity-field.component';

describe('ActualActivityFieldComponent', () => {
  let component: ActualActivityFieldComponent;
  let fixture: ComponentFixture<ActualActivityFieldComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ActualActivityFieldComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ActualActivityFieldComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
