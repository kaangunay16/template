jest.mock('@angular/router');

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { of, Subject } from 'rxjs';

import { PayPhoneService } from '../service/pay-phone.service';
import { IPayPhone, PayPhone } from '../pay-phone.model';

import { PayPhoneUpdateComponent } from './pay-phone-update.component';

describe('PayPhone Management Update Component', () => {
  let comp: PayPhoneUpdateComponent;
  let fixture: ComponentFixture<PayPhoneUpdateComponent>;
  let activatedRoute: ActivatedRoute;
  let payPhoneService: PayPhoneService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      declarations: [PayPhoneUpdateComponent],
      providers: [FormBuilder, ActivatedRoute],
    })
      .overrideTemplate(PayPhoneUpdateComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(PayPhoneUpdateComponent);
    activatedRoute = TestBed.inject(ActivatedRoute);
    payPhoneService = TestBed.inject(PayPhoneService);

    comp = fixture.componentInstance;
  });

  describe('ngOnInit', () => {
    it('Should update editForm', () => {
      const payPhone: IPayPhone = { id: 456 };

      activatedRoute.data = of({ payPhone });
      comp.ngOnInit();

      expect(comp.editForm.value).toEqual(expect.objectContaining(payPhone));
    });
  });

  describe('save', () => {
    it('Should call update service on save for existing entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<PayPhone>>();
      const payPhone = { id: 123 };
      jest.spyOn(payPhoneService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ payPhone });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: payPhone }));
      saveSubject.complete();

      // THEN
      expect(comp.previousState).toHaveBeenCalled();
      expect(payPhoneService.update).toHaveBeenCalledWith(payPhone);
      expect(comp.isSaving).toEqual(false);
    });

    it('Should call create service on save for new entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<PayPhone>>();
      const payPhone = new PayPhone();
      jest.spyOn(payPhoneService, 'create').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ payPhone });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: payPhone }));
      saveSubject.complete();

      // THEN
      expect(payPhoneService.create).toHaveBeenCalledWith(payPhone);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).toHaveBeenCalled();
    });

    it('Should set isSaving to false on error', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<PayPhone>>();
      const payPhone = { id: 123 };
      jest.spyOn(payPhoneService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ payPhone });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.error('This is an error!');

      // THEN
      expect(payPhoneService.update).toHaveBeenCalledWith(payPhone);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).not.toHaveBeenCalled();
    });
  });
});
