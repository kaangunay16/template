export interface IActivityField {
  institution?: string | null;
  status?: string | null;
  section?: string | null;
  organization?: string | null;
  city?: string | null;
  district?: string | null;
}

export class ActivityField implements IActivityField {
  constructor(
    public institution?: string | null,
    public status?: string | null,
    public section?: string | null,
    public organization?: string | null,
    public city?: string | null,
    public district?: string | null
  ) {}
}
