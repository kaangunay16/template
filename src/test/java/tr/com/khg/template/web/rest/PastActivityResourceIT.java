package tr.com.khg.template.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import tr.com.khg.template.IntegrationTest;
import tr.com.khg.template.domain.PastActivity;
import tr.com.khg.template.repository.PastActivityRepository;

/**
 * Integration tests for the {@link PastActivityResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class PastActivityResourceIT {

    private static final String DEFAULT_CITY = "AAAAAAAAAA";
    private static final String UPDATED_CITY = "BBBBBBBBBB";

    private static final Integer DEFAULT_START_YEAR = 1;
    private static final Integer UPDATED_START_YEAR = 2;

    private static final Integer DEFAULT_END_YEAR = 1;
    private static final Integer UPDATED_END_YEAR = 2;

    private static final Boolean DEFAULT_DELETED = false;
    private static final Boolean UPDATED_DELETED = true;

    private static final String ENTITY_API_URL = "/api/past-activities";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private PastActivityRepository pastActivityRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restPastActivityMockMvc;

    private PastActivity pastActivity;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static PastActivity createEntity(EntityManager em) {
        PastActivity pastActivity = new PastActivity()
            .city(DEFAULT_CITY)
            .startYear(DEFAULT_START_YEAR)
            .endYear(DEFAULT_END_YEAR)
            .deleted(DEFAULT_DELETED);
        return pastActivity;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static PastActivity createUpdatedEntity(EntityManager em) {
        PastActivity pastActivity = new PastActivity()
            .city(UPDATED_CITY)
            .startYear(UPDATED_START_YEAR)
            .endYear(UPDATED_END_YEAR)
            .deleted(UPDATED_DELETED);
        return pastActivity;
    }

    @BeforeEach
    public void initTest() {
        pastActivity = createEntity(em);
    }

    @Test
    @Transactional
    void createPastActivity() throws Exception {
        int databaseSizeBeforeCreate = pastActivityRepository.findAll().size();
        // Create the PastActivity
        restPastActivityMockMvc
            .perform(
                post(ENTITY_API_URL)
                    .with(csrf())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(pastActivity))
            )
            .andExpect(status().isCreated());

        // Validate the PastActivity in the database
        List<PastActivity> pastActivityList = pastActivityRepository.findAll();
        assertThat(pastActivityList).hasSize(databaseSizeBeforeCreate + 1);
        PastActivity testPastActivity = pastActivityList.get(pastActivityList.size() - 1);
        assertThat(testPastActivity.getCity()).isEqualTo(DEFAULT_CITY);
        assertThat(testPastActivity.getStartYear()).isEqualTo(DEFAULT_START_YEAR);
        assertThat(testPastActivity.getEndYear()).isEqualTo(DEFAULT_END_YEAR);
        assertThat(testPastActivity.getDeleted()).isEqualTo(DEFAULT_DELETED);
    }

    @Test
    @Transactional
    void createPastActivityWithExistingId() throws Exception {
        // Create the PastActivity with an existing ID
        pastActivity.setId(1L);

        int databaseSizeBeforeCreate = pastActivityRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restPastActivityMockMvc
            .perform(
                post(ENTITY_API_URL)
                    .with(csrf())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(pastActivity))
            )
            .andExpect(status().isBadRequest());

        // Validate the PastActivity in the database
        List<PastActivity> pastActivityList = pastActivityRepository.findAll();
        assertThat(pastActivityList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void getAllPastActivities() throws Exception {
        // Initialize the database
        pastActivityRepository.saveAndFlush(pastActivity);

        // Get all the pastActivityList
        restPastActivityMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(pastActivity.getId().intValue())))
            .andExpect(jsonPath("$.[*].city").value(hasItem(DEFAULT_CITY)))
            .andExpect(jsonPath("$.[*].startYear").value(hasItem(DEFAULT_START_YEAR)))
            .andExpect(jsonPath("$.[*].endYear").value(hasItem(DEFAULT_END_YEAR)))
            .andExpect(jsonPath("$.[*].deleted").value(hasItem(DEFAULT_DELETED.booleanValue())));
    }

    @Test
    @Transactional
    void getPastActivity() throws Exception {
        // Initialize the database
        pastActivityRepository.saveAndFlush(pastActivity);

        // Get the pastActivity
        restPastActivityMockMvc
            .perform(get(ENTITY_API_URL_ID, pastActivity.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(pastActivity.getId().intValue()))
            .andExpect(jsonPath("$.city").value(DEFAULT_CITY))
            .andExpect(jsonPath("$.startYear").value(DEFAULT_START_YEAR))
            .andExpect(jsonPath("$.endYear").value(DEFAULT_END_YEAR))
            .andExpect(jsonPath("$.deleted").value(DEFAULT_DELETED.booleanValue()));
    }

    @Test
    @Transactional
    void getNonExistingPastActivity() throws Exception {
        // Get the pastActivity
        restPastActivityMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putNewPastActivity() throws Exception {
        // Initialize the database
        pastActivityRepository.saveAndFlush(pastActivity);

        int databaseSizeBeforeUpdate = pastActivityRepository.findAll().size();

        // Update the pastActivity
        PastActivity updatedPastActivity = pastActivityRepository.findById(pastActivity.getId()).get();
        // Disconnect from session so that the updates on updatedPastActivity are not directly saved in db
        em.detach(updatedPastActivity);
        updatedPastActivity.city(UPDATED_CITY).startYear(UPDATED_START_YEAR).endYear(UPDATED_END_YEAR).deleted(UPDATED_DELETED);

        restPastActivityMockMvc
            .perform(
                put(ENTITY_API_URL_ID, updatedPastActivity.getId())
                    .with(csrf())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(updatedPastActivity))
            )
            .andExpect(status().isOk());

        // Validate the PastActivity in the database
        List<PastActivity> pastActivityList = pastActivityRepository.findAll();
        assertThat(pastActivityList).hasSize(databaseSizeBeforeUpdate);
        PastActivity testPastActivity = pastActivityList.get(pastActivityList.size() - 1);
        assertThat(testPastActivity.getCity()).isEqualTo(UPDATED_CITY);
        assertThat(testPastActivity.getStartYear()).isEqualTo(UPDATED_START_YEAR);
        assertThat(testPastActivity.getEndYear()).isEqualTo(UPDATED_END_YEAR);
        assertThat(testPastActivity.getDeleted()).isEqualTo(UPDATED_DELETED);
    }

    @Test
    @Transactional
    void putNonExistingPastActivity() throws Exception {
        int databaseSizeBeforeUpdate = pastActivityRepository.findAll().size();
        pastActivity.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restPastActivityMockMvc
            .perform(
                put(ENTITY_API_URL_ID, pastActivity.getId())
                    .with(csrf())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(pastActivity))
            )
            .andExpect(status().isBadRequest());

        // Validate the PastActivity in the database
        List<PastActivity> pastActivityList = pastActivityRepository.findAll();
        assertThat(pastActivityList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchPastActivity() throws Exception {
        int databaseSizeBeforeUpdate = pastActivityRepository.findAll().size();
        pastActivity.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restPastActivityMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .with(csrf())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(pastActivity))
            )
            .andExpect(status().isBadRequest());

        // Validate the PastActivity in the database
        List<PastActivity> pastActivityList = pastActivityRepository.findAll();
        assertThat(pastActivityList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamPastActivity() throws Exception {
        int databaseSizeBeforeUpdate = pastActivityRepository.findAll().size();
        pastActivity.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restPastActivityMockMvc
            .perform(
                put(ENTITY_API_URL)
                    .with(csrf())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(pastActivity))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the PastActivity in the database
        List<PastActivity> pastActivityList = pastActivityRepository.findAll();
        assertThat(pastActivityList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdatePastActivityWithPatch() throws Exception {
        // Initialize the database
        pastActivityRepository.saveAndFlush(pastActivity);

        int databaseSizeBeforeUpdate = pastActivityRepository.findAll().size();

        // Update the pastActivity using partial update
        PastActivity partialUpdatedPastActivity = new PastActivity();
        partialUpdatedPastActivity.setId(pastActivity.getId());

        partialUpdatedPastActivity.startYear(UPDATED_START_YEAR);

        restPastActivityMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedPastActivity.getId())
                    .with(csrf())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedPastActivity))
            )
            .andExpect(status().isOk());

        // Validate the PastActivity in the database
        List<PastActivity> pastActivityList = pastActivityRepository.findAll();
        assertThat(pastActivityList).hasSize(databaseSizeBeforeUpdate);
        PastActivity testPastActivity = pastActivityList.get(pastActivityList.size() - 1);
        assertThat(testPastActivity.getCity()).isEqualTo(DEFAULT_CITY);
        assertThat(testPastActivity.getStartYear()).isEqualTo(UPDATED_START_YEAR);
        assertThat(testPastActivity.getEndYear()).isEqualTo(DEFAULT_END_YEAR);
        assertThat(testPastActivity.getDeleted()).isEqualTo(DEFAULT_DELETED);
    }

    @Test
    @Transactional
    void fullUpdatePastActivityWithPatch() throws Exception {
        // Initialize the database
        pastActivityRepository.saveAndFlush(pastActivity);

        int databaseSizeBeforeUpdate = pastActivityRepository.findAll().size();

        // Update the pastActivity using partial update
        PastActivity partialUpdatedPastActivity = new PastActivity();
        partialUpdatedPastActivity.setId(pastActivity.getId());

        partialUpdatedPastActivity.city(UPDATED_CITY).startYear(UPDATED_START_YEAR).endYear(UPDATED_END_YEAR).deleted(UPDATED_DELETED);

        restPastActivityMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedPastActivity.getId())
                    .with(csrf())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedPastActivity))
            )
            .andExpect(status().isOk());

        // Validate the PastActivity in the database
        List<PastActivity> pastActivityList = pastActivityRepository.findAll();
        assertThat(pastActivityList).hasSize(databaseSizeBeforeUpdate);
        PastActivity testPastActivity = pastActivityList.get(pastActivityList.size() - 1);
        assertThat(testPastActivity.getCity()).isEqualTo(UPDATED_CITY);
        assertThat(testPastActivity.getStartYear()).isEqualTo(UPDATED_START_YEAR);
        assertThat(testPastActivity.getEndYear()).isEqualTo(UPDATED_END_YEAR);
        assertThat(testPastActivity.getDeleted()).isEqualTo(UPDATED_DELETED);
    }

    @Test
    @Transactional
    void patchNonExistingPastActivity() throws Exception {
        int databaseSizeBeforeUpdate = pastActivityRepository.findAll().size();
        pastActivity.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restPastActivityMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, pastActivity.getId())
                    .with(csrf())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(pastActivity))
            )
            .andExpect(status().isBadRequest());

        // Validate the PastActivity in the database
        List<PastActivity> pastActivityList = pastActivityRepository.findAll();
        assertThat(pastActivityList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchPastActivity() throws Exception {
        int databaseSizeBeforeUpdate = pastActivityRepository.findAll().size();
        pastActivity.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restPastActivityMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .with(csrf())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(pastActivity))
            )
            .andExpect(status().isBadRequest());

        // Validate the PastActivity in the database
        List<PastActivity> pastActivityList = pastActivityRepository.findAll();
        assertThat(pastActivityList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamPastActivity() throws Exception {
        int databaseSizeBeforeUpdate = pastActivityRepository.findAll().size();
        pastActivity.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restPastActivityMockMvc
            .perform(
                patch(ENTITY_API_URL)
                    .with(csrf())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(pastActivity))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the PastActivity in the database
        List<PastActivity> pastActivityList = pastActivityRepository.findAll();
        assertThat(pastActivityList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deletePastActivity() throws Exception {
        // Initialize the database
        pastActivityRepository.saveAndFlush(pastActivity);

        int databaseSizeBeforeDelete = pastActivityRepository.findAll().size();

        // Delete the pastActivity
        restPastActivityMockMvc
            .perform(delete(ENTITY_API_URL_ID, pastActivity.getId()).with(csrf()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<PastActivity> pastActivityList = pastActivityRepository.findAll();
        assertThat(pastActivityList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
