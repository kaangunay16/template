import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';

import { IStatistic, Statistic } from '../../shared/model/statistic.model';

@Injectable({
  providedIn: 'root',
})
export class YearsStatsService {
  result?: IStatistic[];

  getResults(): Observable<IStatistic[]> {
    if (this.result !== undefined) {
      return of(this.result);
    }

    this.result = [new Statistic('2020', 25), new Statistic('2021', 28)];

    return of(this.result);
  }
}
