import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PersonDeletionConfirmationComponent } from './person-deletion-confirmation.component';

describe('PersonDeletionConfirmationComponent', () => {
  let component: PersonDeletionConfirmationComponent;
  let fixture: ComponentFixture<PersonDeletionConfirmationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [PersonDeletionConfirmationComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PersonDeletionConfirmationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
