import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { errorRoute } from './layouts/error/error.route';
import { navbarRoute } from './layouts/navbar/navbar.route';
import { DEBUG_INFO_ENABLED } from 'app/app.constants';
import { Authority } from 'app/config/authority.constants';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';

const LAYOUT_ROUTES = [navbarRoute, ...errorRoute];

@NgModule({
  imports: [
    RouterModule.forRoot(
      [
        {
          path: 'admin',
          data: {
            authorities: [Authority.ADMIN],
          },
          canActivate: [UserRouteAccessService],
          loadChildren: () => import('./admin/admin-routing.module').then(m => m.AdminRoutingModule),
        },
        {
          path: 'account',
          loadChildren: () => import('./account/account.module').then(m => m.AccountModule),
        },
        {
          path: 'login',
          loadChildren: () => import('./login/login.module').then(m => m.LoginModule),
        },
        {
          path: 'person',
          loadChildren: () => import('./pages/person/person.module').then(m => m.PersonModule),
        },
        {
          path: 'other-person',
          loadChildren: () => import('./pages/other-person/other-person.module').then(m => m.OtherPersonModule),
        },
        {
          path: 'meeting',
          loadChildren: () => import('./pages/meeting/meeting.module').then(m => m.MeetingModule),
        },
        {
          path: 'tabs',
          loadChildren: () => import('./pages/tabs/tabs.module').then(m => m.TabsModule),
        },
        {
          path: 'ngrx',
          loadChildren: () => import('./pages/ngrx-course/ngrx-course.module').then(m => m.NgrxCourseModule),
        },
        {
          path: 'rxjs',
          loadChildren: () => import('./pages/rxjs-course/rxjs-course.module').then(m => m.RxjsCourseModule),
        },
        ...LAYOUT_ROUTES,
      ],
      { enableTracing: DEBUG_INFO_ENABLED }
    ),
  ],
  exports: [RouterModule],
})
export class AppRoutingModule {}
