package tr.com.khg.template.service;

import java.util.List;
import java.util.Optional;
import tr.com.khg.template.domain.PayPhone;

/**
 * Service Interface for managing {@link PayPhone}.
 */
public interface PayPhoneService {
    /**
     * Save a payPhone.
     *
     * @param payPhone the entity to save.
     * @return the persisted entity.
     */
    PayPhone save(PayPhone payPhone);

    /**
     * Partially updates a payPhone.
     *
     * @param payPhone the entity to update partially.
     * @return the persisted entity.
     */
    Optional<PayPhone> partialUpdate(PayPhone payPhone);

    /**
     * Get all the payPhones.
     *
     * @return the list of entities.
     */
    List<PayPhone> findAll();

    /**
     * Get the "id" payPhone.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<PayPhone> findOne(Long id);

    /**
     * Delete the "id" payPhone.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
