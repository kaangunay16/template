import { Component, Input, OnInit } from '@angular/core';
import { ControlValueAccessor, FormBuilder, FormGroup, NG_VALUE_ACCESSOR } from '@angular/forms';

import { DynamicFormFunctions } from 'app/components/dynamic-form-creator/dynamic-form-field.model';
import { DYNAMIC_FIELD } from 'app/components/dynamic-form-creator/dyanmic-form.token';
import { DynamicFormService } from 'app/components/dynamic-form-creator/dynamic-form.service';
import { OtherName } from 'app/entities/other-name/other-name.model';
import { OtherNameService } from 'app/entities/other-name/service/other-name.service';
import { PersonModuleService } from 'app/pages/person/person-module.service';
import { FormFieldsService } from '../form-fields.service';

@Component({
  selector: 'jhi-other-name-field',
  templateUrl: './other-name-field.component.html',
  styleUrls: ['./other-name-field.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: OtherNameFieldComponent,
      multi: true,
    },
    {
      provide: DYNAMIC_FIELD,
      useExisting: OtherNameFieldComponent,
    },
  ],
})
export class OtherNameFieldComponent implements OnInit, ControlValueAccessor, DynamicFormFunctions {
  form!: FormGroup;

  @Input()
  readOnly?: boolean = false;

  onTouched!: () => void;

  constructor(
    private formBuilder: FormBuilder,
    private formFieldsService: FormFieldsService,
    private service: OtherNameService,
    private personModuleService: PersonModuleService,
    private dynamicFormService: DynamicFormService
  ) {}

  ngOnInit(): void {
    this.form = this.formBuilder.group({
      id: [{ value: null }],
      name: [{ value: null, disabled: this.readOnly }],
      person: [{ value: null }],
    });
  }

  registerOnChange(fn: (value: any) => void): void {
    this.form.valueChanges.subscribe(fn);
  }

  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

  writeValue(obj: OtherName | null): void {
    this.form.patchValue({
      id: obj ? obj.id : null,
      name: obj ? obj.name : null,
      person: obj ? obj.person : null,
    });
  }

  remove(): void {
    const id = this.form.get('id')?.value;

    if (id) {
      this.formFieldsService.remove(id, this.service);
      this.personModuleService.removeOtherName(this.dynamicFormService.getEntityId(), id);
    }
  }
}
