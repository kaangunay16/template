package tr.com.khg.template.repository;

import java.util.Optional;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;
import tr.com.khg.template.domain.Identity;

/**
 * Spring Data SQL repository for the Identity entity.
 */
@SuppressWarnings("unused")
@Repository
public interface IdentityRepository extends JpaRepository<Identity, Long> {
    Optional<Identity> findByIdentityNo(String identityNo);
}
