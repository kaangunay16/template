export interface ISelection {
  id?: number;
  key?: string | null;
  value?: string | null;
  priority?: number | null;
}

export class Selection implements ISelection {
  constructor(public id?: number, public key?: string | null, public value?: string | null, public priority?: number | null) {}
}

export function getSelectionIdentifier(selection: ISelection): number | undefined {
  return selection.id;
}
