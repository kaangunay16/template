import { Component, OnInit } from '@angular/core';
import { Observable, Subscriber } from 'rxjs';
import { ToastrService } from 'ngx-toastr';

/**
 * Yeni observable oluşturma <hr/>
 * https://www.youtube.com/watch?v=0qdw9gK1AZ0
 */

@Component({
  selector: 'jhi-rxjs-course5',
  templateUrl: './rxjs-course5.component.html',
  styleUrls: ['./rxjs-course5.component.scss'],
})
export class RxjsCourse5Component implements OnInit {
  constructor(private toastrService: ToastrService) {}

  ngOnInit(): void {
    const newObservable = new Observable((observer: Subscriber<number>) => {
      for (let i = 0; i < 5; i++) {
        if (i === 3) {
          observer.error('3 olmaz!');
        }
        observer.next(i);
      }
      observer.complete();
    });

    const observer: Subscriber<number> = new Subscriber<number>({
      next: (value: number) => {
        this.toastrService.info(value.toString());
      },
      error: (err: string) => {
        this.toastrService.error(err);
      },
      complete: () => {
        this.toastrService.warning('Subscriber completed.');
      },
    });

    newObservable.subscribe(observer);
  }
}
