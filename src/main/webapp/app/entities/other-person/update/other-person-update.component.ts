import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import { IOtherPerson, OtherPerson } from '../other-person.model';
import { OtherPersonService } from '../service/other-person.service';
import { IIdentity } from 'app/entities/identity/identity.model';
import { IdentityService } from 'app/entities/identity/service/identity.service';

@Component({
  selector: 'jhi-other-person-update',
  templateUrl: './other-person-update.component.html',
})
export class OtherPersonUpdateComponent implements OnInit {
  isSaving = false;

  identitiesCollection: IIdentity[] = [];

  editForm = this.fb.group({
    id: [],
    identity: [],
  });

  constructor(
    protected otherPersonService: OtherPersonService,
    protected identityService: IdentityService,
    protected activatedRoute: ActivatedRoute,
    protected fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ otherPerson }) => {
      this.updateForm(otherPerson);

      this.loadRelationshipsOptions();
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const otherPerson = this.createFromForm();
    if (otherPerson.id !== undefined) {
      this.subscribeToSaveResponse(this.otherPersonService.update(otherPerson));
    } else {
      this.subscribeToSaveResponse(this.otherPersonService.create(otherPerson));
    }
  }

  trackIdentityById(index: number, item: IIdentity): number {
    return item.id!;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IOtherPerson>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(otherPerson: IOtherPerson): void {
    this.editForm.patchValue({
      id: otherPerson.id,
      identity: otherPerson.identity,
    });

    this.identitiesCollection = this.identityService.addIdentityToCollectionIfMissing(this.identitiesCollection, otherPerson.identity);
  }

  protected loadRelationshipsOptions(): void {
    this.identityService
      .query({ filter: 'otherperson-is-null' })
      .pipe(map((res: HttpResponse<IIdentity[]>) => res.body ?? []))
      .pipe(
        map((identities: IIdentity[]) =>
          this.identityService.addIdentityToCollectionIfMissing(identities, this.editForm.get('identity')!.value)
        )
      )
      .subscribe((identities: IIdentity[]) => (this.identitiesCollection = identities));
  }

  protected createFromForm(): IOtherPerson {
    return {
      ...new OtherPerson(),
      id: this.editForm.get(['id'])!.value,
      identity: this.editForm.get(['identity'])!.value,
    };
  }
}
