import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: 'identity',
        data: { pageTitle: 'templateApp.identity.home.title' },
        loadChildren: () => import('./identity/identity.module').then(m => m.IdentityModule),
      },
      {
        path: 'abroad-activity',
        data: { pageTitle: 'templateApp.abroadActivity.home.title' },
        loadChildren: () => import('./abroad-activity/abroad-activity.module').then(m => m.AbroadActivityModule),
      },
      {
        path: 'actual-activity',
        data: { pageTitle: 'templateApp.actualActivity.home.title' },
        loadChildren: () => import('./actual-activity/actual-activity.module').then(m => m.ActualActivityModule),
      },
      {
        path: 'past-activity',
        data: { pageTitle: 'templateApp.pastActivity.home.title' },
        loadChildren: () => import('./past-activity/past-activity.module').then(m => m.PastActivityModule),
      },
      {
        path: 'other-name',
        data: { pageTitle: 'templateApp.otherName.home.title' },
        loadChildren: () => import('./other-name/other-name.module').then(m => m.OtherNameModule),
      },
      {
        path: 'phone',
        data: { pageTitle: 'templateApp.phone.home.title' },
        loadChildren: () => import('./phone/phone.module').then(m => m.PhoneModule),
      },
      {
        path: 'pay-phone',
        data: { pageTitle: 'templateApp.payPhone.home.title' },
        loadChildren: () => import('./pay-phone/pay-phone.module').then(m => m.PayPhoneModule),
      },
      {
        path: 'selection',
        data: { pageTitle: 'templateApp.selection.home.title' },
        loadChildren: () => import('./selection/selection.module').then(m => m.SelectionModule),
      },
      {
        path: 'other-person-record',
        data: { pageTitle: 'templateApp.otherPersonRecord.home.title' },
        loadChildren: () => import('./other-person-record/other-person-record.module').then(m => m.OtherPersonRecordModule),
      },
      {
        path: 'meeting-attendee',
        data: { pageTitle: 'templateApp.meetingAttendee.home.title' },
        loadChildren: () => import('./meeting-attendee/meeting-attendee.module').then(m => m.MeetingAttendeeModule),
      },
      {
        path: 'demand',
        data: { pageTitle: 'templateApp.demand.home.title' },
        loadChildren: () => import('./demand/demand.module').then(m => m.DemandModule),
      },
      /* jhipster-needle-add-entity-route - JHipster will add entity modules routes here */
    ]),
  ],
})
export class EntityRoutingModule {}
