import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IPayPhone } from '../pay-phone.model';

@Component({
  selector: 'jhi-pay-phone-detail',
  templateUrl: './pay-phone-detail.component.html',
})
export class PayPhoneDetailComponent implements OnInit {
  payPhone: IPayPhone | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ payPhone }) => {
      this.payPhone = payPhone;
    });
  }

  previousState(): void {
    window.history.back();
  }
}
