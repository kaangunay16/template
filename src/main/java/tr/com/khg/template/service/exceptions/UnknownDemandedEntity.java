package tr.com.khg.template.service.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.NOT_ACCEPTABLE, reason = "Bilinmeyen Kayıt Tipi")
public class UnknownDemandedEntity extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public UnknownDemandedEntity() {
        super("Bilinmeyen bir tipte kayda silme talebi yapılmıştır.");
    }
}
