import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { isPresent } from 'app/core/util/operators';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { IOtherPersonRecord, getOtherPersonRecordIdentifier } from '../other-person-record.model';

export type EntityResponseType = HttpResponse<IOtherPersonRecord>;
export type EntityArrayResponseType = HttpResponse<IOtherPersonRecord[]>;

@Injectable({ providedIn: 'root' })
export class OtherPersonRecordService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/other-person-records');

  constructor(protected http: HttpClient, protected applicationConfigService: ApplicationConfigService) {}

  create(otherPersonRecord: IOtherPersonRecord): Observable<EntityResponseType> {
    return this.http.post<IOtherPersonRecord>(this.resourceUrl, otherPersonRecord, { observe: 'response' });
  }

  update(otherPersonRecord: IOtherPersonRecord): Observable<EntityResponseType> {
    return this.http.put<IOtherPersonRecord>(
      `${this.resourceUrl}/${getOtherPersonRecordIdentifier(otherPersonRecord) as number}`,
      otherPersonRecord,
      { observe: 'response' }
    );
  }

  updateAll(otherPersonRecords: IOtherPersonRecord[]): Observable<EntityArrayResponseType> {
    return this.http.post<IOtherPersonRecord[]>(`${this.resourceUrl}/update-all`, otherPersonRecords, { observe: 'response' });
  }

  partialUpdate(otherPersonRecord: IOtherPersonRecord): Observable<EntityResponseType> {
    return this.http.patch<IOtherPersonRecord>(
      `${this.resourceUrl}/${getOtherPersonRecordIdentifier(otherPersonRecord) as number}`,
      otherPersonRecord,
      { observe: 'response' }
    );
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IOtherPersonRecord>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IOtherPersonRecord[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  addOtherPersonRecordToCollectionIfMissing(
    otherPersonRecordCollection: IOtherPersonRecord[],
    ...otherPersonRecordsToCheck: (IOtherPersonRecord | null | undefined)[]
  ): IOtherPersonRecord[] {
    const otherPersonRecords: IOtherPersonRecord[] = otherPersonRecordsToCheck.filter(isPresent);
    if (otherPersonRecords.length > 0) {
      const otherPersonRecordCollectionIdentifiers = otherPersonRecordCollection.map(
        otherPersonRecordItem => getOtherPersonRecordIdentifier(otherPersonRecordItem)!
      );
      const otherPersonRecordsToAdd = otherPersonRecords.filter(otherPersonRecordItem => {
        const otherPersonRecordIdentifier = getOtherPersonRecordIdentifier(otherPersonRecordItem);
        if (otherPersonRecordIdentifier == null || otherPersonRecordCollectionIdentifiers.includes(otherPersonRecordIdentifier)) {
          return false;
        }
        otherPersonRecordCollectionIdentifiers.push(otherPersonRecordIdentifier);
        return true;
      });
      return [...otherPersonRecordsToAdd, ...otherPersonRecordCollection];
    }
    return otherPersonRecordCollection;
  }
}
