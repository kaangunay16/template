import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CityDistrictSelectionComponent } from './city-district-selection.component';

describe('CityDistrictSelectionComponent', () => {
  let component: CityDistrictSelectionComponent;
  let fixture: ComponentFixture<CityDistrictSelectionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [CityDistrictSelectionComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CityDistrictSelectionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
