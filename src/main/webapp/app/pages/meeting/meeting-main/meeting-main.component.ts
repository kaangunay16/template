import { Component } from '@angular/core';

@Component({
  selector: 'jhi-meeting-main',
  templateUrl: './meeting-main.component.html',
  styleUrls: ['./meeting-main.component.scss'],
})
export class MeetingMainComponent {}
