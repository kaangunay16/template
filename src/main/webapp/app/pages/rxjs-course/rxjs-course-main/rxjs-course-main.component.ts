import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'jhi-rxjs-course-main',
  templateUrl: './rxjs-course-main.component.html',
  styleUrls: ['./rxjs-course-main.component.scss'],
})
export class RxjsCourseMainComponent implements OnInit {
  form!: FormGroup;

  constructor(private router: Router) {}

  ngOnInit(): void {
    this.form = new FormGroup({ num: new FormControl(null) });
  }

  onSubmit(): void {
    const num = this.form.get('num')?.value;

    if (num) {
      this.router.navigate(['/rxjs', num.toString()]);
    }
  }
}
