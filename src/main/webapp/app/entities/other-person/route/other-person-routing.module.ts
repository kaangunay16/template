import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { OtherPersonComponent } from '../list/other-person.component';
import { OtherPersonDetailComponent } from '../detail/other-person-detail.component';
import { OtherPersonUpdateComponent } from '../update/other-person-update.component';
import { OtherPersonRoutingResolveService } from './other-person-routing-resolve.service';

const otherPersonRoute: Routes = [
  {
    path: '',
    component: OtherPersonComponent,
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: OtherPersonDetailComponent,
    resolve: {
      otherPerson: OtherPersonRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: OtherPersonUpdateComponent,
    resolve: {
      otherPerson: OtherPersonRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: OtherPersonUpdateComponent,
    resolve: {
      otherPerson: OtherPersonRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
];

@NgModule({
  imports: [RouterModule.forChild(otherPersonRoute)],
  exports: [RouterModule],
})
export class OtherPersonRoutingModule {}
