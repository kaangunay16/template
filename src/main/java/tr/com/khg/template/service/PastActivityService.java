package tr.com.khg.template.service;

import java.util.List;
import java.util.Optional;
import tr.com.khg.template.domain.PastActivity;

/**
 * Service Interface for managing {@link PastActivity}.
 */
public interface PastActivityService {
    /**
     * Save a pastActivity.
     *
     * @param pastActivity the entity to save.
     * @return the persisted entity.
     */
    PastActivity save(PastActivity pastActivity);

    /**
     * Partially updates a pastActivity.
     *
     * @param pastActivity the entity to update partially.
     * @return the persisted entity.
     */
    Optional<PastActivity> partialUpdate(PastActivity pastActivity);

    /**
     * Get all the pastActivities.
     *
     * @return the list of entities.
     */
    List<PastActivity> findAll();

    /**
     * Get the "id" pastActivity.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<PastActivity> findOne(Long id);

    /**
     * Delete the "id" pastActivity.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
