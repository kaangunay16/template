import { IconProp } from '@fortawesome/fontawesome-svg-core';

export interface TableAction {
  link: string[];
  tooltip: string;
  icon: IconProp;
  extras?: { key: string; value: string };
}
