package tr.com.khg.template.domain.enumeration;

/**
 * The Genders enumeration.
 */
public enum Genders {
    ERKEK,
    KADIN,
}
