package tr.com.khg.template.domain;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import tr.com.khg.template.web.rest.TestUtil;

class PastActivityTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(PastActivity.class);
        PastActivity pastActivity1 = new PastActivity();
        pastActivity1.setId(1L);
        PastActivity pastActivity2 = new PastActivity();
        pastActivity2.setId(pastActivity1.getId());
        assertThat(pastActivity1).isEqualTo(pastActivity2);
        pastActivity2.setId(2L);
        assertThat(pastActivity1).isNotEqualTo(pastActivity2);
        pastActivity1.setId(null);
        assertThat(pastActivity1).isNotEqualTo(pastActivity2);
    }
}
