jest.mock('@angular/router');

import { TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ActivatedRouteSnapshot, Router } from '@angular/router';
import { of } from 'rxjs';

import { IAbroadActivity, AbroadActivity } from '../abroad-activity.model';
import { AbroadActivityService } from '../service/abroad-activity.service';

import { AbroadActivityRoutingResolveService } from './abroad-activity-routing-resolve.service';

describe('AbroadActivity routing resolve service', () => {
  let mockRouter: Router;
  let mockActivatedRouteSnapshot: ActivatedRouteSnapshot;
  let routingResolveService: AbroadActivityRoutingResolveService;
  let service: AbroadActivityService;
  let resultAbroadActivity: IAbroadActivity | undefined;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [Router, ActivatedRouteSnapshot],
    });
    mockRouter = TestBed.inject(Router);
    mockActivatedRouteSnapshot = TestBed.inject(ActivatedRouteSnapshot);
    routingResolveService = TestBed.inject(AbroadActivityRoutingResolveService);
    service = TestBed.inject(AbroadActivityService);
    resultAbroadActivity = undefined;
  });

  describe('resolve', () => {
    it('should return IAbroadActivity returned by find', () => {
      // GIVEN
      service.find = jest.fn(id => of(new HttpResponse({ body: { id } })));
      mockActivatedRouteSnapshot.params = { id: 123 };

      // WHEN
      routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
        resultAbroadActivity = result;
      });

      // THEN
      expect(service.find).toBeCalledWith(123);
      expect(resultAbroadActivity).toEqual({ id: 123 });
    });

    it('should return new IAbroadActivity if id is not provided', () => {
      // GIVEN
      service.find = jest.fn();
      mockActivatedRouteSnapshot.params = {};

      // WHEN
      routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
        resultAbroadActivity = result;
      });

      // THEN
      expect(service.find).not.toBeCalled();
      expect(resultAbroadActivity).toEqual(new AbroadActivity());
    });

    it('should route to 404 page if data not found in server', () => {
      // GIVEN
      jest.spyOn(service, 'find').mockReturnValue(of(new HttpResponse({ body: null as unknown as AbroadActivity })));
      mockActivatedRouteSnapshot.params = { id: 123 };

      // WHEN
      routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
        resultAbroadActivity = result;
      });

      // THEN
      expect(service.find).toBeCalledWith(123);
      expect(resultAbroadActivity).toEqual(undefined);
      expect(mockRouter.navigate).toHaveBeenCalledWith(['404']);
    });
  });
});
