import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IOtherPerson } from '../other-person.model';
import { OtherPersonService } from '../service/other-person.service';
import { OtherPersonDeleteDialogComponent } from '../delete/other-person-delete-dialog.component';

@Component({
  selector: 'jhi-other-person',
  templateUrl: './other-person.component.html',
})
export class OtherPersonComponent implements OnInit {
  otherPeople?: IOtherPerson[];
  isLoading = false;

  constructor(protected otherPersonService: OtherPersonService, protected modalService: NgbModal) {}

  loadAll(): void {
    this.isLoading = true;

    this.otherPersonService.query().subscribe(
      (res: HttpResponse<IOtherPerson[]>) => {
        this.isLoading = false;
        this.otherPeople = res.body ?? [];
      },
      () => {
        this.isLoading = false;
      }
    );
  }

  ngOnInit(): void {
    this.loadAll();
  }

  trackId(index: number, item: IOtherPerson): number {
    return item.id!;
  }

  delete(otherPerson: IOtherPerson): void {
    const modalRef = this.modalService.open(OtherPersonDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.otherPerson = otherPerson;
    // unsubscribe not needed because closed completes on modal close
    modalRef.closed.subscribe(reason => {
      if (reason === 'deleted') {
        this.loadAll();
      }
    });
  }
}
