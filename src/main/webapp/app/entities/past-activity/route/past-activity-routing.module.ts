import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { PastActivityComponent } from '../list/past-activity.component';
import { PastActivityDetailComponent } from '../detail/past-activity-detail.component';
import { PastActivityUpdateComponent } from '../update/past-activity-update.component';
import { PastActivityRoutingResolveService } from './past-activity-routing-resolve.service';

const pastActivityRoute: Routes = [
  {
    path: '',
    component: PastActivityComponent,
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: PastActivityDetailComponent,
    resolve: {
      pastActivity: PastActivityRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: PastActivityUpdateComponent,
    resolve: {
      pastActivity: PastActivityRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: PastActivityUpdateComponent,
    resolve: {
      pastActivity: PastActivityRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
];

@NgModule({
  imports: [RouterModule.forChild(pastActivityRoute)],
  exports: [RouterModule],
})
export class PastActivityRoutingModule {}
