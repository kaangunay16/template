import { Component, Input } from '@angular/core';

import { TableConfig } from '../models/table-config.model';

@Component({
  selector: 'jhi-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss'],
})
export class TableComponent {
  @Input()
  configs!: TableConfig;
}
