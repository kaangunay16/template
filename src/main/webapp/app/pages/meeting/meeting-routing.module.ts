import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { MeetingMainComponent } from './meeting-main/meeting-main.component';
import { MeetingInfoComponent } from './meeting-info/meeting-info.component';
import { MeetingAttendeesComponent } from './meeting-attendees/meeting-attendees.component';
import { MeetingListComponent } from './meeting-list/meeting-list.component';
import { MeetingFinalizeComponent } from './meeting-finalize/meeting-finalize.component';

const routes: Routes = [
  {
    path: '',
    component: MeetingMainComponent,
    children: [
      {
        path: 'info',
        component: MeetingInfoComponent,
      },
      {
        path: 'attendees',
        component: MeetingAttendeesComponent,
      },
      {
        path: 'unfinalized-meetings',
        component: MeetingListComponent,
      },
      {
        path: 'finalize-meeting',
        component: MeetingFinalizeComponent,
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MeetingRoutingModule {}
