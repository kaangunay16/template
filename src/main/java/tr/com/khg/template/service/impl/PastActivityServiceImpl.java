package tr.com.khg.template.service.impl;

import java.util.List;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tr.com.khg.template.domain.PastActivity;
import tr.com.khg.template.repository.PastActivityRepository;
import tr.com.khg.template.service.PastActivityService;

/**
 * Service Implementation for managing {@link PastActivity}.
 */
@Service
@Transactional
public class PastActivityServiceImpl implements PastActivityService {

    private final Logger log = LoggerFactory.getLogger(PastActivityServiceImpl.class);

    private final PastActivityRepository pastActivityRepository;

    public PastActivityServiceImpl(PastActivityRepository pastActivityRepository) {
        this.pastActivityRepository = pastActivityRepository;
    }

    @Override
    public PastActivity save(PastActivity pastActivity) {
        log.debug("Request to save PastActivity : {}", pastActivity);
        return pastActivityRepository.save(pastActivity);
    }

    @Override
    public Optional<PastActivity> partialUpdate(PastActivity pastActivity) {
        log.debug("Request to partially update PastActivity : {}", pastActivity);

        return pastActivityRepository
            .findById(pastActivity.getId())
            .map(existingPastActivity -> {
                if (pastActivity.getCity() != null) {
                    existingPastActivity.setCity(pastActivity.getCity());
                }
                if (pastActivity.getStartYear() != null) {
                    existingPastActivity.setStartYear(pastActivity.getStartYear());
                }
                if (pastActivity.getEndYear() != null) {
                    existingPastActivity.setEndYear(pastActivity.getEndYear());
                }
                if (pastActivity.getDeleted() != null) {
                    existingPastActivity.setDeleted(pastActivity.getDeleted());
                }

                return existingPastActivity;
            })
            .map(pastActivityRepository::save);
    }

    @Override
    @Transactional(readOnly = true)
    public List<PastActivity> findAll() {
        log.debug("Request to get all PastActivities");
        return pastActivityRepository.findAll();
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<PastActivity> findOne(Long id) {
        log.debug("Request to get PastActivity : {}", id);
        return pastActivityRepository.findById(id);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete PastActivity : {}", id);
        pastActivityRepository.deleteById(id);
    }
}
