import { IIdentity } from 'app/entities/identity/identity.model';

export interface AttendeeField {
  id?: number;
  identityNumber?: string;
  nameAndSurname?: string;
  status?: string;
  identity?: IIdentity;
}
