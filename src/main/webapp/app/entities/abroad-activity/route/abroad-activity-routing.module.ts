import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { AbroadActivityComponent } from '../list/abroad-activity.component';
import { AbroadActivityDetailComponent } from '../detail/abroad-activity-detail.component';
import { AbroadActivityUpdateComponent } from '../update/abroad-activity-update.component';
import { AbroadActivityRoutingResolveService } from './abroad-activity-routing-resolve.service';

const abroadActivityRoute: Routes = [
  {
    path: '',
    component: AbroadActivityComponent,
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: AbroadActivityDetailComponent,
    resolve: {
      abroadActivity: AbroadActivityRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: AbroadActivityUpdateComponent,
    resolve: {
      abroadActivity: AbroadActivityRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: AbroadActivityUpdateComponent,
    resolve: {
      abroadActivity: AbroadActivityRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
];

@NgModule({
  imports: [RouterModule.forChild(abroadActivityRoute)],
  exports: [RouterModule],
})
export class AbroadActivityRoutingModule {}
