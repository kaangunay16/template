import { Component } from '@angular/core';

@Component({
  selector: 'jhi-loading-dialog',
  templateUrl: './loading-dialog.component.html',
  styleUrls: ['./loading-dialog.component.scss'],
})
export class LoadingDialogComponent {}
