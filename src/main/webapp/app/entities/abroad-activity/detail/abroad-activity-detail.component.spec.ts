import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { AbroadActivityDetailComponent } from './abroad-activity-detail.component';

describe('AbroadActivity Management Detail Component', () => {
  let comp: AbroadActivityDetailComponent;
  let fixture: ComponentFixture<AbroadActivityDetailComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [AbroadActivityDetailComponent],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: { data: of({ abroadActivity: { id: 123 } }) },
        },
      ],
    })
      .overrideTemplate(AbroadActivityDetailComponent, '')
      .compileComponents();
    fixture = TestBed.createComponent(AbroadActivityDetailComponent);
    comp = fixture.componentInstance;
  });

  describe('OnInit', () => {
    it('Should load abroadActivity on init', () => {
      // WHEN
      comp.ngOnInit();

      // THEN
      expect(comp.abroadActivity).toEqual(expect.objectContaining({ id: 123 }));
    });
  });
});
