import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NgrxCounterInputComponent } from './ngrx-counter-input.component';

describe('NgrxCounterInputComponent', () => {
  let component: NgrxCounterInputComponent;
  let fixture: ComponentFixture<NgrxCounterInputComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [NgrxCounterInputComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NgrxCounterInputComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
