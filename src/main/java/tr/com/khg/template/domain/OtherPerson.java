package tr.com.khg.template.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;

/**
 * A OtherPerson.
 */
@Entity
@Table(name = "other_person")
public class OtherPerson implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @OneToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(unique = true)
    private Identity identity;

    @OneToMany(mappedBy = "otherPerson", cascade = CascadeType.ALL)
    @JsonIgnoreProperties(value = { "otherPerson" }, allowSetters = true)
    private Set<OtherPersonRecord> otherPersonRecords = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public OtherPerson id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Identity getIdentity() {
        return this.identity;
    }

    public void setIdentity(Identity identity) {
        this.identity = identity;
    }

    public OtherPerson identity(Identity identity) {
        this.setIdentity(identity);
        return this;
    }

    public Set<OtherPersonRecord> getOtherPersonRecords() {
        return this.otherPersonRecords;
    }

    public void setOtherPersonRecords(Set<OtherPersonRecord> otherPersonRecords) {
        if (this.otherPersonRecords != null) {
            this.otherPersonRecords.forEach(i -> i.setOtherPerson(null));
        }
        if (otherPersonRecords != null) {
            otherPersonRecords.forEach(i -> i.setOtherPerson(this));
        }
        this.otherPersonRecords = otherPersonRecords;
    }

    public OtherPerson otherPersonRecords(Set<OtherPersonRecord> otherPersonRecords) {
        this.setOtherPersonRecords(otherPersonRecords);
        return this;
    }

    public OtherPerson addOtherPersonRecord(OtherPersonRecord otherPersonRecord) {
        this.otherPersonRecords.add(otherPersonRecord);
        otherPersonRecord.setOtherPerson(this);
        return this;
    }

    public OtherPerson removeOtherPersonRecord(OtherPersonRecord otherPersonRecord) {
        this.otherPersonRecords.remove(otherPersonRecord);
        otherPersonRecord.setOtherPerson(null);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof OtherPerson)) {
            return false;
        }
        return id != null && id.equals(((OtherPerson) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "OtherPerson{" +
            "id=" + getId() +
            "}";
    }
}
