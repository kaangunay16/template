import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { NgrxCourseMainComponent } from './ngrx-course-main/ngrx-course-main.component';

const routes: Routes = [
  {
    path: '',
    component: NgrxCourseMainComponent,
    children: [
      {
        path: 'counter',
        loadChildren: () => import('./ngrx-course-counter/ngrx-course-counter.module').then(m => m.NgrxCourseCounterModule),
      },
      {
        path: 'posts',
        loadChildren: () => import('./ngrx-course-posts/ngrx-course-posts.module').then(m => m.NgrxCoursePostsModule),
      },
      {
        path: 'side-effects',
        loadChildren: () => import('./ngrx-course-side-effects/ngrx-course-side-effects.module').then(m => m.NgrxCourseSideEffectsModule),
      },
      {
        path: 'data',
        loadChildren: () => import('./ngrx-course-data/ngrx-course-data.module').then(m => m.NgrxCourseDataModule),
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class NgrxCourseRoutingModule {}
