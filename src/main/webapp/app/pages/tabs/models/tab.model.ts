import { Type } from '@angular/core';

import { ITabData } from './tab-data.model';

export interface ITab {
  id: number;
  title: string;
  tabData: ITabData;
  active: boolean;
  component: Type<any>;
}

export class Tab implements ITab {
  constructor(public id: number, public title: string, public tabData: ITabData, public active: boolean, public component: Type<any>) {}
}
