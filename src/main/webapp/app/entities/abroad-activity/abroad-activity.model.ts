import { IPerson } from 'app/entities/person/person.model';

import { HasId } from '../form-entity.model';

export interface IAbroadActivity extends HasId {
  id?: number;
  country?: string | null;
  startYear?: number | null;
  endYear?: number | null;
  position?: string | null;
  deleted?: boolean | null;
  person?: IPerson | null;
}

export class AbroadActivity implements IAbroadActivity {
  constructor(
    public id?: number,
    public country?: string | null,
    public startYear?: number | null,
    public endYear?: number | null,
    public position?: string | null,
    public deleted?: boolean | null,
    public person?: IPerson | null
  ) {
    this.deleted = this.deleted ?? false;
  }
}

export function getAbroadActivityIdentifier(abroadActivity: IAbroadActivity): number | undefined {
  return abroadActivity.id;
}
