import { createAction, props } from '@ngrx/store';
import { Update } from '@ngrx/entity';

import { Post } from '../models/post.model';

export const POST_ADD = '[post page] add post';
export const POST_UPDATE = '[post page] update post';
export const POST_DELETE = '[post page] delete post';

export const addPost = createAction(POST_ADD, props<{ post: Post }>());
// ngrx/entity kütüphanesi kullanulması nedeniyle Update<Post> kullanılmıştır.
// Effect içinden update çağırılıyorsa orada da güncellenmesi gerekmektedir.
export const updatePost = createAction(POST_UPDATE, props<{ post: Update<Post> }>());
export const deletePost = createAction(POST_DELETE, props<{ id: string }>());
