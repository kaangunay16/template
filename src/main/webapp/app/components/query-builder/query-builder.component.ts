import { Component, EventEmitter, Input, OnDestroy, OnInit, Output, ViewChild } from '@angular/core';
import { Subscription } from 'rxjs';

import { ParameterFieldComponent } from '../form-fields/parameter-field/parameter-field.component';
import { QueryBuilderService } from './query-builder.service';
import { Parameter } from '../models/parameter.model';
import { HasId } from 'app/entities/form-entity.model';
import { QueryBuilderConfig } from '../models/query-builder-config';
import { ErrorSupportService } from 'app/shared/services/error-support.service';

@Component({
  selector: 'jhi-query-builder',
  templateUrl: './query-builder.component.html',
  styleUrls: ['./query-builder.component.scss'],
})
export class QueryBuilderComponent implements OnInit, OnDestroy {
  @ViewChild('parameterField', { static: true })
  parameterField!: ParameterFieldComponent;

  @Input()
  config!: QueryBuilderConfig;

  @Output()
  resultChanged = new EventEmitter<HasId[] | null>();

  parameters!: Parameter[];

  parameterFieldSub!: Subscription;

  parametersSub!: Subscription;

  resultSub?: Subscription;

  constructor(private queryBuilderService: QueryBuilderService, private errorService: ErrorSupportService) {}

  ngOnInit(): void {
    this.parameterFieldSub = this.parameterField.parameterEmitter.subscribe(param => {
      this.queryBuilderService.addNewParameter(param);
    });

    this.parametersSub = this.queryBuilderService.parametersSub.subscribe(params => {
      this.parameters = params;
    });
  }

  ngOnDestroy(): void {
    this.parametersSub.unsubscribe();
    this.parameterFieldSub.unsubscribe();
    this.resultSub?.unsubscribe();
  }

  search(): void {
    this.resultSub = this.config.service.search({ parameters: this.parameters, joins: this.config.joins }).subscribe(
      result => {
        if (result.body) {
          this.resultChanged.emit(result.body);
        }
      },
      err => this.errorService.errorHandler(err)
    );
  }
}
