import { NgModule } from '@angular/core';
import { SharedModule } from 'app/shared/shared.module';
import { AbroadActivityComponent } from './list/abroad-activity.component';
import { AbroadActivityDetailComponent } from './detail/abroad-activity-detail.component';
import { AbroadActivityUpdateComponent } from './update/abroad-activity-update.component';
import { AbroadActivityDeleteDialogComponent } from './delete/abroad-activity-delete-dialog.component';
import { AbroadActivityRoutingModule } from './route/abroad-activity-routing.module';

@NgModule({
  imports: [SharedModule, AbroadActivityRoutingModule],
  declarations: [
    AbroadActivityComponent,
    AbroadActivityDetailComponent,
    AbroadActivityUpdateComponent,
    AbroadActivityDeleteDialogComponent,
  ],
  entryComponents: [AbroadActivityDeleteDialogComponent],
})
export class AbroadActivityModule {}
