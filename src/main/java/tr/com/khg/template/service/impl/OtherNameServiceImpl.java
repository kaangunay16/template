package tr.com.khg.template.service.impl;

import java.util.List;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tr.com.khg.template.domain.OtherName;
import tr.com.khg.template.repository.OtherNameRepository;
import tr.com.khg.template.service.OtherNameService;

/**
 * Service Implementation for managing {@link OtherName}.
 */
@Service
@Transactional
public class OtherNameServiceImpl implements OtherNameService {

    private final Logger log = LoggerFactory.getLogger(OtherNameServiceImpl.class);

    private final OtherNameRepository otherNameRepository;

    public OtherNameServiceImpl(OtherNameRepository otherNameRepository) {
        this.otherNameRepository = otherNameRepository;
    }

    @Override
    public OtherName save(OtherName otherName) {
        log.debug("Request to save OtherName : {}", otherName);
        return otherNameRepository.save(otherName);
    }

    @Override
    public Optional<OtherName> partialUpdate(OtherName otherName) {
        log.debug("Request to partially update OtherName : {}", otherName);

        return otherNameRepository
            .findById(otherName.getId())
            .map(existingOtherName -> {
                if (otherName.getName() != null) {
                    existingOtherName.setName(otherName.getName());
                }

                return existingOtherName;
            })
            .map(otherNameRepository::save);
    }

    @Override
    @Transactional(readOnly = true)
    public List<OtherName> findAll() {
        log.debug("Request to get all OtherNames");
        return otherNameRepository.findAll();
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<OtherName> findOne(Long id) {
        log.debug("Request to get OtherName : {}", id);
        return otherNameRepository.findById(id);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete OtherName : {}", id);
        otherNameRepository.deleteById(id);
    }
}
