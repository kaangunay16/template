import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { tap } from 'rxjs/operators';

import { ISelection } from 'app/entities/selection/selection.model';
import { SelectionService } from 'app/entities/selection/service/selection.service';

@Injectable({
  providedIn: 'root',
})
export class SelectionsRetrieverService {
  cachedValues: [{ key: string; values: ISelection[] }?] = [];
  educations: ISelection[] = [];

  constructor(private selectionService: SelectionService) {}

  getOptions(optionType: string | null | undefined): Observable<ISelection[]> {
    if (!optionType) {
      return of([]);
    }

    const cache = this.cachedValues.find(cachedValue => cachedValue?.key === optionType);

    if (!cache) {
      return this.selectionService.findByKey(optionType).pipe(
        tap(response => {
          this.cachedValues.push({ key: optionType, values: response });
        })
      );
    }

    return of(cache.values);
  }
}
