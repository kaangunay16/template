import { NgModule } from '@angular/core';
import { SharedModule } from 'app/shared/shared.module';
import { PastActivityComponent } from './list/past-activity.component';
import { PastActivityDetailComponent } from './detail/past-activity-detail.component';
import { PastActivityUpdateComponent } from './update/past-activity-update.component';
import { PastActivityDeleteDialogComponent } from './delete/past-activity-delete-dialog.component';
import { PastActivityRoutingModule } from './route/past-activity-routing.module';

@NgModule({
  imports: [SharedModule, PastActivityRoutingModule],
  declarations: [PastActivityComponent, PastActivityDetailComponent, PastActivityUpdateComponent, PastActivityDeleteDialogComponent],
  entryComponents: [PastActivityDeleteDialogComponent],
})
export class PastActivityModule {}
