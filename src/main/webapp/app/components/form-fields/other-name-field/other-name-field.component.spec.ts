import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OtherNameFieldComponent } from './other-name-field.component';

describe('OtherNameFieldComponent', () => {
  let component: OtherNameFieldComponent;
  let fixture: ComponentFixture<OtherNameFieldComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [OtherNameFieldComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OtherNameFieldComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
