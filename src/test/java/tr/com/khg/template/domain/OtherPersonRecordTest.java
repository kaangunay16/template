package tr.com.khg.template.domain;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import tr.com.khg.template.web.rest.TestUtil;

class OtherPersonRecordTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(OtherPersonRecord.class);
        OtherPersonRecord otherPersonRecord1 = new OtherPersonRecord();
        otherPersonRecord1.setId(1L);
        OtherPersonRecord otherPersonRecord2 = new OtherPersonRecord();
        otherPersonRecord2.setId(otherPersonRecord1.getId());
        assertThat(otherPersonRecord1).isEqualTo(otherPersonRecord2);
        otherPersonRecord2.setId(2L);
        assertThat(otherPersonRecord1).isNotEqualTo(otherPersonRecord2);
        otherPersonRecord1.setId(null);
        assertThat(otherPersonRecord1).isNotEqualTo(otherPersonRecord2);
    }
}
