import { IMeeting } from 'app/entities/meeting/meeting.model';
import { IIdentity } from 'app/entities/identity/identity.model';
import { HasId } from '../form-entity.model';

export interface IMeetingAttendee extends HasId {
  id?: number;
  status?: string | null;
  deleted?: boolean | null;
  meeting?: IMeeting | null;
  identity?: IIdentity | null;
}

export class MeetingAttendee implements IMeetingAttendee {
  constructor(
    public id?: number,
    public status?: string | null,
    public deleted?: boolean | null,
    public meeting?: IMeeting | null,
    public identity?: IIdentity | null
  ) {
    this.deleted = this.deleted ?? false;
  }
}

export function getMeetingAttendeeIdentifier(meetingAttendee: IMeetingAttendee): number | undefined {
  return meetingAttendee.id;
}
