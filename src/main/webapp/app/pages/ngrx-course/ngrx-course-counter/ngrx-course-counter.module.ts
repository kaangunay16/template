import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StoreModule } from '@ngrx/store';

import { SharedModule } from '../../../shared/shared.module';
import { counterReducer } from './state/counter.reducer';
import { COUNTER_STATE_NAME } from './state/counter.selectors';
import { NgrxCourseCounterRoutingModule } from './ngrx-course-counter-routing.module';
import { NgrxCounterComponent } from './ngrx-counter/ngrx-counter.component';
import { NgrxCounterButtonsComponent } from './ngrx-counter-buttons/ngrx-counter-buttons.component';
import { NgrxCounterOutputComponent } from './ngrx-counter-output/ngrx-counter-output.component';
import { NgrxCounterInputComponent } from './ngrx-counter-input/ngrx-counter-input.component';

@NgModule({
  declarations: [NgrxCounterComponent, NgrxCounterButtonsComponent, NgrxCounterOutputComponent, NgrxCounterInputComponent],
  imports: [CommonModule, SharedModule, NgrxCourseCounterRoutingModule, StoreModule.forFeature(COUNTER_STATE_NAME, counterReducer)],
})
export class NgrxCourseCounterModule {}
