package tr.com.khg.template.service;

import java.util.List;
import java.util.Optional;
import tr.com.khg.template.domain.OtherPerson;
import tr.com.khg.template.domain.request.Parameter;
import tr.com.khg.template.domain.request.Query;

/**
 * Service Interface for managing {@link OtherPerson}.
 */
public interface OtherPersonService {
    /**
     * Save a otherPerson.
     *
     * @param otherPerson the entity to save.
     * @return the persisted entity.
     */
    OtherPerson save(OtherPerson otherPerson);

    /**
     * Partially updates a otherPerson.
     *
     * @param otherPerson the entity to update partially.
     * @return the persisted entity.
     */
    Optional<OtherPerson> partialUpdate(OtherPerson otherPerson);

    /**
     * Get all the otherPeople.
     *
     * @return the list of entities.
     */
    List<OtherPerson> findAll();

    /**
     * Get the "id" otherPerson.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<OtherPerson> findOne(Long id);

    /**
     * Delete the "id" otherPerson.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);

    /**
     * Get other person who has "identity number"
     *
     * @param identityNo identity number of the person
     * @return the other person who has the identity number
     */
    Optional<OtherPerson> findByIdentityNumber(String identityNo);

    /**
     * Search persons with parameters
     *
     * @param query searching parameters
     * @return found other persons
     */
    List<OtherPerson> filterPerson(Query query);

    /**
     * Get the other person has the record which has the "id"
     *
     * @param recordId id of the record
     * @return the other person
     */
    Optional<OtherPerson> findByRecordId(Long recordId);

    /**
     * Delete entity with record id
     *
     * @param recordId id of the record
     */
    void deleteWithRecordId(Long recordId);

    /**
     * Returns existing other person identity numbers from given list
     *
     * @param identityNumbers checking identity numbers
     * @param city the city of other person record
     * @return existing other person's identity numbers
     */
    List<String> checkOtherPersonIfExists(List<String> identityNumbers, String city);
}
