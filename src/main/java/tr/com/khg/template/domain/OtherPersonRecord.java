package tr.com.khg.template.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import javax.persistence.*;

/**
 * A OtherPersonRecord.
 */
@Entity
@Table(name = "op_record")
public class OtherPersonRecord implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @Column(name = "city")
    private String city;

    @Column(name = "rank")
    private String rank;

    @Column(name = "section")
    private String section;

    @Column(name = "meeting_status")
    private String meetingStatus;

    @Column(name = "status")
    private String status;

    @Column(name = "speaker")
    private Boolean speaker;

    @Column(name = "added_by_unit")
    private String addedByUnit;

    @Column(name = "profession")
    private String profession;

    @Column(name = "document")
    private String document;

    @Column(name = "jhi_type")
    private String type;

    @ManyToOne
    @JsonIgnoreProperties(value = { "identity", "otherPersonRecords" }, allowSetters = true)
    private OtherPerson otherPerson;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public OtherPersonRecord id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCity() {
        return this.city;
    }

    public OtherPersonRecord city(String city) {
        this.setCity(city);
        return this;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getRank() {
        return this.rank;
    }

    public OtherPersonRecord rank(String rank) {
        this.setRank(rank);
        return this;
    }

    public void setRank(String rank) {
        this.rank = rank;
    }

    public String getSection() {
        return this.section;
    }

    public OtherPersonRecord section(String section) {
        this.setSection(section);
        return this;
    }

    public void setSection(String section) {
        this.section = section;
    }

    public String getMeetingStatus() {
        return this.meetingStatus;
    }

    public OtherPersonRecord meetingStatus(String meetingStatus) {
        this.setMeetingStatus(meetingStatus);
        return this;
    }

    public void setMeetingStatus(String meetingStatus) {
        this.meetingStatus = meetingStatus;
    }

    public String getStatus() {
        return this.status;
    }

    public OtherPersonRecord status(String status) {
        this.setStatus(status);
        return this;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Boolean getSpeaker() {
        return this.speaker;
    }

    public OtherPersonRecord speaker(Boolean speaker) {
        this.setSpeaker(speaker);
        return this;
    }

    public void setSpeaker(Boolean speaker) {
        this.speaker = speaker;
    }

    public String getAddedByUnit() {
        return this.addedByUnit;
    }

    public OtherPersonRecord addedByUnit(String addedByUnit) {
        this.setAddedByUnit(addedByUnit);
        return this;
    }

    public void setAddedByUnit(String addedByUnit) {
        this.addedByUnit = addedByUnit;
    }

    public String getProfession() {
        return this.profession;
    }

    public OtherPersonRecord profession(String profession) {
        this.setProfession(profession);
        return this;
    }

    public void setProfession(String profession) {
        this.profession = profession;
    }

    public String getDocument() {
        return this.document;
    }

    public OtherPersonRecord document(String document) {
        this.setDocument(document);
        return this;
    }

    public void setDocument(String document) {
        this.document = document;
    }

    public String getType() {
        return this.type;
    }

    public OtherPersonRecord type(String type) {
        this.setType(type);
        return this;
    }

    public void setType(String type) {
        this.type = type;
    }

    public OtherPerson getOtherPerson() {
        return this.otherPerson;
    }

    public void setOtherPerson(OtherPerson otherPerson) {
        this.otherPerson = otherPerson;
    }

    public OtherPersonRecord otherPerson(OtherPerson otherPerson) {
        this.setOtherPerson(otherPerson);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof OtherPersonRecord)) {
            return false;
        }
        return id != null && id.equals(((OtherPersonRecord) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "OtherPersonRecord{" +
            "id=" + getId() +
            ", city='" + getCity() + "'" +
            ", rank='" + getRank() + "'" +
            ", section='" + getSection() + "'" +
            ", meetingStatus='" + getMeetingStatus() + "'" +
            ", status='" + getStatus() + "'" +
            ", speaker='" + getSpeaker() + "'" +
            ", addedByUnit='" + getAddedByUnit() + "'" +
            ", profession='" + getProfession() + "'" +
            ", document='" + getDocument() + "'" +
            ", type='" + getType() + "'" +
            "}";
    }
}
