package tr.com.khg.template.service.impl;

import java.util.List;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tr.com.khg.template.domain.Selection;
import tr.com.khg.template.repository.SelectionRepository;
import tr.com.khg.template.service.SelectionService;

/**
 * Service Implementation for managing {@link Selection}.
 */
@Service
@Transactional
public class SelectionServiceImpl implements SelectionService {

    private final Logger log = LoggerFactory.getLogger(SelectionServiceImpl.class);

    private final SelectionRepository selectionRepository;

    public SelectionServiceImpl(SelectionRepository selectionRepository) {
        this.selectionRepository = selectionRepository;
    }

    @Override
    public Selection save(Selection selection) {
        log.debug("Request to save Selection : {}", selection);
        return selectionRepository.save(selection);
    }

    @Override
    public Optional<Selection> partialUpdate(Selection selection) {
        log.debug("Request to partially update Selection : {}", selection);

        return selectionRepository
            .findById(selection.getId())
            .map(existingSelection -> {
                if (selection.getKey() != null) {
                    existingSelection.setKey(selection.getKey());
                }
                if (selection.getValue() != null) {
                    existingSelection.setValue(selection.getValue());
                }
                if (selection.getPriority() != null) {
                    existingSelection.setPriority(selection.getPriority());
                }

                return existingSelection;
            })
            .map(selectionRepository::save);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Selection> findAll() {
        log.debug("Request to get all Selections");
        return selectionRepository.findAll();
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<Selection> findOne(Long id) {
        log.debug("Request to get Selection : {}", id);
        return selectionRepository.findById(id);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete Selection : {}", id);
        selectionRepository.deleteById(id);
    }

    @Override
    public List<Selection> findByKey(String key) {
        return selectionRepository.findByKeyOrderByPriority(key);
    }
}
