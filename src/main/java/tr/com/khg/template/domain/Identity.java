package tr.com.khg.template.domain;

import java.io.Serializable;
import java.time.LocalDate;
import javax.persistence.*;
import tr.com.khg.template.domain.enumeration.Genders;
import tr.com.khg.template.domain.enumeration.MaritalStatus;

/**
 * A Identity.
 */
@Entity
@Table(name = "identity")
public class Identity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "surname")
    private String surname;

    @Enumerated(EnumType.STRING)
    @Column(name = "gender")
    private Genders gender;

    @Column(name = "place_of_birth")
    private String placeOfBirth;

    @Column(name = "registered_city")
    private String registeredCity;

    @Column(name = "registered_district")
    private String registeredDistrict;

    @Column(name = "registered_village")
    private String registeredVillage;

    @Column(name = "mother_name")
    private String motherName;

    @Column(name = "father_name")
    private String fatherName;

    @Enumerated(EnumType.STRING)
    @Column(name = "marital_status")
    private MaritalStatus maritalStatus;

    @Column(name = "education")
    private String education;

    @Column(name = "address")
    private String address;

    @Column(name = "profession")
    private String profession;

    @Column(name = "birthday")
    private LocalDate birthday;

    @Column(name = "identity_no", unique = true)
    private String identityNo;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public Identity id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public Identity name(String name) {
        this.setName(name);
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return this.surname;
    }

    public Identity surname(String surname) {
        this.setSurname(surname);
        return this;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public Genders getGender() {
        return this.gender;
    }

    public Identity gender(Genders gender) {
        this.setGender(gender);
        return this;
    }

    public void setGender(Genders gender) {
        this.gender = gender;
    }

    public String getPlaceOfBirth() {
        return this.placeOfBirth;
    }

    public Identity placeOfBirth(String placeOfBirth) {
        this.setPlaceOfBirth(placeOfBirth);
        return this;
    }

    public void setPlaceOfBirth(String placeOfBirth) {
        this.placeOfBirth = placeOfBirth;
    }

    public String getRegisteredCity() {
        return this.registeredCity;
    }

    public Identity registeredCity(String registeredCity) {
        this.setRegisteredCity(registeredCity);
        return this;
    }

    public void setRegisteredCity(String registeredCity) {
        this.registeredCity = registeredCity;
    }

    public String getRegisteredDistrict() {
        return this.registeredDistrict;
    }

    public Identity registeredDistrict(String registeredDistrict) {
        this.setRegisteredDistrict(registeredDistrict);
        return this;
    }

    public void setRegisteredDistrict(String registeredDistrict) {
        this.registeredDistrict = registeredDistrict;
    }

    public String getRegisteredVillage() {
        return this.registeredVillage;
    }

    public Identity registeredVillage(String registeredVillage) {
        this.setRegisteredVillage(registeredVillage);
        return this;
    }

    public void setRegisteredVillage(String registeredVillage) {
        this.registeredVillage = registeredVillage;
    }

    public String getMotherName() {
        return this.motherName;
    }

    public Identity motherName(String motherName) {
        this.setMotherName(motherName);
        return this;
    }

    public void setMotherName(String motherName) {
        this.motherName = motherName;
    }

    public String getFatherName() {
        return this.fatherName;
    }

    public Identity fatherName(String fatherName) {
        this.setFatherName(fatherName);
        return this;
    }

    public void setFatherName(String fatherName) {
        this.fatherName = fatherName;
    }

    public MaritalStatus getMaritalStatus() {
        return this.maritalStatus;
    }

    public Identity maritalStatus(MaritalStatus maritalStatus) {
        this.setMaritalStatus(maritalStatus);
        return this;
    }

    public void setMaritalStatus(MaritalStatus maritalStatus) {
        this.maritalStatus = maritalStatus;
    }

    public String getEducation() {
        return this.education;
    }

    public Identity education(String education) {
        this.setEducation(education);
        return this;
    }

    public void setEducation(String education) {
        this.education = education;
    }

    public String getAddress() {
        return this.address;
    }

    public Identity address(String address) {
        this.setAddress(address);
        return this;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getProfession() {
        return this.profession;
    }

    public Identity profession(String profession) {
        this.setProfession(profession);
        return this;
    }

    public void setProfession(String profession) {
        this.profession = profession;
    }

    public LocalDate getBirthday() {
        return this.birthday;
    }

    public Identity birthday(LocalDate birthday) {
        this.setBirthday(birthday);
        return this;
    }

    public void setBirthday(LocalDate birthday) {
        this.birthday = birthday;
    }

    public String getIdentityNo() {
        return this.identityNo;
    }

    public Identity identityNo(String identityNo) {
        this.setIdentityNo(identityNo);
        return this;
    }

    public void setIdentityNo(String identityNo) {
        this.identityNo = identityNo;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Identity)) {
            return false;
        }
        return id != null && id.equals(((Identity) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Identity{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", surname='" + getSurname() + "'" +
            ", gender='" + getGender() + "'" +
            ", placeOfBirth='" + getPlaceOfBirth() + "'" +
            ", registeredCity='" + getRegisteredCity() + "'" +
            ", registeredDistrict='" + getRegisteredDistrict() + "'" +
            ", registeredVillage='" + getRegisteredVillage() + "'" +
            ", motherName='" + getMotherName() + "'" +
            ", fatherName='" + getFatherName() + "'" +
            ", maritalStatus='" + getMaritalStatus() + "'" +
            ", education='" + getEducation() + "'" +
            ", address='" + getAddress() + "'" +
            ", profession='" + getProfession() + "'" +
            ", birthday='" + getBirthday() + "'" +
            ", identityNo='" + getIdentityNo() + "'" +
            "}";
    }
}
