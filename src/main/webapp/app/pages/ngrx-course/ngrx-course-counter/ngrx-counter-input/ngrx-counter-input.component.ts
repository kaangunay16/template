import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';

import { customIncrement, textChanged } from '../state/counter.actions';
import { getText } from '../state/counter.selectors';
import { AppState } from '../../store/app.state';

@Component({
  selector: 'jhi-ngrx-counter-input',
  templateUrl: './ngrx-counter-input.component.html',
  styleUrls: ['./ngrx-counter-input.component.scss'],
})
export class NgrxCounterInputComponent implements OnInit {
  value = 1;
  text!: string;

  constructor(private store: Store<AppState>) {}

  ngOnInit(): void {
    this.store.select(getText).subscribe(data => {
      this.text = data;
    });
  }

  onAdd(): void {
    this.store.dispatch(customIncrement({ value: this.value }));
  }

  onChangeText(): void {
    this.store.dispatch(textChanged({ text: this.text }));
  }
}
