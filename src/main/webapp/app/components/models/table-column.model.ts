import { HasId } from 'app/entities/form-entity.model';

export interface TableColumn {
  column: string;
  parser?: (arg: HasId[]) => string;
}
