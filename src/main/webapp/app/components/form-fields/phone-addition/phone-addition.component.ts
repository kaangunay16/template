import { Component, Input, OnInit } from '@angular/core';
import { ControlValueAccessor, FormBuilder, FormGroup, NG_VALUE_ACCESSOR } from '@angular/forms';

import { PhoneService } from 'app/entities/phone/service/phone.service';
import { Phone } from 'app/entities/phone/phone.model';
import { DynamicFormFunctions } from 'app/components/dynamic-form-creator/dynamic-form-field.model';
import { DYNAMIC_FIELD } from 'app/components/dynamic-form-creator/dyanmic-form.token';
import { DynamicFormService } from 'app/components/dynamic-form-creator/dynamic-form.service';
import { PersonModuleService } from 'app/pages/person/person-module.service';
import { FormFieldsService } from '../form-fields.service';

@Component({
  selector: 'jhi-phone-addition',
  templateUrl: './phone-addition.component.html',
  styleUrls: ['./phone-addition.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: PhoneAdditionComponent,
      multi: true,
    },
    {
      provide: DYNAMIC_FIELD,
      useExisting: PhoneAdditionComponent,
    },
  ],
})
export class PhoneAdditionComponent implements OnInit, ControlValueAccessor, DynamicFormFunctions {
  form!: FormGroup;

  @Input()
  readOnly?: boolean = false;

  onTouched!: () => void;

  constructor(
    private formBuilder: FormBuilder,
    private formFieldsService: FormFieldsService,
    private service: PhoneService,
    private personModuleService: PersonModuleService,
    private dynamicFormService: DynamicFormService
  ) {}

  ngOnInit(): void {
    this.form = this.formBuilder.group({
      id: [{ value: null }],
      phoneNumber: [{ value: null, disabled: this.readOnly }],
      type: [{ value: null, disabled: this.readOnly }],
      startYear: [{ value: null, disabled: this.readOnly }],
      endYear: [{ value: null, disabled: this.readOnly }],
      deleted: [{ value: null }],
      person: [{ value: null }],
    });
  }

  registerOnChange(fn: (value: any) => void): void {
    this.form.valueChanges.subscribe(fn);
  }

  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

  writeValue(obj: Phone | null): void {
    this.form.patchValue({
      id: obj ? obj.id : null,
      phoneNumber: obj ? obj.phoneNumber : null,
      type: obj ? obj.type : null,
      startYear: obj ? obj.startYear : null,
      endYear: obj ? obj.endYear : null,
      deleted: obj ? obj.deleted : false,
      person: obj ? obj.person : null,
    });
  }

  remove(): void {
    const id = this.form.get('id')?.value;

    if (id) {
      this.personModuleService.removePhone(this.dynamicFormService.getEntityId(), id);
      this.formFieldsService.remove(id, this.service);
    }
  }
}
