import { IPerson } from 'app/entities/person/person.model';
import { HasId } from '../form-entity.model';

export interface IActualActivity extends HasId {
  id?: number;
  city?: string | null;
  position?: string | null;
  startYear?: number | null;
  deleted?: boolean | null;
  field?: string | null;
  person?: IPerson | null;
}

export class ActualActivity implements IActualActivity {
  constructor(
    public id?: number,
    public city?: string | null,
    public position?: string | null,
    public startYear?: number | null,
    public deleted?: boolean | null,
    public field?: string | null,
    public person?: IPerson | null
  ) {
    this.deleted = this.deleted ?? false;
  }
}

export function getActualActivityIdentifier(actualActivity: IActualActivity): number | undefined {
  return actualActivity.id;
}
