import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';

import { getCounter, getText } from '../state/counter.selectors';
import { AppState } from '../../store/app.state';

@Component({
  selector: 'jhi-ngrx-counter-output',
  templateUrl: './ngrx-counter-output.component.html',
  styleUrls: ['./ngrx-counter-output.component.scss'],
})
export class NgrxCounterOutputComponent implements OnInit {
  counter$!: Observable<number>;
  text$!: Observable<string>;

  constructor(private store: Store<AppState>) {}

  ngOnInit(): void {
    this.counter$ = this.store.select(getCounter);
    this.text$ = this.store.select(getText);
  }
}
