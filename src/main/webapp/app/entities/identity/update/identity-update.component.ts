import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';

import { IIdentity, Identity } from '../identity.model';
import { IdentityService } from '../service/identity.service';
import { Genders } from 'app/entities/enumerations/genders.model';
import { MaritalStatus } from 'app/entities/enumerations/marital-status.model';

@Component({
  selector: 'jhi-identity-update',
  templateUrl: './identity-update.component.html',
})
export class IdentityUpdateComponent implements OnInit {
  isSaving = false;
  gendersValues = Object.keys(Genders);
  maritalStatusValues = Object.keys(MaritalStatus);

  editForm = this.fb.group({
    id: [],
    name: [],
    surname: [],
    gender: [],
    placeOfBirth: [],
    registeredCity: [],
    registeredDistrict: [],
    registeredVillage: [],
    motherName: [],
    fatherName: [],
    maritalStatus: [],
    education: [],
    address: [],
    profession: [],
    birthday: [],
    identityNo: [],
  });

  constructor(protected identityService: IdentityService, protected activatedRoute: ActivatedRoute, protected fb: FormBuilder) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ identity }) => {
      this.updateForm(identity);
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const identity = this.createFromForm();
    if (identity.id !== undefined) {
      this.subscribeToSaveResponse(this.identityService.update(identity));
    } else {
      this.subscribeToSaveResponse(this.identityService.create(identity));
    }
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IIdentity>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(identity: IIdentity): void {
    this.editForm.patchValue({
      id: identity.id,
      name: identity.name,
      surname: identity.surname,
      gender: identity.gender,
      placeOfBirth: identity.placeOfBirth,
      registeredCity: identity.registeredCity,
      registeredDistrict: identity.registeredDistrict,
      registeredVillage: identity.registeredVillage,
      motherName: identity.motherName,
      fatherName: identity.fatherName,
      maritalStatus: identity.maritalStatus,
      education: identity.education,
      address: identity.address,
      profession: identity.profession,
      birthday: identity.birthday,
      identityNo: identity.identityNo,
    });
  }

  protected createFromForm(): IIdentity {
    return {
      ...new Identity(),
      id: this.editForm.get(['id'])!.value,
      name: this.editForm.get(['name'])!.value,
      surname: this.editForm.get(['surname'])!.value,
      gender: this.editForm.get(['gender'])!.value,
      placeOfBirth: this.editForm.get(['placeOfBirth'])!.value,
      registeredCity: this.editForm.get(['registeredCity'])!.value,
      registeredDistrict: this.editForm.get(['registeredDistrict'])!.value,
      registeredVillage: this.editForm.get(['registeredVillage'])!.value,
      motherName: this.editForm.get(['motherName'])!.value,
      fatherName: this.editForm.get(['fatherName'])!.value,
      maritalStatus: this.editForm.get(['maritalStatus'])!.value,
      education: this.editForm.get(['education'])!.value,
      address: this.editForm.get(['address'])!.value,
      profession: this.editForm.get(['profession'])!.value,
      birthday: this.editForm.get(['birthday'])!.value,
      identityNo: this.editForm.get(['identityNo'])!.value,
    };
  }
}
