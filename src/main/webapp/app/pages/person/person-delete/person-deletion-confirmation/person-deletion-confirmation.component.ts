import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';

import { DynamicFormCreatorModel, IDynamicFormCreatorModel } from 'app/components/dynamic-form-creator/dynamic-form-field.model';
import { FormValueConverterService } from 'app/components/dynamic-form-creator/form-value-converter.service';
import { PersonService } from 'app/entities/person/service/person.service';
import { IDemand } from 'app/entities/demand/demand.model';
import { DemandService } from 'app/entities/demand/service/demand.service';
import { ErrorSupportService } from 'app/shared/services/error-support.service';
import { DemandType } from 'app/entities/enumerations/demand-type.model';
import { EntityDemandService } from 'app/shared/services/entity-demand.service';

@Component({
  selector: 'jhi-person-deletion-confirmation',
  templateUrl: './person-deletion-confirmation.component.html',
  styleUrls: ['./person-deletion-confirmation.component.scss'],
})
export class PersonDeletionConfirmationComponent implements OnInit, OnDestroy {
  loading = true;
  demandSub!: Subscription;
  formCreator?: IDynamicFormCreatorModel;
  demand!: IDemand;
  updateSub?: Subscription;

  constructor(
    private activatedRoute: ActivatedRoute,
    private personService: PersonService,
    private errorService: ErrorSupportService,
    private entityDemandService: EntityDemandService,
    private demandService: DemandService,
    private formValueService: FormValueConverterService,
    private toastr: ToastrService
  ) {}

  ngOnInit(): void {
    this.demandSub = this.activatedRoute.queryParams
      .pipe(
        switchMap(params => {
          this.demand = this.entityDemandService.entityCheck(params);
          return this.personService.find(this.demand.entityId!);
        })
      )
      .subscribe(
        res => {
          if (res.body) {
            this.formCreator = new DynamicFormCreatorModel([
              {
                id: 'name',
                type: 'text',
                label: 'Talep Edilen Kayıt',
                value: `${res.body.identity!.name!} ${res.body.identity!.surname!} (TC:${res.body.identity!.identityNo!})`,
                readOnly: true,
              },
              {
                id: 'demandBy',
                type: 'text',
                label: 'Talep Eden',
                value: this.demand.demandedBy,
                readOnly: true,
              },
              {
                id: 'document',
                type: 'text',
                label: 'Evrak',
                value: this.demand.document,
                readOnly: true,
              },
              {
                id: 'description',
                type: 'textarea',
                label: 'Açıklama',
                validators: [Validators.required],
              },
            ]);
            this.loading = false;
          }
        },
        err => this.errorService.errorHandler(err)
      );
  }

  ngOnDestroy(): void {
    this.demandSub.unsubscribe();
    this.updateSub?.unsubscribe();
  }

  process(process: string): void {
    this.demand.status = (<any>DemandType)[process];
    this.demand.processDescription = this.formValueService.getFormValue(this.formCreator!, 'description') as string;
    this.updateSub = this.demandService.update(this.demand).subscribe(
      () => {
        this.toastr.success('Kayıt silme talebi sonuçlandırıldı.');
        this.entityDemandService.remove(this.demand.id!);
        window.history.back();
      },
      err => this.errorService.errorHandler(err)
    );
  }
}
