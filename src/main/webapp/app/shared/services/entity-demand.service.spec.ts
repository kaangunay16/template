import { TestBed } from '@angular/core/testing';

import { EntityDemandService } from './entity-demand.service';

describe('EntityDemandService', () => {
  let service: EntityDemandService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(EntityDemandService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
