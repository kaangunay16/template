package tr.com.khg.template.repository;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;
import tr.com.khg.template.domain.PastActivity;

/**
 * Spring Data SQL repository for the PastActivity entity.
 */
@SuppressWarnings("unused")
@Repository
public interface PastActivityRepository extends JpaRepository<PastActivity, Long> {}
