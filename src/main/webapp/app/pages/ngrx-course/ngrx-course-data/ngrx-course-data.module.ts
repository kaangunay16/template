import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EntityDataService } from '@ngrx/data';

import { SharedModule } from 'app/shared/shared.module';
import { DataPostDataService } from './data-post-data.service';
import { NgrxCourseDataRoutingModule } from './ngrx-course-data-routing.module';
import { NgrxCourseDataMainComponent } from './ngrx-course-data-main/ngrx-course-data-main.component';
import { NgrxDataPostListComponent } from './ngrx-data-post-list/ngrx-data-post-list.component';
import { NgrxDataSinglePostComponent } from './ngrx-data-single-post/ngrx-data-single-post.component';
import { NgrxDataEditPostComponent } from './ngrx-data-edit-post/ngrx-data-edit-post.component';
import { NgrxDataAddPostComponent } from './ngrx-data-add-post/ngrx-data-add-post.component';

@NgModule({
  declarations: [
    NgrxCourseDataMainComponent,
    NgrxDataPostListComponent,
    NgrxDataSinglePostComponent,
    NgrxDataEditPostComponent,
    NgrxDataAddPostComponent,
  ],
  imports: [CommonModule, SharedModule, NgrxCourseDataRoutingModule],
  providers: [DataPostDataService],
})
export class NgrxCourseDataModule {
  // Register custom data service
  constructor(entityDataService: EntityDataService, dataPostDataService: DataPostDataService) {
    entityDataService.registerService('Selection', dataPostDataService);
  }
}
