import * as dayjs from 'dayjs';
import { Genders } from 'app/entities/enumerations/genders.model';
import { MaritalStatus } from 'app/entities/enumerations/marital-status.model';
import { HasId } from '../form-entity.model';

export interface IIdentity extends HasId {
  id?: number;
  name?: string | null;
  surname?: string | null;
  gender?: Genders | null;
  placeOfBirth?: string | null;
  registeredCity?: string | null;
  registeredDistrict?: string | null;
  registeredVillage?: string | null;
  motherName?: string | null;
  fatherName?: string | null;
  maritalStatus?: MaritalStatus | null;
  education?: string | null;
  address?: string | null;
  profession?: string | null;
  birthday?: dayjs.Dayjs | null;
  identityNo?: string | null;
}

export class Identity implements IIdentity {
  constructor(
    public id?: number,
    public name?: string | null,
    public surname?: string | null,
    public gender?: Genders | null,
    public placeOfBirth?: string | null,
    public registeredCity?: string | null,
    public registeredDistrict?: string | null,
    public registeredVillage?: string | null,
    public motherName?: string | null,
    public fatherName?: string | null,
    public maritalStatus?: MaritalStatus | null,
    public education?: string | null,
    public address?: string | null,
    public profession?: string | null,
    public birthday?: dayjs.Dayjs | null,
    public identityNo?: string | null
  ) {}
}

export function getIdentityIdentifier(identity: IIdentity): number | undefined {
  return identity.id;
}
