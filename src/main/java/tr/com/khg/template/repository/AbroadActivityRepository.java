package tr.com.khg.template.repository;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;
import tr.com.khg.template.domain.AbroadActivity;

/**
 * Spring Data SQL repository for the AbroadActivity entity.
 */
@SuppressWarnings("unused")
@Repository
public interface AbroadActivityRepository extends JpaRepository<AbroadActivity, Long> {}
