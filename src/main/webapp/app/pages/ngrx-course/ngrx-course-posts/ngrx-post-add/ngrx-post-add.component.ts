import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Store } from '@ngrx/store';

import { Post } from '../models/post.model';
import { AppState } from '../../store/app.state';
import { addPost } from '../state/posts.actions';

@Component({
  selector: 'jhi-ngrx-post-add',
  templateUrl: './ngrx-post-add.component.html',
  styleUrls: ['./ngrx-post-add.component.scss'],
})
export class NgrxPostAddComponent implements OnInit {
  postForm!: FormGroup;

  constructor(private store: Store<AppState>) {}

  ngOnInit(): void {
    this.postForm = new FormGroup({
      title: new FormControl(null, [Validators.required, Validators.minLength(6)]),
      description: new FormControl(null, [Validators.required, Validators.minLength(10)]),
    });
  }

  onSubmit(): void {
    if (this.postForm.valid) {
      const post: Post = {
        title: this.postForm.get('title')?.value,
        description: this.postForm.get('description')?.value,
      };

      this.store.dispatch(addPost({ post }));
    }
  }
}
