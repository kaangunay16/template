import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { of } from 'rxjs';

import { OtherPersonService } from '../service/other-person.service';

import { OtherPersonComponent } from './other-person.component';

describe('OtherPerson Management Component', () => {
  let comp: OtherPersonComponent;
  let fixture: ComponentFixture<OtherPersonComponent>;
  let service: OtherPersonService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      declarations: [OtherPersonComponent],
    })
      .overrideTemplate(OtherPersonComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(OtherPersonComponent);
    comp = fixture.componentInstance;
    service = TestBed.inject(OtherPersonService);

    const headers = new HttpHeaders();
    jest.spyOn(service, 'query').mockReturnValue(
      of(
        new HttpResponse({
          body: [{ id: 123 }],
          headers,
        })
      )
    );
  });

  it('Should call load all on init', () => {
    // WHEN
    comp.ngOnInit();

    // THEN
    expect(service.query).toHaveBeenCalled();
    expect(comp.otherPeople?.[0]).toEqual(expect.objectContaining({ id: 123 }));
  });
});
