import { Component, Input, OnInit } from '@angular/core';
import * as uuid from 'uuid';

import { ITabData } from 'app/pages/tabs/models/tab-data.model';
import { TabsService } from '../tabs.service';

@Component({
  selector: 'jhi-tab1',
  templateUrl: './tab1.component.html',
  styleUrls: ['./tab1.component.scss'],
})
export class Tab1Component implements OnInit {
  @Input()
  tabData!: ITabData;

  id!: string;

  constructor(private tabsService: TabsService) {}

  ngOnInit(): void {
    this.id = uuid.v4();
  }

  generateNewTab(): void {
    const id = uuid.v4();
    this.tabsService.addNewTab({
      title: id.substr(0, 5),
      tabData: { tabString: id },
      id: 3,
      active: true,
      component: Tab1Component,
    });
  }
}
