import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { switchMap } from 'rxjs/operators';

import { IMeeting } from 'app/entities/meeting/meeting.model';
import { MeetingService } from 'app/entities/meeting/service/meeting.service';
import { TableConfig } from 'app/components/models/table-config.model';
import { ErrorSupportService } from 'app/shared/services/error-support.service';
import { MeetingModuleService } from '../meeting-module.service';
import { finalizedActions, meetingTableConfigs, unfinalizedActions } from '../meeting-components.config';

@Component({
  selector: 'jhi-meeting-list',
  templateUrl: './meeting-list.component.html',
  styleUrls: ['./meeting-list.component.scss'],
})
export class MeetingListComponent implements OnInit, OnDestroy {
  tableConfig?: TableConfig;
  meetings!: IMeeting[];
  loading = true;
  finalized = false;
  routeSub!: Subscription;

  constructor(
    private meetingService: MeetingService,
    private meetingModuleService: MeetingModuleService,
    private errorService: ErrorSupportService,
    private activatedRoute: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.routeSub = this.activatedRoute.queryParams
      .pipe(
        switchMap(params => {
          this.finalized = params['mode'] !== 'unfinalized';
          return this.meetingService.getWithFinalizedStatus(this.finalized);
        })
      )
      .subscribe(
        res => {
          if (res.body) {
            this.loading = false;
            this.meetings = res.body;
            this.meetingModuleService.entities = this.meetings;

            this.tableConfig = {
              columns: meetingTableConfigs.columns,
              data: this.meetings,
              actions: this.finalized ? finalizedActions : unfinalizedActions,
            };
          }
        },
        err => this.errorService.errorHandler(err)
      );
  }

  ngOnDestroy(): void {
    this.routeSub.unsubscribe();
  }
}
