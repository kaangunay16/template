package tr.com.khg.template.service;

import java.util.List;
import java.util.Optional;
import tr.com.khg.template.domain.Selection;

/**
 * Service Interface for managing {@link Selection}.
 */
public interface SelectionService {
    /**
     * Save a selection.
     *
     * @param selection the entity to save.
     * @return the persisted entity.
     */
    Selection save(Selection selection);

    /**
     * Partially updates a selection.
     *
     * @param selection the entity to update partially.
     * @return the persisted entity.
     */
    Optional<Selection> partialUpdate(Selection selection);

    /**
     * Get all the selections.
     *
     * @return the list of entities.
     */
    List<Selection> findAll();

    /**
     * Get the "id" selection.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<Selection> findOne(Long id);

    /**
     * Delete the "id" selection.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);

    /**
     * Get all values have the given key
     *
     * @param key the key of values
     * @return value list
     */
    List<Selection> findByKey(String key);
}
