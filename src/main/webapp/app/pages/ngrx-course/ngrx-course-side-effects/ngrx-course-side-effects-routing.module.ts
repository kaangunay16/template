import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { NgrxApiCallComponent } from './ngrx-api-call/ngrx-api-call.component';
import { NgrxSingleCallComponent } from './ngrx-single-call/ngrx-single-call.component';

const routes: Routes = [
  {
    path: '',
    component: NgrxApiCallComponent,
  },
  {
    path: 'side/:id',
    component: NgrxSingleCallComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class NgrxCourseSideEffectsRoutingModule {}
