package tr.com.khg.template.service.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.NOT_ACCEPTABLE, reason = "Aynı Kayıt")
public class AlreadyExistSameOtherPersonRecord extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public AlreadyExistSameOtherPersonRecord() {
        super("Şahsın aynı ilde kaydı bulumaktadır.");
    }
}
