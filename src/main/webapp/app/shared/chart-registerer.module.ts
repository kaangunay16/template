import { NgModule } from '@angular/core';
import { CategoryScale, Chart, LinearScale, LineController, LineElement, PointElement } from 'chart.js';

@NgModule()
export class ChartRegistererModule {
  constructor() {
    Chart.register(LineController, CategoryScale, LinearScale, PointElement, LineElement);
  }
}
