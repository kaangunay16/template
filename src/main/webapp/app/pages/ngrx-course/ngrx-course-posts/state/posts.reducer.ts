import { Action, createReducer, on } from '@ngrx/store';
import * as uuid from 'uuid';

import { initialState, postAdapter } from './posts.state';
import { addPost, deletePost, updatePost } from './posts.actions';

const _postsReducer = createReducer(
  initialState,
  on(addPost, (state, action) => postAdapter.addOne({ ...action.post, id: uuid.v4() }, { ...state, count: state.count + 1 })),
  on(updatePost, (state, action) => postAdapter.updateOne(action.post, state)),
  on(deletePost, (state, { id }) => postAdapter.removeOne(id, { ...state, count: state.count - 1 }))
);

export function postsReducer(state: any, action: Action): any {
  return _postsReducer(state, action);
}
