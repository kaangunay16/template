import { createFeatureSelector, createSelector } from '@ngrx/store';

import { SideEffectState } from './side-effect.state';

export const SIDE_EFFECT_STATE_NAME = 'effects';

const getSideEffectSelector = createFeatureSelector<SideEffectState>(SIDE_EFFECT_STATE_NAME);

export const getSelections = createSelector(getSideEffectSelector, state => state.selections);
