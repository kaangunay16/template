import { TableAction } from '../table/table-action.model';
import { ParamType } from './param-type.model';
import { TableColumn } from './table-column.model';

export interface PageTableConfigs {
  columns: TableColumn[];
  actions: TableAction[];
  searchParameters: ParamType[];
}
