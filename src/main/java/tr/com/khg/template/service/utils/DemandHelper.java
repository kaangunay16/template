package tr.com.khg.template.service.utils;

import org.springframework.stereotype.Service;
import tr.com.khg.template.domain.Demand;
import tr.com.khg.template.service.OtherPersonService;
import tr.com.khg.template.service.PersonService;
import tr.com.khg.template.service.exceptions.UnknownDemandedEntity;

@Service
public class DemandHelper {

    private final PersonService personService;

    private final OtherPersonService otherPersonService;

    public DemandHelper(PersonService personService, OtherPersonService otherPersonService) {
        this.personService = personService;
        this.otherPersonService = otherPersonService;
    }

    public void deleteDemandedEntity(Demand demand) {
        switch (demand.getDemandType()) {
            case "PERSON_DELETION":
                personService.delete(demand.getEntityId());
                break;
            case "OTHER_PERSON_DELETION":
                otherPersonService.deleteWithRecordId(demand.getEntityId());
                break;
            default:
                throw new UnknownDemandedEntity();
        }
    }
}
