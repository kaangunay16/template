import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { Validators } from '@angular/forms';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { Subscription } from 'rxjs';
import { ToastrService } from 'ngx-toastr';

import { DynamicFormCreatorModel, IDynamicFormCreatorModel } from '../dynamic-form-creator/dynamic-form-field.model';
import { LoadingDialogComponent } from '../loading-dialog/loading-dialog.component';
import { FormValueConverterService } from '../dynamic-form-creator/form-value-converter.service';

@Component({
  selector: 'jhi-identity-retriever',
  templateUrl: './identity-retriever.component.html',
  styleUrls: ['./identity-retriever.component.scss'],
})
export class IdentityRetrieverComponent implements OnInit {
  formCreator!: IDynamicFormCreatorModel;

  dialogRef?: MatDialogRef<LoadingDialogComponent>;

  identitySub?: Subscription;

  @Output()
  identityChanged = new EventEmitter<string>();

  constructor(private toastr: ToastrService, private dialog: MatDialog, private formValueService: FormValueConverterService) {}

  ngOnInit(): void {
    this.formCreator = new DynamicFormCreatorModel([
      {
        id: 'identity',
        type: 'text',
        label: 'Kimlik Numarası',
        validators: [Validators.pattern(/^[1-9][0-9]{9}[02468]$/)],
      },
    ]);
  }

  submit(): void {
    if (this.formCreator.form.untouched) {
      return;
    }

    this.dialogRef = this.dialog.open(LoadingDialogComponent, {
      panelClass: 'transparent',
      disableClose: false,
    });
    const tckNo = this.formValueService.getFormValue(this.formCreator, 'identity') as string;

    if (!tckNo) {
      this.dialogRef.close();
      this.toastr.error('Lütfen bir kimlik numarası giriniz.', 'Hata');
      return;
    }

    this.identityChanged.emit(tckNo);
  }
}
