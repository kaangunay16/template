package tr.com.khg.template.web.rest;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import tr.com.khg.template.domain.Identity;
import tr.com.khg.template.service.IdentityRetrieverService;

@RestController
@RequestMapping("/api")
public class IdentityRetrieverResource {

    private final IdentityRetrieverService identityRetrieverService;

    public IdentityRetrieverResource(IdentityRetrieverService identityRetrieverService) {
        this.identityRetrieverService = identityRetrieverService;
    }

    @GetMapping("/identities/identity={identityNumber}")
    ResponseEntity<Identity> getIdentityInfo(@PathVariable String identityNumber) {
        return ResponseEntity.ok().body(identityRetrieverService.getIdentity(identityNumber));
    }
}
