import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { MeetingAttendeeDetailComponent } from './meeting-attendee-detail.component';

describe('MeetingAttendee Management Detail Component', () => {
  let comp: MeetingAttendeeDetailComponent;
  let fixture: ComponentFixture<MeetingAttendeeDetailComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [MeetingAttendeeDetailComponent],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: { data: of({ meetingAttendee: { id: 123 } }) },
        },
      ],
    })
      .overrideTemplate(MeetingAttendeeDetailComponent, '')
      .compileComponents();
    fixture = TestBed.createComponent(MeetingAttendeeDetailComponent);
    comp = fixture.componentInstance;
  });

  describe('OnInit', () => {
    it('Should load meetingAttendee on init', () => {
      // WHEN
      comp.ngOnInit();

      // THEN
      expect(comp.meetingAttendee).toEqual(expect.objectContaining({ id: 123 }));
    });
  });
});
