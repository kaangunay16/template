import { NgModule } from '@angular/core';
import { SharedModule } from 'app/shared/shared.module';
import { ActualActivityComponent } from './list/actual-activity.component';
import { ActualActivityDetailComponent } from './detail/actual-activity-detail.component';
import { ActualActivityUpdateComponent } from './update/actual-activity-update.component';
import { ActualActivityDeleteDialogComponent } from './delete/actual-activity-delete-dialog.component';
import { ActualActivityRoutingModule } from './route/actual-activity-routing.module';

@NgModule({
  imports: [SharedModule, ActualActivityRoutingModule],
  declarations: [
    ActualActivityComponent,
    ActualActivityDetailComponent,
    ActualActivityUpdateComponent,
    ActualActivityDeleteDialogComponent,
  ],
  entryComponents: [ActualActivityDeleteDialogComponent],
})
export class ActualActivityModule {}
