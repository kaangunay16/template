import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class MainService {
  sideNavOption = false;
  sideNavOptionSub = new BehaviorSubject<boolean>(this.sideNavOption);

  changeSideNav(status?: boolean): void {
    if (status !== undefined) {
      this.sideNavOption = status;
    } else {
      this.sideNavOption = !this.sideNavOption;
    }
    this.sideNavOptionSub.next(this.sideNavOption);
  }
}
