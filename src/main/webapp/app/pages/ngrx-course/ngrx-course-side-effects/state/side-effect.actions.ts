import { createAction, props } from '@ngrx/store';

import { ISelection } from 'app/entities/selection/selection.model';

export const SELECTIONS_REQUEST_START = '[side-effects page] selections request start';
export const SELECTIONS_SUCCESS = '[side-effects page] selections request success';
export const SELECTIONS_FAIL = '[side-effects page] selections request fail';

export const selectionRequestStart = createAction(SELECTIONS_REQUEST_START, props<{ selectionType: string }>());
export const selectionRequestSuccess = createAction(SELECTIONS_SUCCESS, props<{ selections: ISelection[] }>());
export const selectionRequestFail = createAction(SELECTIONS_FAIL);
export const dummyAction = createAction('[dummy-action]');
