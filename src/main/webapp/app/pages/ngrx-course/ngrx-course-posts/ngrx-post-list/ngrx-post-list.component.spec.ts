import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NgrxPostListComponent } from './ngrx-post-list.component';

describe('NgrxPostListComponent', () => {
  let component: NgrxPostListComponent;
  let fixture: ComponentFixture<NgrxPostListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [NgrxPostListComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NgrxPostListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
