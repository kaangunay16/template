package tr.com.khg.template.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import tr.com.khg.template.IntegrationTest;
import tr.com.khg.template.domain.ActualActivity;
import tr.com.khg.template.repository.ActualActivityRepository;

/**
 * Integration tests for the {@link ActualActivityResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class ActualActivityResourceIT {

    private static final String DEFAULT_CITY = "AAAAAAAAAA";
    private static final String UPDATED_CITY = "BBBBBBBBBB";

    private static final String DEFAULT_POSITION = "AAAAAAAAAA";
    private static final String UPDATED_POSITION = "BBBBBBBBBB";

    private static final Integer DEFAULT_START_YEAR = 1;
    private static final Integer UPDATED_START_YEAR = 2;

    private static final Boolean DEFAULT_DELETED = false;
    private static final Boolean UPDATED_DELETED = true;

    private static final String DEFAULT_FIELD = "AAAAAAAAAA";
    private static final String UPDATED_FIELD = "BBBBBBBBBB";

    private static final String ENTITY_API_URL = "/api/actual-activities";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private ActualActivityRepository actualActivityRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restActualActivityMockMvc;

    private ActualActivity actualActivity;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ActualActivity createEntity(EntityManager em) {
        ActualActivity actualActivity = new ActualActivity()
            .city(DEFAULT_CITY)
            .position(DEFAULT_POSITION)
            .startYear(DEFAULT_START_YEAR)
            .deleted(DEFAULT_DELETED)
            .field(DEFAULT_FIELD);
        return actualActivity;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ActualActivity createUpdatedEntity(EntityManager em) {
        ActualActivity actualActivity = new ActualActivity()
            .city(UPDATED_CITY)
            .position(UPDATED_POSITION)
            .startYear(UPDATED_START_YEAR)
            .deleted(UPDATED_DELETED)
            .field(UPDATED_FIELD);
        return actualActivity;
    }

    @BeforeEach
    public void initTest() {
        actualActivity = createEntity(em);
    }

    @Test
    @Transactional
    void createActualActivity() throws Exception {
        int databaseSizeBeforeCreate = actualActivityRepository.findAll().size();
        // Create the ActualActivity
        restActualActivityMockMvc
            .perform(
                post(ENTITY_API_URL)
                    .with(csrf())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(actualActivity))
            )
            .andExpect(status().isCreated());

        // Validate the ActualActivity in the database
        List<ActualActivity> actualActivityList = actualActivityRepository.findAll();
        assertThat(actualActivityList).hasSize(databaseSizeBeforeCreate + 1);
        ActualActivity testActualActivity = actualActivityList.get(actualActivityList.size() - 1);
        assertThat(testActualActivity.getCity()).isEqualTo(DEFAULT_CITY);
        assertThat(testActualActivity.getPosition()).isEqualTo(DEFAULT_POSITION);
        assertThat(testActualActivity.getStartYear()).isEqualTo(DEFAULT_START_YEAR);
        assertThat(testActualActivity.getDeleted()).isEqualTo(DEFAULT_DELETED);
        assertThat(testActualActivity.getField()).isEqualTo(DEFAULT_FIELD);
    }

    @Test
    @Transactional
    void createActualActivityWithExistingId() throws Exception {
        // Create the ActualActivity with an existing ID
        actualActivity.setId(1L);

        int databaseSizeBeforeCreate = actualActivityRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restActualActivityMockMvc
            .perform(
                post(ENTITY_API_URL)
                    .with(csrf())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(actualActivity))
            )
            .andExpect(status().isBadRequest());

        // Validate the ActualActivity in the database
        List<ActualActivity> actualActivityList = actualActivityRepository.findAll();
        assertThat(actualActivityList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void getAllActualActivities() throws Exception {
        // Initialize the database
        actualActivityRepository.saveAndFlush(actualActivity);

        // Get all the actualActivityList
        restActualActivityMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(actualActivity.getId().intValue())))
            .andExpect(jsonPath("$.[*].city").value(hasItem(DEFAULT_CITY)))
            .andExpect(jsonPath("$.[*].position").value(hasItem(DEFAULT_POSITION)))
            .andExpect(jsonPath("$.[*].startYear").value(hasItem(DEFAULT_START_YEAR)))
            .andExpect(jsonPath("$.[*].deleted").value(hasItem(DEFAULT_DELETED.booleanValue())))
            .andExpect(jsonPath("$.[*].field").value(hasItem(DEFAULT_FIELD)));
    }

    @Test
    @Transactional
    void getActualActivity() throws Exception {
        // Initialize the database
        actualActivityRepository.saveAndFlush(actualActivity);

        // Get the actualActivity
        restActualActivityMockMvc
            .perform(get(ENTITY_API_URL_ID, actualActivity.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(actualActivity.getId().intValue()))
            .andExpect(jsonPath("$.city").value(DEFAULT_CITY))
            .andExpect(jsonPath("$.position").value(DEFAULT_POSITION))
            .andExpect(jsonPath("$.startYear").value(DEFAULT_START_YEAR))
            .andExpect(jsonPath("$.deleted").value(DEFAULT_DELETED.booleanValue()))
            .andExpect(jsonPath("$.field").value(DEFAULT_FIELD));
    }

    @Test
    @Transactional
    void getNonExistingActualActivity() throws Exception {
        // Get the actualActivity
        restActualActivityMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putNewActualActivity() throws Exception {
        // Initialize the database
        actualActivityRepository.saveAndFlush(actualActivity);

        int databaseSizeBeforeUpdate = actualActivityRepository.findAll().size();

        // Update the actualActivity
        ActualActivity updatedActualActivity = actualActivityRepository.findById(actualActivity.getId()).get();
        // Disconnect from session so that the updates on updatedActualActivity are not directly saved in db
        em.detach(updatedActualActivity);
        updatedActualActivity
            .city(UPDATED_CITY)
            .position(UPDATED_POSITION)
            .startYear(UPDATED_START_YEAR)
            .deleted(UPDATED_DELETED)
            .field(UPDATED_FIELD);

        restActualActivityMockMvc
            .perform(
                put(ENTITY_API_URL_ID, updatedActualActivity.getId())
                    .with(csrf())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(updatedActualActivity))
            )
            .andExpect(status().isOk());

        // Validate the ActualActivity in the database
        List<ActualActivity> actualActivityList = actualActivityRepository.findAll();
        assertThat(actualActivityList).hasSize(databaseSizeBeforeUpdate);
        ActualActivity testActualActivity = actualActivityList.get(actualActivityList.size() - 1);
        assertThat(testActualActivity.getCity()).isEqualTo(UPDATED_CITY);
        assertThat(testActualActivity.getPosition()).isEqualTo(UPDATED_POSITION);
        assertThat(testActualActivity.getStartYear()).isEqualTo(UPDATED_START_YEAR);
        assertThat(testActualActivity.getDeleted()).isEqualTo(UPDATED_DELETED);
        assertThat(testActualActivity.getField()).isEqualTo(UPDATED_FIELD);
    }

    @Test
    @Transactional
    void putNonExistingActualActivity() throws Exception {
        int databaseSizeBeforeUpdate = actualActivityRepository.findAll().size();
        actualActivity.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restActualActivityMockMvc
            .perform(
                put(ENTITY_API_URL_ID, actualActivity.getId())
                    .with(csrf())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(actualActivity))
            )
            .andExpect(status().isBadRequest());

        // Validate the ActualActivity in the database
        List<ActualActivity> actualActivityList = actualActivityRepository.findAll();
        assertThat(actualActivityList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchActualActivity() throws Exception {
        int databaseSizeBeforeUpdate = actualActivityRepository.findAll().size();
        actualActivity.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restActualActivityMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .with(csrf())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(actualActivity))
            )
            .andExpect(status().isBadRequest());

        // Validate the ActualActivity in the database
        List<ActualActivity> actualActivityList = actualActivityRepository.findAll();
        assertThat(actualActivityList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamActualActivity() throws Exception {
        int databaseSizeBeforeUpdate = actualActivityRepository.findAll().size();
        actualActivity.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restActualActivityMockMvc
            .perform(
                put(ENTITY_API_URL)
                    .with(csrf())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(actualActivity))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the ActualActivity in the database
        List<ActualActivity> actualActivityList = actualActivityRepository.findAll();
        assertThat(actualActivityList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateActualActivityWithPatch() throws Exception {
        // Initialize the database
        actualActivityRepository.saveAndFlush(actualActivity);

        int databaseSizeBeforeUpdate = actualActivityRepository.findAll().size();

        // Update the actualActivity using partial update
        ActualActivity partialUpdatedActualActivity = new ActualActivity();
        partialUpdatedActualActivity.setId(actualActivity.getId());

        partialUpdatedActualActivity.city(UPDATED_CITY).startYear(UPDATED_START_YEAR).deleted(UPDATED_DELETED).field(UPDATED_FIELD);

        restActualActivityMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedActualActivity.getId())
                    .with(csrf())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedActualActivity))
            )
            .andExpect(status().isOk());

        // Validate the ActualActivity in the database
        List<ActualActivity> actualActivityList = actualActivityRepository.findAll();
        assertThat(actualActivityList).hasSize(databaseSizeBeforeUpdate);
        ActualActivity testActualActivity = actualActivityList.get(actualActivityList.size() - 1);
        assertThat(testActualActivity.getCity()).isEqualTo(UPDATED_CITY);
        assertThat(testActualActivity.getPosition()).isEqualTo(DEFAULT_POSITION);
        assertThat(testActualActivity.getStartYear()).isEqualTo(UPDATED_START_YEAR);
        assertThat(testActualActivity.getDeleted()).isEqualTo(UPDATED_DELETED);
        assertThat(testActualActivity.getField()).isEqualTo(UPDATED_FIELD);
    }

    @Test
    @Transactional
    void fullUpdateActualActivityWithPatch() throws Exception {
        // Initialize the database
        actualActivityRepository.saveAndFlush(actualActivity);

        int databaseSizeBeforeUpdate = actualActivityRepository.findAll().size();

        // Update the actualActivity using partial update
        ActualActivity partialUpdatedActualActivity = new ActualActivity();
        partialUpdatedActualActivity.setId(actualActivity.getId());

        partialUpdatedActualActivity
            .city(UPDATED_CITY)
            .position(UPDATED_POSITION)
            .startYear(UPDATED_START_YEAR)
            .deleted(UPDATED_DELETED)
            .field(UPDATED_FIELD);

        restActualActivityMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedActualActivity.getId())
                    .with(csrf())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedActualActivity))
            )
            .andExpect(status().isOk());

        // Validate the ActualActivity in the database
        List<ActualActivity> actualActivityList = actualActivityRepository.findAll();
        assertThat(actualActivityList).hasSize(databaseSizeBeforeUpdate);
        ActualActivity testActualActivity = actualActivityList.get(actualActivityList.size() - 1);
        assertThat(testActualActivity.getCity()).isEqualTo(UPDATED_CITY);
        assertThat(testActualActivity.getPosition()).isEqualTo(UPDATED_POSITION);
        assertThat(testActualActivity.getStartYear()).isEqualTo(UPDATED_START_YEAR);
        assertThat(testActualActivity.getDeleted()).isEqualTo(UPDATED_DELETED);
        assertThat(testActualActivity.getField()).isEqualTo(UPDATED_FIELD);
    }

    @Test
    @Transactional
    void patchNonExistingActualActivity() throws Exception {
        int databaseSizeBeforeUpdate = actualActivityRepository.findAll().size();
        actualActivity.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restActualActivityMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, actualActivity.getId())
                    .with(csrf())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(actualActivity))
            )
            .andExpect(status().isBadRequest());

        // Validate the ActualActivity in the database
        List<ActualActivity> actualActivityList = actualActivityRepository.findAll();
        assertThat(actualActivityList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchActualActivity() throws Exception {
        int databaseSizeBeforeUpdate = actualActivityRepository.findAll().size();
        actualActivity.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restActualActivityMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .with(csrf())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(actualActivity))
            )
            .andExpect(status().isBadRequest());

        // Validate the ActualActivity in the database
        List<ActualActivity> actualActivityList = actualActivityRepository.findAll();
        assertThat(actualActivityList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamActualActivity() throws Exception {
        int databaseSizeBeforeUpdate = actualActivityRepository.findAll().size();
        actualActivity.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restActualActivityMockMvc
            .perform(
                patch(ENTITY_API_URL)
                    .with(csrf())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(actualActivity))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the ActualActivity in the database
        List<ActualActivity> actualActivityList = actualActivityRepository.findAll();
        assertThat(actualActivityList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteActualActivity() throws Exception {
        // Initialize the database
        actualActivityRepository.saveAndFlush(actualActivity);

        int databaseSizeBeforeDelete = actualActivityRepository.findAll().size();

        // Delete the actualActivity
        restActualActivityMockMvc
            .perform(delete(ENTITY_API_URL_ID, actualActivity.getId()).with(csrf()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<ActualActivity> actualActivityList = actualActivityRepository.findAll();
        assertThat(actualActivityList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
