jest.mock('@angular/router');

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { of, Subject } from 'rxjs';

import { AbroadActivityService } from '../service/abroad-activity.service';
import { IAbroadActivity, AbroadActivity } from '../abroad-activity.model';
import { IPerson } from 'app/entities/person/person.model';
import { PersonService } from 'app/entities/person/service/person.service';

import { AbroadActivityUpdateComponent } from './abroad-activity-update.component';

describe('AbroadActivity Management Update Component', () => {
  let comp: AbroadActivityUpdateComponent;
  let fixture: ComponentFixture<AbroadActivityUpdateComponent>;
  let activatedRoute: ActivatedRoute;
  let abroadActivityService: AbroadActivityService;
  let personService: PersonService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      declarations: [AbroadActivityUpdateComponent],
      providers: [FormBuilder, ActivatedRoute],
    })
      .overrideTemplate(AbroadActivityUpdateComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(AbroadActivityUpdateComponent);
    activatedRoute = TestBed.inject(ActivatedRoute);
    abroadActivityService = TestBed.inject(AbroadActivityService);
    personService = TestBed.inject(PersonService);

    comp = fixture.componentInstance;
  });

  describe('ngOnInit', () => {
    it('Should call Person query and add missing value', () => {
      const abroadActivity: IAbroadActivity = { id: 456 };
      const person: IPerson = { id: 6440 };
      abroadActivity.person = person;

      const personCollection: IPerson[] = [{ id: 54994 }];
      jest.spyOn(personService, 'query').mockReturnValue(of(new HttpResponse({ body: personCollection })));
      const additionalPeople = [person];
      const expectedCollection: IPerson[] = [...additionalPeople, ...personCollection];
      jest.spyOn(personService, 'addPersonToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ abroadActivity });
      comp.ngOnInit();

      expect(personService.query).toHaveBeenCalled();
      expect(personService.addPersonToCollectionIfMissing).toHaveBeenCalledWith(personCollection, ...additionalPeople);
      expect(comp.peopleSharedCollection).toEqual(expectedCollection);
    });

    it('Should update editForm', () => {
      const abroadActivity: IAbroadActivity = { id: 456 };
      const person: IPerson = { id: 47062 };
      abroadActivity.person = person;

      activatedRoute.data = of({ abroadActivity });
      comp.ngOnInit();

      expect(comp.editForm.value).toEqual(expect.objectContaining(abroadActivity));
      expect(comp.peopleSharedCollection).toContain(person);
    });
  });

  describe('save', () => {
    it('Should call update service on save for existing entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<AbroadActivity>>();
      const abroadActivity = { id: 123 };
      jest.spyOn(abroadActivityService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ abroadActivity });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: abroadActivity }));
      saveSubject.complete();

      // THEN
      expect(comp.previousState).toHaveBeenCalled();
      expect(abroadActivityService.update).toHaveBeenCalledWith(abroadActivity);
      expect(comp.isSaving).toEqual(false);
    });

    it('Should call create service on save for new entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<AbroadActivity>>();
      const abroadActivity = new AbroadActivity();
      jest.spyOn(abroadActivityService, 'create').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ abroadActivity });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: abroadActivity }));
      saveSubject.complete();

      // THEN
      expect(abroadActivityService.create).toHaveBeenCalledWith(abroadActivity);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).toHaveBeenCalled();
    });

    it('Should set isSaving to false on error', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<AbroadActivity>>();
      const abroadActivity = { id: 123 };
      jest.spyOn(abroadActivityService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ abroadActivity });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.error('This is an error!');

      // THEN
      expect(abroadActivityService.update).toHaveBeenCalledWith(abroadActivity);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).not.toHaveBeenCalled();
    });
  });

  describe('Tracking relationships identifiers', () => {
    describe('trackPersonById', () => {
      it('Should return tracked Person primary key', () => {
        const entity = { id: 123 };
        const trackResult = comp.trackPersonById(0, entity);
        expect(trackResult).toEqual(entity.id);
      });
    });
  });
});
