package tr.com.khg.template.service;

import java.util.List;
import java.util.Optional;
import tr.com.khg.template.domain.ActualActivity;

/**
 * Service Interface for managing {@link ActualActivity}.
 */
public interface ActualActivityService {
    /**
     * Save a actualActivity.
     *
     * @param actualActivity the entity to save.
     * @return the persisted entity.
     */
    ActualActivity save(ActualActivity actualActivity);

    /**
     * Partially updates a actualActivity.
     *
     * @param actualActivity the entity to update partially.
     * @return the persisted entity.
     */
    Optional<ActualActivity> partialUpdate(ActualActivity actualActivity);

    /**
     * Get all the actualActivities.
     *
     * @return the list of entities.
     */
    List<ActualActivity> findAll();

    /**
     * Get the "id" actualActivity.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<ActualActivity> findOne(Long id);

    /**
     * Delete the "id" actualActivity.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
