import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IActualActivity } from '../actual-activity.model';
import { ActualActivityService } from '../service/actual-activity.service';
import { ActualActivityDeleteDialogComponent } from '../delete/actual-activity-delete-dialog.component';

@Component({
  selector: 'jhi-actual-activity',
  templateUrl: './actual-activity.component.html',
})
export class ActualActivityComponent implements OnInit {
  actualActivities?: IActualActivity[];
  isLoading = false;

  constructor(protected actualActivityService: ActualActivityService, protected modalService: NgbModal) {}

  loadAll(): void {
    this.isLoading = true;

    this.actualActivityService.query().subscribe(
      (res: HttpResponse<IActualActivity[]>) => {
        this.isLoading = false;
        this.actualActivities = res.body ?? [];
      },
      () => {
        this.isLoading = false;
      }
    );
  }

  ngOnInit(): void {
    this.loadAll();
  }

  trackId(index: number, item: IActualActivity): number {
    return item.id!;
  }

  delete(actualActivity: IActualActivity): void {
    const modalRef = this.modalService.open(ActualActivityDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.actualActivity = actualActivity;
    // unsubscribe not needed because closed completes on modal close
    modalRef.closed.subscribe(reason => {
      if (reason === 'deleted') {
        this.loadAll();
      }
    });
  }
}
