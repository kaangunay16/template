import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { PayPhoneComponent } from '../list/pay-phone.component';
import { PayPhoneDetailComponent } from '../detail/pay-phone-detail.component';
import { PayPhoneUpdateComponent } from '../update/pay-phone-update.component';
import { PayPhoneRoutingResolveService } from './pay-phone-routing-resolve.service';

const payPhoneRoute: Routes = [
  {
    path: '',
    component: PayPhoneComponent,
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: PayPhoneDetailComponent,
    resolve: {
      payPhone: PayPhoneRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: PayPhoneUpdateComponent,
    resolve: {
      payPhone: PayPhoneRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: PayPhoneUpdateComponent,
    resolve: {
      payPhone: PayPhoneRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
];

@NgModule({
  imports: [RouterModule.forChild(payPhoneRoute)],
  exports: [RouterModule],
})
export class PayPhoneRoutingModule {}
