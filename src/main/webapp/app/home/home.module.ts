import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SharedModule } from 'app/shared/shared.module';
import { HOME_ROUTE } from './home.route';
import { HomeComponent } from './home.component';
import { CalenderComponent } from './calender/calender.component';
import { QuickSearchComponent } from './quick-search/quick-search.component';
import { YearsStatsComponent } from './years-stats/years-stats.component';

@NgModule({
  imports: [SharedModule, RouterModule.forChild([HOME_ROUTE])],
  declarations: [HomeComponent, CalenderComponent, QuickSearchComponent, YearsStatsComponent],
})
export class HomeModule {}
