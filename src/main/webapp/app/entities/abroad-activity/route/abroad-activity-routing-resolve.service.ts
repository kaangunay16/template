import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { IAbroadActivity, AbroadActivity } from '../abroad-activity.model';
import { AbroadActivityService } from '../service/abroad-activity.service';

@Injectable({ providedIn: 'root' })
export class AbroadActivityRoutingResolveService implements Resolve<IAbroadActivity> {
  constructor(protected service: AbroadActivityService, protected router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IAbroadActivity> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        mergeMap((abroadActivity: HttpResponse<AbroadActivity>) => {
          if (abroadActivity.body) {
            return of(abroadActivity.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new AbroadActivity());
  }
}
