package tr.com.khg.template.domain;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import tr.com.khg.template.web.rest.TestUtil;

class AbroadActivityTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(AbroadActivity.class);
        AbroadActivity abroadActivity1 = new AbroadActivity();
        abroadActivity1.setId(1L);
        AbroadActivity abroadActivity2 = new AbroadActivity();
        abroadActivity2.setId(abroadActivity1.getId());
        assertThat(abroadActivity1).isEqualTo(abroadActivity2);
        abroadActivity2.setId(2L);
        assertThat(abroadActivity1).isNotEqualTo(abroadActivity2);
        abroadActivity1.setId(null);
        assertThat(abroadActivity1).isNotEqualTo(abroadActivity2);
    }
}
