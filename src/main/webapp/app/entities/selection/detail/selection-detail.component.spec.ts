import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { SelectionDetailComponent } from './selection-detail.component';

describe('Selection Management Detail Component', () => {
  let comp: SelectionDetailComponent;
  let fixture: ComponentFixture<SelectionDetailComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [SelectionDetailComponent],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: { data: of({ selection: { id: 123 } }) },
        },
      ],
    })
      .overrideTemplate(SelectionDetailComponent, '')
      .compileComponents();
    fixture = TestBed.createComponent(SelectionDetailComponent);
    comp = fixture.componentInstance;
  });

  describe('OnInit', () => {
    it('Should load selection on init', () => {
      // WHEN
      comp.ngOnInit();

      // THEN
      expect(comp.selection).toEqual(expect.objectContaining({ id: 123 }));
    });
  });
});
