package tr.com.khg.template.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.ResponseUtil;
import tr.com.khg.template.domain.OtherPersonRecord;
import tr.com.khg.template.repository.OtherPersonRecordRepository;
import tr.com.khg.template.service.OtherPersonRecordService;
import tr.com.khg.template.web.rest.errors.BadRequestAlertException;

/**
 * REST controller for managing {@link tr.com.khg.template.domain.OtherPersonRecord}.
 */
@RestController
@RequestMapping("/api")
public class OtherPersonRecordResource {

    private final Logger log = LoggerFactory.getLogger(OtherPersonRecordResource.class);

    private static final String ENTITY_NAME = "otherPersonRecord";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final OtherPersonRecordService otherPersonRecordService;

    private final OtherPersonRecordRepository otherPersonRecordRepository;

    public OtherPersonRecordResource(
        OtherPersonRecordService otherPersonRecordService,
        OtherPersonRecordRepository otherPersonRecordRepository
    ) {
        this.otherPersonRecordService = otherPersonRecordService;
        this.otherPersonRecordRepository = otherPersonRecordRepository;
    }

    /**
     * {@code POST  /other-person-records} : Create a new otherPersonRecord.
     *
     * @param otherPersonRecord the otherPersonRecord to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new otherPersonRecord, or with status {@code 400 (Bad Request)} if the otherPersonRecord has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/other-person-records")
    public ResponseEntity<OtherPersonRecord> createOtherPersonRecord(@RequestBody OtherPersonRecord otherPersonRecord)
        throws URISyntaxException {
        log.debug("REST request to save OtherPersonRecord : {}", otherPersonRecord);
        if (otherPersonRecord.getId() != null) {
            throw new BadRequestAlertException("A new otherPersonRecord cannot already have an ID", ENTITY_NAME, "idexists");
        }
        OtherPersonRecord result = otherPersonRecordService.save(otherPersonRecord);
        return ResponseEntity
            .created(new URI("/api/other-person-records/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /other-person-records/:id} : Updates an existing otherPersonRecord.
     *
     * @param id the id of the otherPersonRecord to save.
     * @param otherPersonRecord the otherPersonRecord to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated otherPersonRecord,
     * or with status {@code 400 (Bad Request)} if the otherPersonRecord is not valid,
     * or with status {@code 500 (Internal Server Error)} if the otherPersonRecord couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/other-person-records/{id}")
    public ResponseEntity<OtherPersonRecord> updateOtherPersonRecord(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody OtherPersonRecord otherPersonRecord
    ) throws URISyntaxException {
        log.debug("REST request to update OtherPersonRecord : {}, {}", id, otherPersonRecord);
        if (otherPersonRecord.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, otherPersonRecord.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!otherPersonRecordRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        OtherPersonRecord result = otherPersonRecordService.save(otherPersonRecord);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, otherPersonRecord.getId().toString()))
            .body(result);
    }

    /**
     * {@code POST  /other-person-records/update-all} : Update all otherPersonRecords.
     *
     * @param records the otherPersonRecords to update.
     * @return the {@link ResponseEntity} with status {@code 200 (Ok)} and with body the updated otherPersonRecords, or with status {@code 400 (Bad Request)} if the otherPersonRecord has no ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/other-person-records/update-all")
    public ResponseEntity<List<OtherPersonRecord>> updateAllOtherPersonRecords(@RequestBody List<OtherPersonRecord> records)
        throws URISyntaxException {
        log.debug("REST request to update all OtherPersonRecords");
        records.forEach(record -> {
            if (record.getId() == null) {
                throw new BadRequestAlertException("There is a entity has no id", ENTITY_NAME, "noid");
            }
        });
        List<OtherPersonRecord> results = otherPersonRecordService.updateAll(records);
        return ResponseEntity.ok().body(results);
    }

    /**
     * {@code PATCH  /other-person-records/:id} : Partial updates given fields of an existing otherPersonRecord, field will ignore if it is null
     *
     * @param id the id of the otherPersonRecord to save.
     * @param otherPersonRecord the otherPersonRecord to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated otherPersonRecord,
     * or with status {@code 400 (Bad Request)} if the otherPersonRecord is not valid,
     * or with status {@code 404 (Not Found)} if the otherPersonRecord is not found,
     * or with status {@code 500 (Internal Server Error)} if the otherPersonRecord couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/other-person-records/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<OtherPersonRecord> partialUpdateOtherPersonRecord(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody OtherPersonRecord otherPersonRecord
    ) throws URISyntaxException {
        log.debug("REST request to partial update OtherPersonRecord partially : {}, {}", id, otherPersonRecord);
        if (otherPersonRecord.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, otherPersonRecord.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!otherPersonRecordRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<OtherPersonRecord> result = otherPersonRecordService.partialUpdate(otherPersonRecord);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, otherPersonRecord.getId().toString())
        );
    }

    /**
     * {@code GET  /other-person-records} : get all the otherPersonRecords.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of otherPersonRecords in body.
     */
    @GetMapping("/other-person-records")
    public List<OtherPersonRecord> getAllOtherPersonRecords() {
        log.debug("REST request to get all OtherPersonRecords");
        return otherPersonRecordService.findAll();
    }

    /**
     * {@code GET  /other-person-records/:id} : get the "id" otherPersonRecord.
     *
     * @param id the id of the otherPersonRecord to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the otherPersonRecord, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/other-person-records/{id}")
    public ResponseEntity<OtherPersonRecord> getOtherPersonRecord(@PathVariable Long id) {
        log.debug("REST request to get OtherPersonRecord : {}", id);
        Optional<OtherPersonRecord> otherPersonRecord = otherPersonRecordService.findOne(id);
        return ResponseUtil.wrapOrNotFound(otherPersonRecord);
    }

    /**
     * {@code DELETE  /other-person-records/:id} : delete the "id" otherPersonRecord.
     *
     * @param id the id of the otherPersonRecord to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/other-person-records/{id}")
    public ResponseEntity<Void> deleteOtherPersonRecord(@PathVariable Long id) {
        log.debug("REST request to delete OtherPersonRecord : {}", id);
        otherPersonRecordService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }
}
