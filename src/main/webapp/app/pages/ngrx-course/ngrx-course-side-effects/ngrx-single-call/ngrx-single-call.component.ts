import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';

import { ISelection } from 'app/entities/selection/selection.model';
import { AppState } from '../../store/app.state';
import { getSelections } from '../state/side-effect.selector';

@Component({
  selector: 'jhi-ngrx-single-call',
  templateUrl: './ngrx-single-call.component.html',
  styleUrls: ['./ngrx-single-call.component.scss'],
})
export class NgrxSingleCallComponent implements OnInit {
  selections$!: Observable<ISelection[] | null | undefined>;

  constructor(private store: Store<AppState>) {}

  ngOnInit(): void {
    this.selections$ = this.store.select(getSelections);
  }
}
