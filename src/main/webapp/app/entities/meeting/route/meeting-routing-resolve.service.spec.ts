jest.mock('@angular/router');

import { TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ActivatedRouteSnapshot, Router } from '@angular/router';
import { of } from 'rxjs';

import { IMeeting, Meeting } from '../meeting.model';
import { MeetingService } from '../service/meeting.service';

import { MeetingRoutingResolveService } from './meeting-routing-resolve.service';

describe('Meeting routing resolve service', () => {
  let mockRouter: Router;
  let mockActivatedRouteSnapshot: ActivatedRouteSnapshot;
  let routingResolveService: MeetingRoutingResolveService;
  let service: MeetingService;
  let resultMeeting: IMeeting | undefined;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [Router, ActivatedRouteSnapshot],
    });
    mockRouter = TestBed.inject(Router);
    mockActivatedRouteSnapshot = TestBed.inject(ActivatedRouteSnapshot);
    routingResolveService = TestBed.inject(MeetingRoutingResolveService);
    service = TestBed.inject(MeetingService);
    resultMeeting = undefined;
  });

  describe('resolve', () => {
    it('should return IMeeting returned by find', () => {
      // GIVEN
      service.find = jest.fn(id => of(new HttpResponse({ body: { id } })));
      mockActivatedRouteSnapshot.params = { id: 123 };

      // WHEN
      routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
        resultMeeting = result;
      });

      // THEN
      expect(service.find).toBeCalledWith(123);
      expect(resultMeeting).toEqual({ id: 123 });
    });

    it('should return new IMeeting if id is not provided', () => {
      // GIVEN
      service.find = jest.fn();
      mockActivatedRouteSnapshot.params = {};

      // WHEN
      routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
        resultMeeting = result;
      });

      // THEN
      expect(service.find).not.toBeCalled();
      expect(resultMeeting).toEqual(new Meeting());
    });

    it('should route to 404 page if data not found in server', () => {
      // GIVEN
      jest.spyOn(service, 'find').mockReturnValue(of(new HttpResponse({ body: null as unknown as Meeting })));
      mockActivatedRouteSnapshot.params = { id: 123 };

      // WHEN
      routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
        resultMeeting = result;
      });

      // THEN
      expect(service.find).toBeCalledWith(123);
      expect(resultMeeting).toEqual(undefined);
      expect(mockRouter.navigate).toHaveBeenCalledWith(['404']);
    });
  });
});
