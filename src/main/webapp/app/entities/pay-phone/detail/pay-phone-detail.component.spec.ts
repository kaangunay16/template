import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { PayPhoneDetailComponent } from './pay-phone-detail.component';

describe('PayPhone Management Detail Component', () => {
  let comp: PayPhoneDetailComponent;
  let fixture: ComponentFixture<PayPhoneDetailComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [PayPhoneDetailComponent],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: { data: of({ payPhone: { id: 123 } }) },
        },
      ],
    })
      .overrideTemplate(PayPhoneDetailComponent, '')
      .compileComponents();
    fixture = TestBed.createComponent(PayPhoneDetailComponent);
    comp = fixture.componentInstance;
  });

  describe('OnInit', () => {
    it('Should load payPhone on init', () => {
      // WHEN
      comp.ngOnInit();

      // THEN
      expect(comp.payPhone).toEqual(expect.objectContaining({ id: 123 }));
    });
  });
});
