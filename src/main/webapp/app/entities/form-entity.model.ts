import { HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

export interface IFormEntity {
  delete: (id: number) => Observable<HttpResponse<{}>>;
}

export interface HasId {
  id?: number;
}
