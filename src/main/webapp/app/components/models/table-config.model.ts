import { HasId } from 'app/entities/form-entity.model';
import { TableAction } from 'app/components/table/table-action.model';
import { TableColumn } from 'app/components/models/table-column.model';

export interface TableConfig {
  columns: TableColumn[];
  data: HasId[] | null;
  actions?: TableAction[];
}
