import { Component, OnDestroy, OnInit } from '@angular/core';
import { Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { ToastrService } from 'ngx-toastr';

import { IPerson } from 'app/entities/person/person.model';
import { PersonModuleService } from 'app/pages/person/person-module.service';
import { DynamicFormCreatorModel, IDynamicFormCreatorModel } from 'app/components/dynamic-form-creator/dynamic-form-field.model';
import { IdentityService } from 'app/entities/identity/service/identity.service';
import { ErrorSupportService } from 'app/shared/services/error-support.service';

@Component({
  selector: 'jhi-identity-info',
  templateUrl: './identity-info.component.html',
  styleUrls: ['./identity-info.component.scss'],
})
export class IdentityInfoComponent implements OnInit, OnDestroy {
  person!: IPerson;
  formCreator?: IDynamicFormCreatorModel;
  routeSub!: Subscription;
  updateSub?: Subscription;

  constructor(
    private toastr: ToastrService,
    private personModuleService: PersonModuleService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private identityService: IdentityService,
    private errorService: ErrorSupportService
  ) {}

  ngOnInit(): void {
    this.routeSub = this.activatedRoute.queryParams.subscribe(params => {
      this.person = this.personModuleService.entityCheck(params);
      this.formCreator = new DynamicFormCreatorModel([
        {
          id: 'identityNo',
          label: 'Kimlik Numarası',
          type: 'text',
          value: this.person.identity?.identityNo,
          validators: [Validators.required],
          readOnly: true,
        },
        {
          id: 'name',
          label: 'İsim',
          type: 'text',
          value: this.person.identity?.name,
          validators: [Validators.required],
          readOnly: true,
        },
        {
          id: 'surname',
          label: 'Soyisim',
          type: 'text',
          value: this.person.identity?.surname,
          validators: [Validators.required],
          readOnly: true,
        },
        {
          id: 'placeOfBirth',
          label: 'Doğum Yeri',
          type: 'text',
          value: this.person.identity?.placeOfBirth,
          validators: [Validators.required],
          readOnly: true,
        },
        {
          id: 'birthday',
          label: 'Doğum Tarihi',
          type: 'text',
          value: this.person.identity?.birthday?.format('DD.MM.YYYY'),
          validators: [Validators.required],
          readOnly: true,
        },
        {
          id: 'registeredCity',
          label: 'NKO İl',
          type: 'text',
          value: this.person.identity?.registeredCity,
          validators: [Validators.required],
          readOnly: true,
        },
        {
          id: 'registeredDistrict',
          label: 'NKO İlçe',
          type: 'text',
          value: this.person.identity?.registeredDistrict,
          validators: [Validators.required],
          readOnly: true,
        },
        {
          id: 'registeredVillage',
          label: 'NKO Köy',
          type: 'text',
          value: this.person.identity?.registeredVillage,
          validators: [Validators.required],
          readOnly: true,
        },
        {
          id: 'motherName',
          label: 'Ana Adı:',
          type: 'text',
          value: this.person.identity?.motherName,
          validators: [Validators.required],
          readOnly: true,
        },
        {
          id: 'fatherName',
          label: 'Baba Adı:',
          type: 'text',
          value: this.person.identity?.fatherName,
          validators: [Validators.required],
          readOnly: true,
        },
        {
          id: 'maritalStatus',
          label: 'Medeni Hali:',
          type: 'text',
          value: this.person.identity?.maritalStatus,
          validators: [Validators.required],
          readOnly: true,
        },
        {
          id: 'education',
          label: 'Eğitim:',
          type: 'text',
          value: this.person.identity?.education,
        },
        {
          id: 'profession',
          label: 'Meslek:',
          type: 'text',
          value: this.person.identity?.profession,
        },
        {
          id: 'address',
          label: 'Adres:',
          type: 'textarea',
          value: this.person.identity?.address,
        },
      ]);
    });
  }

  ngOnDestroy(): void {
    this.routeSub.unsubscribe();
    this.updateSub?.unsubscribe();
  }

  submit(): void {
    this.setFormValuesToEntity();
    this.router.navigate(['/person', 'organizational-info']);
  }

  updateSubmit(): void {
    const oldPerson = { ...this.person };
    this.setFormValuesToEntity();
    this.updateSub = this.identityService.update(this.person.identity!).subscribe(
      res => {
        if (res.body) {
          this.personModuleService.changeEntity(oldPerson, this.person);
        }
      },
      err => this.errorService.errorHandler(err)
    );
    this.personModuleService.updatePerson(this.person);
  }

  private setFormValuesToEntity(): void {
    this.person.identity!.education = this.formCreator?.form.get('education')?.value;
    this.person.identity!.profession = this.formCreator?.form.get('profession')?.value;
    this.person.identity!.address = this.formCreator?.form.get('address')?.value;
  }
}
