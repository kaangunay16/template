import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OtherPersonPersonInfoComponent } from './other-person-person-info.component';

describe('OtherPersonPersonInfoComponent', () => {
  let component: OtherPersonPersonInfoComponent;
  let fixture: ComponentFixture<OtherPersonPersonInfoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [OtherPersonPersonInfoComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OtherPersonPersonInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
