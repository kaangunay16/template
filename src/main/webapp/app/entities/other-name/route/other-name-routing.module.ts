import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { OtherNameComponent } from '../list/other-name.component';
import { OtherNameDetailComponent } from '../detail/other-name-detail.component';
import { OtherNameUpdateComponent } from '../update/other-name-update.component';
import { OtherNameRoutingResolveService } from './other-name-routing-resolve.service';

const otherNameRoute: Routes = [
  {
    path: '',
    component: OtherNameComponent,
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: OtherNameDetailComponent,
    resolve: {
      otherName: OtherNameRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: OtherNameUpdateComponent,
    resolve: {
      otherName: OtherNameRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: OtherNameUpdateComponent,
    resolve: {
      otherName: OtherNameRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
];

@NgModule({
  imports: [RouterModule.forChild(otherNameRoute)],
  exports: [RouterModule],
})
export class OtherNameRoutingModule {}
