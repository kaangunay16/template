import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IOtherName } from '../other-name.model';

@Component({
  selector: 'jhi-other-name-detail',
  templateUrl: './other-name-detail.component.html',
})
export class OtherNameDetailComponent implements OnInit {
  otherName: IOtherName | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ otherName }) => {
      this.otherName = otherName;
    });
  }

  previousState(): void {
    window.history.back();
  }
}
