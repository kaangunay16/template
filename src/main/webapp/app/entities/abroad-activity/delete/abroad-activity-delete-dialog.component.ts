import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { IAbroadActivity } from '../abroad-activity.model';
import { AbroadActivityService } from '../service/abroad-activity.service';

@Component({
  templateUrl: './abroad-activity-delete-dialog.component.html',
})
export class AbroadActivityDeleteDialogComponent {
  abroadActivity?: IAbroadActivity;

  constructor(protected abroadActivityService: AbroadActivityService, protected activeModal: NgbActiveModal) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.abroadActivityService.delete(id).subscribe(() => {
      this.activeModal.close('deleted');
    });
  }
}
