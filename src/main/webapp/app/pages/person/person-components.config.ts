import { PageTableConfigs } from 'app/components/models/page-table-config.model';
import { IPhone } from 'app/entities/phone/phone.model';
import { IPastActivity } from 'app/entities/past-activity/past-activity.model';

export const personTableConfigs: PageTableConfigs = {
  columns: [
    {
      column: 'identity.name',
    },
    {
      column: 'identity.surname',
    },
    {
      column: 'phones',
      parser: phonesFieldParser,
    },
    {
      column: 'pastActivities',
      parser: pastActivitiesFieldParser,
    },
  ],
  actions: [
    {
      link: ['/person', 'identity-info'],
      icon: 'user-edit',
      tooltip: 'Kişisel Bilgileri Güncelle',
    },
    {
      link: ['/person', 'organizational-info'],
      icon: 'cogs',
      tooltip: 'Oragnizasyon Bilgilerini Güncelle',
    },
    {
      link: ['/person', 'past-activities'],
      icon: 'clock',
      tooltip: 'Geçmiş Faaliyetleri Güncelle',
    },
    {
      link: ['/person', 'actual-activities'],
      icon: 'play-circle',
      tooltip: 'Güncel Faaliyetleri Güncelle',
    },
    {
      link: ['/person', 'abroad-activities'],
      icon: 'flag',
      tooltip: 'Yutdışı Faaliyetlerini Güncelle',
    },
    {
      link: ['/person', 'person-deletion'],
      icon: 'trash-alt',
      tooltip: 'Silme İsteği Oluştur',
    },
  ],
  searchParameters: [
    {
      field: 'identity.name',
      type: 'text',
    },
    {
      field: 'identity.surname',
      type: 'text',
    },
    {
      field: 'identity.identityNumber',
      type: 'text',
    },
    {
      field: 'city',
      type: 'text',
    },
    {
      field: 'description',
      type: 'text',
    },
    {
      field: 'identity.fatherName',
      type: 'text',
    },
    {
      field: 'identity.motherName',
      type: 'text',
    },
  ],
};

function phonesFieldParser(phones: IPhone[]): string {
  const result: string[] = [];
  phones.forEach(phone => {
    result.push(`${phone.phoneNumber!}`);
  });

  return result.join(' - ');
}

function pastActivitiesFieldParser(pastActivities: IPastActivity[]): string {
  const result: string[] = [];
  pastActivities.forEach(pastActivity => {
    result.push(`${pastActivity.city!}`);
  });

  return result.join(' - ');
}
