package tr.com.khg.template.service;

import java.util.List;
import java.util.Optional;
import tr.com.khg.template.domain.Demand;

/**
 * Service Interface for managing {@link Demand}.
 */
public interface DemandService {
    /**
     * Save a demand.
     *
     * @param demand the entity to save.
     * @return the persisted entity.
     */
    Demand save(Demand demand);

    /**
     * Partially updates a demand.
     *
     * @param demand the entity to update partially.
     * @return the persisted entity.
     */
    Optional<Demand> partialUpdate(Demand demand);

    /**
     * Get all the demands.
     *
     * @return the list of entities.
     */
    List<Demand> findAll();

    /**
     * Get the "id" demand.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<Demand> findOne(Long id);

    /**
     * Delete the "id" demand.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);

    /**
     * Get all demands which have waiting status
     *
     * @param demandType Demand type of waiting demands
     * @return All waiting demands
     */
    List<Demand> findAllWaitingDemands(String demandType);
}
