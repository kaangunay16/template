package tr.com.khg.template.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;

/**
 * A Meeting.
 */
@Entity
@Table(name = "meeting")
public class Meeting implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @Column(name = "city")
    private String city;

    @Column(name = "total_places")
    private Integer totalPlaces;

    @Column(name = "jhi_date")
    private LocalDate date;

    @Column(name = "subject")
    private String subject;

    @Column(name = "unit")
    private String unit;

    @Column(name = "total_attendee")
    private Integer totalAttendee;

    @Column(name = "attendee")
    private Integer attendee;

    @Column(name = "jhi_type")
    private String type;

    @Column(name = "finalized")
    private Boolean finalized;

    @OneToMany(mappedBy = "meeting", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JsonIgnoreProperties(value = { "meeting" }, allowSetters = true)
    private Set<MeetingAttendee> meetingAttendees = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public Meeting id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCity() {
        return this.city;
    }

    public Meeting city(String city) {
        this.setCity(city);
        return this;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public Integer getTotalPlaces() {
        return this.totalPlaces;
    }

    public Meeting totalPlaces(Integer totalPlaces) {
        this.setTotalPlaces(totalPlaces);
        return this;
    }

    public void setTotalPlaces(Integer totalPlaces) {
        this.totalPlaces = totalPlaces;
    }

    public LocalDate getDate() {
        return this.date;
    }

    public Meeting date(LocalDate date) {
        this.setDate(date);
        return this;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public String getSubject() {
        return this.subject;
    }

    public Meeting subject(String subject) {
        this.setSubject(subject);
        return this;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getUnit() {
        return this.unit;
    }

    public Meeting unit(String unit) {
        this.setUnit(unit);
        return this;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public Integer getTotalAttendee() {
        return this.totalAttendee;
    }

    public Meeting totalAttendee(Integer totalAttendee) {
        this.setTotalAttendee(totalAttendee);
        return this;
    }

    public void setTotalAttendee(Integer totalAttendee) {
        this.totalAttendee = totalAttendee;
    }

    public Integer getAttendee() {
        return this.attendee;
    }

    public Meeting attendee(Integer attendee) {
        this.setAttendee(attendee);
        return this;
    }

    public void setAttendee(Integer attendee) {
        this.attendee = attendee;
    }

    public String getType() {
        return this.type;
    }

    public Meeting type(String type) {
        this.setType(type);
        return this;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Boolean getFinalized() {
        return this.finalized;
    }

    public Meeting finalized(Boolean finalized) {
        this.setFinalized(finalized);
        return this;
    }

    public void setFinalized(Boolean finalized) {
        this.finalized = finalized;
    }

    public Set<MeetingAttendee> getMeetingAttendees() {
        return this.meetingAttendees;
    }

    public void setMeetingAttendees(Set<MeetingAttendee> meetingAttendees) {
        if (this.meetingAttendees != null) {
            this.meetingAttendees.forEach(i -> i.setMeeting(null));
        }
        if (meetingAttendees != null) {
            meetingAttendees.forEach(i -> i.setMeeting(this));
        }
        this.meetingAttendees = meetingAttendees;
    }

    public Meeting meetingAttendees(Set<MeetingAttendee> meetingAttendees) {
        this.setMeetingAttendees(meetingAttendees);
        return this;
    }

    public Meeting addMeetingAttendee(MeetingAttendee meetingAttendee) {
        this.meetingAttendees.add(meetingAttendee);
        meetingAttendee.setMeeting(this);
        return this;
    }

    public Meeting removeMeetingAttendee(MeetingAttendee meetingAttendee) {
        this.meetingAttendees.remove(meetingAttendee);
        meetingAttendee.setMeeting(null);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Meeting)) {
            return false;
        }
        return id != null && id.equals(((Meeting) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Meeting{" +
            "id=" + getId() +
            ", city='" + getCity() + "'" +
            ", totalPlaces=" + getTotalPlaces() +
            ", date='" + getDate() + "'" +
            ", subject='" + getSubject() + "'" +
            ", unit='" + getUnit() + "'" +
            ", totalAttendee=" + getTotalAttendee() +
            ", attendee=" + getAttendee() +
            ", type='" + getType() + "'" +
            ", finalized='" + getFinalized() + "'" +
            "}";
    }
}
