import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { isPresent } from 'app/core/util/operators';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { ISelection, getSelectionIdentifier } from '../selection.model';

export type EntityResponseType = HttpResponse<ISelection>;
export type EntityArrayResponseType = HttpResponse<ISelection[]>;

@Injectable({ providedIn: 'root' })
export class SelectionService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/selections');

  constructor(protected http: HttpClient, protected applicationConfigService: ApplicationConfigService) {}

  create(selection: ISelection): Observable<EntityResponseType> {
    return this.http.post<ISelection>(this.resourceUrl, selection, { observe: 'response' });
  }

  update(selection: ISelection): Observable<EntityResponseType> {
    return this.http.put<ISelection>(`${this.resourceUrl}/${getSelectionIdentifier(selection) as number}`, selection, {
      observe: 'response',
    });
  }

  partialUpdate(selection: ISelection): Observable<EntityResponseType> {
    return this.http.patch<ISelection>(`${this.resourceUrl}/${getSelectionIdentifier(selection) as number}`, selection, {
      observe: 'response',
    });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<ISelection>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  findByKey(key: string): Observable<ISelection[]> {
    return this.http.get<ISelection[]>(`${this.resourceUrl}/key=${key}`);
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<ISelection[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  addSelectionToCollectionIfMissing(
    selectionCollection: ISelection[],
    ...selectionsToCheck: (ISelection | null | undefined)[]
  ): ISelection[] {
    const selections: ISelection[] = selectionsToCheck.filter(isPresent);
    if (selections.length > 0) {
      const selectionCollectionIdentifiers = selectionCollection.map(selectionItem => getSelectionIdentifier(selectionItem)!);
      const selectionsToAdd = selections.filter(selectionItem => {
        const selectionIdentifier = getSelectionIdentifier(selectionItem);
        if (selectionIdentifier == null || selectionCollectionIdentifiers.includes(selectionIdentifier)) {
          return false;
        }
        selectionCollectionIdentifiers.push(selectionIdentifier);
        return true;
      });
      return [...selectionsToAdd, ...selectionCollection];
    }
    return selectionCollection;
  }
}
