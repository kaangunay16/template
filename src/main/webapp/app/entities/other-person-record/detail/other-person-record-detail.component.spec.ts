import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { OtherPersonRecordDetailComponent } from './other-person-record-detail.component';

describe('OtherPersonRecord Management Detail Component', () => {
  let comp: OtherPersonRecordDetailComponent;
  let fixture: ComponentFixture<OtherPersonRecordDetailComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [OtherPersonRecordDetailComponent],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: { data: of({ otherPersonRecord: { id: 123 } }) },
        },
      ],
    })
      .overrideTemplate(OtherPersonRecordDetailComponent, '')
      .compileComponents();
    fixture = TestBed.createComponent(OtherPersonRecordDetailComponent);
    comp = fixture.componentInstance;
  });

  describe('OnInit', () => {
    it('Should load otherPersonRecord on init', () => {
      // WHEN
      comp.ngOnInit();

      // THEN
      expect(comp.otherPersonRecord).toEqual(expect.objectContaining({ id: 123 }));
    });
  });
});
