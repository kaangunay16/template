import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PersonDeletionDemandsComponent } from './person-deletion-demands.component';

describe('PersonDeletionDemandsComponent', () => {
  let component: PersonDeletionDemandsComponent;
  let fixture: ComponentFixture<PersonDeletionDemandsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [PersonDeletionDemandsComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PersonDeletionDemandsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
