package tr.com.khg.template.service;

import java.util.List;
import java.util.Optional;
import tr.com.khg.template.domain.Phone;

/**
 * Service Interface for managing {@link Phone}.
 */
public interface PhoneService {
    /**
     * Save a phone.
     *
     * @param phone the entity to save.
     * @return the persisted entity.
     */
    Phone save(Phone phone);

    /**
     * Partially updates a phone.
     *
     * @param phone the entity to update partially.
     * @return the persisted entity.
     */
    Optional<Phone> partialUpdate(Phone phone);

    /**
     * Get all the phones.
     *
     * @return the list of entities.
     */
    List<Phone> findAll();

    /**
     * Get the "id" phone.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<Phone> findOne(Long id);

    /**
     * Delete the "id" phone.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
