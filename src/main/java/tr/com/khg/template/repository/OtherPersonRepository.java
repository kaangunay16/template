package tr.com.khg.template.repository;

import java.util.List;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import tr.com.khg.template.domain.OtherPerson;

/**
 * Spring Data SQL repository for the OtherPerson entity.
 */
@SuppressWarnings("unused")
@Repository
public interface OtherPersonRepository extends JpaRepository<OtherPerson, Long> {
    @Query("SELECT p FROM OtherPerson p JOIN FETCH p.identity i LEFT JOIN FETCH p.otherPersonRecords WHERE i.identityNo = :identityNo")
    Optional<OtherPerson> findByIdentityNoFetchAllRelations(@Param("identityNo") String identityNo);

    @Query(
        "SELECT p.identity.identityNo FROM OtherPerson p LEFT JOIN p.identity i LEFT JOIN p.otherPersonRecords r WHERE i.identityNo IN :identityNumbers AND r.city = :city"
    )
    List<String> findExistingOtherPersons(@Param("identityNumbers") List<String> identityNumbers, @Param("city") String city);

    Optional<OtherPerson> findByOtherPersonRecordsId(Long otherPersonRecordsId);
}
