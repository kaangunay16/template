import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
import { ToastrService } from 'ngx-toastr';

import { IOtherPerson } from 'app/entities/other-person/other-person.model';
import { DemandService } from 'app/entities/demand/service/demand.service';
import { Demand } from 'app/entities/demand/demand.model';
import { DemandType } from 'app/entities/enumerations/demand-type.model';
import { DynamicFormCreatorModel, IDynamicFormCreatorModel } from 'app/components/dynamic-form-creator/dynamic-form-field.model';
import { FormValueConverterService } from 'app/components/dynamic-form-creator/form-value-converter.service';
import { ErrorSupportService } from 'app/shared/services/error-support.service';
import { OtherPersonModuleService } from 'app/pages/other-person/other-person-module.service';
import { OTHER_PERSON_DELETION } from 'app/shared/services/demands-types';

@Component({
  selector: 'jhi-other-person-deletion',
  templateUrl: './other-person-deletion.component.html',
  styleUrls: ['./other-person-deletion.component.scss'],
})
export class OtherPersonDeletionComponent implements OnInit, OnDestroy {
  otherPerson!: IOtherPerson;
  formCreator?: IDynamicFormCreatorModel;
  routeSub!: Subscription;
  demandSub?: Subscription;
  recordId!: number;

  constructor(
    private toastr: ToastrService,
    private otherPersonModuleService: OtherPersonModuleService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private demandService: DemandService,
    private formValueService: FormValueConverterService,
    private errorService: ErrorSupportService
  ) {}

  ngOnInit(): void {
    this.routeSub = this.activatedRoute.queryParams.subscribe(params => {
      this.otherPerson = this.otherPersonModuleService.entityCheck(params);

      const recordId = params['recordId'];
      if (!recordId) {
        this.toastr.error('Hatalı talep');
        window.history.back();
      }
      this.recordId = Number(recordId);

      this.formCreator = new DynamicFormCreatorModel([
        {
          id: 'name',
          label: 'Adı Soyadı',
          type: 'text',
          value: `${this.otherPerson.identity!.name!} ${this.otherPerson.identity!.surname!} (TC:${this.otherPerson.identity!
            .identityNo!})`,
          readOnly: true,
        },
        {
          id: 'recordCity',
          label: 'Faaliyet İli',
          type: 'text',
          value: this.getCity(),
          readOnly: true,
        },
        {
          id: 'document',
          label: 'Evrak Numarası',
          type: 'document',
          validators: [Validators.required],
        },
        {
          id: 'description',
          type: 'textarea',
          label: 'Açıklama',
          validators: [Validators.required],
        },
      ]);
    });
  }

  ngOnDestroy(): void {
    this.routeSub.unsubscribe();
    this.demandSub?.unsubscribe();
  }

  getCity(): string {
    const record = this.otherPerson.otherPersonRecords?.find(rec => rec.id === this.recordId);
    if (!record) {
      this.toastr.error('Hatalı talep');
      window.history.back();
    }

    return record!.city!;
  }

  submit(): void {
    const demand = new Demand();
    demand.document = this.formValueService.getFormValue(this.formCreator!, 'document') as string;
    demand.description = this.formValueService.getFormValue(this.formCreator!, 'description') as string;
    demand.status = DemandType.WAITING;
    demand.demandType = OTHER_PERSON_DELETION;
    demand.entityId = this.otherPerson.id;
    this.demandSub = this.demandService.create(demand).subscribe(
      res => {
        if (res.body) {
          this.toastr.success('Talep başarıyla oluşturuldu');
          window.history.back();
        }
      },
      err => this.errorService.errorHandler(err)
    );
  }
}
