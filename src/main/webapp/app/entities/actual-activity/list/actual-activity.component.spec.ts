import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { of } from 'rxjs';

import { ActualActivityService } from '../service/actual-activity.service';

import { ActualActivityComponent } from './actual-activity.component';

describe('ActualActivity Management Component', () => {
  let comp: ActualActivityComponent;
  let fixture: ComponentFixture<ActualActivityComponent>;
  let service: ActualActivityService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      declarations: [ActualActivityComponent],
    })
      .overrideTemplate(ActualActivityComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(ActualActivityComponent);
    comp = fixture.componentInstance;
    service = TestBed.inject(ActualActivityService);

    const headers = new HttpHeaders();
    jest.spyOn(service, 'query').mockReturnValue(
      of(
        new HttpResponse({
          body: [{ id: 123 }],
          headers,
        })
      )
    );
  });

  it('Should call load all on init', () => {
    // WHEN
    comp.ngOnInit();

    // THEN
    expect(service.query).toHaveBeenCalled();
    expect(comp.actualActivities?.[0]).toEqual(expect.objectContaining({ id: 123 }));
  });
});
