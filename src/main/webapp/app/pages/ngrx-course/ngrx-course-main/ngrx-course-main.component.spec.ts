import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NgrxCourseMainComponent } from './ngrx-course-main.component';

describe('NgrxCourseMainComponent', () => {
  let component: NgrxCourseMainComponent;
  let fixture: ComponentFixture<NgrxCourseMainComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [NgrxCourseMainComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NgrxCourseMainComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
