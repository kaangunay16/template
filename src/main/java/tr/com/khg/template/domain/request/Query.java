package tr.com.khg.template.domain.request;

import java.io.Serializable;
import java.util.List;

public class Query implements Serializable {

    private List<String> joins;
    private List<Parameter> parameters;

    public List<String> getJoins() {
        return joins;
    }

    public void setJoins(List<String> joins) {
        this.joins = joins;
    }

    public List<Parameter> getParameters() {
        return parameters;
    }

    public void setParameters(List<Parameter> parameters) {
        this.parameters = parameters;
    }
}
