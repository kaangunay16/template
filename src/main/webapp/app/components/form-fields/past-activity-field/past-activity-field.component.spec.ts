import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PastActivityFieldComponent } from './past-activity-field.component';

describe('PastActivityFieldComponent', () => {
  let component: PastActivityFieldComponent;
  let fixture: ComponentFixture<PastActivityFieldComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [PastActivityFieldComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PastActivityFieldComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
