package tr.com.khg.template.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import tr.com.khg.template.IntegrationTest;
import tr.com.khg.template.domain.OtherPerson;
import tr.com.khg.template.repository.OtherPersonRepository;

/**
 * Integration tests for the {@link OtherPersonResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class OtherPersonResourceIT {

    private static final String ENTITY_API_URL = "/api/other-people";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private OtherPersonRepository otherPersonRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restOtherPersonMockMvc;

    private OtherPerson otherPerson;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static OtherPerson createEntity(EntityManager em) {
        OtherPerson otherPerson = new OtherPerson();
        return otherPerson;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static OtherPerson createUpdatedEntity(EntityManager em) {
        OtherPerson otherPerson = new OtherPerson();
        return otherPerson;
    }

    @BeforeEach
    public void initTest() {
        otherPerson = createEntity(em);
    }

    @Test
    @Transactional
    void createOtherPerson() throws Exception {
        int databaseSizeBeforeCreate = otherPersonRepository.findAll().size();
        // Create the OtherPerson
        restOtherPersonMockMvc
            .perform(
                post(ENTITY_API_URL)
                    .with(csrf())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(otherPerson))
            )
            .andExpect(status().isCreated());

        // Validate the OtherPerson in the database
        List<OtherPerson> otherPersonList = otherPersonRepository.findAll();
        assertThat(otherPersonList).hasSize(databaseSizeBeforeCreate + 1);
        OtherPerson testOtherPerson = otherPersonList.get(otherPersonList.size() - 1);
    }

    @Test
    @Transactional
    void createOtherPersonWithExistingId() throws Exception {
        // Create the OtherPerson with an existing ID
        otherPerson.setId(1L);

        int databaseSizeBeforeCreate = otherPersonRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restOtherPersonMockMvc
            .perform(
                post(ENTITY_API_URL)
                    .with(csrf())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(otherPerson))
            )
            .andExpect(status().isBadRequest());

        // Validate the OtherPerson in the database
        List<OtherPerson> otherPersonList = otherPersonRepository.findAll();
        assertThat(otherPersonList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void getAllOtherPeople() throws Exception {
        // Initialize the database
        otherPersonRepository.saveAndFlush(otherPerson);

        // Get all the otherPersonList
        restOtherPersonMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(otherPerson.getId().intValue())));
    }

    @Test
    @Transactional
    void getOtherPerson() throws Exception {
        // Initialize the database
        otherPersonRepository.saveAndFlush(otherPerson);

        // Get the otherPerson
        restOtherPersonMockMvc
            .perform(get(ENTITY_API_URL_ID, otherPerson.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(otherPerson.getId().intValue()));
    }

    @Test
    @Transactional
    void getNonExistingOtherPerson() throws Exception {
        // Get the otherPerson
        restOtherPersonMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putNewOtherPerson() throws Exception {
        // Initialize the database
        otherPersonRepository.saveAndFlush(otherPerson);

        int databaseSizeBeforeUpdate = otherPersonRepository.findAll().size();

        // Update the otherPerson
        OtherPerson updatedOtherPerson = otherPersonRepository.findById(otherPerson.getId()).get();
        // Disconnect from session so that the updates on updatedOtherPerson are not directly saved in db
        em.detach(updatedOtherPerson);

        restOtherPersonMockMvc
            .perform(
                put(ENTITY_API_URL_ID, updatedOtherPerson.getId())
                    .with(csrf())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(updatedOtherPerson))
            )
            .andExpect(status().isOk());

        // Validate the OtherPerson in the database
        List<OtherPerson> otherPersonList = otherPersonRepository.findAll();
        assertThat(otherPersonList).hasSize(databaseSizeBeforeUpdate);
        OtherPerson testOtherPerson = otherPersonList.get(otherPersonList.size() - 1);
    }

    @Test
    @Transactional
    void putNonExistingOtherPerson() throws Exception {
        int databaseSizeBeforeUpdate = otherPersonRepository.findAll().size();
        otherPerson.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restOtherPersonMockMvc
            .perform(
                put(ENTITY_API_URL_ID, otherPerson.getId())
                    .with(csrf())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(otherPerson))
            )
            .andExpect(status().isBadRequest());

        // Validate the OtherPerson in the database
        List<OtherPerson> otherPersonList = otherPersonRepository.findAll();
        assertThat(otherPersonList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchOtherPerson() throws Exception {
        int databaseSizeBeforeUpdate = otherPersonRepository.findAll().size();
        otherPerson.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restOtherPersonMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .with(csrf())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(otherPerson))
            )
            .andExpect(status().isBadRequest());

        // Validate the OtherPerson in the database
        List<OtherPerson> otherPersonList = otherPersonRepository.findAll();
        assertThat(otherPersonList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamOtherPerson() throws Exception {
        int databaseSizeBeforeUpdate = otherPersonRepository.findAll().size();
        otherPerson.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restOtherPersonMockMvc
            .perform(
                put(ENTITY_API_URL)
                    .with(csrf())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(otherPerson))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the OtherPerson in the database
        List<OtherPerson> otherPersonList = otherPersonRepository.findAll();
        assertThat(otherPersonList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateOtherPersonWithPatch() throws Exception {
        // Initialize the database
        otherPersonRepository.saveAndFlush(otherPerson);

        int databaseSizeBeforeUpdate = otherPersonRepository.findAll().size();

        // Update the otherPerson using partial update
        OtherPerson partialUpdatedOtherPerson = new OtherPerson();
        partialUpdatedOtherPerson.setId(otherPerson.getId());

        restOtherPersonMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedOtherPerson.getId())
                    .with(csrf())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedOtherPerson))
            )
            .andExpect(status().isOk());

        // Validate the OtherPerson in the database
        List<OtherPerson> otherPersonList = otherPersonRepository.findAll();
        assertThat(otherPersonList).hasSize(databaseSizeBeforeUpdate);
        OtherPerson testOtherPerson = otherPersonList.get(otherPersonList.size() - 1);
    }

    @Test
    @Transactional
    void fullUpdateOtherPersonWithPatch() throws Exception {
        // Initialize the database
        otherPersonRepository.saveAndFlush(otherPerson);

        int databaseSizeBeforeUpdate = otherPersonRepository.findAll().size();

        // Update the otherPerson using partial update
        OtherPerson partialUpdatedOtherPerson = new OtherPerson();
        partialUpdatedOtherPerson.setId(otherPerson.getId());

        restOtherPersonMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedOtherPerson.getId())
                    .with(csrf())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedOtherPerson))
            )
            .andExpect(status().isOk());

        // Validate the OtherPerson in the database
        List<OtherPerson> otherPersonList = otherPersonRepository.findAll();
        assertThat(otherPersonList).hasSize(databaseSizeBeforeUpdate);
        OtherPerson testOtherPerson = otherPersonList.get(otherPersonList.size() - 1);
    }

    @Test
    @Transactional
    void patchNonExistingOtherPerson() throws Exception {
        int databaseSizeBeforeUpdate = otherPersonRepository.findAll().size();
        otherPerson.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restOtherPersonMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, otherPerson.getId())
                    .with(csrf())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(otherPerson))
            )
            .andExpect(status().isBadRequest());

        // Validate the OtherPerson in the database
        List<OtherPerson> otherPersonList = otherPersonRepository.findAll();
        assertThat(otherPersonList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchOtherPerson() throws Exception {
        int databaseSizeBeforeUpdate = otherPersonRepository.findAll().size();
        otherPerson.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restOtherPersonMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .with(csrf())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(otherPerson))
            )
            .andExpect(status().isBadRequest());

        // Validate the OtherPerson in the database
        List<OtherPerson> otherPersonList = otherPersonRepository.findAll();
        assertThat(otherPersonList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamOtherPerson() throws Exception {
        int databaseSizeBeforeUpdate = otherPersonRepository.findAll().size();
        otherPerson.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restOtherPersonMockMvc
            .perform(
                patch(ENTITY_API_URL)
                    .with(csrf())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(otherPerson))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the OtherPerson in the database
        List<OtherPerson> otherPersonList = otherPersonRepository.findAll();
        assertThat(otherPersonList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteOtherPerson() throws Exception {
        // Initialize the database
        otherPersonRepository.saveAndFlush(otherPerson);

        int databaseSizeBeforeDelete = otherPersonRepository.findAll().size();

        // Delete the otherPerson
        restOtherPersonMockMvc
            .perform(delete(ENTITY_API_URL_ID, otherPerson.getId()).with(csrf()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<OtherPerson> otherPersonList = otherPersonRepository.findAll();
        assertThat(otherPersonList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
