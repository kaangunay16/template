package tr.com.khg.template.service.utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.*;
import tr.com.khg.template.domain.request.Parameter;
import tr.com.khg.template.domain.request.Query;

public class DynamicQueryBuilder<T> {

    private final EntityManager entityManager;

    private final CriteriaBuilder builder;

    private final CriteriaQuery<T> criteriaQuery;

    private final Root<T> root;

    private final List<QueryJoin> queryJoins;

    public DynamicQueryBuilder(EntityManager entityManager, Class<T> className) {
        this.entityManager = entityManager;
        builder = entityManager.getCriteriaBuilder();
        criteriaQuery = builder.createQuery(className);
        root = criteriaQuery.from(className);
        queryJoins = new ArrayList<>();
    }

    public List<T> createQuery(Query query) {
        createJoins(query.getJoins());
        List<Predicate> predicates = new ArrayList<>();
        query.getParameters().forEach(parameter -> predicates.add(this.createPredicate(parameter)));
        CriteriaQuery<T> q = criteriaQuery.where(builder.and(predicates.toArray(new Predicate[] {}))).distinct(true);
        TypedQuery<T> typedQuery = entityManager.createQuery(q);

        return typedQuery.getResultList();
    }

    private void createJoins(List<String> joins) {
        joins.forEach(join -> root.fetch(join, JoinType.INNER));
    }

    private Predicate createPredicate(Parameter parameter) {
        switch (parameter.getSqlOperation()) {
            case "equals":
                return builder.equal(getPath(parameter.getField()), parameter.getValue());
            case "not-equals":
                return builder.notEqual(getPath(parameter.getField()), parameter.getValue());
            case "like":
                return builder.like(getPath(parameter.getField()), "%" + parameter.getValue() + "%");
            default:
                return null;
        }
    }

    private Path<String> getPath(String field) {
        List<String> fields = new ArrayList<>(Arrays.asList(field.split("\\.")));
        if (fields.size() == 1) {
            return root.get(field);
        }

        return getJoinPath(fields);
    }

    private Path<String> getJoinPath(List<String> fields) {
        Join<Object, Object> join = getJoin(fields.get(0));
        if (join != null) {
            return join.get(fields.get(1));
        } else {
            Join<Object, Object> j = root.join(fields.get(0));
            queryJoins.add(new QueryJoin(fields.get(0), j));
            return j.get(fields.get(1));
        }
    }

    private Join<Object, Object> getJoin(String name) {
        List<QueryJoin> collect = queryJoins.stream().filter(join -> Objects.equals(join.getName(), name)).collect(Collectors.toList());
        return collect.size() > 0 ? collect.get(0).getJoin() : null;
    }
}
