import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';

import { ISelection } from 'app/entities/selection/selection.model';
import { DataPostService } from '../data-post.service';

@Component({
  selector: 'jhi-ngrx-data-add-post',
  templateUrl: './ngrx-data-add-post.component.html',
  styleUrls: ['./ngrx-data-add-post.component.scss'],
})
export class NgrxDataAddPostComponent implements OnInit {
  selectionForm!: FormGroup;

  constructor(private postService: DataPostService, private router: Router) {}

  ngOnInit(): void {
    this.selectionForm = new FormGroup({
      key: new FormControl(null),
      value: new FormControl(null),
      priority: new FormControl(null),
    });
  }

  onSelectionAdd(): void {
    const selection: ISelection = this.selectionForm.value;
    this.postService.add(selection).subscribe(() => {
      this.router.navigate(['/ngrx/data']);
    });
  }
}
