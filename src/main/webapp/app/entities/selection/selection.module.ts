import { NgModule } from '@angular/core';
import { SharedModule } from 'app/shared/shared.module';
import { SelectionComponent } from './list/selection.component';
import { SelectionDetailComponent } from './detail/selection-detail.component';
import { SelectionUpdateComponent } from './update/selection-update.component';
import { SelectionDeleteDialogComponent } from './delete/selection-delete-dialog.component';
import { SelectionRoutingModule } from './route/selection-routing.module';

@NgModule({
  imports: [SharedModule, SelectionRoutingModule],
  declarations: [SelectionComponent, SelectionDetailComponent, SelectionUpdateComponent, SelectionDeleteDialogComponent],
  entryComponents: [SelectionDeleteDialogComponent],
})
export class SelectionModule {}
