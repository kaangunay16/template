import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IPastActivity } from '../past-activity.model';
import { PastActivityService } from '../service/past-activity.service';
import { PastActivityDeleteDialogComponent } from '../delete/past-activity-delete-dialog.component';

@Component({
  selector: 'jhi-past-activity',
  templateUrl: './past-activity.component.html',
})
export class PastActivityComponent implements OnInit {
  pastActivities?: IPastActivity[];
  isLoading = false;

  constructor(protected pastActivityService: PastActivityService, protected modalService: NgbModal) {}

  loadAll(): void {
    this.isLoading = true;

    this.pastActivityService.query().subscribe(
      (res: HttpResponse<IPastActivity[]>) => {
        this.isLoading = false;
        this.pastActivities = res.body ?? [];
      },
      () => {
        this.isLoading = false;
      }
    );
  }

  ngOnInit(): void {
    this.loadAll();
  }

  trackId(index: number, item: IPastActivity): number {
    return item.id!;
  }

  delete(pastActivity: IPastActivity): void {
    const modalRef = this.modalService.open(PastActivityDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.pastActivity = pastActivity;
    // unsubscribe not needed because closed completes on modal close
    modalRef.closed.subscribe(reason => {
      if (reason === 'deleted') {
        this.loadAll();
      }
    });
  }
}
