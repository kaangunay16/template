import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { IPastActivity, PastActivity } from '../past-activity.model';
import { PastActivityService } from '../service/past-activity.service';

@Injectable({ providedIn: 'root' })
export class PastActivityRoutingResolveService implements Resolve<IPastActivity> {
  constructor(protected service: PastActivityService, protected router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IPastActivity> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        mergeMap((pastActivity: HttpResponse<PastActivity>) => {
          if (pastActivity.body) {
            return of(pastActivity.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new PastActivity());
  }
}
