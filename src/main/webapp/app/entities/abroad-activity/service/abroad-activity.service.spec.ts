import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { IAbroadActivity, AbroadActivity } from '../abroad-activity.model';

import { AbroadActivityService } from './abroad-activity.service';

describe('AbroadActivity Service', () => {
  let service: AbroadActivityService;
  let httpMock: HttpTestingController;
  let elemDefault: IAbroadActivity;
  let expectedResult: IAbroadActivity | IAbroadActivity[] | boolean | null;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    expectedResult = null;
    service = TestBed.inject(AbroadActivityService);
    httpMock = TestBed.inject(HttpTestingController);

    elemDefault = {
      id: 0,
      country: 'AAAAAAA',
      startYear: 0,
      endYear: 0,
      position: 'AAAAAAA',
      deleted: false,
    };
  });

  describe('Service methods', () => {
    it('should find an element', () => {
      const returnedFromService = Object.assign({}, elemDefault);

      service.find(123).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(elemDefault);
    });

    it('should create a AbroadActivity', () => {
      const returnedFromService = Object.assign(
        {
          id: 0,
        },
        elemDefault
      );

      const expected = Object.assign({}, returnedFromService);

      service.create(new AbroadActivity()).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'POST' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should update a AbroadActivity', () => {
      const returnedFromService = Object.assign(
        {
          id: 1,
          country: 'BBBBBB',
          startYear: 1,
          endYear: 1,
          position: 'BBBBBB',
          deleted: true,
        },
        elemDefault
      );

      const expected = Object.assign({}, returnedFromService);

      service.update(expected).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PUT' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should partial update a AbroadActivity', () => {
      const patchObject = Object.assign(
        {
          country: 'BBBBBB',
          position: 'BBBBBB',
        },
        new AbroadActivity()
      );

      const returnedFromService = Object.assign(patchObject, elemDefault);

      const expected = Object.assign({}, returnedFromService);

      service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PATCH' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should return a list of AbroadActivity', () => {
      const returnedFromService = Object.assign(
        {
          id: 1,
          country: 'BBBBBB',
          startYear: 1,
          endYear: 1,
          position: 'BBBBBB',
          deleted: true,
        },
        elemDefault
      );

      const expected = Object.assign({}, returnedFromService);

      service.query().subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush([returnedFromService]);
      httpMock.verify();
      expect(expectedResult).toContainEqual(expected);
    });

    it('should delete a AbroadActivity', () => {
      service.delete(123).subscribe(resp => (expectedResult = resp.ok));

      const req = httpMock.expectOne({ method: 'DELETE' });
      req.flush({ status: 200 });
      expect(expectedResult);
    });

    describe('addAbroadActivityToCollectionIfMissing', () => {
      it('should add a AbroadActivity to an empty array', () => {
        const abroadActivity: IAbroadActivity = { id: 123 };
        expectedResult = service.addAbroadActivityToCollectionIfMissing([], abroadActivity);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(abroadActivity);
      });

      it('should not add a AbroadActivity to an array that contains it', () => {
        const abroadActivity: IAbroadActivity = { id: 123 };
        const abroadActivityCollection: IAbroadActivity[] = [
          {
            ...abroadActivity,
          },
          { id: 456 },
        ];
        expectedResult = service.addAbroadActivityToCollectionIfMissing(abroadActivityCollection, abroadActivity);
        expect(expectedResult).toHaveLength(2);
      });

      it("should add a AbroadActivity to an array that doesn't contain it", () => {
        const abroadActivity: IAbroadActivity = { id: 123 };
        const abroadActivityCollection: IAbroadActivity[] = [{ id: 456 }];
        expectedResult = service.addAbroadActivityToCollectionIfMissing(abroadActivityCollection, abroadActivity);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(abroadActivity);
      });

      it('should add only unique AbroadActivity to an array', () => {
        const abroadActivityArray: IAbroadActivity[] = [{ id: 123 }, { id: 456 }, { id: 5932 }];
        const abroadActivityCollection: IAbroadActivity[] = [{ id: 123 }];
        expectedResult = service.addAbroadActivityToCollectionIfMissing(abroadActivityCollection, ...abroadActivityArray);
        expect(expectedResult).toHaveLength(3);
      });

      it('should accept varargs', () => {
        const abroadActivity: IAbroadActivity = { id: 123 };
        const abroadActivity2: IAbroadActivity = { id: 456 };
        expectedResult = service.addAbroadActivityToCollectionIfMissing([], abroadActivity, abroadActivity2);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(abroadActivity);
        expect(expectedResult).toContain(abroadActivity2);
      });

      it('should accept null and undefined values', () => {
        const abroadActivity: IAbroadActivity = { id: 123 };
        expectedResult = service.addAbroadActivityToCollectionIfMissing([], null, abroadActivity, undefined);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(abroadActivity);
      });

      it('should return initial array if no AbroadActivity is added', () => {
        const abroadActivityCollection: IAbroadActivity[] = [{ id: 123 }];
        expectedResult = service.addAbroadActivityToCollectionIfMissing(abroadActivityCollection, undefined, null);
        expect(expectedResult).toEqual(abroadActivityCollection);
      });
    });
  });

  afterEach(() => {
    httpMock.verify();
  });
});
