package tr.com.khg.template.service;

import java.util.List;
import java.util.Optional;
import tr.com.khg.template.domain.Identity;

/**
 * Service Interface for managing {@link Identity}.
 */
public interface IdentityService {
    /**
     * Save a identity.
     *
     * @param identity the entity to save.
     * @return the persisted entity.
     */
    Identity save(Identity identity);

    /**
     * Partially updates a identity.
     *
     * @param identity the entity to update partially.
     * @return the persisted entity.
     */
    Optional<Identity> partialUpdate(Identity identity);

    /**
     * Get all the identities.
     *
     * @return the list of entities.
     */
    List<Identity> findAll();

    /**
     * Get the "id" identity.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<Identity> findOne(Long id);

    /**
     * Delete the "id" identity.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);

    /**
     * Get the identity with identity number or create new onr
     *
     * @param identityNumber Searching identity number
     * @return identity information
     */
    Identity getIdentityWithIdentityNumber(String identityNumber);
}
