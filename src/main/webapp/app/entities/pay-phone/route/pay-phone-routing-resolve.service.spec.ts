jest.mock('@angular/router');

import { TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ActivatedRouteSnapshot, Router } from '@angular/router';
import { of } from 'rxjs';

import { IPayPhone, PayPhone } from '../pay-phone.model';
import { PayPhoneService } from '../service/pay-phone.service';

import { PayPhoneRoutingResolveService } from './pay-phone-routing-resolve.service';

describe('PayPhone routing resolve service', () => {
  let mockRouter: Router;
  let mockActivatedRouteSnapshot: ActivatedRouteSnapshot;
  let routingResolveService: PayPhoneRoutingResolveService;
  let service: PayPhoneService;
  let resultPayPhone: IPayPhone | undefined;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [Router, ActivatedRouteSnapshot],
    });
    mockRouter = TestBed.inject(Router);
    mockActivatedRouteSnapshot = TestBed.inject(ActivatedRouteSnapshot);
    routingResolveService = TestBed.inject(PayPhoneRoutingResolveService);
    service = TestBed.inject(PayPhoneService);
    resultPayPhone = undefined;
  });

  describe('resolve', () => {
    it('should return IPayPhone returned by find', () => {
      // GIVEN
      service.find = jest.fn(id => of(new HttpResponse({ body: { id } })));
      mockActivatedRouteSnapshot.params = { id: 123 };

      // WHEN
      routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
        resultPayPhone = result;
      });

      // THEN
      expect(service.find).toBeCalledWith(123);
      expect(resultPayPhone).toEqual({ id: 123 });
    });

    it('should return new IPayPhone if id is not provided', () => {
      // GIVEN
      service.find = jest.fn();
      mockActivatedRouteSnapshot.params = {};

      // WHEN
      routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
        resultPayPhone = result;
      });

      // THEN
      expect(service.find).not.toBeCalled();
      expect(resultPayPhone).toEqual(new PayPhone());
    });

    it('should route to 404 page if data not found in server', () => {
      // GIVEN
      jest.spyOn(service, 'find').mockReturnValue(of(new HttpResponse({ body: null as unknown as PayPhone })));
      mockActivatedRouteSnapshot.params = { id: 123 };

      // WHEN
      routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
        resultPayPhone = result;
      });

      // THEN
      expect(service.find).toBeCalledWith(123);
      expect(resultPayPhone).toEqual(undefined);
      expect(mockRouter.navigate).toHaveBeenCalledWith(['404']);
    });
  });
});
