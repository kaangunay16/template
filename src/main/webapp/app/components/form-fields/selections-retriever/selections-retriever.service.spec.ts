import { TestBed } from '@angular/core/testing';

import { SelectionsRetrieverService } from './selections-retriever.service';

describe('SelectionsRetrieverService', () => {
  let service: SelectionsRetrieverService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SelectionsRetrieverService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
