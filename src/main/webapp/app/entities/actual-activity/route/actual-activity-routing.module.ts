import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { ActualActivityComponent } from '../list/actual-activity.component';
import { ActualActivityDetailComponent } from '../detail/actual-activity-detail.component';
import { ActualActivityUpdateComponent } from '../update/actual-activity-update.component';
import { ActualActivityRoutingResolveService } from './actual-activity-routing-resolve.service';

const actualActivityRoute: Routes = [
  {
    path: '',
    component: ActualActivityComponent,
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: ActualActivityDetailComponent,
    resolve: {
      actualActivity: ActualActivityRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: ActualActivityUpdateComponent,
    resolve: {
      actualActivity: ActualActivityRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: ActualActivityUpdateComponent,
    resolve: {
      actualActivity: ActualActivityRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
];

@NgModule({
  imports: [RouterModule.forChild(actualActivityRoute)],
  exports: [RouterModule],
})
export class ActualActivityRoutingModule {}
