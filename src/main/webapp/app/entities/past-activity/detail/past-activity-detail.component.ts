import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IPastActivity } from '../past-activity.model';

@Component({
  selector: 'jhi-past-activity-detail',
  templateUrl: './past-activity-detail.component.html',
})
export class PastActivityDetailComponent implements OnInit {
  pastActivity: IPastActivity | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ pastActivity }) => {
      this.pastActivity = pastActivity;
    });
  }

  previousState(): void {
    window.history.back();
  }
}
