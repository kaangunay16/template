import { IIdentity } from 'app/entities/identity/identity.model';
import { IAbroadActivity } from 'app/entities/abroad-activity/abroad-activity.model';
import { IActualActivity } from 'app/entities/actual-activity/actual-activity.model';
import { IPastActivity } from 'app/entities/past-activity/past-activity.model';
import { IOtherName } from 'app/entities/other-name/other-name.model';
import { IPhone } from 'app/entities/phone/phone.model';
import { HasId } from '../form-entity.model';

export interface IPerson extends HasId {
  id?: number;
  description?: string | null;
  position?: string | null;
  section?: string | null;
  city?: string | null;
  district?: string | null;
  institution?: string | null;
  organization?: string | null;
  deleted?: boolean | null;
  status?: string | null;
  identity?: IIdentity | null;
  abroadActivities?: IAbroadActivity[] | null;
  actualActivities?: IActualActivity[] | null;
  pastActivities?: IPastActivity[] | null;
  otherNames?: IOtherName[] | null;
  phones?: IPhone[] | null;
}

export class Person implements IPerson {
  constructor(
    public id?: number,
    public description?: string | null,
    public position?: string | null,
    public section?: string | null,
    public city?: string | null,
    public district?: string | null,
    public institution?: string | null,
    public organization?: string | null,
    public deleted?: boolean | null,
    public status?: string | null,
    public identity?: IIdentity | null,
    public abroadActivities?: IAbroadActivity[] | null,
    public actualActivities?: IActualActivity[] | null,
    public pastActivities?: IPastActivity[] | null,
    public otherNames?: IOtherName[] | null,
    public phones?: IPhone[] | null
  ) {
    this.deleted = this.deleted ?? false;
  }
}

export function getPersonIdentifier(person: IPerson): number | undefined {
  return person.id;
}
