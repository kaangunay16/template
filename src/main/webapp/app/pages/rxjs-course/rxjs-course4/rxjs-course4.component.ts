import { AfterViewInit, Component, OnInit } from '@angular/core';
import { from, fromEvent } from 'rxjs';
import { ToastrService } from 'ngx-toastr';

/*
 * Convert Arrays, DOM Events and Promises to Observable
 * https://www.youtube.com/watch?v=Xz4CGz0F7DA
 */

@Component({
  selector: 'jhi-rxjs-course4',
  templateUrl: './rxjs-course4.component.html',
  styleUrls: ['./rxjs-course4.component.scss'],
})
export class RxjsCourse4Component implements OnInit, AfterViewInit {
  clickCount = 0;

  posts: { id: number; title: string; description: string }[] = [
    { id: 1, title: 'Title1', description: 'Description1' },
    { id: 2, title: 'Title2', description: 'Description2' },
    { id: 3, title: 'Title3', description: 'Description3' },
  ];

  // From ile listeden observable elde edilmesi
  posts$ = from(this.posts);

  promise = new Promise(resolve => {
    setTimeout(() => {
      resolve('Resolve the promise. Sending Data');
    }, 3000);
  });

  promise$ = from(this.promise);

  constructor(private toastrService: ToastrService) {}

  ngOnInit(): void {
    const p: string[] = [];
    this.posts$.subscribe(
      data => p.push(data.title),
      err => p.push(err.toString()),
      () => this.toastrService.info(p.join(', '))
    );

    this.promise$.subscribe({
      next: data => this.toastrService.success(data as string),
      error: err => this.toastrService.error(err.toString()),
      complete: () => this.toastrService.success('Promise$ Completed'),
    });
  }

  ngAfterViewInit(): void {
    fromEvent(document.getElementById('number-four')!, 'click').subscribe(() => {
      this.toastrService.info(`${++this.clickCount} kere tıklandı.`);
    });
  }
}
