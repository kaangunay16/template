import { HasId } from '../form-entity.model';

export interface IPayPhone extends HasId {
  id?: number;
  city?: string | null;
  phoneNumber?: string | null;
}

export class PayPhone implements IPayPhone {
  constructor(public id?: number, public city?: string | null, public phoneNumber?: string | null) {}
}

export function getPayPhoneIdentifier(payPhone: IPayPhone): number | undefined {
  return payPhone.id;
}
