import { forwardRef, Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { Observable } from 'rxjs';
import { first, tap } from 'rxjs/operators';

import { DataPostService } from './data-post.service';
import { NgrxCourseDataModule } from './ngrx-course-data.module';

@Injectable({
  providedIn: forwardRef(() => NgrxCourseDataModule),
})
export class DataPostResolver implements Resolve<boolean> {
  constructor(private dataPostService: DataPostService) {}

  resolve(): Observable<boolean> | Promise<boolean> | boolean {
    return this.dataPostService.loaded$.pipe(
      tap(loaded => {
        if (!loaded) {
          this.dataPostService.getAll();
        }
      }),
      first()
    );
  }
}
