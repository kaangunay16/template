import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';

import { IDemand, Demand } from '../demand.model';
import { DemandService } from '../service/demand.service';
import { DemandType } from 'app/entities/enumerations/demand-type.model';

@Component({
  selector: 'jhi-demand-update',
  templateUrl: './demand-update.component.html',
})
export class DemandUpdateComponent implements OnInit {
  isSaving = false;
  demandTypeValues = Object.keys(DemandType);

  editForm = this.fb.group({
    id: [],
    document: [],
    demandedBy: [],
    description: [],
    demandType: [],
    status: [],
    processedBy: [],
    entityId: [],
    processDescription: [],
  });

  constructor(protected demandService: DemandService, protected activatedRoute: ActivatedRoute, protected fb: FormBuilder) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ demand }) => {
      this.updateForm(demand);
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const demand = this.createFromForm();
    if (demand.id !== undefined) {
      this.subscribeToSaveResponse(this.demandService.update(demand));
    } else {
      this.subscribeToSaveResponse(this.demandService.create(demand));
    }
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IDemand>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(demand: IDemand): void {
    this.editForm.patchValue({
      id: demand.id,
      document: demand.document,
      demandedBy: demand.demandedBy,
      description: demand.description,
      demandType: demand.demandType,
      status: demand.status,
      processedBy: demand.processedBy,
      entityId: demand.entityId,
      processDescription: demand.processDescription,
    });
  }

  protected createFromForm(): IDemand {
    return {
      ...new Demand(),
      id: this.editForm.get(['id'])!.value,
      document: this.editForm.get(['document'])!.value,
      demandedBy: this.editForm.get(['demandedBy'])!.value,
      description: this.editForm.get(['description'])!.value,
      demandType: this.editForm.get(['demandType'])!.value,
      status: this.editForm.get(['status'])!.value,
      processedBy: this.editForm.get(['processedBy'])!.value,
      entityId: this.editForm.get(['entityId'])!.value,
      processDescription: this.editForm.get(['processDescription'])!.value,
    };
  }
}
