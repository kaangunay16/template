import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { ISelection, Selection } from '../selection.model';

import { SelectionService } from './selection.service';

describe('Selection Service', () => {
  let service: SelectionService;
  let httpMock: HttpTestingController;
  let elemDefault: ISelection;
  let expectedResult: ISelection | ISelection[] | boolean | null;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    expectedResult = null;
    service = TestBed.inject(SelectionService);
    httpMock = TestBed.inject(HttpTestingController);

    elemDefault = {
      id: 0,
      key: 'AAAAAAA',
      value: 'AAAAAAA',
      priority: 0,
    };
  });

  describe('Service methods', () => {
    it('should find an element', () => {
      const returnedFromService = Object.assign({}, elemDefault);

      service.find(123).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(elemDefault);
    });

    it('should create a Selection', () => {
      const returnedFromService = Object.assign(
        {
          id: 0,
        },
        elemDefault
      );

      const expected = Object.assign({}, returnedFromService);

      service.create(new Selection()).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'POST' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should update a Selection', () => {
      const returnedFromService = Object.assign(
        {
          id: 1,
          key: 'BBBBBB',
          value: 'BBBBBB',
          priority: 1,
        },
        elemDefault
      );

      const expected = Object.assign({}, returnedFromService);

      service.update(expected).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PUT' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should partial update a Selection', () => {
      const patchObject = Object.assign(
        {
          priority: 1,
        },
        new Selection()
      );

      const returnedFromService = Object.assign(patchObject, elemDefault);

      const expected = Object.assign({}, returnedFromService);

      service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PATCH' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should return a list of Selection', () => {
      const returnedFromService = Object.assign(
        {
          id: 1,
          key: 'BBBBBB',
          value: 'BBBBBB',
          priority: 1,
        },
        elemDefault
      );

      const expected = Object.assign({}, returnedFromService);

      service.query().subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush([returnedFromService]);
      httpMock.verify();
      expect(expectedResult).toContainEqual(expected);
    });

    it('should delete a Selection', () => {
      service.delete(123).subscribe(resp => (expectedResult = resp.ok));

      const req = httpMock.expectOne({ method: 'DELETE' });
      req.flush({ status: 200 });
      expect(expectedResult);
    });

    describe('addSelectionToCollectionIfMissing', () => {
      it('should add a Selection to an empty array', () => {
        const selection: ISelection = { id: 123 };
        expectedResult = service.addSelectionToCollectionIfMissing([], selection);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(selection);
      });

      it('should not add a Selection to an array that contains it', () => {
        const selection: ISelection = { id: 123 };
        const selectionCollection: ISelection[] = [
          {
            ...selection,
          },
          { id: 456 },
        ];
        expectedResult = service.addSelectionToCollectionIfMissing(selectionCollection, selection);
        expect(expectedResult).toHaveLength(2);
      });

      it("should add a Selection to an array that doesn't contain it", () => {
        const selection: ISelection = { id: 123 };
        const selectionCollection: ISelection[] = [{ id: 456 }];
        expectedResult = service.addSelectionToCollectionIfMissing(selectionCollection, selection);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(selection);
      });

      it('should add only unique Selection to an array', () => {
        const selectionArray: ISelection[] = [{ id: 123 }, { id: 456 }, { id: 86187 }];
        const selectionCollection: ISelection[] = [{ id: 123 }];
        expectedResult = service.addSelectionToCollectionIfMissing(selectionCollection, ...selectionArray);
        expect(expectedResult).toHaveLength(3);
      });

      it('should accept varargs', () => {
        const selection: ISelection = { id: 123 };
        const selection2: ISelection = { id: 456 };
        expectedResult = service.addSelectionToCollectionIfMissing([], selection, selection2);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(selection);
        expect(expectedResult).toContain(selection2);
      });

      it('should accept null and undefined values', () => {
        const selection: ISelection = { id: 123 };
        expectedResult = service.addSelectionToCollectionIfMissing([], null, selection, undefined);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(selection);
      });

      it('should return initial array if no Selection is added', () => {
        const selectionCollection: ISelection[] = [{ id: 123 }];
        expectedResult = service.addSelectionToCollectionIfMissing(selectionCollection, undefined, null);
        expect(expectedResult).toEqual(selectionCollection);
      });
    });
  });

  afterEach(() => {
    httpMock.verify();
  });
});
