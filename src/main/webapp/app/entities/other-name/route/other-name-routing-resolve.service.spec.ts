jest.mock('@angular/router');

import { TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ActivatedRouteSnapshot, Router } from '@angular/router';
import { of } from 'rxjs';

import { IOtherName, OtherName } from '../other-name.model';
import { OtherNameService } from '../service/other-name.service';

import { OtherNameRoutingResolveService } from './other-name-routing-resolve.service';

describe('OtherName routing resolve service', () => {
  let mockRouter: Router;
  let mockActivatedRouteSnapshot: ActivatedRouteSnapshot;
  let routingResolveService: OtherNameRoutingResolveService;
  let service: OtherNameService;
  let resultOtherName: IOtherName | undefined;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [Router, ActivatedRouteSnapshot],
    });
    mockRouter = TestBed.inject(Router);
    mockActivatedRouteSnapshot = TestBed.inject(ActivatedRouteSnapshot);
    routingResolveService = TestBed.inject(OtherNameRoutingResolveService);
    service = TestBed.inject(OtherNameService);
    resultOtherName = undefined;
  });

  describe('resolve', () => {
    it('should return IOtherName returned by find', () => {
      // GIVEN
      service.find = jest.fn(id => of(new HttpResponse({ body: { id } })));
      mockActivatedRouteSnapshot.params = { id: 123 };

      // WHEN
      routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
        resultOtherName = result;
      });

      // THEN
      expect(service.find).toBeCalledWith(123);
      expect(resultOtherName).toEqual({ id: 123 });
    });

    it('should return new IOtherName if id is not provided', () => {
      // GIVEN
      service.find = jest.fn();
      mockActivatedRouteSnapshot.params = {};

      // WHEN
      routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
        resultOtherName = result;
      });

      // THEN
      expect(service.find).not.toBeCalled();
      expect(resultOtherName).toEqual(new OtherName());
    });

    it('should route to 404 page if data not found in server', () => {
      // GIVEN
      jest.spyOn(service, 'find').mockReturnValue(of(new HttpResponse({ body: null as unknown as OtherName })));
      mockActivatedRouteSnapshot.params = { id: 123 };

      // WHEN
      routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
        resultOtherName = result;
      });

      // THEN
      expect(service.find).toBeCalledWith(123);
      expect(resultOtherName).toEqual(undefined);
      expect(mockRouter.navigate).toHaveBeenCalledWith(['404']);
    });
  });
});
