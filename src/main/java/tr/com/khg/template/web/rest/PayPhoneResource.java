package tr.com.khg.template.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.ResponseUtil;
import tr.com.khg.template.domain.PayPhone;
import tr.com.khg.template.repository.PayPhoneRepository;
import tr.com.khg.template.service.PayPhoneService;
import tr.com.khg.template.web.rest.errors.BadRequestAlertException;

/**
 * REST controller for managing {@link tr.com.khg.template.domain.PayPhone}.
 */
@RestController
@RequestMapping("/api")
public class PayPhoneResource {

    private final Logger log = LoggerFactory.getLogger(PayPhoneResource.class);

    private static final String ENTITY_NAME = "payPhone";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final PayPhoneService payPhoneService;

    private final PayPhoneRepository payPhoneRepository;

    public PayPhoneResource(PayPhoneService payPhoneService, PayPhoneRepository payPhoneRepository) {
        this.payPhoneService = payPhoneService;
        this.payPhoneRepository = payPhoneRepository;
    }

    /**
     * {@code POST  /pay-phones} : Create a new payPhone.
     *
     * @param payPhone the payPhone to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new payPhone, or with status {@code 400 (Bad Request)} if the payPhone has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/pay-phones")
    public ResponseEntity<PayPhone> createPayPhone(@RequestBody PayPhone payPhone) throws URISyntaxException {
        log.debug("REST request to save PayPhone : {}", payPhone);
        if (payPhone.getId() != null) {
            throw new BadRequestAlertException("A new payPhone cannot already have an ID", ENTITY_NAME, "idexists");
        }
        PayPhone result = payPhoneService.save(payPhone);
        return ResponseEntity
            .created(new URI("/api/pay-phones/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /pay-phones/:id} : Updates an existing payPhone.
     *
     * @param id the id of the payPhone to save.
     * @param payPhone the payPhone to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated payPhone,
     * or with status {@code 400 (Bad Request)} if the payPhone is not valid,
     * or with status {@code 500 (Internal Server Error)} if the payPhone couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/pay-phones/{id}")
    public ResponseEntity<PayPhone> updatePayPhone(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody PayPhone payPhone
    ) throws URISyntaxException {
        log.debug("REST request to update PayPhone : {}, {}", id, payPhone);
        if (payPhone.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, payPhone.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!payPhoneRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        PayPhone result = payPhoneService.save(payPhone);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, payPhone.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /pay-phones/:id} : Partial updates given fields of an existing payPhone, field will ignore if it is null
     *
     * @param id the id of the payPhone to save.
     * @param payPhone the payPhone to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated payPhone,
     * or with status {@code 400 (Bad Request)} if the payPhone is not valid,
     * or with status {@code 404 (Not Found)} if the payPhone is not found,
     * or with status {@code 500 (Internal Server Error)} if the payPhone couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/pay-phones/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<PayPhone> partialUpdatePayPhone(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody PayPhone payPhone
    ) throws URISyntaxException {
        log.debug("REST request to partial update PayPhone partially : {}, {}", id, payPhone);
        if (payPhone.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, payPhone.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!payPhoneRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<PayPhone> result = payPhoneService.partialUpdate(payPhone);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, payPhone.getId().toString())
        );
    }

    /**
     * {@code GET  /pay-phones} : get all the payPhones.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of payPhones in body.
     */
    @GetMapping("/pay-phones")
    public List<PayPhone> getAllPayPhones() {
        log.debug("REST request to get all PayPhones");
        return payPhoneService.findAll();
    }

    /**
     * {@code GET  /pay-phones/:id} : get the "id" payPhone.
     *
     * @param id the id of the payPhone to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the payPhone, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/pay-phones/{id}")
    public ResponseEntity<PayPhone> getPayPhone(@PathVariable Long id) {
        log.debug("REST request to get PayPhone : {}", id);
        Optional<PayPhone> payPhone = payPhoneService.findOne(id);
        return ResponseUtil.wrapOrNotFound(payPhone);
    }

    /**
     * {@code DELETE  /pay-phones/:id} : delete the "id" payPhone.
     *
     * @param id the id of the payPhone to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/pay-phones/{id}")
    public ResponseEntity<Void> deletePayPhone(@PathVariable Long id) {
        log.debug("REST request to delete PayPhone : {}", id);
        payPhoneService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }
}
