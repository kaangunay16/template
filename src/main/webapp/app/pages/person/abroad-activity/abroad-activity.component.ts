import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { ToastrService } from 'ngx-toastr';

import { IPerson } from 'app/entities/person/person.model';
import { DynamicFormCreatorModel, IDynamicFormCreatorModel } from 'app/components/dynamic-form-creator/dynamic-form-field.model';
import { PersonModuleService } from 'app/pages/person/person-module.service';
import { FormValueConverterService } from 'app/components/dynamic-form-creator/form-value-converter.service';
import { IAbroadActivity } from 'app/entities/abroad-activity/abroad-activity.model';
import { PersonService } from 'app/entities/person/service/person.service';
import { ErrorSupportService } from 'app/shared/services/error-support.service';

@Component({
  selector: 'jhi-abroad-activity',
  templateUrl: './abroad-activity.component.html',
  styleUrls: ['./abroad-activity.component.scss'],
})
export class AbroadActivityComponent implements OnInit, OnDestroy {
  person!: IPerson;
  formCreator!: IDynamicFormCreatorModel;
  routeSub!: Subscription;

  constructor(
    private personModuleService: PersonModuleService,
    private router: Router,
    private formValueService: FormValueConverterService,
    private personService: PersonService,
    private toastr: ToastrService,
    private activatedRoute: ActivatedRoute,
    private errorService: ErrorSupportService
  ) {}

  ngOnInit(): void {
    this.routeSub = this.activatedRoute.queryParams.subscribe(params => {
      this.person = this.personModuleService.entityCheck(params);
      this.formCreator = new DynamicFormCreatorModel([
        {
          id: 'abroadActivity',
          type: 'abroadActivity',
          label: 'Yurtdışı Faaliyeti',
          multiple: true,
          value: this.person.abroadActivities,
        },
      ]);
    });
  }

  ngOnDestroy(): void {
    this.routeSub.unsubscribe();
  }

  submit(): void {
    this.setFormValuesToEntity();
    this.personService.create(this.person).subscribe(
      () => {
        this.toastr.success('Kayıt başarılı ile eklendi.');
        this.router.navigate(['person', 'get-person-info']).then(() => {
          this.personModuleService.clearEntities();
        });
      },
      err => this.errorService.errorHandler(err)
    );
  }

  back(): void {
    this.setFormValuesToEntity();
    window.history.back();
  }

  updateSubmit(): void {
    this.setFormValuesToEntity();
    this.personModuleService.updatePerson(this.person);
  }

  private setFormValuesToEntity(): void {
    this.person.abroadActivities = this.formValueService.getFormValue(this.formCreator, 'abroadActivity') as IAbroadActivity[];
  }
}
