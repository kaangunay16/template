package tr.com.khg.template.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import javax.persistence.*;
import org.hibernate.annotations.ResultCheckStyle;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

/**
 * A ActualActivity.
 */
@Entity
@Table(name = "ac_activity")
@Where(clause = "deleted = false")
@SQLDelete(sql = "update ac_activity set deleted = true where id = ?", check = ResultCheckStyle.COUNT)
public class ActualActivity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @Column(name = "city")
    private String city;

    @Column(name = "position")
    private String position;

    @Column(name = "start_year")
    private Integer startYear;

    @Column(name = "deleted")
    private Boolean deleted;

    @Column(name = "field")
    private String field;

    @ManyToOne
    @JsonIgnoreProperties(
        value = { "identity", "abroadActivities", "actualActivities", "pastActivities", "otherNames", "phones" },
        allowSetters = true
    )
    private Person person;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public ActualActivity id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCity() {
        return this.city;
    }

    public ActualActivity city(String city) {
        this.setCity(city);
        return this;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getPosition() {
        return this.position;
    }

    public ActualActivity position(String position) {
        this.setPosition(position);
        return this;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public Integer getStartYear() {
        return this.startYear;
    }

    public ActualActivity startYear(Integer startYear) {
        this.setStartYear(startYear);
        return this;
    }

    public void setStartYear(Integer startYear) {
        this.startYear = startYear;
    }

    public Boolean getDeleted() {
        return this.deleted;
    }

    public ActualActivity deleted(Boolean deleted) {
        this.setDeleted(deleted);
        return this;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    public String getField() {
        return this.field;
    }

    public ActualActivity field(String field) {
        this.setField(field);
        return this;
    }

    public void setField(String field) {
        this.field = field;
    }

    public Person getPerson() {
        return this.person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }

    public ActualActivity person(Person person) {
        this.setPerson(person);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ActualActivity)) {
            return false;
        }
        return id != null && id.equals(((ActualActivity) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "ActualActivity{" +
            "id=" + getId() +
            ", city='" + getCity() + "'" +
            ", position='" + getPosition() + "'" +
            ", startYear=" + getStartYear() +
            ", deleted='" + getDeleted() + "'" +
            ", field='" + getField() + "'" +
            "}";
    }
}
