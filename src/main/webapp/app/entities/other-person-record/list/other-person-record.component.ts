import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IOtherPersonRecord } from '../other-person-record.model';
import { OtherPersonRecordService } from '../service/other-person-record.service';
import { OtherPersonRecordDeleteDialogComponent } from '../delete/other-person-record-delete-dialog.component';

@Component({
  selector: 'jhi-other-person-record',
  templateUrl: './other-person-record.component.html',
})
export class OtherPersonRecordComponent implements OnInit {
  otherPersonRecords?: IOtherPersonRecord[];
  isLoading = false;

  constructor(protected otherPersonRecordService: OtherPersonRecordService, protected modalService: NgbModal) {}

  loadAll(): void {
    this.isLoading = true;

    this.otherPersonRecordService.query().subscribe(
      (res: HttpResponse<IOtherPersonRecord[]>) => {
        this.isLoading = false;
        this.otherPersonRecords = res.body ?? [];
      },
      () => {
        this.isLoading = false;
      }
    );
  }

  ngOnInit(): void {
    this.loadAll();
  }

  trackId(index: number, item: IOtherPersonRecord): number {
    return item.id!;
  }

  delete(otherPersonRecord: IOtherPersonRecord): void {
    const modalRef = this.modalService.open(OtherPersonRecordDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.otherPersonRecord = otherPersonRecord;
    // unsubscribe not needed because closed completes on modal close
    modalRef.closed.subscribe(reason => {
      if (reason === 'deleted') {
        this.loadAll();
      }
    });
  }
}
