import { Component, OnDestroy, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';

import { IdentityRetrieverComponent } from 'app/components/identity-retriever/identity-retriever.component';
import { OtherPersonService } from 'app/entities/other-person/service/other-person.service';
import { ErrorSupportService } from 'app/shared/services/error-support.service';
import { OtherPersonModuleService } from 'app/pages/other-person/other-person-module.service';

@Component({
  selector: 'jhi-other-person-person-info',
  templateUrl: './other-person-person-info.component.html',
  styleUrls: ['./other-person-person-info.component.scss'],
})
export class OtherPersonPersonInfoComponent implements OnDestroy {
  @ViewChild('retriever')
  retriever!: IdentityRetrieverComponent;

  otherPersonSub?: Subscription;

  constructor(
    private otherPersonService: OtherPersonService,
    private errorService: ErrorSupportService,
    private otherPersonModuleService: OtherPersonModuleService,
    private router: Router
  ) {}

  ngOnDestroy(): void {
    this.otherPersonSub?.unsubscribe();
  }

  identityInfoChanged(identity: string): void {
    this.otherPersonSub = this.otherPersonService.findByIdentityNumber(identity).subscribe(
      res => {
        if (res.body) {
          this.otherPersonModuleService.addSingleEntity(res.body);
          this.router.navigate(['/other-person', 'other-person-result'], { queryParams: { mode: 'update' } });
        }
      },
      err => this.errorService.errorHandler(err)
    );

    this.retriever.dialogRef?.close();
  }
}
