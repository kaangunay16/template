import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PersonGetIdentityComponent } from './person-get-identity.component';

describe('PersonGetIdentityComponent', () => {
  let component: PersonGetIdentityComponent;
  let fixture: ComponentFixture<PersonGetIdentityComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [PersonGetIdentityComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PersonGetIdentityComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
