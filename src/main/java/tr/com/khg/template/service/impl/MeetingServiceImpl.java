package tr.com.khg.template.service.impl;

import java.util.List;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tr.com.khg.template.domain.Meeting;
import tr.com.khg.template.repository.IdentityRepository;
import tr.com.khg.template.repository.MeetingRepository;
import tr.com.khg.template.service.MeetingService;

/**
 * Service Implementation for managing {@link Meeting}.
 */
@Service
@Transactional
public class MeetingServiceImpl implements MeetingService {

    private final Logger log = LoggerFactory.getLogger(MeetingServiceImpl.class);

    private final MeetingRepository meetingRepository;

    private final IdentityRepository identityRepository;

    public MeetingServiceImpl(MeetingRepository meetingRepository, IdentityRepository identityRepository) {
        this.meetingRepository = meetingRepository;
        this.identityRepository = identityRepository;
    }

    @Override
    public Meeting save(Meeting meeting) {
        log.debug("Request to save Meeting : {}", meeting);
        if (meeting.getMeetingAttendees() != null) {
            meeting.getMeetingAttendees().forEach(meetingAttendee -> meetingAttendee.setMeeting(meeting));

            meeting
                .getMeetingAttendees()
                .forEach(meetingAttendee -> {
                    if (meetingAttendee.getId() == null) {
                        meetingAttendee.setIdentity(identityRepository.save(meetingAttendee.getIdentity()));
                    }
                });
        }

        return meetingRepository.save(meeting);
    }

    @Override
    public Optional<Meeting> partialUpdate(Meeting meeting) {
        log.debug("Request to partially update Meeting : {}", meeting);

        return meetingRepository
            .findById(meeting.getId())
            .map(existingMeeting -> {
                if (meeting.getCity() != null) {
                    existingMeeting.setCity(meeting.getCity());
                }
                if (meeting.getTotalPlaces() != null) {
                    existingMeeting.setTotalPlaces(meeting.getTotalPlaces());
                }
                if (meeting.getDate() != null) {
                    existingMeeting.setDate(meeting.getDate());
                }
                if (meeting.getSubject() != null) {
                    existingMeeting.setSubject(meeting.getSubject());
                }
                if (meeting.getUnit() != null) {
                    existingMeeting.setUnit(meeting.getUnit());
                }
                if (meeting.getTotalAttendee() != null) {
                    existingMeeting.setTotalAttendee(meeting.getTotalAttendee());
                }
                if (meeting.getAttendee() != null) {
                    existingMeeting.setAttendee(meeting.getAttendee());
                }
                if (meeting.getType() != null) {
                    existingMeeting.setType(meeting.getType());
                }
                if (meeting.getFinalized() != null) {
                    existingMeeting.setFinalized(meeting.getFinalized());
                }

                return existingMeeting;
            })
            .map(meetingRepository::save);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Meeting> findAll() {
        log.debug("Request to get all Meetings");
        return meetingRepository.findAll();
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<Meeting> findOne(Long id) {
        log.debug("Request to get Meeting : {}", id);
        return meetingRepository.findById(id);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete Meeting : {}", id);
        meetingRepository.deleteById(id);
    }

    @Override
    public List<Meeting> findByFinalized(boolean finalized) {
        log.debug("Request to find meetings with finalized status");
        return meetingRepository.findByFinalized(finalized);
    }
}
