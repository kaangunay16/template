import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { IActualActivity, ActualActivity } from '../actual-activity.model';

import { ActualActivityService } from './actual-activity.service';

describe('ActualActivity Service', () => {
  let service: ActualActivityService;
  let httpMock: HttpTestingController;
  let elemDefault: IActualActivity;
  let expectedResult: IActualActivity | IActualActivity[] | boolean | null;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    expectedResult = null;
    service = TestBed.inject(ActualActivityService);
    httpMock = TestBed.inject(HttpTestingController);

    elemDefault = {
      id: 0,
      city: 'AAAAAAA',
      position: 'AAAAAAA',
      startYear: 0,
      deleted: false,
      field: 'AAAAAAA',
    };
  });

  describe('Service methods', () => {
    it('should find an element', () => {
      const returnedFromService = Object.assign({}, elemDefault);

      service.find(123).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(elemDefault);
    });

    it('should create a ActualActivity', () => {
      const returnedFromService = Object.assign(
        {
          id: 0,
        },
        elemDefault
      );

      const expected = Object.assign({}, returnedFromService);

      service.create(new ActualActivity()).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'POST' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should update a ActualActivity', () => {
      const returnedFromService = Object.assign(
        {
          id: 1,
          city: 'BBBBBB',
          position: 'BBBBBB',
          startYear: 1,
          deleted: true,
          field: 'BBBBBB',
        },
        elemDefault
      );

      const expected = Object.assign({}, returnedFromService);

      service.update(expected).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PUT' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should partial update a ActualActivity', () => {
      const patchObject = Object.assign(
        {
          city: 'BBBBBB',
          deleted: true,
        },
        new ActualActivity()
      );

      const returnedFromService = Object.assign(patchObject, elemDefault);

      const expected = Object.assign({}, returnedFromService);

      service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PATCH' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should return a list of ActualActivity', () => {
      const returnedFromService = Object.assign(
        {
          id: 1,
          city: 'BBBBBB',
          position: 'BBBBBB',
          startYear: 1,
          deleted: true,
          field: 'BBBBBB',
        },
        elemDefault
      );

      const expected = Object.assign({}, returnedFromService);

      service.query().subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush([returnedFromService]);
      httpMock.verify();
      expect(expectedResult).toContainEqual(expected);
    });

    it('should delete a ActualActivity', () => {
      service.delete(123).subscribe(resp => (expectedResult = resp.ok));

      const req = httpMock.expectOne({ method: 'DELETE' });
      req.flush({ status: 200 });
      expect(expectedResult);
    });

    describe('addActualActivityToCollectionIfMissing', () => {
      it('should add a ActualActivity to an empty array', () => {
        const actualActivity: IActualActivity = { id: 123 };
        expectedResult = service.addActualActivityToCollectionIfMissing([], actualActivity);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(actualActivity);
      });

      it('should not add a ActualActivity to an array that contains it', () => {
        const actualActivity: IActualActivity = { id: 123 };
        const actualActivityCollection: IActualActivity[] = [
          {
            ...actualActivity,
          },
          { id: 456 },
        ];
        expectedResult = service.addActualActivityToCollectionIfMissing(actualActivityCollection, actualActivity);
        expect(expectedResult).toHaveLength(2);
      });

      it("should add a ActualActivity to an array that doesn't contain it", () => {
        const actualActivity: IActualActivity = { id: 123 };
        const actualActivityCollection: IActualActivity[] = [{ id: 456 }];
        expectedResult = service.addActualActivityToCollectionIfMissing(actualActivityCollection, actualActivity);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(actualActivity);
      });

      it('should add only unique ActualActivity to an array', () => {
        const actualActivityArray: IActualActivity[] = [{ id: 123 }, { id: 456 }, { id: 44003 }];
        const actualActivityCollection: IActualActivity[] = [{ id: 123 }];
        expectedResult = service.addActualActivityToCollectionIfMissing(actualActivityCollection, ...actualActivityArray);
        expect(expectedResult).toHaveLength(3);
      });

      it('should accept varargs', () => {
        const actualActivity: IActualActivity = { id: 123 };
        const actualActivity2: IActualActivity = { id: 456 };
        expectedResult = service.addActualActivityToCollectionIfMissing([], actualActivity, actualActivity2);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(actualActivity);
        expect(expectedResult).toContain(actualActivity2);
      });

      it('should accept null and undefined values', () => {
        const actualActivity: IActualActivity = { id: 123 };
        expectedResult = service.addActualActivityToCollectionIfMissing([], null, actualActivity, undefined);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(actualActivity);
      });

      it('should return initial array if no ActualActivity is added', () => {
        const actualActivityCollection: IActualActivity[] = [{ id: 123 }];
        expectedResult = service.addActualActivityToCollectionIfMissing(actualActivityCollection, undefined, null);
        expect(expectedResult).toEqual(actualActivityCollection);
      });
    });
  });

  afterEach(() => {
    httpMock.verify();
  });
});
