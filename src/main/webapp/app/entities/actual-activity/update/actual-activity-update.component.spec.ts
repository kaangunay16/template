jest.mock('@angular/router');

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { of, Subject } from 'rxjs';

import { ActualActivityService } from '../service/actual-activity.service';
import { IActualActivity, ActualActivity } from '../actual-activity.model';
import { IPerson } from 'app/entities/person/person.model';
import { PersonService } from 'app/entities/person/service/person.service';

import { ActualActivityUpdateComponent } from './actual-activity-update.component';

describe('ActualActivity Management Update Component', () => {
  let comp: ActualActivityUpdateComponent;
  let fixture: ComponentFixture<ActualActivityUpdateComponent>;
  let activatedRoute: ActivatedRoute;
  let actualActivityService: ActualActivityService;
  let personService: PersonService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      declarations: [ActualActivityUpdateComponent],
      providers: [FormBuilder, ActivatedRoute],
    })
      .overrideTemplate(ActualActivityUpdateComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(ActualActivityUpdateComponent);
    activatedRoute = TestBed.inject(ActivatedRoute);
    actualActivityService = TestBed.inject(ActualActivityService);
    personService = TestBed.inject(PersonService);

    comp = fixture.componentInstance;
  });

  describe('ngOnInit', () => {
    it('Should call Person query and add missing value', () => {
      const actualActivity: IActualActivity = { id: 456 };
      const person: IPerson = { id: 51523 };
      actualActivity.person = person;

      const personCollection: IPerson[] = [{ id: 56093 }];
      jest.spyOn(personService, 'query').mockReturnValue(of(new HttpResponse({ body: personCollection })));
      const additionalPeople = [person];
      const expectedCollection: IPerson[] = [...additionalPeople, ...personCollection];
      jest.spyOn(personService, 'addPersonToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ actualActivity });
      comp.ngOnInit();

      expect(personService.query).toHaveBeenCalled();
      expect(personService.addPersonToCollectionIfMissing).toHaveBeenCalledWith(personCollection, ...additionalPeople);
      expect(comp.peopleSharedCollection).toEqual(expectedCollection);
    });

    it('Should update editForm', () => {
      const actualActivity: IActualActivity = { id: 456 };
      const person: IPerson = { id: 21238 };
      actualActivity.person = person;

      activatedRoute.data = of({ actualActivity });
      comp.ngOnInit();

      expect(comp.editForm.value).toEqual(expect.objectContaining(actualActivity));
      expect(comp.peopleSharedCollection).toContain(person);
    });
  });

  describe('save', () => {
    it('Should call update service on save for existing entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<ActualActivity>>();
      const actualActivity = { id: 123 };
      jest.spyOn(actualActivityService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ actualActivity });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: actualActivity }));
      saveSubject.complete();

      // THEN
      expect(comp.previousState).toHaveBeenCalled();
      expect(actualActivityService.update).toHaveBeenCalledWith(actualActivity);
      expect(comp.isSaving).toEqual(false);
    });

    it('Should call create service on save for new entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<ActualActivity>>();
      const actualActivity = new ActualActivity();
      jest.spyOn(actualActivityService, 'create').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ actualActivity });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: actualActivity }));
      saveSubject.complete();

      // THEN
      expect(actualActivityService.create).toHaveBeenCalledWith(actualActivity);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).toHaveBeenCalled();
    });

    it('Should set isSaving to false on error', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<ActualActivity>>();
      const actualActivity = { id: 123 };
      jest.spyOn(actualActivityService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ actualActivity });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.error('This is an error!');

      // THEN
      expect(actualActivityService.update).toHaveBeenCalledWith(actualActivity);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).not.toHaveBeenCalled();
    });
  });

  describe('Tracking relationships identifiers', () => {
    describe('trackPersonById', () => {
      it('Should return tracked Person primary key', () => {
        const entity = { id: 123 };
        const trackResult = comp.trackPersonById(0, entity);
        expect(trackResult).toEqual(entity.id);
      });
    });
  });
});
