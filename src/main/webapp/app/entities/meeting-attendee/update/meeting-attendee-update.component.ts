import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import { IMeetingAttendee, MeetingAttendee } from '../meeting-attendee.model';
import { MeetingAttendeeService } from '../service/meeting-attendee.service';
import { IMeeting } from 'app/entities/meeting/meeting.model';
import { MeetingService } from 'app/entities/meeting/service/meeting.service';
import { IIdentity } from 'app/entities/identity/identity.model';
import { IdentityService } from 'app/entities/identity/service/identity.service';

@Component({
  selector: 'jhi-meeting-attendee-update',
  templateUrl: './meeting-attendee-update.component.html',
})
export class MeetingAttendeeUpdateComponent implements OnInit {
  isSaving = false;

  meetingsSharedCollection: IMeeting[] = [];
  identitiesSharedCollection: IIdentity[] = [];

  editForm = this.fb.group({
    id: [],
    status: [],
    deleted: [],
    meeting: [],
    identity: [],
  });

  constructor(
    protected meetingAttendeeService: MeetingAttendeeService,
    protected meetingService: MeetingService,
    protected identityService: IdentityService,
    protected activatedRoute: ActivatedRoute,
    protected fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ meetingAttendee }) => {
      this.updateForm(meetingAttendee);

      this.loadRelationshipsOptions();
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const meetingAttendee = this.createFromForm();
    if (meetingAttendee.id !== undefined) {
      this.subscribeToSaveResponse(this.meetingAttendeeService.update(meetingAttendee));
    } else {
      this.subscribeToSaveResponse(this.meetingAttendeeService.create(meetingAttendee));
    }
  }

  trackMeetingById(index: number, item: IMeeting): number {
    return item.id!;
  }

  trackIdentityById(index: number, item: IIdentity): number {
    return item.id!;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IMeetingAttendee>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(meetingAttendee: IMeetingAttendee): void {
    this.editForm.patchValue({
      id: meetingAttendee.id,
      status: meetingAttendee.status,
      deleted: meetingAttendee.deleted,
      meeting: meetingAttendee.meeting,
      identity: meetingAttendee.identity,
    });

    this.meetingsSharedCollection = this.meetingService.addMeetingToCollectionIfMissing(
      this.meetingsSharedCollection,
      meetingAttendee.meeting
    );
    this.identitiesSharedCollection = this.identityService.addIdentityToCollectionIfMissing(
      this.identitiesSharedCollection,
      meetingAttendee.identity
    );
  }

  protected loadRelationshipsOptions(): void {
    this.meetingService
      .query()
      .pipe(map((res: HttpResponse<IMeeting[]>) => res.body ?? []))
      .pipe(
        map((meetings: IMeeting[]) => this.meetingService.addMeetingToCollectionIfMissing(meetings, this.editForm.get('meeting')!.value))
      )
      .subscribe((meetings: IMeeting[]) => (this.meetingsSharedCollection = meetings));

    this.identityService
      .query()
      .pipe(map((res: HttpResponse<IIdentity[]>) => res.body ?? []))
      .pipe(
        map((identities: IIdentity[]) =>
          this.identityService.addIdentityToCollectionIfMissing(identities, this.editForm.get('identity')!.value)
        )
      )
      .subscribe((identities: IIdentity[]) => (this.identitiesSharedCollection = identities));
  }

  protected createFromForm(): IMeetingAttendee {
    return {
      ...new MeetingAttendee(),
      id: this.editForm.get(['id'])!.value,
      status: this.editForm.get(['status'])!.value,
      deleted: this.editForm.get(['deleted'])!.value,
      meeting: this.editForm.get(['meeting'])!.value,
      identity: this.editForm.get(['identity'])!.value,
    };
  }
}
