import { Component, OnDestroy } from '@angular/core';

import { QueryBuilderService } from 'app/components/query-builder/query-builder.service';
import { PersonModuleService } from '../person-module.service';

@Component({
  selector: 'jhi-person-main',
  templateUrl: './person-main.component.html',
  styleUrls: ['./person-main.component.scss'],
})
export class PersonMainComponent implements OnDestroy {
  constructor(private queryBuilderService: QueryBuilderService, private personModuleService: PersonModuleService) {}

  ngOnDestroy(): void {
    this.queryBuilderService.clearParameters();
    this.personModuleService.clearEntities();
  }
}
