import { createFeatureSelector, createSelector } from '@ngrx/store';
import { Dictionary } from '@ngrx/entity';

import { postAdapter, PostsState } from './posts.state';
import { getCurrentRouter } from '../../store/router/router.selector';
import { RouterStateUrl } from '../../store/router/custom-serializer';
import { Post } from '../models/post.model';

export const POST_STATE_NAME = 'posts';

const getPostsSelector = createFeatureSelector<PostsState>(POST_STATE_NAME);

export const postSelector = postAdapter.getSelectors();

export const getPosts = createSelector(getPostsSelector, postSelector.selectAll);
export const getPostEntities = createSelector(getPostsSelector, postSelector.selectEntities);
export const getPostById = createSelector(getPostEntities, getCurrentRouter, (posts: Dictionary<Post> | undefined, route: RouterStateUrl) =>
  posts ? posts[route.params['id']] : null
);
export const getCount = createSelector(getPostsSelector, state => state.count);
