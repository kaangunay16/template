import { NgModule } from '@angular/core';
import { SharedModule } from 'app/shared/shared.module';
import { MeetingAttendeeComponent } from './list/meeting-attendee.component';
import { MeetingAttendeeDetailComponent } from './detail/meeting-attendee-detail.component';
import { MeetingAttendeeUpdateComponent } from './update/meeting-attendee-update.component';
import { MeetingAttendeeDeleteDialogComponent } from './delete/meeting-attendee-delete-dialog.component';
import { MeetingAttendeeRoutingModule } from './route/meeting-attendee-routing.module';

@NgModule({
  imports: [SharedModule, MeetingAttendeeRoutingModule],
  declarations: [
    MeetingAttendeeComponent,
    MeetingAttendeeDetailComponent,
    MeetingAttendeeUpdateComponent,
    MeetingAttendeeDeleteDialogComponent,
  ],
  entryComponents: [MeetingAttendeeDeleteDialogComponent],
})
export class MeetingAttendeeModule {}
