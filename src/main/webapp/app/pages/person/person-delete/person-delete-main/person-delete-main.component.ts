import { Component, OnDestroy } from '@angular/core';

import { EntityDemandService } from 'app/shared/services/entity-demand.service';

@Component({
  selector: 'jhi-person-delete-main',
  templateUrl: './person-delete-main.component.html',
  styleUrls: ['./person-delete-main.component.scss'],
})
export class PersonDeleteMainComponent implements OnDestroy {
  constructor(private entityDemandService: EntityDemandService) {}

  ngOnDestroy(): void {
    this.entityDemandService.clearEntities();
  }
}
