import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { IOtherPerson, OtherPerson } from '../other-person.model';
import { OtherPersonService } from '../service/other-person.service';

@Injectable({ providedIn: 'root' })
export class OtherPersonRoutingResolveService implements Resolve<IOtherPerson> {
  constructor(protected service: OtherPersonService, protected router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IOtherPerson> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        mergeMap((otherPerson: HttpResponse<OtherPerson>) => {
          if (otherPerson.body) {
            return of(otherPerson.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new OtherPerson());
  }
}
