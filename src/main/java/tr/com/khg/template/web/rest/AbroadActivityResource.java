package tr.com.khg.template.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.ResponseUtil;
import tr.com.khg.template.domain.AbroadActivity;
import tr.com.khg.template.repository.AbroadActivityRepository;
import tr.com.khg.template.service.AbroadActivityService;
import tr.com.khg.template.web.rest.errors.BadRequestAlertException;

/**
 * REST controller for managing {@link tr.com.khg.template.domain.AbroadActivity}.
 */
@RestController
@RequestMapping("/api")
public class AbroadActivityResource {

    private final Logger log = LoggerFactory.getLogger(AbroadActivityResource.class);

    private static final String ENTITY_NAME = "abroadActivity";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final AbroadActivityService abroadActivityService;

    private final AbroadActivityRepository abroadActivityRepository;

    public AbroadActivityResource(AbroadActivityService abroadActivityService, AbroadActivityRepository abroadActivityRepository) {
        this.abroadActivityService = abroadActivityService;
        this.abroadActivityRepository = abroadActivityRepository;
    }

    /**
     * {@code POST  /abroad-activities} : Create a new abroadActivity.
     *
     * @param abroadActivity the abroadActivity to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new abroadActivity, or with status {@code 400 (Bad Request)} if the abroadActivity has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/abroad-activities")
    public ResponseEntity<AbroadActivity> createAbroadActivity(@RequestBody AbroadActivity abroadActivity) throws URISyntaxException {
        log.debug("REST request to save AbroadActivity : {}", abroadActivity);
        if (abroadActivity.getId() != null) {
            throw new BadRequestAlertException("A new abroadActivity cannot already have an ID", ENTITY_NAME, "idexists");
        }
        AbroadActivity result = abroadActivityService.save(abroadActivity);
        return ResponseEntity
            .created(new URI("/api/abroad-activities/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /abroad-activities/:id} : Updates an existing abroadActivity.
     *
     * @param id the id of the abroadActivity to save.
     * @param abroadActivity the abroadActivity to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated abroadActivity,
     * or with status {@code 400 (Bad Request)} if the abroadActivity is not valid,
     * or with status {@code 500 (Internal Server Error)} if the abroadActivity couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/abroad-activities/{id}")
    public ResponseEntity<AbroadActivity> updateAbroadActivity(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody AbroadActivity abroadActivity
    ) throws URISyntaxException {
        log.debug("REST request to update AbroadActivity : {}, {}", id, abroadActivity);
        if (abroadActivity.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, abroadActivity.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!abroadActivityRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        AbroadActivity result = abroadActivityService.save(abroadActivity);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, abroadActivity.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /abroad-activities/:id} : Partial updates given fields of an existing abroadActivity, field will ignore if it is null
     *
     * @param id the id of the abroadActivity to save.
     * @param abroadActivity the abroadActivity to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated abroadActivity,
     * or with status {@code 400 (Bad Request)} if the abroadActivity is not valid,
     * or with status {@code 404 (Not Found)} if the abroadActivity is not found,
     * or with status {@code 500 (Internal Server Error)} if the abroadActivity couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/abroad-activities/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<AbroadActivity> partialUpdateAbroadActivity(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody AbroadActivity abroadActivity
    ) throws URISyntaxException {
        log.debug("REST request to partial update AbroadActivity partially : {}, {}", id, abroadActivity);
        if (abroadActivity.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, abroadActivity.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!abroadActivityRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<AbroadActivity> result = abroadActivityService.partialUpdate(abroadActivity);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, abroadActivity.getId().toString())
        );
    }

    /**
     * {@code GET  /abroad-activities} : get all the abroadActivities.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of abroadActivities in body.
     */
    @GetMapping("/abroad-activities")
    public List<AbroadActivity> getAllAbroadActivities() {
        log.debug("REST request to get all AbroadActivities");
        return abroadActivityService.findAll();
    }

    /**
     * {@code GET  /abroad-activities/:id} : get the "id" abroadActivity.
     *
     * @param id the id of the abroadActivity to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the abroadActivity, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/abroad-activities/{id}")
    public ResponseEntity<AbroadActivity> getAbroadActivity(@PathVariable Long id) {
        log.debug("REST request to get AbroadActivity : {}", id);
        Optional<AbroadActivity> abroadActivity = abroadActivityService.findOne(id);
        return ResponseUtil.wrapOrNotFound(abroadActivity);
    }

    /**
     * {@code DELETE  /abroad-activities/:id} : delete the "id" abroadActivity.
     *
     * @param id the id of the abroadActivity to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/abroad-activities/{id}")
    public ResponseEntity<Void> deleteAbroadActivity(@PathVariable Long id) {
        log.debug("REST request to delete AbroadActivity : {}", id);
        abroadActivityService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }
}
