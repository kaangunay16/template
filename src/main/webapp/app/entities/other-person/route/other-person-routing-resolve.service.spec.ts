jest.mock('@angular/router');

import { TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ActivatedRouteSnapshot, Router } from '@angular/router';
import { of } from 'rxjs';

import { IOtherPerson, OtherPerson } from '../other-person.model';
import { OtherPersonService } from '../service/other-person.service';

import { OtherPersonRoutingResolveService } from './other-person-routing-resolve.service';

describe('OtherPerson routing resolve service', () => {
  let mockRouter: Router;
  let mockActivatedRouteSnapshot: ActivatedRouteSnapshot;
  let routingResolveService: OtherPersonRoutingResolveService;
  let service: OtherPersonService;
  let resultOtherPerson: IOtherPerson | undefined;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [Router, ActivatedRouteSnapshot],
    });
    mockRouter = TestBed.inject(Router);
    mockActivatedRouteSnapshot = TestBed.inject(ActivatedRouteSnapshot);
    routingResolveService = TestBed.inject(OtherPersonRoutingResolveService);
    service = TestBed.inject(OtherPersonService);
    resultOtherPerson = undefined;
  });

  describe('resolve', () => {
    it('should return IOtherPerson returned by find', () => {
      // GIVEN
      service.find = jest.fn(id => of(new HttpResponse({ body: { id } })));
      mockActivatedRouteSnapshot.params = { id: 123 };

      // WHEN
      routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
        resultOtherPerson = result;
      });

      // THEN
      expect(service.find).toBeCalledWith(123);
      expect(resultOtherPerson).toEqual({ id: 123 });
    });

    it('should return new IOtherPerson if id is not provided', () => {
      // GIVEN
      service.find = jest.fn();
      mockActivatedRouteSnapshot.params = {};

      // WHEN
      routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
        resultOtherPerson = result;
      });

      // THEN
      expect(service.find).not.toBeCalled();
      expect(resultOtherPerson).toEqual(new OtherPerson());
    });

    it('should route to 404 page if data not found in server', () => {
      // GIVEN
      jest.spyOn(service, 'find').mockReturnValue(of(new HttpResponse({ body: null as unknown as OtherPerson })));
      mockActivatedRouteSnapshot.params = { id: 123 };

      // WHEN
      routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
        resultOtherPerson = result;
      });

      // THEN
      expect(service.find).toBeCalledWith(123);
      expect(resultOtherPerson).toEqual(undefined);
      expect(mockRouter.navigate).toHaveBeenCalledWith(['404']);
    });
  });
});
