package tr.com.khg.template.service;

import tr.com.khg.template.domain.Identity;

public interface IdentityRetrieverService {
    Identity getIdentity(String identityNumber);
}
