import { IIdentity } from 'app/entities/identity/identity.model';
import { IOtherPersonRecord } from 'app/entities/other-person-record/other-person-record.model';
import { HasId } from '../form-entity.model';

export interface IOtherPerson extends HasId {
  id?: number;
  identity?: IIdentity | null;
  otherPersonRecords?: IOtherPersonRecord[] | null;
}

export class OtherPerson implements IOtherPerson {
  constructor(public id?: number, public identity?: IIdentity | null, public otherPersonRecords?: IOtherPersonRecord[] | null) {}
}

export function getOtherPersonIdentifier(otherPerson: IOtherPerson): number | undefined {
  return otherPerson.id;
}
