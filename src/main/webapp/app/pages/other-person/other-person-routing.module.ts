import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { OtherPersonMainComponent } from './other-person-main/other-person-main.component';
import { OtherPersonGetIdentityComponent } from './other-person-get-identity/other-person-get-identity.component';
import { OtherPersonInformationComponent } from './other-person-information/other-person-information.component';
import { OtherPersonPersonInfoComponent } from './other-person-person-info/other-person-person-info.component';
import { OtherPersonResultTableComponent } from './other-person-result-table/other-person-result-table.component';

const routes: Routes = [
  {
    path: '',
    component: OtherPersonMainComponent,
    children: [
      {
        path: '',
        component: OtherPersonGetIdentityComponent,
      },
      {
        path: 'get-other-person',
        component: OtherPersonPersonInfoComponent,
      },
      {
        path: 'information',
        component: OtherPersonInformationComponent,
      },
      {
        path: 'other-person-result',
        component: OtherPersonResultTableComponent,
      },
      {
        path: 'other-person-deletion',
        loadChildren: () => import('./other-person-delete/other-person-delete.module').then(m => m.OtherPersonDeleteModule),
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class OtherPersonRoutingModule {}
