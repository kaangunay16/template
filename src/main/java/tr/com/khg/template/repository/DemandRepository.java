package tr.com.khg.template.repository;

import java.util.List;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;
import tr.com.khg.template.domain.Demand;
import tr.com.khg.template.domain.enumeration.DemandType;

/**
 * Spring Data SQL repository for the Demand entity.
 */
@SuppressWarnings("unused")
@Repository
public interface DemandRepository extends JpaRepository<Demand, Long> {
    List<Demand> findByStatusAndDemandType(DemandType status, String demandType);
}
