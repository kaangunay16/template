import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StoreModule } from '@ngrx/store';

import { SharedModule } from '../../../shared/shared.module';
import { postsReducer } from './state/posts.reducer';
import { POST_STATE_NAME } from './state/posts.selector';
import { NgrxCoursePostsRoutingModule } from './ngrx-course-posts-routing.module';
import { NgrxPostListComponent } from './ngrx-post-list/ngrx-post-list.component';
import { NgrxPostAddComponent } from './ngrx-post-add/ngrx-post-add.component';
import { NgrxPostEditComponent } from './ngrx-post-edit/ngrx-post-edit.component';

@NgModule({
  declarations: [NgrxPostListComponent, NgrxPostAddComponent, NgrxPostEditComponent],
  imports: [CommonModule, SharedModule, NgrxCoursePostsRoutingModule, StoreModule.forFeature(POST_STATE_NAME, postsReducer)],
})
export class NgrxCoursePostsModule {}
