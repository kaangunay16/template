package tr.com.khg.template.service.impl;

import java.util.List;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tr.com.khg.template.domain.Identity;
import tr.com.khg.template.repository.IdentityRepository;
import tr.com.khg.template.service.IdentityRetrieverService;
import tr.com.khg.template.service.IdentityService;

/**
 * Service Implementation for managing {@link Identity}.
 */
@Service
@Transactional
public class IdentityServiceImpl implements IdentityService {

    private final Logger log = LoggerFactory.getLogger(IdentityServiceImpl.class);

    private final IdentityRepository identityRepository;

    private final IdentityRetrieverService identityRetrieverService;

    public IdentityServiceImpl(IdentityRepository identityRepository, IdentityRetrieverService identityRetrieverService) {
        this.identityRepository = identityRepository;
        this.identityRetrieverService = identityRetrieverService;
    }

    @Override
    public Identity save(Identity identity) {
        log.debug("Request to save Identity : {}", identity);
        return identityRepository.save(identity);
    }

    @Override
    public Optional<Identity> partialUpdate(Identity identity) {
        log.debug("Request to partially update Identity : {}", identity);

        return identityRepository
            .findById(identity.getId())
            .map(existingIdentity -> {
                if (identity.getName() != null) {
                    existingIdentity.setName(identity.getName());
                }
                if (identity.getSurname() != null) {
                    existingIdentity.setSurname(identity.getSurname());
                }
                if (identity.getGender() != null) {
                    existingIdentity.setGender(identity.getGender());
                }
                if (identity.getPlaceOfBirth() != null) {
                    existingIdentity.setPlaceOfBirth(identity.getPlaceOfBirth());
                }
                if (identity.getRegisteredCity() != null) {
                    existingIdentity.setRegisteredCity(identity.getRegisteredCity());
                }
                if (identity.getRegisteredDistrict() != null) {
                    existingIdentity.setRegisteredDistrict(identity.getRegisteredDistrict());
                }
                if (identity.getRegisteredVillage() != null) {
                    existingIdentity.setRegisteredVillage(identity.getRegisteredVillage());
                }
                if (identity.getMotherName() != null) {
                    existingIdentity.setMotherName(identity.getMotherName());
                }
                if (identity.getFatherName() != null) {
                    existingIdentity.setFatherName(identity.getFatherName());
                }
                if (identity.getMaritalStatus() != null) {
                    existingIdentity.setMaritalStatus(identity.getMaritalStatus());
                }
                if (identity.getEducation() != null) {
                    existingIdentity.setEducation(identity.getEducation());
                }
                if (identity.getAddress() != null) {
                    existingIdentity.setAddress(identity.getAddress());
                }
                if (identity.getProfession() != null) {
                    existingIdentity.setProfession(identity.getProfession());
                }
                if (identity.getBirthday() != null) {
                    existingIdentity.setBirthday(identity.getBirthday());
                }
                if (identity.getIdentityNo() != null) {
                    existingIdentity.setIdentityNo(identity.getIdentityNo());
                }

                return existingIdentity;
            })
            .map(identityRepository::save);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Identity> findAll() {
        log.debug("Request to get all Identities");
        return identityRepository.findAll();
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<Identity> findOne(Long id) {
        log.debug("Request to get Identity : {}", id);
        return identityRepository.findById(id);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete Identity : {}", id);
        identityRepository.deleteById(id);
    }

    @Override
    @Transactional
    public Identity getIdentityWithIdentityNumber(String identityNumber) {
        return identityRepository.findByIdentityNo(identityNumber).orElseGet(() -> identityRetrieverService.getIdentity(identityNumber));
    }
}
