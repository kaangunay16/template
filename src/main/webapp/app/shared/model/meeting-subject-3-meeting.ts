import { IMeeting } from 'app/entities/meeting/meeting.model';

export interface MeetingSubject3Meeting {
  meeting: IMeeting;
  notExistsAttendees: string[];
}
