import { Component, Input } from '@angular/core';

import { DynamicFormCreatorModel } from '../dynamic-form-creator/dynamic-form-field.model';

@Component({
  selector: 'jhi-form-card',
  templateUrl: './form-card.component.html',
  styleUrls: ['./form-card.component.scss'],
})
export class FormCardComponent {
  @Input()
  title?: string;

  @Input()
  formCreator!: DynamicFormCreatorModel;

  @Input()
  backButton?: boolean = false;

  @Input()
  size?: 'full' | 'medium' = 'medium';
}
