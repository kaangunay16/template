import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { SharedModule } from '../shared/shared.module';
import { LoadingDialogComponent } from './loading-dialog/loading-dialog.component';
import { FormCardComponent } from './form-card/form-card.component';
import { DynamicFormCreatorComponent } from './dynamic-form-creator/dynamic-form-creator.component';
import { DynamicFormFieldComponent } from './dynamic-form-creator/dynamic-form-field/dynamic-form-field.component';
import { PhoneAdditionComponent } from './form-fields/phone-addition/phone-addition.component';
import { CityDistrictSelectionComponent } from './form-fields/city-district-selection/city-district-selection.component';
import { ActivityFieldComponent } from './form-fields/activity-field/activity-field.component';
import { PastActivityFieldComponent } from './form-fields/past-activity-field/past-activity-field.component';
import { ActualActivityFieldComponent } from './form-fields/actual-activity-field/actual-activity-field.component';
import { AbroadActivityFieldComponent } from './form-fields/abroad-activity-field/abroad-activity-field.component';
import { SelectionsRetrieverComponent } from './form-fields/selections-retriever/selections-retriever.component';
import { BackButtonComponent } from './back-button/back-button.component';
import { OtherNameFieldComponent } from './form-fields/other-name-field/other-name-field.component';
import { TableComponent } from './table/table.component';
import { PropertyExtractorPipe } from './table/property-extractor.pipe';
import { ParameterFieldComponent } from './form-fields/parameter-field/parameter-field.component';
import { QueryBuilderComponent } from './query-builder/query-builder.component';
import { ParametersComponent } from './query-builder/parameters/parameters.component';
import { ParametersWrapperComponent } from './query-builder/parameters-wrapper/parameters-wrapper.component';
import { IdentityInfoRetrieverComponent } from './identity-retriever/identity-info-retriever/identity-info-retriever.component';
import { IdentityRetrieverComponent } from './identity-retriever/identity-retriever.component';
import { CardWrapperComponent } from './card-wrapper/card-wrapper.component';
import { RemoveItemDialogComponent } from './dialogs/remove-item-dialog/remove-item-dialog.component';
import { PageTitleComponent } from './page-title/page-title.component';
import { ExcelExporterComponent } from './excel-exporter/excel-exporter.component';
import { BooleanFieldComponent } from './form-fields/boolean-field/boolean-field.component';
import { DocumentCheckerComponent } from './form-fields/document-checker/document-checker.component';
import { DocumentValidatorDirective } from './form-fields/document-checker/document-validator.directive';
import { LoadingSpinnerComponent } from './loading-spinner/loading-spinner.component';
import { MultiSelectionsRetrieverComponent } from './form-fields/multi-selections-retriever/multi-selections-retriever.component';
import { AttendeeFieldComponent } from './form-fields/attendee-field/attendee-field.component';
import { AttendeeValidatorDirective } from './form-fields/attendee-field/attendee-validator.directive';

@NgModule({
  declarations: [
    LoadingDialogComponent,
    DynamicFormCreatorComponent,
    DynamicFormFieldComponent,
    FormCardComponent,
    PhoneAdditionComponent,
    CityDistrictSelectionComponent,
    ActivityFieldComponent,
    PastActivityFieldComponent,
    ActualActivityFieldComponent,
    AbroadActivityFieldComponent,
    SelectionsRetrieverComponent,
    BackButtonComponent,
    OtherNameFieldComponent,
    TableComponent,
    PropertyExtractorPipe,
    ParameterFieldComponent,
    QueryBuilderComponent,
    ParametersComponent,
    ParametersWrapperComponent,
    IdentityInfoRetrieverComponent,
    IdentityRetrieverComponent,
    CardWrapperComponent,
    RemoveItemDialogComponent,
    PageTitleComponent,
    ExcelExporterComponent,
    BooleanFieldComponent,
    DocumentCheckerComponent,
    DocumentValidatorDirective,
    LoadingSpinnerComponent,
    MultiSelectionsRetrieverComponent,
    AttendeeFieldComponent,
    AttendeeValidatorDirective,
  ],
  imports: [CommonModule, SharedModule, RouterModule],
  exports: [
    LoadingDialogComponent,
    DynamicFormCreatorComponent,
    FormCardComponent,
    TableComponent,
    QueryBuilderComponent,
    BackButtonComponent,
    IdentityRetrieverComponent,
    IdentityInfoRetrieverComponent,
    CardWrapperComponent,
    PageTitleComponent,
    LoadingSpinnerComponent,
  ],
})
export class ComponentsModule {}
