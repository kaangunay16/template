import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { of } from 'rxjs';

import { AbroadActivityService } from '../service/abroad-activity.service';

import { AbroadActivityComponent } from './abroad-activity.component';

describe('AbroadActivity Management Component', () => {
  let comp: AbroadActivityComponent;
  let fixture: ComponentFixture<AbroadActivityComponent>;
  let service: AbroadActivityService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      declarations: [AbroadActivityComponent],
    })
      .overrideTemplate(AbroadActivityComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(AbroadActivityComponent);
    comp = fixture.componentInstance;
    service = TestBed.inject(AbroadActivityService);

    const headers = new HttpHeaders();
    jest.spyOn(service, 'query').mockReturnValue(
      of(
        new HttpResponse({
          body: [{ id: 123 }],
          headers,
        })
      )
    );
  });

  it('Should call load all on init', () => {
    // WHEN
    comp.ngOnInit();

    // THEN
    expect(service.query).toHaveBeenCalled();
    expect(comp.abroadActivities?.[0]).toEqual(expect.objectContaining({ id: 123 }));
  });
});
