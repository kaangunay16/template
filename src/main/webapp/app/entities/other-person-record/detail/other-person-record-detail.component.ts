import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IOtherPersonRecord } from '../other-person-record.model';

@Component({
  selector: 'jhi-other-person-record-detail',
  templateUrl: './other-person-record-detail.component.html',
})
export class OtherPersonRecordDetailComponent implements OnInit {
  otherPersonRecord: IOtherPersonRecord | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ otherPersonRecord }) => {
      this.otherPersonRecord = otherPersonRecord;
    });
  }

  previousState(): void {
    window.history.back();
  }
}
