import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PersonGetPersonInfoComponent } from './person-get-person-info.component';

describe('PersonGetPersonInfoComponent', () => {
  let component: PersonGetPersonInfoComponent;
  let fixture: ComponentFixture<PersonGetPersonInfoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [PersonGetPersonInfoComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PersonGetPersonInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
