import { Component } from '@angular/core';
import { Store } from '@ngrx/store';

import { decrement, increment, reset } from '../state/counter.actions';
import { AppState } from '../../store/app.state';

@Component({
  selector: 'jhi-ngrx-counter-buttons',
  templateUrl: './ngrx-counter-buttons.component.html',
  styleUrls: ['./ngrx-counter-buttons.component.scss'],
})
export class NgrxCounterButtonsComponent {
  constructor(private store: Store<AppState>) {}

  onIncrement(): void {
    this.store.dispatch(increment());
  }

  onDecrement(): void {
    this.store.dispatch(decrement());
  }

  onReset(): void {
    this.store.dispatch(reset());
  }
}
