import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { of } from 'rxjs';

import { OtherPersonRecordService } from '../service/other-person-record.service';

import { OtherPersonRecordComponent } from './other-person-record.component';

describe('OtherPersonRecord Management Component', () => {
  let comp: OtherPersonRecordComponent;
  let fixture: ComponentFixture<OtherPersonRecordComponent>;
  let service: OtherPersonRecordService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      declarations: [OtherPersonRecordComponent],
    })
      .overrideTemplate(OtherPersonRecordComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(OtherPersonRecordComponent);
    comp = fixture.componentInstance;
    service = TestBed.inject(OtherPersonRecordService);

    const headers = new HttpHeaders();
    jest.spyOn(service, 'query').mockReturnValue(
      of(
        new HttpResponse({
          body: [{ id: 123 }],
          headers,
        })
      )
    );
  });

  it('Should call load all on init', () => {
    // WHEN
    comp.ngOnInit();

    // THEN
    expect(service.query).toHaveBeenCalled();
    expect(comp.otherPersonRecords?.[0]).toEqual(expect.objectContaining({ id: 123 }));
  });
});
