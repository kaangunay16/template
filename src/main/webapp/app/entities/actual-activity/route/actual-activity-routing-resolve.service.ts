import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { IActualActivity, ActualActivity } from '../actual-activity.model';
import { ActualActivityService } from '../service/actual-activity.service';

@Injectable({ providedIn: 'root' })
export class ActualActivityRoutingResolveService implements Resolve<IActualActivity> {
  constructor(protected service: ActualActivityService, protected router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IActualActivity> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        mergeMap((actualActivity: HttpResponse<ActualActivity>) => {
          if (actualActivity.body) {
            return of(actualActivity.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new ActualActivity());
  }
}
