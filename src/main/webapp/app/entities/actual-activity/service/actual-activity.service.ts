import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { isPresent } from 'app/core/util/operators';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { IActualActivity, getActualActivityIdentifier } from '../actual-activity.model';
import { IFormEntity } from '../../form-entity.model';

export type EntityResponseType = HttpResponse<IActualActivity>;
export type EntityArrayResponseType = HttpResponse<IActualActivity[]>;

@Injectable({ providedIn: 'root' })
export class ActualActivityService implements IFormEntity {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/actual-activities');

  constructor(protected http: HttpClient, protected applicationConfigService: ApplicationConfigService) {}

  create(actualActivity: IActualActivity): Observable<EntityResponseType> {
    return this.http.post<IActualActivity>(this.resourceUrl, actualActivity, { observe: 'response' });
  }

  update(actualActivity: IActualActivity): Observable<EntityResponseType> {
    return this.http.put<IActualActivity>(`${this.resourceUrl}/${getActualActivityIdentifier(actualActivity) as number}`, actualActivity, {
      observe: 'response',
    });
  }

  partialUpdate(actualActivity: IActualActivity): Observable<EntityResponseType> {
    return this.http.patch<IActualActivity>(
      `${this.resourceUrl}/${getActualActivityIdentifier(actualActivity) as number}`,
      actualActivity,
      { observe: 'response' }
    );
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IActualActivity>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IActualActivity[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  addActualActivityToCollectionIfMissing(
    actualActivityCollection: IActualActivity[],
    ...actualActivitiesToCheck: (IActualActivity | null | undefined)[]
  ): IActualActivity[] {
    const actualActivities: IActualActivity[] = actualActivitiesToCheck.filter(isPresent);
    if (actualActivities.length > 0) {
      const actualActivityCollectionIdentifiers = actualActivityCollection.map(
        actualActivityItem => getActualActivityIdentifier(actualActivityItem)!
      );
      const actualActivitiesToAdd = actualActivities.filter(actualActivityItem => {
        const actualActivityIdentifier = getActualActivityIdentifier(actualActivityItem);
        if (actualActivityIdentifier == null || actualActivityCollectionIdentifiers.includes(actualActivityIdentifier)) {
          return false;
        }
        actualActivityCollectionIdentifiers.push(actualActivityIdentifier);
        return true;
      });
      return [...actualActivitiesToAdd, ...actualActivityCollection];
    }
    return actualActivityCollection;
  }
}
