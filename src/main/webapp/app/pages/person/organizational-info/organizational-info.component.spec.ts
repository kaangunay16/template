import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OrganizationalInfoComponent } from './organizational-info.component';

describe('OrganizationalInfoComponent', () => {
  let component: OrganizationalInfoComponent;
  let fixture: ComponentFixture<OrganizationalInfoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [OrganizationalInfoComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OrganizationalInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
