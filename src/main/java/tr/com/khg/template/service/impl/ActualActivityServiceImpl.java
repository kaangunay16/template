package tr.com.khg.template.service.impl;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tr.com.khg.template.domain.ActualActivity;
import tr.com.khg.template.domain.PastActivity;
import tr.com.khg.template.repository.ActualActivityRepository;
import tr.com.khg.template.repository.PastActivityRepository;
import tr.com.khg.template.service.ActualActivityService;

/**
 * Service Implementation for managing {@link ActualActivity}.
 */
@Service
@Transactional
public class ActualActivityServiceImpl implements ActualActivityService {

    private final Logger log = LoggerFactory.getLogger(ActualActivityServiceImpl.class);

    private final ActualActivityRepository actualActivityRepository;

    public final PastActivityRepository pastActivityRepository;

    public ActualActivityServiceImpl(ActualActivityRepository actualActivityRepository, PastActivityRepository pastActivityRepository) {
        this.actualActivityRepository = actualActivityRepository;
        this.pastActivityRepository = pastActivityRepository;
    }

    @Override
    public ActualActivity save(ActualActivity actualActivity) {
        log.debug("Request to save ActualActivity : {}", actualActivity);
        return actualActivityRepository.save(actualActivity);
    }

    @Override
    public Optional<ActualActivity> partialUpdate(ActualActivity actualActivity) {
        log.debug("Request to partially update ActualActivity : {}", actualActivity);

        return actualActivityRepository
            .findById(actualActivity.getId())
            .map(existingActualActivity -> {
                if (actualActivity.getCity() != null) {
                    existingActualActivity.setCity(actualActivity.getCity());
                }
                if (actualActivity.getPosition() != null) {
                    existingActualActivity.setPosition(actualActivity.getPosition());
                }
                if (actualActivity.getStartYear() != null) {
                    existingActualActivity.setStartYear(actualActivity.getStartYear());
                }
                if (actualActivity.getDeleted() != null) {
                    existingActualActivity.setDeleted(actualActivity.getDeleted());
                }
                if (actualActivity.getField() != null) {
                    existingActualActivity.setField(actualActivity.getField());
                }

                return existingActualActivity;
            })
            .map(actualActivityRepository::save);
    }

    @Override
    @Transactional(readOnly = true)
    public List<ActualActivity> findAll() {
        log.debug("Request to get all ActualActivities");
        return actualActivityRepository.findAll();
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<ActualActivity> findOne(Long id) {
        log.debug("Request to get ActualActivity : {}", id);
        return actualActivityRepository.findById(id);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete ActualActivity : {}", id);
        actualActivityRepository.deleteById(id);
    }
}
