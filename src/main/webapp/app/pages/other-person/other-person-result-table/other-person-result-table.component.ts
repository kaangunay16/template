import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';

import { QueryBuilderConfig } from 'app/components/models/query-builder-config';
import { TableConfig } from 'app/components/models/table-config.model';
import { IOtherPerson } from 'app/entities/other-person/other-person.model';
import { OtherPersonService } from 'app/entities/other-person/service/other-person.service';
import { OtherPersonModuleService } from 'app/pages/other-person/other-person-module.service';
import { otherPersonTableConfigs } from 'app/pages/other-person/other-person-components.config';

@Component({
  selector: 'jhi-other-person-result-table',
  templateUrl: './other-person-result-table.component.html',
  styleUrls: ['./other-person-result-table.component.scss'],
})
export class OtherPersonResultTableComponent implements OnInit, OnDestroy {
  data!: IOtherPerson[] | null;
  queryBuilderConfig!: QueryBuilderConfig;
  routeSub!: Subscription;
  mode?: 'update' | 'search' | 'meeting';
  tableConfig!: TableConfig;

  constructor(
    private otherPersonModuleService: OtherPersonModuleService,
    private otherPersonService: OtherPersonService,
    private activatedRoute: ActivatedRoute,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.routeSub = this.activatedRoute.queryParams.subscribe(params => {
      this.mode = params['mode'];
      this.data = this.otherPersonModuleService.entities;

      if ((!this.mode || this.mode === 'update') && !this.data) {
        this.router.navigate(['/other-person', 'information']);
      }

      this.tableConfig = {
        columns: otherPersonTableConfigs.columns,
        data: this.data,
        actions: otherPersonTableConfigs.actions,
      };

      this.queryBuilderConfig = {
        service: this.otherPersonService,
        fields: otherPersonTableConfigs.searchParameters,
        joins: ['identity', 'otherPersonRecords'],
      };
    });
  }

  ngOnDestroy(): void {
    this.routeSub.unsubscribe();
  }

  resultChanged(result: IOtherPerson[] | null): void {
    this.otherPersonModuleService.entities = result;
    this.data = result;
    this.tableConfig.data = result;
  }

  completeOtherPersonCreations(): void {
    // TODO Tüm şahısların bilgileri girildikten sonra tıklanarak gerekli yere yönlendirilecek
    this.otherPersonModuleService.subject3;
  }
}
