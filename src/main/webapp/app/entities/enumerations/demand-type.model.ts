export enum DemandType {
  WAITING = 'WAITING',

  CONFIRMED = 'CONFIRMED',

  DENIED = 'DENIED',
}
