import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { IOtherName, OtherName } from '../other-name.model';
import { OtherNameService } from '../service/other-name.service';

@Injectable({ providedIn: 'root' })
export class OtherNameRoutingResolveService implements Resolve<IOtherName> {
  constructor(protected service: OtherNameService, protected router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IOtherName> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        mergeMap((otherName: HttpResponse<OtherName>) => {
          if (otherName.body) {
            return of(otherName.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new OtherName());
  }
}
