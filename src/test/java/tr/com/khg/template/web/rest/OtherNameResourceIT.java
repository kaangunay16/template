package tr.com.khg.template.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import tr.com.khg.template.IntegrationTest;
import tr.com.khg.template.domain.OtherName;
import tr.com.khg.template.repository.OtherNameRepository;

/**
 * Integration tests for the {@link OtherNameResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class OtherNameResourceIT {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final String ENTITY_API_URL = "/api/other-names";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private OtherNameRepository otherNameRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restOtherNameMockMvc;

    private OtherName otherName;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static OtherName createEntity(EntityManager em) {
        OtherName otherName = new OtherName().name(DEFAULT_NAME);
        return otherName;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static OtherName createUpdatedEntity(EntityManager em) {
        OtherName otherName = new OtherName().name(UPDATED_NAME);
        return otherName;
    }

    @BeforeEach
    public void initTest() {
        otherName = createEntity(em);
    }

    @Test
    @Transactional
    void createOtherName() throws Exception {
        int databaseSizeBeforeCreate = otherNameRepository.findAll().size();
        // Create the OtherName
        restOtherNameMockMvc
            .perform(
                post(ENTITY_API_URL)
                    .with(csrf())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(otherName))
            )
            .andExpect(status().isCreated());

        // Validate the OtherName in the database
        List<OtherName> otherNameList = otherNameRepository.findAll();
        assertThat(otherNameList).hasSize(databaseSizeBeforeCreate + 1);
        OtherName testOtherName = otherNameList.get(otherNameList.size() - 1);
        assertThat(testOtherName.getName()).isEqualTo(DEFAULT_NAME);
    }

    @Test
    @Transactional
    void createOtherNameWithExistingId() throws Exception {
        // Create the OtherName with an existing ID
        otherName.setId(1L);

        int databaseSizeBeforeCreate = otherNameRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restOtherNameMockMvc
            .perform(
                post(ENTITY_API_URL)
                    .with(csrf())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(otherName))
            )
            .andExpect(status().isBadRequest());

        // Validate the OtherName in the database
        List<OtherName> otherNameList = otherNameRepository.findAll();
        assertThat(otherNameList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void getAllOtherNames() throws Exception {
        // Initialize the database
        otherNameRepository.saveAndFlush(otherName);

        // Get all the otherNameList
        restOtherNameMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(otherName.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)));
    }

    @Test
    @Transactional
    void getOtherName() throws Exception {
        // Initialize the database
        otherNameRepository.saveAndFlush(otherName);

        // Get the otherName
        restOtherNameMockMvc
            .perform(get(ENTITY_API_URL_ID, otherName.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(otherName.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME));
    }

    @Test
    @Transactional
    void getNonExistingOtherName() throws Exception {
        // Get the otherName
        restOtherNameMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putNewOtherName() throws Exception {
        // Initialize the database
        otherNameRepository.saveAndFlush(otherName);

        int databaseSizeBeforeUpdate = otherNameRepository.findAll().size();

        // Update the otherName
        OtherName updatedOtherName = otherNameRepository.findById(otherName.getId()).get();
        // Disconnect from session so that the updates on updatedOtherName are not directly saved in db
        em.detach(updatedOtherName);
        updatedOtherName.name(UPDATED_NAME);

        restOtherNameMockMvc
            .perform(
                put(ENTITY_API_URL_ID, updatedOtherName.getId())
                    .with(csrf())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(updatedOtherName))
            )
            .andExpect(status().isOk());

        // Validate the OtherName in the database
        List<OtherName> otherNameList = otherNameRepository.findAll();
        assertThat(otherNameList).hasSize(databaseSizeBeforeUpdate);
        OtherName testOtherName = otherNameList.get(otherNameList.size() - 1);
        assertThat(testOtherName.getName()).isEqualTo(UPDATED_NAME);
    }

    @Test
    @Transactional
    void putNonExistingOtherName() throws Exception {
        int databaseSizeBeforeUpdate = otherNameRepository.findAll().size();
        otherName.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restOtherNameMockMvc
            .perform(
                put(ENTITY_API_URL_ID, otherName.getId())
                    .with(csrf())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(otherName))
            )
            .andExpect(status().isBadRequest());

        // Validate the OtherName in the database
        List<OtherName> otherNameList = otherNameRepository.findAll();
        assertThat(otherNameList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchOtherName() throws Exception {
        int databaseSizeBeforeUpdate = otherNameRepository.findAll().size();
        otherName.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restOtherNameMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .with(csrf())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(otherName))
            )
            .andExpect(status().isBadRequest());

        // Validate the OtherName in the database
        List<OtherName> otherNameList = otherNameRepository.findAll();
        assertThat(otherNameList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamOtherName() throws Exception {
        int databaseSizeBeforeUpdate = otherNameRepository.findAll().size();
        otherName.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restOtherNameMockMvc
            .perform(
                put(ENTITY_API_URL)
                    .with(csrf())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(otherName))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the OtherName in the database
        List<OtherName> otherNameList = otherNameRepository.findAll();
        assertThat(otherNameList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateOtherNameWithPatch() throws Exception {
        // Initialize the database
        otherNameRepository.saveAndFlush(otherName);

        int databaseSizeBeforeUpdate = otherNameRepository.findAll().size();

        // Update the otherName using partial update
        OtherName partialUpdatedOtherName = new OtherName();
        partialUpdatedOtherName.setId(otherName.getId());

        restOtherNameMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedOtherName.getId())
                    .with(csrf())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedOtherName))
            )
            .andExpect(status().isOk());

        // Validate the OtherName in the database
        List<OtherName> otherNameList = otherNameRepository.findAll();
        assertThat(otherNameList).hasSize(databaseSizeBeforeUpdate);
        OtherName testOtherName = otherNameList.get(otherNameList.size() - 1);
        assertThat(testOtherName.getName()).isEqualTo(DEFAULT_NAME);
    }

    @Test
    @Transactional
    void fullUpdateOtherNameWithPatch() throws Exception {
        // Initialize the database
        otherNameRepository.saveAndFlush(otherName);

        int databaseSizeBeforeUpdate = otherNameRepository.findAll().size();

        // Update the otherName using partial update
        OtherName partialUpdatedOtherName = new OtherName();
        partialUpdatedOtherName.setId(otherName.getId());

        partialUpdatedOtherName.name(UPDATED_NAME);

        restOtherNameMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedOtherName.getId())
                    .with(csrf())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedOtherName))
            )
            .andExpect(status().isOk());

        // Validate the OtherName in the database
        List<OtherName> otherNameList = otherNameRepository.findAll();
        assertThat(otherNameList).hasSize(databaseSizeBeforeUpdate);
        OtherName testOtherName = otherNameList.get(otherNameList.size() - 1);
        assertThat(testOtherName.getName()).isEqualTo(UPDATED_NAME);
    }

    @Test
    @Transactional
    void patchNonExistingOtherName() throws Exception {
        int databaseSizeBeforeUpdate = otherNameRepository.findAll().size();
        otherName.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restOtherNameMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, otherName.getId())
                    .with(csrf())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(otherName))
            )
            .andExpect(status().isBadRequest());

        // Validate the OtherName in the database
        List<OtherName> otherNameList = otherNameRepository.findAll();
        assertThat(otherNameList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchOtherName() throws Exception {
        int databaseSizeBeforeUpdate = otherNameRepository.findAll().size();
        otherName.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restOtherNameMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .with(csrf())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(otherName))
            )
            .andExpect(status().isBadRequest());

        // Validate the OtherName in the database
        List<OtherName> otherNameList = otherNameRepository.findAll();
        assertThat(otherNameList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamOtherName() throws Exception {
        int databaseSizeBeforeUpdate = otherNameRepository.findAll().size();
        otherName.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restOtherNameMockMvc
            .perform(
                patch(ENTITY_API_URL)
                    .with(csrf())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(otherName))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the OtherName in the database
        List<OtherName> otherNameList = otherNameRepository.findAll();
        assertThat(otherNameList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteOtherName() throws Exception {
        // Initialize the database
        otherNameRepository.saveAndFlush(otherName);

        int databaseSizeBeforeDelete = otherNameRepository.findAll().size();

        // Delete the otherName
        restOtherNameMockMvc
            .perform(delete(ENTITY_API_URL_ID, otherName.getId()).with(csrf()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<OtherName> otherNameList = otherNameRepository.findAll();
        assertThat(otherNameList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
