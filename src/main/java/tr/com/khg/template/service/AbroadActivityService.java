package tr.com.khg.template.service;

import java.util.List;
import java.util.Optional;
import tr.com.khg.template.domain.AbroadActivity;

/**
 * Service Interface for managing {@link AbroadActivity}.
 */
public interface AbroadActivityService {
    /**
     * Save a abroadActivity.
     *
     * @param abroadActivity the entity to save.
     * @return the persisted entity.
     */
    AbroadActivity save(AbroadActivity abroadActivity);

    /**
     * Partially updates a abroadActivity.
     *
     * @param abroadActivity the entity to update partially.
     * @return the persisted entity.
     */
    Optional<AbroadActivity> partialUpdate(AbroadActivity abroadActivity);

    /**
     * Get all the abroadActivities.
     *
     * @return the list of entities.
     */
    List<AbroadActivity> findAll();

    /**
     * Get the "id" abroadActivity.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<AbroadActivity> findOne(Long id);

    /**
     * Delete the "id" abroadActivity.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
