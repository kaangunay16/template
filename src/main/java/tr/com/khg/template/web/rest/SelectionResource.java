package tr.com.khg.template.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.ResponseUtil;
import tr.com.khg.template.domain.Selection;
import tr.com.khg.template.repository.SelectionRepository;
import tr.com.khg.template.service.SelectionService;
import tr.com.khg.template.web.rest.errors.BadRequestAlertException;

/**
 * REST controller for managing {@link tr.com.khg.template.domain.Selection}.
 */
@RestController
@RequestMapping("/api")
public class SelectionResource {

    private final Logger log = LoggerFactory.getLogger(SelectionResource.class);

    private static final String ENTITY_NAME = "selection";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final SelectionService selectionService;

    private final SelectionRepository selectionRepository;

    public SelectionResource(SelectionService selectionService, SelectionRepository selectionRepository) {
        this.selectionService = selectionService;
        this.selectionRepository = selectionRepository;
    }

    /**
     * {@code POST  /selections} : Create a new selection.
     *
     * @param selection the selection to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new selection, or with status {@code 400 (Bad Request)} if the selection has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/selections")
    public ResponseEntity<Selection> createSelection(@RequestBody Selection selection) throws URISyntaxException {
        log.debug("REST request to save Selection : {}", selection);
        if (selection.getId() != null) {
            throw new BadRequestAlertException("A new selection cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Selection result = selectionService.save(selection);
        return ResponseEntity
            .created(new URI("/api/selections/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /selections/:id} : Updates an existing selection.
     *
     * @param id the id of the selection to save.
     * @param selection the selection to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated selection,
     * or with status {@code 400 (Bad Request)} if the selection is not valid,
     * or with status {@code 500 (Internal Server Error)} if the selection couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/selections/{id}")
    public ResponseEntity<Selection> updateSelection(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody Selection selection
    ) throws URISyntaxException {
        log.debug("REST request to update Selection : {}, {}", id, selection);
        if (selection.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, selection.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!selectionRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Selection result = selectionService.save(selection);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, selection.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /selections/:id} : Partial updates given fields of an existing selection, field will ignore if it is null
     *
     * @param id the id of the selection to save.
     * @param selection the selection to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated selection,
     * or with status {@code 400 (Bad Request)} if the selection is not valid,
     * or with status {@code 404 (Not Found)} if the selection is not found,
     * or with status {@code 500 (Internal Server Error)} if the selection couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/selections/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<Selection> partialUpdateSelection(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody Selection selection
    ) throws URISyntaxException {
        log.debug("REST request to partial update Selection partially : {}, {}", id, selection);
        if (selection.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, selection.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!selectionRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<Selection> result = selectionService.partialUpdate(selection);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, selection.getId().toString())
        );
    }

    /**
     * {@code GET  /selections} : get all the selections.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of selections in body.
     */
    @GetMapping("/selections")
    public List<Selection> getAllSelections() {
        log.debug("REST request to get all Selections");
        return selectionService.findAll();
    }

    /**
     * {@code GET  /selections/:id} : get the "id" selection.
     *
     * @param id the id of the selection to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the selection, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/selections/{id}")
    public ResponseEntity<Selection> getSelection(@PathVariable Long id) {
        log.debug("REST request to get Selection : {}", id);
        Optional<Selection> selection = selectionService.findOne(id);
        return ResponseUtil.wrapOrNotFound(selection);
    }

    /**
     * {@code GET  /selections/key=:key} : get the selections have "key"
     * .
     * @param key the key parameter of selections to retrieve
     * @return selection list for the key
     */
    @GetMapping("/selections/key={key}")
    public List<Selection> getSelectionsWithKey(@PathVariable String key) {
        log.debug("REST request to get Selection : {}", key);
        return selectionRepository.findByKeyOrderByPriority(key);
    }

    /**
     * {@code DELETE  /selections/:id} : delete the "id" selection.
     *
     * @param id the id of the selection to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/selections/{id}")
    public ResponseEntity<Void> deleteSelection(@PathVariable Long id) {
        log.debug("REST request to delete Selection : {}", id);
        selectionService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }
}
