package tr.com.khg.template.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import tr.com.khg.template.IntegrationTest;
import tr.com.khg.template.domain.Demand;
import tr.com.khg.template.domain.enumeration.DemandType;
import tr.com.khg.template.repository.DemandRepository;

/**
 * Integration tests for the {@link DemandResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class DemandResourceIT {

    private static final String DEFAULT_DOCUMENT = "AAAAAAAAAA";
    private static final String UPDATED_DOCUMENT = "BBBBBBBBBB";

    private static final String DEFAULT_DEMANDED_BY = "AAAAAAAAAA";
    private static final String UPDATED_DEMANDED_BY = "BBBBBBBBBB";

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    private static final String DEFAULT_DEMAND_TYPE = "AAAAAAAAAA";
    private static final String UPDATED_DEMAND_TYPE = "BBBBBBBBBB";

    private static final DemandType DEFAULT_STATUS = DemandType.WAITING;
    private static final DemandType UPDATED_STATUS = DemandType.CONFIRMED;

    private static final String DEFAULT_PROCESSED_BY = "AAAAAAAAAA";
    private static final String UPDATED_PROCESSED_BY = "BBBBBBBBBB";

    private static final Long DEFAULT_ENTITY_ID = 1L;
    private static final Long UPDATED_ENTITY_ID = 2L;

    private static final String DEFAULT_PROCESS_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_PROCESS_DESCRIPTION = "BBBBBBBBBB";

    private static final String ENTITY_API_URL = "/api/demands";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private DemandRepository demandRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restDemandMockMvc;

    private Demand demand;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Demand createEntity(EntityManager em) {
        Demand demand = new Demand()
            .document(DEFAULT_DOCUMENT)
            .demandedBy(DEFAULT_DEMANDED_BY)
            .description(DEFAULT_DESCRIPTION)
            .demandType(DEFAULT_DEMAND_TYPE)
            .status(DEFAULT_STATUS)
            .processedBy(DEFAULT_PROCESSED_BY)
            .entityId(DEFAULT_ENTITY_ID)
            .processDescription(DEFAULT_PROCESS_DESCRIPTION);
        return demand;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Demand createUpdatedEntity(EntityManager em) {
        Demand demand = new Demand()
            .document(UPDATED_DOCUMENT)
            .demandedBy(UPDATED_DEMANDED_BY)
            .description(UPDATED_DESCRIPTION)
            .demandType(UPDATED_DEMAND_TYPE)
            .status(UPDATED_STATUS)
            .processedBy(UPDATED_PROCESSED_BY)
            .entityId(UPDATED_ENTITY_ID)
            .processDescription(UPDATED_PROCESS_DESCRIPTION);
        return demand;
    }

    @BeforeEach
    public void initTest() {
        demand = createEntity(em);
    }

    @Test
    @Transactional
    void createDemand() throws Exception {
        int databaseSizeBeforeCreate = demandRepository.findAll().size();
        // Create the Demand
        restDemandMockMvc
            .perform(
                post(ENTITY_API_URL).with(csrf()).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(demand))
            )
            .andExpect(status().isCreated());

        // Validate the Demand in the database
        List<Demand> demandList = demandRepository.findAll();
        assertThat(demandList).hasSize(databaseSizeBeforeCreate + 1);
        Demand testDemand = demandList.get(demandList.size() - 1);
        assertThat(testDemand.getDocument()).isEqualTo(DEFAULT_DOCUMENT);
        assertThat(testDemand.getDemandedBy()).isEqualTo(DEFAULT_DEMANDED_BY);
        assertThat(testDemand.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
        assertThat(testDemand.getDemandType()).isEqualTo(DEFAULT_DEMAND_TYPE);
        assertThat(testDemand.getStatus()).isEqualTo(DEFAULT_STATUS);
        assertThat(testDemand.getProcessedBy()).isEqualTo(DEFAULT_PROCESSED_BY);
        assertThat(testDemand.getEntityId()).isEqualTo(DEFAULT_ENTITY_ID);
        assertThat(testDemand.getProcessDescription()).isEqualTo(DEFAULT_PROCESS_DESCRIPTION);
    }

    @Test
    @Transactional
    void createDemandWithExistingId() throws Exception {
        // Create the Demand with an existing ID
        demand.setId(1L);

        int databaseSizeBeforeCreate = demandRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restDemandMockMvc
            .perform(
                post(ENTITY_API_URL).with(csrf()).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(demand))
            )
            .andExpect(status().isBadRequest());

        // Validate the Demand in the database
        List<Demand> demandList = demandRepository.findAll();
        assertThat(demandList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void getAllDemands() throws Exception {
        // Initialize the database
        demandRepository.saveAndFlush(demand);

        // Get all the demandList
        restDemandMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(demand.getId().intValue())))
            .andExpect(jsonPath("$.[*].document").value(hasItem(DEFAULT_DOCUMENT)))
            .andExpect(jsonPath("$.[*].demandedBy").value(hasItem(DEFAULT_DEMANDED_BY)))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION)))
            .andExpect(jsonPath("$.[*].demandType").value(hasItem(DEFAULT_DEMAND_TYPE)))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS.toString())))
            .andExpect(jsonPath("$.[*].processedBy").value(hasItem(DEFAULT_PROCESSED_BY)))
            .andExpect(jsonPath("$.[*].entityId").value(hasItem(DEFAULT_ENTITY_ID.intValue())))
            .andExpect(jsonPath("$.[*].processDescription").value(hasItem(DEFAULT_PROCESS_DESCRIPTION)));
    }

    @Test
    @Transactional
    void getDemand() throws Exception {
        // Initialize the database
        demandRepository.saveAndFlush(demand);

        // Get the demand
        restDemandMockMvc
            .perform(get(ENTITY_API_URL_ID, demand.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(demand.getId().intValue()))
            .andExpect(jsonPath("$.document").value(DEFAULT_DOCUMENT))
            .andExpect(jsonPath("$.demandedBy").value(DEFAULT_DEMANDED_BY))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION))
            .andExpect(jsonPath("$.demandType").value(DEFAULT_DEMAND_TYPE))
            .andExpect(jsonPath("$.status").value(DEFAULT_STATUS.toString()))
            .andExpect(jsonPath("$.processedBy").value(DEFAULT_PROCESSED_BY))
            .andExpect(jsonPath("$.entityId").value(DEFAULT_ENTITY_ID.intValue()))
            .andExpect(jsonPath("$.processDescription").value(DEFAULT_PROCESS_DESCRIPTION));
    }

    @Test
    @Transactional
    void getNonExistingDemand() throws Exception {
        // Get the demand
        restDemandMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putNewDemand() throws Exception {
        // Initialize the database
        demandRepository.saveAndFlush(demand);

        int databaseSizeBeforeUpdate = demandRepository.findAll().size();

        // Update the demand
        Demand updatedDemand = demandRepository.findById(demand.getId()).get();
        // Disconnect from session so that the updates on updatedDemand are not directly saved in db
        em.detach(updatedDemand);
        updatedDemand
            .document(UPDATED_DOCUMENT)
            .demandedBy(UPDATED_DEMANDED_BY)
            .description(UPDATED_DESCRIPTION)
            .demandType(UPDATED_DEMAND_TYPE)
            .status(UPDATED_STATUS)
            .processedBy(UPDATED_PROCESSED_BY)
            .entityId(UPDATED_ENTITY_ID)
            .processDescription(UPDATED_PROCESS_DESCRIPTION);

        restDemandMockMvc
            .perform(
                put(ENTITY_API_URL_ID, updatedDemand.getId())
                    .with(csrf())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(updatedDemand))
            )
            .andExpect(status().isOk());

        // Validate the Demand in the database
        List<Demand> demandList = demandRepository.findAll();
        assertThat(demandList).hasSize(databaseSizeBeforeUpdate);
        Demand testDemand = demandList.get(demandList.size() - 1);
        assertThat(testDemand.getDocument()).isEqualTo(UPDATED_DOCUMENT);
        assertThat(testDemand.getDemandedBy()).isEqualTo(UPDATED_DEMANDED_BY);
        assertThat(testDemand.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
        assertThat(testDemand.getDemandType()).isEqualTo(UPDATED_DEMAND_TYPE);
        assertThat(testDemand.getStatus()).isEqualTo(UPDATED_STATUS);
        assertThat(testDemand.getProcessedBy()).isEqualTo(UPDATED_PROCESSED_BY);
        assertThat(testDemand.getEntityId()).isEqualTo(UPDATED_ENTITY_ID);
        assertThat(testDemand.getProcessDescription()).isEqualTo(UPDATED_PROCESS_DESCRIPTION);
    }

    @Test
    @Transactional
    void putNonExistingDemand() throws Exception {
        int databaseSizeBeforeUpdate = demandRepository.findAll().size();
        demand.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restDemandMockMvc
            .perform(
                put(ENTITY_API_URL_ID, demand.getId())
                    .with(csrf())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(demand))
            )
            .andExpect(status().isBadRequest());

        // Validate the Demand in the database
        List<Demand> demandList = demandRepository.findAll();
        assertThat(demandList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchDemand() throws Exception {
        int databaseSizeBeforeUpdate = demandRepository.findAll().size();
        demand.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restDemandMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .with(csrf())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(demand))
            )
            .andExpect(status().isBadRequest());

        // Validate the Demand in the database
        List<Demand> demandList = demandRepository.findAll();
        assertThat(demandList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamDemand() throws Exception {
        int databaseSizeBeforeUpdate = demandRepository.findAll().size();
        demand.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restDemandMockMvc
            .perform(
                put(ENTITY_API_URL).with(csrf()).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(demand))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the Demand in the database
        List<Demand> demandList = demandRepository.findAll();
        assertThat(demandList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateDemandWithPatch() throws Exception {
        // Initialize the database
        demandRepository.saveAndFlush(demand);

        int databaseSizeBeforeUpdate = demandRepository.findAll().size();

        // Update the demand using partial update
        Demand partialUpdatedDemand = new Demand();
        partialUpdatedDemand.setId(demand.getId());

        partialUpdatedDemand.description(UPDATED_DESCRIPTION).processDescription(UPDATED_PROCESS_DESCRIPTION);

        restDemandMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedDemand.getId())
                    .with(csrf())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedDemand))
            )
            .andExpect(status().isOk());

        // Validate the Demand in the database
        List<Demand> demandList = demandRepository.findAll();
        assertThat(demandList).hasSize(databaseSizeBeforeUpdate);
        Demand testDemand = demandList.get(demandList.size() - 1);
        assertThat(testDemand.getDocument()).isEqualTo(DEFAULT_DOCUMENT);
        assertThat(testDemand.getDemandedBy()).isEqualTo(DEFAULT_DEMANDED_BY);
        assertThat(testDemand.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
        assertThat(testDemand.getDemandType()).isEqualTo(DEFAULT_DEMAND_TYPE);
        assertThat(testDemand.getStatus()).isEqualTo(DEFAULT_STATUS);
        assertThat(testDemand.getProcessedBy()).isEqualTo(DEFAULT_PROCESSED_BY);
        assertThat(testDemand.getEntityId()).isEqualTo(DEFAULT_ENTITY_ID);
        assertThat(testDemand.getProcessDescription()).isEqualTo(UPDATED_PROCESS_DESCRIPTION);
    }

    @Test
    @Transactional
    void fullUpdateDemandWithPatch() throws Exception {
        // Initialize the database
        demandRepository.saveAndFlush(demand);

        int databaseSizeBeforeUpdate = demandRepository.findAll().size();

        // Update the demand using partial update
        Demand partialUpdatedDemand = new Demand();
        partialUpdatedDemand.setId(demand.getId());

        partialUpdatedDemand
            .document(UPDATED_DOCUMENT)
            .demandedBy(UPDATED_DEMANDED_BY)
            .description(UPDATED_DESCRIPTION)
            .demandType(UPDATED_DEMAND_TYPE)
            .status(UPDATED_STATUS)
            .processedBy(UPDATED_PROCESSED_BY)
            .entityId(UPDATED_ENTITY_ID)
            .processDescription(UPDATED_PROCESS_DESCRIPTION);

        restDemandMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedDemand.getId())
                    .with(csrf())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedDemand))
            )
            .andExpect(status().isOk());

        // Validate the Demand in the database
        List<Demand> demandList = demandRepository.findAll();
        assertThat(demandList).hasSize(databaseSizeBeforeUpdate);
        Demand testDemand = demandList.get(demandList.size() - 1);
        assertThat(testDemand.getDocument()).isEqualTo(UPDATED_DOCUMENT);
        assertThat(testDemand.getDemandedBy()).isEqualTo(UPDATED_DEMANDED_BY);
        assertThat(testDemand.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
        assertThat(testDemand.getDemandType()).isEqualTo(UPDATED_DEMAND_TYPE);
        assertThat(testDemand.getStatus()).isEqualTo(UPDATED_STATUS);
        assertThat(testDemand.getProcessedBy()).isEqualTo(UPDATED_PROCESSED_BY);
        assertThat(testDemand.getEntityId()).isEqualTo(UPDATED_ENTITY_ID);
        assertThat(testDemand.getProcessDescription()).isEqualTo(UPDATED_PROCESS_DESCRIPTION);
    }

    @Test
    @Transactional
    void patchNonExistingDemand() throws Exception {
        int databaseSizeBeforeUpdate = demandRepository.findAll().size();
        demand.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restDemandMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, demand.getId())
                    .with(csrf())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(demand))
            )
            .andExpect(status().isBadRequest());

        // Validate the Demand in the database
        List<Demand> demandList = demandRepository.findAll();
        assertThat(demandList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchDemand() throws Exception {
        int databaseSizeBeforeUpdate = demandRepository.findAll().size();
        demand.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restDemandMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .with(csrf())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(demand))
            )
            .andExpect(status().isBadRequest());

        // Validate the Demand in the database
        List<Demand> demandList = demandRepository.findAll();
        assertThat(demandList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamDemand() throws Exception {
        int databaseSizeBeforeUpdate = demandRepository.findAll().size();
        demand.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restDemandMockMvc
            .perform(
                patch(ENTITY_API_URL)
                    .with(csrf())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(demand))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the Demand in the database
        List<Demand> demandList = demandRepository.findAll();
        assertThat(demandList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteDemand() throws Exception {
        // Initialize the database
        demandRepository.saveAndFlush(demand);

        int databaseSizeBeforeDelete = demandRepository.findAll().size();

        // Delete the demand
        restDemandMockMvc
            .perform(delete(ENTITY_API_URL_ID, demand.getId()).with(csrf()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Demand> demandList = demandRepository.findAll();
        assertThat(demandList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
