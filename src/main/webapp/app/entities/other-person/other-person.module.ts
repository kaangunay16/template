import { NgModule } from '@angular/core';
import { SharedModule } from 'app/shared/shared.module';
import { OtherPersonComponent } from './list/other-person.component';
import { OtherPersonDetailComponent } from './detail/other-person-detail.component';
import { OtherPersonUpdateComponent } from './update/other-person-update.component';
import { OtherPersonDeleteDialogComponent } from './delete/other-person-delete-dialog.component';
import { OtherPersonRoutingModule } from './route/other-person-routing.module';

@NgModule({
  imports: [SharedModule, OtherPersonRoutingModule],
  declarations: [OtherPersonComponent, OtherPersonDetailComponent, OtherPersonUpdateComponent, OtherPersonDeleteDialogComponent],
  entryComponents: [OtherPersonDeleteDialogComponent],
})
export class OtherPersonModule {}
