import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { ActualActivityDetailComponent } from './actual-activity-detail.component';

describe('ActualActivity Management Detail Component', () => {
  let comp: ActualActivityDetailComponent;
  let fixture: ComponentFixture<ActualActivityDetailComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ActualActivityDetailComponent],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: { data: of({ actualActivity: { id: 123 } }) },
        },
      ],
    })
      .overrideTemplate(ActualActivityDetailComponent, '')
      .compileComponents();
    fixture = TestBed.createComponent(ActualActivityDetailComponent);
    comp = fixture.componentInstance;
  });

  describe('OnInit', () => {
    it('Should load actualActivity on init', () => {
      // WHEN
      comp.ngOnInit();

      // THEN
      expect(comp.actualActivity).toEqual(expect.objectContaining({ id: 123 }));
    });
  });
});
