import { Component, EventEmitter, Input, OnInit } from '@angular/core';
import { ControlValueAccessor, FormBuilder, FormGroup, NG_VALUE_ACCESSOR } from '@angular/forms';

import { Parameter } from '../../models/parameter.model';
import { ParamType } from '../../models/param-type.model';

@Component({
  selector: 'jhi-parameter-field',
  templateUrl: './parameter-field.component.html',
  styleUrls: ['./parameter-field.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: ParameterFieldComponent,
      multi: true,
    },
  ],
})
export class ParameterFieldComponent implements OnInit, ControlValueAccessor {
  form!: FormGroup;

  onTouched!: () => void;

  @Input()
  fields!: ParamType[];

  parameterEmitter: EventEmitter<Parameter> = new EventEmitter();

  set fieldType(type: string | null) {
    this._fieldType = type;
  }

  get fieldType(): string | null {
    return this._fieldType;
  }

  private _fieldType: string | null = null;

  constructor(private formBuilder: FormBuilder) {}

  ngOnInit(): void {
    this.form = this.formBuilder.group({
      field: null,
      fieldType: null,
      value: null,
      sqlOperation: null,
    });
  }

  registerOnChange(fn: (value: any) => void): void {
    this.form.valueChanges.subscribe(fn);
  }

  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

  writeValue(obj: Parameter): void {
    this.form.patchValue({
      field: obj.field,
      fieldType: obj.fieldType,
      value: obj.value,
      sqlOperation: obj.sqlOperation,
    });
  }

  addNewParameter(): void {
    const value = this.form.value as Parameter;

    if (this.fieldType) {
      value.fieldType = this.fieldType;
    }

    if (!value.field || !value.fieldType || !value.value || !value.sqlOperation) {
      return;
    }

    this.parameterEmitter.emit(value);
  }

  selectionChange(): void {
    const field = this.form.get('field')?.value as string;

    if (field) {
      this.fieldType = this.fields.find(f => f.field === field)!.type;
    } else {
      this.fieldType = null;
    }
  }
}
