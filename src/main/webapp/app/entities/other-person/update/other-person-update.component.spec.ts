jest.mock('@angular/router');

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { of, Subject } from 'rxjs';

import { OtherPersonService } from '../service/other-person.service';
import { IOtherPerson, OtherPerson } from '../other-person.model';
import { IIdentity } from 'app/entities/identity/identity.model';
import { IdentityService } from 'app/entities/identity/service/identity.service';

import { OtherPersonUpdateComponent } from './other-person-update.component';

describe('OtherPerson Management Update Component', () => {
  let comp: OtherPersonUpdateComponent;
  let fixture: ComponentFixture<OtherPersonUpdateComponent>;
  let activatedRoute: ActivatedRoute;
  let otherPersonService: OtherPersonService;
  let identityService: IdentityService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      declarations: [OtherPersonUpdateComponent],
      providers: [FormBuilder, ActivatedRoute],
    })
      .overrideTemplate(OtherPersonUpdateComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(OtherPersonUpdateComponent);
    activatedRoute = TestBed.inject(ActivatedRoute);
    otherPersonService = TestBed.inject(OtherPersonService);
    identityService = TestBed.inject(IdentityService);

    comp = fixture.componentInstance;
  });

  describe('ngOnInit', () => {
    it('Should call identity query and add missing value', () => {
      const otherPerson: IOtherPerson = { id: 456 };
      const identity: IIdentity = { id: 51840 };
      otherPerson.identity = identity;

      const identityCollection: IIdentity[] = [{ id: 97942 }];
      jest.spyOn(identityService, 'query').mockReturnValue(of(new HttpResponse({ body: identityCollection })));
      const expectedCollection: IIdentity[] = [identity, ...identityCollection];
      jest.spyOn(identityService, 'addIdentityToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ otherPerson });
      comp.ngOnInit();

      expect(identityService.query).toHaveBeenCalled();
      expect(identityService.addIdentityToCollectionIfMissing).toHaveBeenCalledWith(identityCollection, identity);
      expect(comp.identitiesCollection).toEqual(expectedCollection);
    });

    it('Should update editForm', () => {
      const otherPerson: IOtherPerson = { id: 456 };
      const identity: IIdentity = { id: 57687 };
      otherPerson.identity = identity;

      activatedRoute.data = of({ otherPerson });
      comp.ngOnInit();

      expect(comp.editForm.value).toEqual(expect.objectContaining(otherPerson));
      expect(comp.identitiesCollection).toContain(identity);
    });
  });

  describe('save', () => {
    it('Should call update service on save for existing entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<OtherPerson>>();
      const otherPerson = { id: 123 };
      jest.spyOn(otherPersonService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ otherPerson });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: otherPerson }));
      saveSubject.complete();

      // THEN
      expect(comp.previousState).toHaveBeenCalled();
      expect(otherPersonService.update).toHaveBeenCalledWith(otherPerson);
      expect(comp.isSaving).toEqual(false);
    });

    it('Should call create service on save for new entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<OtherPerson>>();
      const otherPerson = new OtherPerson();
      jest.spyOn(otherPersonService, 'create').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ otherPerson });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: otherPerson }));
      saveSubject.complete();

      // THEN
      expect(otherPersonService.create).toHaveBeenCalledWith(otherPerson);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).toHaveBeenCalled();
    });

    it('Should set isSaving to false on error', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<OtherPerson>>();
      const otherPerson = { id: 123 };
      jest.spyOn(otherPersonService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ otherPerson });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.error('This is an error!');

      // THEN
      expect(otherPersonService.update).toHaveBeenCalledWith(otherPerson);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).not.toHaveBeenCalled();
    });
  });

  describe('Tracking relationships identifiers', () => {
    describe('trackIdentityById', () => {
      it('Should return tracked Identity primary key', () => {
        const entity = { id: 123 };
        const trackResult = comp.trackIdentityById(0, entity);
        expect(trackResult).toEqual(entity.id);
      });
    });
  });
});
