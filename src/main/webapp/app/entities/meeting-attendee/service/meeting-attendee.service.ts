import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { isPresent } from 'app/core/util/operators';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { IMeetingAttendee, getMeetingAttendeeIdentifier } from '../meeting-attendee.model';

export type EntityResponseType = HttpResponse<IMeetingAttendee>;
export type EntityArrayResponseType = HttpResponse<IMeetingAttendee[]>;

@Injectable({ providedIn: 'root' })
export class MeetingAttendeeService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/meeting-attendees');

  constructor(protected http: HttpClient, protected applicationConfigService: ApplicationConfigService) {}

  create(meetingAttendee: IMeetingAttendee): Observable<EntityResponseType> {
    return this.http.post<IMeetingAttendee>(this.resourceUrl, meetingAttendee, { observe: 'response' });
  }

  update(meetingAttendee: IMeetingAttendee): Observable<EntityResponseType> {
    return this.http.put<IMeetingAttendee>(
      `${this.resourceUrl}/${getMeetingAttendeeIdentifier(meetingAttendee) as number}`,
      meetingAttendee,
      { observe: 'response' }
    );
  }

  partialUpdate(meetingAttendee: IMeetingAttendee): Observable<EntityResponseType> {
    return this.http.patch<IMeetingAttendee>(
      `${this.resourceUrl}/${getMeetingAttendeeIdentifier(meetingAttendee) as number}`,
      meetingAttendee,
      { observe: 'response' }
    );
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IMeetingAttendee>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IMeetingAttendee[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  addMeetingAttendeeToCollectionIfMissing(
    meetingAttendeeCollection: IMeetingAttendee[],
    ...meetingAttendeesToCheck: (IMeetingAttendee | null | undefined)[]
  ): IMeetingAttendee[] {
    const meetingAttendees: IMeetingAttendee[] = meetingAttendeesToCheck.filter(isPresent);
    if (meetingAttendees.length > 0) {
      const meetingAttendeeCollectionIdentifiers = meetingAttendeeCollection.map(
        meetingAttendeeItem => getMeetingAttendeeIdentifier(meetingAttendeeItem)!
      );
      const meetingAttendeesToAdd = meetingAttendees.filter(meetingAttendeeItem => {
        const meetingAttendeeIdentifier = getMeetingAttendeeIdentifier(meetingAttendeeItem);
        if (meetingAttendeeIdentifier == null || meetingAttendeeCollectionIdentifiers.includes(meetingAttendeeIdentifier)) {
          return false;
        }
        meetingAttendeeCollectionIdentifiers.push(meetingAttendeeIdentifier);
        return true;
      });
      return [...meetingAttendeesToAdd, ...meetingAttendeeCollection];
    }
    return meetingAttendeeCollection;
  }
}
