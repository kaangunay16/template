import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IOtherName } from '../other-name.model';
import { OtherNameService } from '../service/other-name.service';
import { OtherNameDeleteDialogComponent } from '../delete/other-name-delete-dialog.component';

@Component({
  selector: 'jhi-other-name',
  templateUrl: './other-name.component.html',
})
export class OtherNameComponent implements OnInit {
  otherNames?: IOtherName[];
  isLoading = false;

  constructor(protected otherNameService: OtherNameService, protected modalService: NgbModal) {}

  loadAll(): void {
    this.isLoading = true;

    this.otherNameService.query().subscribe(
      (res: HttpResponse<IOtherName[]>) => {
        this.isLoading = false;
        this.otherNames = res.body ?? [];
      },
      () => {
        this.isLoading = false;
      }
    );
  }

  ngOnInit(): void {
    this.loadAll();
  }

  trackId(index: number, item: IOtherName): number {
    return item.id!;
  }

  delete(otherName: IOtherName): void {
    const modalRef = this.modalService.open(OtherNameDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.otherName = otherName;
    // unsubscribe not needed because closed completes on modal close
    modalRef.closed.subscribe(reason => {
      if (reason === 'deleted') {
        this.loadAll();
      }
    });
  }
}
