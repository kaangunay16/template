import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IdentityRetrieverComponent } from './identity-retriever.component';

describe('IdentityRetrieverComponent', () => {
  let component: IdentityRetrieverComponent;
  let fixture: ComponentFixture<IdentityRetrieverComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [IdentityRetrieverComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(IdentityRetrieverComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
