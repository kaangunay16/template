import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { IPastActivity, PastActivity } from '../past-activity.model';

import { PastActivityService } from './past-activity.service';

describe('PastActivity Service', () => {
  let service: PastActivityService;
  let httpMock: HttpTestingController;
  let elemDefault: IPastActivity;
  let expectedResult: IPastActivity | IPastActivity[] | boolean | null;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    expectedResult = null;
    service = TestBed.inject(PastActivityService);
    httpMock = TestBed.inject(HttpTestingController);

    elemDefault = {
      id: 0,
      city: 'AAAAAAA',
      startYear: 0,
      endYear: 0,
      deleted: false,
    };
  });

  describe('Service methods', () => {
    it('should find an element', () => {
      const returnedFromService = Object.assign({}, elemDefault);

      service.find(123).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(elemDefault);
    });

    it('should create a PastActivity', () => {
      const returnedFromService = Object.assign(
        {
          id: 0,
        },
        elemDefault
      );

      const expected = Object.assign({}, returnedFromService);

      service.create(new PastActivity()).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'POST' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should update a PastActivity', () => {
      const returnedFromService = Object.assign(
        {
          id: 1,
          city: 'BBBBBB',
          startYear: 1,
          endYear: 1,
          deleted: true,
        },
        elemDefault
      );

      const expected = Object.assign({}, returnedFromService);

      service.update(expected).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PUT' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should partial update a PastActivity', () => {
      const patchObject = Object.assign(
        {
          city: 'BBBBBB',
          startYear: 1,
          deleted: true,
        },
        new PastActivity()
      );

      const returnedFromService = Object.assign(patchObject, elemDefault);

      const expected = Object.assign({}, returnedFromService);

      service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PATCH' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should return a list of PastActivity', () => {
      const returnedFromService = Object.assign(
        {
          id: 1,
          city: 'BBBBBB',
          startYear: 1,
          endYear: 1,
          deleted: true,
        },
        elemDefault
      );

      const expected = Object.assign({}, returnedFromService);

      service.query().subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush([returnedFromService]);
      httpMock.verify();
      expect(expectedResult).toContainEqual(expected);
    });

    it('should delete a PastActivity', () => {
      service.delete(123).subscribe(resp => (expectedResult = resp.ok));

      const req = httpMock.expectOne({ method: 'DELETE' });
      req.flush({ status: 200 });
      expect(expectedResult);
    });

    describe('addPastActivityToCollectionIfMissing', () => {
      it('should add a PastActivity to an empty array', () => {
        const pastActivity: IPastActivity = { id: 123 };
        expectedResult = service.addPastActivityToCollectionIfMissing([], pastActivity);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(pastActivity);
      });

      it('should not add a PastActivity to an array that contains it', () => {
        const pastActivity: IPastActivity = { id: 123 };
        const pastActivityCollection: IPastActivity[] = [
          {
            ...pastActivity,
          },
          { id: 456 },
        ];
        expectedResult = service.addPastActivityToCollectionIfMissing(pastActivityCollection, pastActivity);
        expect(expectedResult).toHaveLength(2);
      });

      it("should add a PastActivity to an array that doesn't contain it", () => {
        const pastActivity: IPastActivity = { id: 123 };
        const pastActivityCollection: IPastActivity[] = [{ id: 456 }];
        expectedResult = service.addPastActivityToCollectionIfMissing(pastActivityCollection, pastActivity);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(pastActivity);
      });

      it('should add only unique PastActivity to an array', () => {
        const pastActivityArray: IPastActivity[] = [{ id: 123 }, { id: 456 }, { id: 22145 }];
        const pastActivityCollection: IPastActivity[] = [{ id: 123 }];
        expectedResult = service.addPastActivityToCollectionIfMissing(pastActivityCollection, ...pastActivityArray);
        expect(expectedResult).toHaveLength(3);
      });

      it('should accept varargs', () => {
        const pastActivity: IPastActivity = { id: 123 };
        const pastActivity2: IPastActivity = { id: 456 };
        expectedResult = service.addPastActivityToCollectionIfMissing([], pastActivity, pastActivity2);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(pastActivity);
        expect(expectedResult).toContain(pastActivity2);
      });

      it('should accept null and undefined values', () => {
        const pastActivity: IPastActivity = { id: 123 };
        expectedResult = service.addPastActivityToCollectionIfMissing([], null, pastActivity, undefined);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(pastActivity);
      });

      it('should return initial array if no PastActivity is added', () => {
        const pastActivityCollection: IPastActivity[] = [{ id: 123 }];
        expectedResult = service.addPastActivityToCollectionIfMissing(pastActivityCollection, undefined, null);
        expect(expectedResult).toEqual(pastActivityCollection);
      });
    });
  });

  afterEach(() => {
    httpMock.verify();
  });
});
