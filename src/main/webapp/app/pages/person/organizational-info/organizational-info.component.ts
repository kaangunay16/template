import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';

import { PersonModuleService } from 'app/pages/person/person-module.service';
import { IPerson } from 'app/entities/person/person.model';
import { IOtherName } from 'app/entities/other-name/other-name.model';
import { Phone } from 'app/entities/phone/phone.model';
import { DynamicFormCreatorModel, IDynamicFormCreatorModel } from 'app/components/dynamic-form-creator/dynamic-form-field.model';
import { FormValueConverterService } from 'app/components/dynamic-form-creator/form-value-converter.service';
import { IActivityField } from 'app/components/models/activity-field.model';

@Component({
  selector: 'jhi-organizational-info',
  templateUrl: './organizational-info.component.html',
  styleUrls: ['./organizational-info.component.scss'],
})
export class OrganizationalInfoComponent implements OnInit, OnDestroy {
  person!: IPerson;
  formCreator!: IDynamicFormCreatorModel;
  routeSub!: Subscription;

  constructor(
    private personModuleService: PersonModuleService,
    private formValueService: FormValueConverterService,
    private activatedRoute: ActivatedRoute,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.routeSub = this.activatedRoute.queryParams.subscribe(params => {
      this.person = this.personModuleService.entityCheck(params);
      this.formCreator = new DynamicFormCreatorModel([
        {
          id: 'otherName',
          type: 'otherName',
          label: 'Diğer İsim',
          multiple: true,
          value: this.person.otherNames,
        },
        {
          id: 'status',
          type: 'selections',
          label: 'Durum',
          selectionType: 'org-status',
          placeholder: 'Durum',
          value: this.person.status,
        },
        {
          id: 'description',
          type: 'textarea',
          label: 'Açıklama',
          value: this.person.description,
        },
        {
          id: 'phone',
          type: 'phone',
          label: 'Telefon',
          multiple: true,
          value: this.person.phones,
        },
        {
          id: 'activityField',
          type: 'activityField',
          label: 'Faaliyet Alanı',
          value: this.personModuleService.getActivityField(this.person),
        },
      ]);
    });
  }

  ngOnDestroy(): void {
    this.routeSub.unsubscribe();
  }

  createSubmit(): void {
    this.setFormValuesToEntity();
    this.router.navigate(['/person', 'past-activities']);
  }

  updateSubmit(): void {
    this.setFormValuesToEntity();
    this.personModuleService.updatePerson(this.person);
  }

  private setFormValuesToEntity(): void {
    this.person.otherNames = this.formValueService.getFormValue(this.formCreator, 'otherName') as IOtherName[];
    this.person.status = this.formValueService.getFormValue(this.formCreator, 'status') as string;
    this.person.description = this.formValueService.getFormValue(this.formCreator, 'description') as string;
    this.person.phones = this.formValueService.getFormValue(this.formCreator, 'phone') as Phone[];
    this.personModuleService.setActivityField(
      this.person,
      this.formValueService.getFormValue(this.formCreator, 'activityField') as IActivityField | null
    );
  }
}
