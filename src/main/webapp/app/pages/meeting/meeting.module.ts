import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ComponentsModule } from 'app/components/components.module';
import { SharedModule } from 'app/shared/shared.module';
import { MeetingRoutingModule } from './meeting-routing.module';
import { MeetingMainComponent } from './meeting-main/meeting-main.component';
import { MeetingInfoComponent } from './meeting-info/meeting-info.component';
import { MeetingAttendeesComponent } from './meeting-attendees/meeting-attendees.component';
import { MeetingFinalizeComponent } from './meeting-finalize/meeting-finalize.component';
import { MeetingListComponent } from './meeting-list/meeting-list.component';

@NgModule({
  declarations: [MeetingMainComponent, MeetingInfoComponent, MeetingAttendeesComponent, MeetingFinalizeComponent, MeetingListComponent],
  imports: [CommonModule, MeetingRoutingModule, ComponentsModule, SharedModule],
})
export class MeetingModule {}
