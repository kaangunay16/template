import { Component, ComponentFactoryResolver, Input, OnInit, ViewChild } from '@angular/core';

import { ITab } from '../models/tab.model';
import { ContentContainerDirective } from '../content-container.directive';

@Component({
  selector: 'jhi-tab-content',
  templateUrl: './tab-content.component.html',
  styleUrls: ['./tab-content.component.scss'],
})
export class TabContentComponent implements OnInit {
  @Input()
  tab!: ITab;

  @ViewChild(ContentContainerDirective, { static: true })
  contentContainer!: ContentContainerDirective;

  constructor(private componentFactoryResolver: ComponentFactoryResolver) {}

  ngOnInit(): void {
    const componentFactory = this.componentFactoryResolver.resolveComponentFactory(this.tab.component);
    const componentRef = this.contentContainer.viewContainerRef.createComponent(componentFactory);
    componentRef.instance.tabData = this.tab.tabData;
  }
}
