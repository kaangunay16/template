import { Action, createReducer, on } from '@ngrx/store';

import { initialState } from './shared.state';
import { setErrorMessage, setLoadingSpinner } from './shared.action';

const _sharedReducer = createReducer(
  initialState,
  on(setLoadingSpinner, (state, action) => ({ ...state, showLoading: action.status })),
  on(setErrorMessage, (state, action) => ({ ...state, errorMessage: action.errorMessage }))
);

export function sharedReducer(state: any, action: Action): any {
  return _sharedReducer(state, action);
}
