import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';

import { DataPostService } from '../data-post.service';
import { ISelection } from 'app/entities/selection/selection.model';

@Component({
  selector: 'jhi-ngrx-data-post-list',
  templateUrl: './ngrx-data-post-list.component.html',
  styleUrls: ['./ngrx-data-post-list.component.scss'],
})
export class NgrxDataPostListComponent implements OnInit {
  dataPosts$!: Observable<ISelection[]>;

  constructor(private dataPostService: DataPostService) {}

  ngOnInit(): void {
    this.dataPosts$ = this.dataPostService.entities$;
  }

  delete(id: number | undefined): void {
    if (id) {
      this.dataPostService.delete(id);
    }
  }
}
