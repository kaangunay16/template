package tr.com.khg.template.repository;

import java.util.Optional;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import tr.com.khg.template.domain.Person;

/**
 * Spring Data SQL repository for the Person entity.
 */
@SuppressWarnings("unused")
@Repository
public interface PersonRepository extends JpaRepository<Person, Long> {
    @Query(
        "SELECT p FROM Person p JOIN FETCH p.identity i LEFT JOIN FETCH p.abroadActivities LEFT JOIN FETCH p.actualActivities LEFT JOIN FETCH p.pastActivities LEFT JOIN FETCH p.phones LEFT JOIN FETCH p.otherNames WHERE i.identityNo = :identityNo"
    )
    Optional<Person> findByIdentityNoFetchAllRelations(@Param("identityNo") String identityNo);

    Optional<Person> findByIdentityIdentityNo(String identityNo);

    Optional<Person> findByActualActivitiesId(Long actualActivitiesId);
}
