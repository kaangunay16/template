import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OtherPersonResultTableComponent } from './other-person-result-table.component';

describe('OtherPersonResultTableComponent', () => {
  let component: OtherPersonResultTableComponent;
  let fixture: ComponentFixture<OtherPersonResultTableComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [OtherPersonResultTableComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OtherPersonResultTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
