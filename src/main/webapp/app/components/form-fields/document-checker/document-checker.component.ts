import { Component, Input } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

@Component({
  selector: 'jhi-document-checker',
  templateUrl: './document-checker.component.html',
  styleUrls: ['./document-checker.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: DocumentCheckerComponent,
      multi: true,
    },
  ],
})
export class DocumentCheckerComponent implements ControlValueAccessor {
  @Input()
  readOnly?: boolean = false;

  @Input()
  value?: string | null = null;

  onChange!: (value: string | null) => void;

  onTouched!: () => void;

  registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

  setDisabledState(isDisabled: boolean): void {
    this.readOnly = isDisabled;
  }

  writeValue(obj: string): void {
    this.value = obj;
  }

  modalChange($event: string): void {
    this.onChange($event);
  }
}
