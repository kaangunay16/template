import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PersonDeleteMainComponent } from './person-delete-main.component';

describe('PersonDeleteMainComponent', () => {
  let component: PersonDeleteMainComponent;
  let fixture: ComponentFixture<PersonDeleteMainComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [PersonDeleteMainComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PersonDeleteMainComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
