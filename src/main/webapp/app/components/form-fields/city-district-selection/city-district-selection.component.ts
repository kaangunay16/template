import { Component, Input, OnInit } from '@angular/core';
import { ControlValueAccessor, FormBuilder, FormGroup, NG_VALUE_ACCESSOR } from '@angular/forms';
import { CityDistrict, ICityDistrict } from 'app/components/models/city-district.model';

@Component({
  selector: 'jhi-city-district-selection',
  templateUrl: './city-district-selection.component.html',
  styleUrls: ['./city-district-selection.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: CityDistrictSelectionComponent,
      multi: true,
    },
  ],
})
export class CityDistrictSelectionComponent implements OnInit, ControlValueAccessor {
  form!: FormGroup;
  selection: ICityDistrict = new CityDistrict();

  @Input()
  readOnly?: boolean = false;

  onChange!: (value: ICityDistrict) => void;
  onTouched!: () => void;

  constructor(private formBuilder: FormBuilder) {}

  ngOnInit(): void {
    this.form = this.formBuilder.group({
      city: [{ value: null, disabled: this.readOnly }],
      district: [{ value: null, disabled: this.readOnly }],
    });
  }

  registerOnChange(fn: (value: any) => void): void {
    this.form.valueChanges.subscribe(fn);
  }

  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

  writeValue(obj: ICityDistrict | null): void {
    this.selection.city = obj?.city;
    this.form.patchValue({
      city: obj ? obj.city : null,
      district: obj ? obj.district : null,
    });
  }

  change($event: string): void {
    this.selection.city = $event;
    this.selection.district = null;
    this.form.get('district')?.setValue(null);
  }
}
