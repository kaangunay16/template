jest.mock('@angular/router');

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { of, Subject } from 'rxjs';

import { MeetingAttendeeService } from '../service/meeting-attendee.service';
import { IMeetingAttendee, MeetingAttendee } from '../meeting-attendee.model';
import { IMeeting } from 'app/entities/meeting/meeting.model';
import { MeetingService } from 'app/entities/meeting/service/meeting.service';
import { IIdentity } from 'app/entities/identity/identity.model';
import { IdentityService } from 'app/entities/identity/service/identity.service';

import { MeetingAttendeeUpdateComponent } from './meeting-attendee-update.component';

describe('MeetingAttendee Management Update Component', () => {
  let comp: MeetingAttendeeUpdateComponent;
  let fixture: ComponentFixture<MeetingAttendeeUpdateComponent>;
  let activatedRoute: ActivatedRoute;
  let meetingAttendeeService: MeetingAttendeeService;
  let meetingService: MeetingService;
  let identityService: IdentityService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      declarations: [MeetingAttendeeUpdateComponent],
      providers: [FormBuilder, ActivatedRoute],
    })
      .overrideTemplate(MeetingAttendeeUpdateComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(MeetingAttendeeUpdateComponent);
    activatedRoute = TestBed.inject(ActivatedRoute);
    meetingAttendeeService = TestBed.inject(MeetingAttendeeService);
    meetingService = TestBed.inject(MeetingService);
    identityService = TestBed.inject(IdentityService);

    comp = fixture.componentInstance;
  });

  describe('ngOnInit', () => {
    it('Should call Meeting query and add missing value', () => {
      const meetingAttendee: IMeetingAttendee = { id: 456 };
      const meeting: IMeeting = { id: 971 };
      meetingAttendee.meeting = meeting;

      const meetingCollection: IMeeting[] = [{ id: 12719 }];
      jest.spyOn(meetingService, 'query').mockReturnValue(of(new HttpResponse({ body: meetingCollection })));
      const additionalMeetings = [meeting];
      const expectedCollection: IMeeting[] = [...additionalMeetings, ...meetingCollection];
      jest.spyOn(meetingService, 'addMeetingToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ meetingAttendee });
      comp.ngOnInit();

      expect(meetingService.query).toHaveBeenCalled();
      expect(meetingService.addMeetingToCollectionIfMissing).toHaveBeenCalledWith(meetingCollection, ...additionalMeetings);
      expect(comp.meetingsSharedCollection).toEqual(expectedCollection);
    });

    it('Should call Identity query and add missing value', () => {
      const meetingAttendee: IMeetingAttendee = { id: 456 };
      const identity: IIdentity = { id: 96722 };
      meetingAttendee.identity = identity;

      const identityCollection: IIdentity[] = [{ id: 88817 }];
      jest.spyOn(identityService, 'query').mockReturnValue(of(new HttpResponse({ body: identityCollection })));
      const additionalIdentities = [identity];
      const expectedCollection: IIdentity[] = [...additionalIdentities, ...identityCollection];
      jest.spyOn(identityService, 'addIdentityToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ meetingAttendee });
      comp.ngOnInit();

      expect(identityService.query).toHaveBeenCalled();
      expect(identityService.addIdentityToCollectionIfMissing).toHaveBeenCalledWith(identityCollection, ...additionalIdentities);
      expect(comp.identitiesSharedCollection).toEqual(expectedCollection);
    });

    it('Should update editForm', () => {
      const meetingAttendee: IMeetingAttendee = { id: 456 };
      const meeting: IMeeting = { id: 89699 };
      meetingAttendee.meeting = meeting;
      const identity: IIdentity = { id: 90136 };
      meetingAttendee.identity = identity;

      activatedRoute.data = of({ meetingAttendee });
      comp.ngOnInit();

      expect(comp.editForm.value).toEqual(expect.objectContaining(meetingAttendee));
      expect(comp.meetingsSharedCollection).toContain(meeting);
      expect(comp.identitiesSharedCollection).toContain(identity);
    });
  });

  describe('save', () => {
    it('Should call update service on save for existing entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<MeetingAttendee>>();
      const meetingAttendee = { id: 123 };
      jest.spyOn(meetingAttendeeService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ meetingAttendee });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: meetingAttendee }));
      saveSubject.complete();

      // THEN
      expect(comp.previousState).toHaveBeenCalled();
      expect(meetingAttendeeService.update).toHaveBeenCalledWith(meetingAttendee);
      expect(comp.isSaving).toEqual(false);
    });

    it('Should call create service on save for new entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<MeetingAttendee>>();
      const meetingAttendee = new MeetingAttendee();
      jest.spyOn(meetingAttendeeService, 'create').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ meetingAttendee });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: meetingAttendee }));
      saveSubject.complete();

      // THEN
      expect(meetingAttendeeService.create).toHaveBeenCalledWith(meetingAttendee);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).toHaveBeenCalled();
    });

    it('Should set isSaving to false on error', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<MeetingAttendee>>();
      const meetingAttendee = { id: 123 };
      jest.spyOn(meetingAttendeeService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ meetingAttendee });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.error('This is an error!');

      // THEN
      expect(meetingAttendeeService.update).toHaveBeenCalledWith(meetingAttendee);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).not.toHaveBeenCalled();
    });
  });

  describe('Tracking relationships identifiers', () => {
    describe('trackMeetingById', () => {
      it('Should return tracked Meeting primary key', () => {
        const entity = { id: 123 };
        const trackResult = comp.trackMeetingById(0, entity);
        expect(trackResult).toEqual(entity.id);
      });
    });

    describe('trackIdentityById', () => {
      it('Should return tracked Identity primary key', () => {
        const entity = { id: 123 };
        const trackResult = comp.trackIdentityById(0, entity);
        expect(trackResult).toEqual(entity.id);
      });
    });
  });
});
