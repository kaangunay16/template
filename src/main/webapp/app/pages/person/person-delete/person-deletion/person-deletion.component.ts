import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
import { ToastrService } from 'ngx-toastr';

import { IPerson } from 'app/entities/person/person.model';
import { DemandService } from 'app/entities/demand/service/demand.service';
import { Demand } from 'app/entities/demand/demand.model';
import { DemandType } from 'app/entities/enumerations/demand-type.model';
import { DynamicFormCreatorModel, IDynamicFormCreatorModel } from 'app/components/dynamic-form-creator/dynamic-form-field.model';
import { FormValueConverterService } from 'app/components/dynamic-form-creator/form-value-converter.service';
import { ErrorSupportService } from 'app/shared/services/error-support.service';
import { PersonModuleService } from 'app/pages/person/person-module.service';
import { PERSON_DELETION } from 'app/shared/services/demands-types';

@Component({
  selector: 'jhi-person-deletion',
  templateUrl: './person-deletion.component.html',
  styleUrls: ['./person-deletion.component.scss'],
})
export class PersonDeletionComponent implements OnInit, OnDestroy {
  person!: IPerson;
  formCreator?: IDynamicFormCreatorModel;
  routeSub!: Subscription;
  demandSub?: Subscription;

  constructor(
    private toastr: ToastrService,
    private personModuleService: PersonModuleService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private demandService: DemandService,
    private formValueService: FormValueConverterService,
    private errorService: ErrorSupportService
  ) {}

  ngOnInit(): void {
    this.routeSub = this.activatedRoute.queryParams.subscribe(params => {
      this.person = this.personModuleService.entityCheck(params);
      this.formCreator = new DynamicFormCreatorModel([
        {
          id: 'name',
          label: 'Adı Soyadı',
          type: 'text',
          value: `${this.person.identity!.name!} ${this.person.identity!.surname!} (TC:${this.person.identity!.identityNo!})`,
          readOnly: true,
        },
        {
          id: 'document',
          label: 'Evrak Numarası',
          type: 'document',
          validators: [Validators.required],
        },
        {
          id: 'description',
          type: 'textarea',
          label: 'Açıklama',
          validators: [Validators.required],
        },
      ]);
    });
  }

  ngOnDestroy(): void {
    this.routeSub.unsubscribe();
    this.demandSub?.unsubscribe();
  }

  submit(): void {
    const demand = new Demand();
    demand.document = this.formValueService.getFormValue(this.formCreator!, 'document') as string;
    demand.description = this.formValueService.getFormValue(this.formCreator!, 'description') as string;
    demand.status = DemandType.WAITING;
    demand.demandType = PERSON_DELETION;
    demand.entityId = this.person.id;
    this.demandSub = this.demandService.create(demand).subscribe(
      res => {
        if (res.body) {
          this.toastr.success('Talep başarıyla oluşturuldu');
          this.router.navigate(['/person', 'get-identity-info']);
        }
      },
      err => this.errorService.errorHandler(err)
    );
  }
}
