import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { isPresent } from 'app/core/util/operators';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { IAbroadActivity, getAbroadActivityIdentifier } from '../abroad-activity.model';
import { IFormEntity } from '../../form-entity.model';

export type EntityResponseType = HttpResponse<IAbroadActivity>;
export type EntityArrayResponseType = HttpResponse<IAbroadActivity[]>;

@Injectable({ providedIn: 'root' })
export class AbroadActivityService implements IFormEntity {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/abroad-activities');

  constructor(protected http: HttpClient, protected applicationConfigService: ApplicationConfigService) {}

  create(abroadActivity: IAbroadActivity): Observable<EntityResponseType> {
    return this.http.post<IAbroadActivity>(this.resourceUrl, abroadActivity, { observe: 'response' });
  }

  update(abroadActivity: IAbroadActivity): Observable<EntityResponseType> {
    return this.http.put<IAbroadActivity>(`${this.resourceUrl}/${getAbroadActivityIdentifier(abroadActivity) as number}`, abroadActivity, {
      observe: 'response',
    });
  }

  partialUpdate(abroadActivity: IAbroadActivity): Observable<EntityResponseType> {
    return this.http.patch<IAbroadActivity>(
      `${this.resourceUrl}/${getAbroadActivityIdentifier(abroadActivity) as number}`,
      abroadActivity,
      { observe: 'response' }
    );
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IAbroadActivity>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IAbroadActivity[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  addAbroadActivityToCollectionIfMissing(
    abroadActivityCollection: IAbroadActivity[],
    ...abroadActivitiesToCheck: (IAbroadActivity | null | undefined)[]
  ): IAbroadActivity[] {
    const abroadActivities: IAbroadActivity[] = abroadActivitiesToCheck.filter(isPresent);
    if (abroadActivities.length > 0) {
      const abroadActivityCollectionIdentifiers = abroadActivityCollection.map(
        abroadActivityItem => getAbroadActivityIdentifier(abroadActivityItem)!
      );
      const abroadActivitiesToAdd = abroadActivities.filter(abroadActivityItem => {
        const abroadActivityIdentifier = getAbroadActivityIdentifier(abroadActivityItem);
        if (abroadActivityIdentifier == null || abroadActivityCollectionIdentifiers.includes(abroadActivityIdentifier)) {
          return false;
        }
        abroadActivityCollectionIdentifiers.push(abroadActivityIdentifier);
        return true;
      });
      return [...abroadActivitiesToAdd, ...abroadActivityCollection];
    }
    return abroadActivityCollection;
  }
}
