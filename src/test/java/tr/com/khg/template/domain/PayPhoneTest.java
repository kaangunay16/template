package tr.com.khg.template.domain;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import tr.com.khg.template.web.rest.TestUtil;

class PayPhoneTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(PayPhone.class);
        PayPhone payPhone1 = new PayPhone();
        payPhone1.setId(1L);
        PayPhone payPhone2 = new PayPhone();
        payPhone2.setId(payPhone1.getId());
        assertThat(payPhone1).isEqualTo(payPhone2);
        payPhone2.setId(2L);
        assertThat(payPhone1).isNotEqualTo(payPhone2);
        payPhone1.setId(null);
        assertThat(payPhone1).isNotEqualTo(payPhone2);
    }
}
