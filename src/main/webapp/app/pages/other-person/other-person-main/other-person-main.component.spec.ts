import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OtherPersonMainComponent } from './other-person-main.component';

describe('OtherPersonMainComponent', () => {
  let component: OtherPersonMainComponent;
  let fixture: ComponentFixture<OtherPersonMainComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [OtherPersonMainComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OtherPersonMainComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
