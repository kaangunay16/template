import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import { IPastActivity, PastActivity } from '../past-activity.model';
import { PastActivityService } from '../service/past-activity.service';
import { IPerson } from 'app/entities/person/person.model';
import { PersonService } from 'app/entities/person/service/person.service';

@Component({
  selector: 'jhi-past-activity-update',
  templateUrl: './past-activity-update.component.html',
})
export class PastActivityUpdateComponent implements OnInit {
  isSaving = false;

  peopleSharedCollection: IPerson[] = [];

  editForm = this.fb.group({
    id: [],
    city: [],
    startYear: [],
    endYear: [],
    deleted: [],
    person: [],
  });

  constructor(
    protected pastActivityService: PastActivityService,
    protected personService: PersonService,
    protected activatedRoute: ActivatedRoute,
    protected fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ pastActivity }) => {
      this.updateForm(pastActivity);

      this.loadRelationshipsOptions();
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const pastActivity = this.createFromForm();
    if (pastActivity.id !== undefined) {
      this.subscribeToSaveResponse(this.pastActivityService.update(pastActivity));
    } else {
      this.subscribeToSaveResponse(this.pastActivityService.create(pastActivity));
    }
  }

  trackPersonById(index: number, item: IPerson): number {
    return item.id!;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IPastActivity>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(pastActivity: IPastActivity): void {
    this.editForm.patchValue({
      id: pastActivity.id,
      city: pastActivity.city,
      startYear: pastActivity.startYear,
      endYear: pastActivity.endYear,
      deleted: pastActivity.deleted,
      person: pastActivity.person,
    });

    this.peopleSharedCollection = this.personService.addPersonToCollectionIfMissing(this.peopleSharedCollection, pastActivity.person);
  }

  protected loadRelationshipsOptions(): void {
    this.personService
      .query()
      .pipe(map((res: HttpResponse<IPerson[]>) => res.body ?? []))
      .pipe(map((people: IPerson[]) => this.personService.addPersonToCollectionIfMissing(people, this.editForm.get('person')!.value)))
      .subscribe((people: IPerson[]) => (this.peopleSharedCollection = people));
  }

  protected createFromForm(): IPastActivity {
    return {
      ...new PastActivity(),
      id: this.editForm.get(['id'])!.value,
      city: this.editForm.get(['city'])!.value,
      startYear: this.editForm.get(['startYear'])!.value,
      endYear: this.editForm.get(['endYear'])!.value,
      deleted: this.editForm.get(['deleted'])!.value,
      person: this.editForm.get(['person'])!.value,
    };
  }
}
