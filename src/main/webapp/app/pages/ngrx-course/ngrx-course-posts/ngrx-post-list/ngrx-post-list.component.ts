import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';

import { AppState } from '../../store/app.state';
import { Post } from '../models/post.model';
import { getCount, getPosts } from '../state/posts.selector';
import { deletePost } from '../state/posts.actions';

@Component({
  selector: 'jhi-ngrx-post-list',
  templateUrl: './ngrx-post-list.component.html',
  styleUrls: ['./ngrx-post-list.component.scss'],
})
export class NgrxPostListComponent implements OnInit {
  posts$!: Observable<Post[]>;
  count$!: Observable<number>;

  constructor(private store: Store<AppState>) {}

  ngOnInit(): void {
    this.posts$ = this.store.select(getPosts);
    this.count$ = this.store.select(getCount);
  }

  delete(id: string | undefined): void {
    if (id) {
      this.store.dispatch(deletePost({ id }));
    }
  }
}
