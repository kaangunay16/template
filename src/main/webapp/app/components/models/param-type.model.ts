export interface ParamType {
  field: string;
  type: 'text' | 'number';
}
