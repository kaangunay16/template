import { forwardRef, Injectable } from '@angular/core';
import { EntityCollectionServiceBase, EntityCollectionServiceElementsFactory } from '@ngrx/data';

import { NgrxCourseDataModule } from './ngrx-course-data.module';
import { ISelection } from 'app/entities/selection/selection.model';

@Injectable({
  // Bu servis rootta çalıştırılsa ngrx/data modulü NgrxCourse Modülünde
  // provide edilmesi nedeniyle sonra init olması nedeniyle hata alınmaktadır.
  providedIn: forwardRef(() => NgrxCourseDataModule),
})
export class DataPostService extends EntityCollectionServiceBase<ISelection> {
  constructor(serviceElementFactory: EntityCollectionServiceElementsFactory) {
    super('Selection', serviceElementFactory);
  }
}
