import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';

import { IPayPhone, PayPhone } from '../pay-phone.model';
import { PayPhoneService } from '../service/pay-phone.service';

@Component({
  selector: 'jhi-pay-phone-update',
  templateUrl: './pay-phone-update.component.html',
})
export class PayPhoneUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [],
    city: [],
    phoneNumber: [],
  });

  constructor(protected payPhoneService: PayPhoneService, protected activatedRoute: ActivatedRoute, protected fb: FormBuilder) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ payPhone }) => {
      this.updateForm(payPhone);
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const payPhone = this.createFromForm();
    if (payPhone.id !== undefined) {
      this.subscribeToSaveResponse(this.payPhoneService.update(payPhone));
    } else {
      this.subscribeToSaveResponse(this.payPhoneService.create(payPhone));
    }
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IPayPhone>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(payPhone: IPayPhone): void {
    this.editForm.patchValue({
      id: payPhone.id,
      city: payPhone.city,
      phoneNumber: payPhone.phoneNumber,
    });
  }

  protected createFromForm(): IPayPhone {
    return {
      ...new PayPhone(),
      id: this.editForm.get(['id'])!.value,
      city: this.editForm.get(['city'])!.value,
      phoneNumber: this.editForm.get(['phoneNumber'])!.value,
    };
  }
}
