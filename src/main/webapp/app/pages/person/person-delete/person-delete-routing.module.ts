import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { PersonDeleteMainComponent } from './person-delete-main/person-delete-main.component';
import { PersonDeletionComponent } from './person-deletion/person-deletion.component';
import { PersonDeletionDemandsComponent } from './person-deletion-demands/person-deletion-demands.component';
import { PersonDeletionConfirmationComponent } from './person-deletion-confirmation/person-deletion-confirmation.component';

const routes: Routes = [
  {
    path: '',
    component: PersonDeleteMainComponent,
    children: [
      {
        path: '',
        component: PersonDeletionComponent,
      },
      {
        path: 'demands',
        component: PersonDeletionDemandsComponent,
      },
      {
        path: 'confirmation',
        component: PersonDeletionConfirmationComponent,
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PersonDeleteRoutingModule {}
