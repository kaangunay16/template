import { HasId } from 'app/entities/form-entity.model';
import { ParamType } from './param-type.model';
import { SearchQuery } from './search-query.model';

export interface QueryBuilderConfig {
  fields: ParamType[];
  service: SearchQuery<HasId>;
  joins?: string[];
}
