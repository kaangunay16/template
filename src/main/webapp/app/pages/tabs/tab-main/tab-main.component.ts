import { ChangeDetectorRef, Component, OnDestroy, OnInit } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
import { Subscription } from 'rxjs';

import { ITab } from 'app/pages/tabs/models/tab.model';
import { TabsService } from '../tabs.service';

@Component({
  selector: 'jhi-tab-main',
  templateUrl: './tab-main.component.html',
  styleUrls: ['./tab-main.component.scss'],
})
export class TabMainComponent implements OnInit, OnDestroy {
  tabs!: ITab[];
  selectedTab!: number;
  tabSub!: Subscription;
  routerSub?: Subscription;

  constructor(private tabsService: TabsService, private changeDetector: ChangeDetectorRef, private router: Router) {}

  ngOnInit(): void {
    this.tabSub = this.tabsService.tabSub.subscribe(tabs => {
      this.tabs = tabs;
      this.selectedTab = this.tabs.findIndex(tab => tab.active);

      this.changeDetector.detectChanges();
    });

    this.routerSub = this.router.events.subscribe(evt => {
      if (evt instanceof NavigationEnd) {
        this.tabsService.removeAll();
      }
    });
  }

  ngOnDestroy(): void {
    this.tabSub.unsubscribe();
    this.routerSub?.unsubscribe();
  }

  closeTab(index: number): void {
    this.tabsService.removeTab(index);
  }
}
