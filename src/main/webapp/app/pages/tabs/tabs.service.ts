import { forwardRef, Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

import { TabsModule } from './tabs.module';
import { ITab } from './models/tab.model';
import { Tab1Component } from './tab1/tab1.component';

@Injectable({
  providedIn: forwardRef(() => TabsModule),
})
export class TabsService {
  public tabs: ITab[] = [{ id: 1, active: true, title: 'test', tabData: { tabString: 's' }, component: Tab1Component }];
  public tabSub = new BehaviorSubject<ITab[]>(this.tabs);

  public removeTab(index: number): void {
    const activeTabIndex = this.tabs.indexOf(this.tabs.find(tab => tab.active)!);

    this.tabs.splice(index, 1);

    if (index === activeTabIndex) {
      this.tabs[index - 1].active = true;
    } else if (index > activeTabIndex) {
      this.tabs[activeTabIndex].active = true;
    } else {
      this.tabs[index].active = true;
    }

    this.tabSub.next(this.tabs);
  }

  public removeAll(): void {
    if (this.tabs.length > 1) {
      this.tabs = [this.tabs.find(tab => tab.id === 1)!];
    }
  }

  public addNewTab(tab: ITab): void {
    this.tabs.forEach(t => (t.active = false));
    tab.id = this.tabs.length + 1;
    tab.active = true;
    this.tabs.push(tab);
    this.tabSub.next(this.tabs);
  }
}
