import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';

import { IMeeting, Meeting } from '../meeting.model';
import { MeetingService } from '../service/meeting.service';

@Component({
  selector: 'jhi-meeting-update',
  templateUrl: './meeting-update.component.html',
})
export class MeetingUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [],
    city: [],
    totalPlaces: [],
    date: [],
    subject: [],
    unit: [],
    totalAttendee: [],
    attendee: [],
    type: [],
    finalized: [],
  });

  constructor(protected meetingService: MeetingService, protected activatedRoute: ActivatedRoute, protected fb: FormBuilder) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ meeting }) => {
      this.updateForm(meeting);
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const meeting = this.createFromForm();
    if (meeting.id !== undefined) {
      this.subscribeToSaveResponse(this.meetingService.update(meeting));
    } else {
      this.subscribeToSaveResponse(this.meetingService.create(meeting));
    }
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IMeeting>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(meeting: IMeeting): void {
    this.editForm.patchValue({
      id: meeting.id,
      city: meeting.city,
      totalPlaces: meeting.totalPlaces,
      date: meeting.date,
      subject: meeting.subject,
      unit: meeting.unit,
      totalAttendee: meeting.totalAttendee,
      attendee: meeting.attendee,
      type: meeting.type,
      finalized: meeting.finalized,
    });
  }

  protected createFromForm(): IMeeting {
    return {
      ...new Meeting(),
      id: this.editForm.get(['id'])!.value,
      city: this.editForm.get(['city'])!.value,
      totalPlaces: this.editForm.get(['totalPlaces'])!.value,
      date: this.editForm.get(['date'])!.value,
      subject: this.editForm.get(['subject'])!.value,
      unit: this.editForm.get(['unit'])!.value,
      totalAttendee: this.editForm.get(['totalAttendee'])!.value,
      attendee: this.editForm.get(['attendee'])!.value,
      type: this.editForm.get(['type'])!.value,
      finalized: this.editForm.get(['finalized'])!.value,
    };
  }
}
