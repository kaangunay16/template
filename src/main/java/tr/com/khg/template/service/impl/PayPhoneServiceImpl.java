package tr.com.khg.template.service.impl;

import java.util.List;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tr.com.khg.template.domain.PayPhone;
import tr.com.khg.template.repository.PayPhoneRepository;
import tr.com.khg.template.service.PayPhoneService;

/**
 * Service Implementation for managing {@link PayPhone}.
 */
@Service
@Transactional
public class PayPhoneServiceImpl implements PayPhoneService {

    private final Logger log = LoggerFactory.getLogger(PayPhoneServiceImpl.class);

    private final PayPhoneRepository payPhoneRepository;

    public PayPhoneServiceImpl(PayPhoneRepository payPhoneRepository) {
        this.payPhoneRepository = payPhoneRepository;
    }

    @Override
    public PayPhone save(PayPhone payPhone) {
        log.debug("Request to save PayPhone : {}", payPhone);
        return payPhoneRepository.save(payPhone);
    }

    @Override
    public Optional<PayPhone> partialUpdate(PayPhone payPhone) {
        log.debug("Request to partially update PayPhone : {}", payPhone);

        return payPhoneRepository
            .findById(payPhone.getId())
            .map(existingPayPhone -> {
                if (payPhone.getCity() != null) {
                    existingPayPhone.setCity(payPhone.getCity());
                }
                if (payPhone.getPhoneNumber() != null) {
                    existingPayPhone.setPhoneNumber(payPhone.getPhoneNumber());
                }

                return existingPayPhone;
            })
            .map(payPhoneRepository::save);
    }

    @Override
    @Transactional(readOnly = true)
    public List<PayPhone> findAll() {
        log.debug("Request to get all PayPhones");
        return payPhoneRepository.findAll();
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<PayPhone> findOne(Long id) {
        log.debug("Request to get PayPhone : {}", id);
        return payPhoneRepository.findById(id);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete PayPhone : {}", id);
        payPhoneRepository.deleteById(id);
    }
}
