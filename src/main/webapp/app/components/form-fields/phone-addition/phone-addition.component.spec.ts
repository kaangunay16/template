import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PhoneAdditionComponent } from './phone-addition.component';

describe('PhoneAdditionComponent', () => {
  let component: PhoneAdditionComponent;
  let fixture: ComponentFixture<PhoneAdditionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [PhoneAdditionComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PhoneAdditionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
