import { Injectable } from '@angular/core';

import { DynamicFormService } from './dynamic-form.service';
import { IDynamicFormCreatorModel } from './dynamic-form-field.model';
import { DynamicReturnType } from '../models/dynamic-return-type.model';

@Injectable({
  providedIn: 'root',
})
export class FormValueConverterService {
  constructor(private dynamicFormService: DynamicFormService) {}

  getFormValue(formCreator: IDynamicFormCreatorModel, fieldId: string): DynamicReturnType {
    const control = this.dynamicFormService.getMultiFieldsControls(formCreator, fieldId);
    const field = formCreator.fields.find(f => f.id.startsWith(fieldId));

    if (!field || control.length < 1) {
      return null;
    }

    return this.dynamicFormService.getFormValue(formCreator, field);
  }
}
