import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AbroadActivityFieldComponent } from './abroad-activity-field.component';

describe('AbroadActivityFieldComponent', () => {
  let component: AbroadActivityFieldComponent;
  let fixture: ComponentFixture<AbroadActivityFieldComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [AbroadActivityFieldComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AbroadActivityFieldComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
