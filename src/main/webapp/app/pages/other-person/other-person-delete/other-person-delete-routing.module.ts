import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { OtherPersonDeleteMainComponent } from './other-person-delete-main/other-person-delete-main.component';
import { OtherPersonDeletionComponent } from './other-person-deletion/other-person-deletion.component';
import { OtherPersonDeletionDemandsComponent } from './other-person-deletion-demands/other-person-deletion-demands.component';
import { OtherPersonDeletionConfirmationComponent } from './other-person-deletion-confirmation/other-person-deletion-confirmation.component';

const routes: Routes = [
  {
    path: '',
    component: OtherPersonDeleteMainComponent,
    children: [
      {
        path: '',
        component: OtherPersonDeletionComponent,
      },
      {
        path: 'demands',
        component: OtherPersonDeletionDemandsComponent,
      },
      {
        path: 'confirmation',
        component: OtherPersonDeletionConfirmationComponent,
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class OtherPersonDeleteRoutingModule {}
