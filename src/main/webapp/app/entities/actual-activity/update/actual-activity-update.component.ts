import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import { IActualActivity, ActualActivity } from '../actual-activity.model';
import { ActualActivityService } from '../service/actual-activity.service';
import { IPerson } from 'app/entities/person/person.model';
import { PersonService } from 'app/entities/person/service/person.service';

@Component({
  selector: 'jhi-actual-activity-update',
  templateUrl: './actual-activity-update.component.html',
})
export class ActualActivityUpdateComponent implements OnInit {
  isSaving = false;

  peopleSharedCollection: IPerson[] = [];

  editForm = this.fb.group({
    id: [],
    city: [],
    position: [],
    startYear: [],
    deleted: [],
    field: [],
    person: [],
  });

  constructor(
    protected actualActivityService: ActualActivityService,
    protected personService: PersonService,
    protected activatedRoute: ActivatedRoute,
    protected fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ actualActivity }) => {
      this.updateForm(actualActivity);

      this.loadRelationshipsOptions();
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const actualActivity = this.createFromForm();
    if (actualActivity.id !== undefined) {
      this.subscribeToSaveResponse(this.actualActivityService.update(actualActivity));
    } else {
      this.subscribeToSaveResponse(this.actualActivityService.create(actualActivity));
    }
  }

  trackPersonById(index: number, item: IPerson): number {
    return item.id!;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IActualActivity>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(actualActivity: IActualActivity): void {
    this.editForm.patchValue({
      id: actualActivity.id,
      city: actualActivity.city,
      position: actualActivity.position,
      startYear: actualActivity.startYear,
      deleted: actualActivity.deleted,
      field: actualActivity.field,
      person: actualActivity.person,
    });

    this.peopleSharedCollection = this.personService.addPersonToCollectionIfMissing(this.peopleSharedCollection, actualActivity.person);
  }

  protected loadRelationshipsOptions(): void {
    this.personService
      .query()
      .pipe(map((res: HttpResponse<IPerson[]>) => res.body ?? []))
      .pipe(map((people: IPerson[]) => this.personService.addPersonToCollectionIfMissing(people, this.editForm.get('person')!.value)))
      .subscribe((people: IPerson[]) => (this.peopleSharedCollection = people));
  }

  protected createFromForm(): IActualActivity {
    return {
      ...new ActualActivity(),
      id: this.editForm.get(['id'])!.value,
      city: this.editForm.get(['city'])!.value,
      position: this.editForm.get(['position'])!.value,
      startYear: this.editForm.get(['startYear'])!.value,
      deleted: this.editForm.get(['deleted'])!.value,
      field: this.editForm.get(['field'])!.value,
      person: this.editForm.get(['person'])!.value,
    };
  }
}
