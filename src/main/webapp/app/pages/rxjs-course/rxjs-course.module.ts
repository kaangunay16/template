import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedModule } from 'app/shared/shared.module';
import { ComponentsModule } from 'app/components/components.module';
import { RxjsCourseRoutingModule } from './rxjs-course-routing.module';
import { RxjsCourseMainComponent } from './rxjs-course-main/rxjs-course-main.component';
import { RxjsCourse4Component } from './rxjs-course4/rxjs-course4.component';
import { RxjsCourse5Component } from './rxjs-course5/rxjs-course5.component';

@NgModule({
  declarations: [RxjsCourseMainComponent, RxjsCourse4Component, RxjsCourse5Component],
  imports: [CommonModule, SharedModule, ComponentsModule, RxjsCourseRoutingModule],
})
export class RxjsCourseModule {}
