package tr.com.khg.template.service.impl;

import java.util.List;
import java.util.Optional;
import javax.persistence.EntityManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tr.com.khg.template.domain.Identity;
import tr.com.khg.template.domain.Person;
import tr.com.khg.template.domain.request.Parameter;
import tr.com.khg.template.domain.request.Query;
import tr.com.khg.template.repository.ActualActivityRepository;
import tr.com.khg.template.repository.IdentityRepository;
import tr.com.khg.template.repository.PersonRepository;
import tr.com.khg.template.service.PersonService;
import tr.com.khg.template.service.exceptions.AlreadyHasSameIdentity;
import tr.com.khg.template.service.utils.DynamicQueryBuilder;

/**
 * Service Implementation for managing {@link Person}.
 */
@Service
@Transactional
public class PersonServiceImpl implements PersonService {

    private final Logger log = LoggerFactory.getLogger(PersonServiceImpl.class);

    private final PersonRepository personRepository;

    private final ActualActivityRepository actualActivityRepository;

    private final IdentityRepository identityRepository;

    private final EntityManager entityManager;

    public PersonServiceImpl(
        PersonRepository personRepository,
        ActualActivityRepository actualActivityRepository,
        IdentityRepository identityRepository,
        EntityManager entityManager
    ) {
        this.personRepository = personRepository;
        this.actualActivityRepository = actualActivityRepository;
        this.identityRepository = identityRepository;
        this.entityManager = entityManager;
    }

    @Override
    @Transactional
    public Person save(Person person) {
        log.debug("Request to save Person : {}", person);

        if (person.getId() == null) {
            Optional<Person> prs = personRepository.findByIdentityIdentityNo(person.getIdentity().getIdentityNo());
            prs.ifPresent(p -> {
                throw new AlreadyHasSameIdentity(p.getIdentity().getIdentityNo());
            });
        }

        Optional<Identity> identity = identityRepository.findByIdentityNo(person.getIdentity().getIdentityNo());
        identity.ifPresent(person::setIdentity);

        if (person.getPhones() != null) person.getPhones().forEach(phone -> phone.setPerson(person));
        if (person.getOtherNames() != null) person.getOtherNames().forEach(otherName -> otherName.setPerson(person));
        if (person.getActualActivities() != null) person.getActualActivities().forEach(actualActivity -> actualActivity.setPerson(person));
        if (person.getPastActivities() != null) person.getPastActivities().forEach(pastActivity -> pastActivity.setPerson(person));
        if (person.getAbroadActivities() != null) person.getAbroadActivities().forEach(abroadActivity -> abroadActivity.setPerson(person));
        return personRepository.save(person);
    }

    @Override
    public Optional<Person> partialUpdate(Person person) {
        log.debug("Request to partially update Person : {}", person);

        return personRepository
            .findById(person.getId())
            .map(existingPerson -> {
                if (person.getDescription() != null) {
                    existingPerson.setDescription(person.getDescription());
                }
                if (person.getPosition() != null) {
                    existingPerson.setPosition(person.getPosition());
                }
                if (person.getSection() != null) {
                    existingPerson.setSection(person.getSection());
                }
                if (person.getCity() != null) {
                    existingPerson.setCity(person.getCity());
                }
                if (person.getDistrict() != null) {
                    existingPerson.setDistrict(person.getDistrict());
                }
                if (person.getInstitution() != null) {
                    existingPerson.setInstitution(person.getInstitution());
                }
                if (person.getOrganization() != null) {
                    existingPerson.setOrganization(person.getOrganization());
                }
                if (person.getDeleted() != null) {
                    existingPerson.setDeleted(person.getDeleted());
                }
                if (person.getStatus() != null) {
                    existingPerson.setStatus(person.getStatus());
                }

                return existingPerson;
            })
            .map(personRepository::save);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Person> findAll() {
        log.debug("Request to get all People");
        return personRepository.findAll();
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<Person> findOne(Long id) {
        log.debug("Request to get Person : {}", id);
        return personRepository.findById(id);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete Person : {}", id);
        personRepository.deleteById(id);
    }

    @Override
    public Optional<Person> findByIdentityNumber(String identityNo) {
        return personRepository.findByIdentityNoFetchAllRelations(identityNo);
    }

    @Override
    @Transactional
    public Person convertActualToPastActivity(Long actualActivityId, Person person) {
        actualActivityRepository.deleteById(actualActivityId);
        return this.save(person);
    }

    @Override
    public List<Person> filterPerson(Query query) {
        log.debug("Request to filter person");
        DynamicQueryBuilder<Person> q = new DynamicQueryBuilder<>(entityManager, Person.class);
        return q.createQuery(query);
    }
}
