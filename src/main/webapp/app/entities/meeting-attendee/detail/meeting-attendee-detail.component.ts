import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IMeetingAttendee } from '../meeting-attendee.model';

@Component({
  selector: 'jhi-meeting-attendee-detail',
  templateUrl: './meeting-attendee-detail.component.html',
})
export class MeetingAttendeeDetailComponent implements OnInit {
  meetingAttendee: IMeetingAttendee | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ meetingAttendee }) => {
      this.meetingAttendee = meetingAttendee;
    });
  }

  previousState(): void {
    window.history.back();
  }
}
