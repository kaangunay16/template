import { TestBed } from '@angular/core/testing';

import { FormValueConverterService } from './form-value-converter.service';

describe('FormValueConverterService', () => {
  let service: FormValueConverterService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(FormValueConverterService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
