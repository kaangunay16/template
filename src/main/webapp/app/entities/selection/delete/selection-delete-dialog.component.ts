import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { ISelection } from '../selection.model';
import { SelectionService } from '../service/selection.service';

@Component({
  templateUrl: './selection-delete-dialog.component.html',
})
export class SelectionDeleteDialogComponent {
  selection?: ISelection;

  constructor(protected selectionService: SelectionService, protected activeModal: NgbActiveModal) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.selectionService.delete(id).subscribe(() => {
      this.activeModal.close('deleted');
    });
  }
}
