package tr.com.khg.template.service;

import java.util.List;
import java.util.Optional;
import tr.com.khg.template.domain.OtherPersonRecord;

/**
 * Service Interface for managing {@link OtherPersonRecord}.
 */
public interface OtherPersonRecordService {
    /**
     * Save a otherPersonRecord.
     *
     * @param otherPersonRecord the entity to save.
     * @return the persisted entity.
     */
    OtherPersonRecord save(OtherPersonRecord otherPersonRecord);

    /**
     * Partially updates a otherPersonRecord.
     *
     * @param otherPersonRecord the entity to update partially.
     * @return the persisted entity.
     */
    Optional<OtherPersonRecord> partialUpdate(OtherPersonRecord otherPersonRecord);

    /**
     * Get all the otherPersonRecords.
     *
     * @return the list of entities.
     */
    List<OtherPersonRecord> findAll();

    /**
     * Get the "id" otherPersonRecord.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<OtherPersonRecord> findOne(Long id);

    /**
     * Delete the "id" otherPersonRecord.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);

    /**
     * Update all records
     *
     * @param records the records to be updated
     * @return updated records
     */
    List<OtherPersonRecord> updateAll(List<OtherPersonRecord> records);
}
