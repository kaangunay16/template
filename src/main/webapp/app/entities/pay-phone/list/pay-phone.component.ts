import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IPayPhone } from '../pay-phone.model';
import { PayPhoneService } from '../service/pay-phone.service';
import { PayPhoneDeleteDialogComponent } from '../delete/pay-phone-delete-dialog.component';

@Component({
  selector: 'jhi-pay-phone',
  templateUrl: './pay-phone.component.html',
})
export class PayPhoneComponent implements OnInit {
  payPhones?: IPayPhone[];
  isLoading = false;

  constructor(protected payPhoneService: PayPhoneService, protected modalService: NgbModal) {}

  loadAll(): void {
    this.isLoading = true;

    this.payPhoneService.query().subscribe(
      (res: HttpResponse<IPayPhone[]>) => {
        this.isLoading = false;
        this.payPhones = res.body ?? [];
      },
      () => {
        this.isLoading = false;
      }
    );
  }

  ngOnInit(): void {
    this.loadAll();
  }

  trackId(index: number, item: IPayPhone): number {
    return item.id!;
  }

  delete(payPhone: IPayPhone): void {
    const modalRef = this.modalService.open(PayPhoneDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.payPhone = payPhone;
    // unsubscribe not needed because closed completes on modal close
    modalRef.closed.subscribe(reason => {
      if (reason === 'deleted') {
        this.loadAll();
      }
    });
  }
}
