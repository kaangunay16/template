import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import { IPerson, Person } from '../person.model';
import { PersonService } from '../service/person.service';
import { IIdentity } from 'app/entities/identity/identity.model';
import { IdentityService } from 'app/entities/identity/service/identity.service';

@Component({
  selector: 'jhi-person-update',
  templateUrl: './person-update.component.html',
})
export class PersonUpdateComponent implements OnInit {
  isSaving = false;

  identitiesCollection: IIdentity[] = [];

  editForm = this.fb.group({
    id: [],
    description: [],
    position: [],
    section: [],
    city: [],
    district: [],
    institution: [],
    organization: [],
    deleted: [],
    status: [],
    identity: [],
  });

  constructor(
    protected personService: PersonService,
    protected identityService: IdentityService,
    protected activatedRoute: ActivatedRoute,
    protected fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ person }) => {
      this.updateForm(person);

      this.loadRelationshipsOptions();
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const person = this.createFromForm();
    if (person.id !== undefined) {
      this.subscribeToSaveResponse(this.personService.update(person));
    } else {
      this.subscribeToSaveResponse(this.personService.create(person));
    }
  }

  trackIdentityById(index: number, item: IIdentity): number {
    return item.id!;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IPerson>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(person: IPerson): void {
    this.editForm.patchValue({
      id: person.id,
      description: person.description,
      position: person.position,
      section: person.section,
      city: person.city,
      district: person.district,
      institution: person.institution,
      organization: person.organization,
      deleted: person.deleted,
      status: person.status,
      identity: person.identity,
    });

    this.identitiesCollection = this.identityService.addIdentityToCollectionIfMissing(this.identitiesCollection, person.identity);
  }

  protected loadRelationshipsOptions(): void {
    this.identityService
      .query({ filter: 'person-is-null' })
      .pipe(map((res: HttpResponse<IIdentity[]>) => res.body ?? []))
      .pipe(
        map((identities: IIdentity[]) =>
          this.identityService.addIdentityToCollectionIfMissing(identities, this.editForm.get('identity')!.value)
        )
      )
      .subscribe((identities: IIdentity[]) => (this.identitiesCollection = identities));
  }

  protected createFromForm(): IPerson {
    return {
      ...new Person(),
      id: this.editForm.get(['id'])!.value,
      description: this.editForm.get(['description'])!.value,
      position: this.editForm.get(['position'])!.value,
      section: this.editForm.get(['section'])!.value,
      city: this.editForm.get(['city'])!.value,
      district: this.editForm.get(['district'])!.value,
      institution: this.editForm.get(['institution'])!.value,
      organization: this.editForm.get(['organization'])!.value,
      deleted: this.editForm.get(['deleted'])!.value,
      status: this.editForm.get(['status'])!.value,
      identity: this.editForm.get(['identity'])!.value,
    };
  }
}
