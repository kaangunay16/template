import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NgrxDataEditPostComponent } from './ngrx-data-edit-post.component';

describe('NgrxDataEditPostComponent', () => {
  let component: NgrxDataEditPostComponent;
  let fixture: ComponentFixture<NgrxDataEditPostComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [NgrxDataEditPostComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NgrxDataEditPostComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
