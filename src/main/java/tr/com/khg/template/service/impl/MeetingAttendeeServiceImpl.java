package tr.com.khg.template.service.impl;

import java.util.List;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tr.com.khg.template.domain.MeetingAttendee;
import tr.com.khg.template.repository.MeetingAttendeeRepository;
import tr.com.khg.template.service.MeetingAttendeeService;

/**
 * Service Implementation for managing {@link MeetingAttendee}.
 */
@Service
@Transactional
public class MeetingAttendeeServiceImpl implements MeetingAttendeeService {

    private final Logger log = LoggerFactory.getLogger(MeetingAttendeeServiceImpl.class);

    private final MeetingAttendeeRepository meetingAttendeeRepository;

    public MeetingAttendeeServiceImpl(MeetingAttendeeRepository meetingAttendeeRepository) {
        this.meetingAttendeeRepository = meetingAttendeeRepository;
    }

    @Override
    public MeetingAttendee save(MeetingAttendee meetingAttendee) {
        log.debug("Request to save MeetingAttendee : {}", meetingAttendee);
        return meetingAttendeeRepository.save(meetingAttendee);
    }

    @Override
    public Optional<MeetingAttendee> partialUpdate(MeetingAttendee meetingAttendee) {
        log.debug("Request to partially update MeetingAttendee : {}", meetingAttendee);

        return meetingAttendeeRepository
            .findById(meetingAttendee.getId())
            .map(existingMeetingAttendee -> {
                if (meetingAttendee.getStatus() != null) {
                    existingMeetingAttendee.setStatus(meetingAttendee.getStatus());
                }

                return existingMeetingAttendee;
            })
            .map(meetingAttendeeRepository::save);
    }

    @Override
    @Transactional(readOnly = true)
    public List<MeetingAttendee> findAll() {
        log.debug("Request to get all MeetingAttendees");
        return meetingAttendeeRepository.findAll();
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<MeetingAttendee> findOne(Long id) {
        log.debug("Request to get MeetingAttendee : {}", id);
        return meetingAttendeeRepository.findById(id);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete MeetingAttendee : {}", id);
        meetingAttendeeRepository
            .findById(id)
            .ifPresent(meetingAttendee -> {
                meetingAttendee.setDeleted(true);
                meetingAttendeeRepository.save(meetingAttendee);
            });
    }
}
