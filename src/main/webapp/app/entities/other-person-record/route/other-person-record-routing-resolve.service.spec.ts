jest.mock('@angular/router');

import { TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ActivatedRouteSnapshot, Router } from '@angular/router';
import { of } from 'rxjs';

import { IOtherPersonRecord, OtherPersonRecord } from '../other-person-record.model';
import { OtherPersonRecordService } from '../service/other-person-record.service';

import { OtherPersonRecordRoutingResolveService } from './other-person-record-routing-resolve.service';

describe('OtherPersonRecord routing resolve service', () => {
  let mockRouter: Router;
  let mockActivatedRouteSnapshot: ActivatedRouteSnapshot;
  let routingResolveService: OtherPersonRecordRoutingResolveService;
  let service: OtherPersonRecordService;
  let resultOtherPersonRecord: IOtherPersonRecord | undefined;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [Router, ActivatedRouteSnapshot],
    });
    mockRouter = TestBed.inject(Router);
    mockActivatedRouteSnapshot = TestBed.inject(ActivatedRouteSnapshot);
    routingResolveService = TestBed.inject(OtherPersonRecordRoutingResolveService);
    service = TestBed.inject(OtherPersonRecordService);
    resultOtherPersonRecord = undefined;
  });

  describe('resolve', () => {
    it('should return IOtherPersonRecord returned by find', () => {
      // GIVEN
      service.find = jest.fn(id => of(new HttpResponse({ body: { id } })));
      mockActivatedRouteSnapshot.params = { id: 123 };

      // WHEN
      routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
        resultOtherPersonRecord = result;
      });

      // THEN
      expect(service.find).toBeCalledWith(123);
      expect(resultOtherPersonRecord).toEqual({ id: 123 });
    });

    it('should return new IOtherPersonRecord if id is not provided', () => {
      // GIVEN
      service.find = jest.fn();
      mockActivatedRouteSnapshot.params = {};

      // WHEN
      routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
        resultOtherPersonRecord = result;
      });

      // THEN
      expect(service.find).not.toBeCalled();
      expect(resultOtherPersonRecord).toEqual(new OtherPersonRecord());
    });

    it('should route to 404 page if data not found in server', () => {
      // GIVEN
      jest.spyOn(service, 'find').mockReturnValue(of(new HttpResponse({ body: null as unknown as OtherPersonRecord })));
      mockActivatedRouteSnapshot.params = { id: 123 };

      // WHEN
      routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
        resultOtherPersonRecord = result;
      });

      // THEN
      expect(service.find).toBeCalledWith(123);
      expect(resultOtherPersonRecord).toEqual(undefined);
      expect(mockRouter.navigate).toHaveBeenCalledWith(['404']);
    });
  });
});
