import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { of } from 'rxjs';

import { SelectionService } from '../service/selection.service';

import { SelectionComponent } from './selection.component';

describe('Selection Management Component', () => {
  let comp: SelectionComponent;
  let fixture: ComponentFixture<SelectionComponent>;
  let service: SelectionService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      declarations: [SelectionComponent],
    })
      .overrideTemplate(SelectionComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(SelectionComponent);
    comp = fixture.componentInstance;
    service = TestBed.inject(SelectionService);

    const headers = new HttpHeaders();
    jest.spyOn(service, 'query').mockReturnValue(
      of(
        new HttpResponse({
          body: [{ id: 123 }],
          headers,
        })
      )
    );
  });

  it('Should call load all on init', () => {
    // WHEN
    comp.ngOnInit();

    // THEN
    expect(service.query).toHaveBeenCalled();
    expect(comp.selections?.[0]).toEqual(expect.objectContaining({ id: 123 }));
  });
});
