import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { IPayPhone, PayPhone } from '../pay-phone.model';

import { PayPhoneService } from './pay-phone.service';

describe('PayPhone Service', () => {
  let service: PayPhoneService;
  let httpMock: HttpTestingController;
  let elemDefault: IPayPhone;
  let expectedResult: IPayPhone | IPayPhone[] | boolean | null;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    expectedResult = null;
    service = TestBed.inject(PayPhoneService);
    httpMock = TestBed.inject(HttpTestingController);

    elemDefault = {
      id: 0,
      city: 'AAAAAAA',
      phoneNumber: 'AAAAAAA',
    };
  });

  describe('Service methods', () => {
    it('should find an element', () => {
      const returnedFromService = Object.assign({}, elemDefault);

      service.find(123).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(elemDefault);
    });

    it('should create a PayPhone', () => {
      const returnedFromService = Object.assign(
        {
          id: 0,
        },
        elemDefault
      );

      const expected = Object.assign({}, returnedFromService);

      service.create(new PayPhone()).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'POST' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should update a PayPhone', () => {
      const returnedFromService = Object.assign(
        {
          id: 1,
          city: 'BBBBBB',
          phoneNumber: 'BBBBBB',
        },
        elemDefault
      );

      const expected = Object.assign({}, returnedFromService);

      service.update(expected).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PUT' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should partial update a PayPhone', () => {
      const patchObject = Object.assign({}, new PayPhone());

      const returnedFromService = Object.assign(patchObject, elemDefault);

      const expected = Object.assign({}, returnedFromService);

      service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PATCH' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should return a list of PayPhone', () => {
      const returnedFromService = Object.assign(
        {
          id: 1,
          city: 'BBBBBB',
          phoneNumber: 'BBBBBB',
        },
        elemDefault
      );

      const expected = Object.assign({}, returnedFromService);

      service.query().subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush([returnedFromService]);
      httpMock.verify();
      expect(expectedResult).toContainEqual(expected);
    });

    it('should delete a PayPhone', () => {
      service.delete(123).subscribe(resp => (expectedResult = resp.ok));

      const req = httpMock.expectOne({ method: 'DELETE' });
      req.flush({ status: 200 });
      expect(expectedResult);
    });

    describe('addPayPhoneToCollectionIfMissing', () => {
      it('should add a PayPhone to an empty array', () => {
        const payPhone: IPayPhone = { id: 123 };
        expectedResult = service.addPayPhoneToCollectionIfMissing([], payPhone);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(payPhone);
      });

      it('should not add a PayPhone to an array that contains it', () => {
        const payPhone: IPayPhone = { id: 123 };
        const payPhoneCollection: IPayPhone[] = [
          {
            ...payPhone,
          },
          { id: 456 },
        ];
        expectedResult = service.addPayPhoneToCollectionIfMissing(payPhoneCollection, payPhone);
        expect(expectedResult).toHaveLength(2);
      });

      it("should add a PayPhone to an array that doesn't contain it", () => {
        const payPhone: IPayPhone = { id: 123 };
        const payPhoneCollection: IPayPhone[] = [{ id: 456 }];
        expectedResult = service.addPayPhoneToCollectionIfMissing(payPhoneCollection, payPhone);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(payPhone);
      });

      it('should add only unique PayPhone to an array', () => {
        const payPhoneArray: IPayPhone[] = [{ id: 123 }, { id: 456 }, { id: 99530 }];
        const payPhoneCollection: IPayPhone[] = [{ id: 123 }];
        expectedResult = service.addPayPhoneToCollectionIfMissing(payPhoneCollection, ...payPhoneArray);
        expect(expectedResult).toHaveLength(3);
      });

      it('should accept varargs', () => {
        const payPhone: IPayPhone = { id: 123 };
        const payPhone2: IPayPhone = { id: 456 };
        expectedResult = service.addPayPhoneToCollectionIfMissing([], payPhone, payPhone2);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(payPhone);
        expect(expectedResult).toContain(payPhone2);
      });

      it('should accept null and undefined values', () => {
        const payPhone: IPayPhone = { id: 123 };
        expectedResult = service.addPayPhoneToCollectionIfMissing([], null, payPhone, undefined);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(payPhone);
      });

      it('should return initial array if no PayPhone is added', () => {
        const payPhoneCollection: IPayPhone[] = [{ id: 123 }];
        expectedResult = service.addPayPhoneToCollectionIfMissing(payPhoneCollection, undefined, null);
        expect(expectedResult).toEqual(payPhoneCollection);
      });
    });
  });

  afterEach(() => {
    httpMock.verify();
  });
});
