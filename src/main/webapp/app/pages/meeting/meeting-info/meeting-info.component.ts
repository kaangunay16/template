import { AfterViewInit, ChangeDetectorRef, Component, Input, OnDestroy, OnInit } from '@angular/core';
import { Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { ToastrService } from 'ngx-toastr';
import * as dayjs from 'dayjs';

import { IMeeting, Meeting } from 'app/entities/meeting/meeting.model';
import { MeetingService } from 'app/entities/meeting/service/meeting.service';
import { FormValueConverterService } from 'app/components/dynamic-form-creator/form-value-converter.service';
import { MeetingModuleService } from 'app/pages/meeting/meeting-module.service';
import { DynamicFormCreatorModel, IDynamicFormCreatorModel } from 'app/components/dynamic-form-creator/dynamic-form-field.model';
import { ErrorSupportService } from 'app/shared/services/error-support.service';

@Component({
  selector: 'jhi-meeting-info',
  templateUrl: './meeting-info.component.html',
  styleUrls: ['./meeting-info.component.scss'],
})
export class MeetingInfoComponent implements OnInit, OnDestroy, AfterViewInit {
  meeting!: IMeeting;
  formCreator?: IDynamicFormCreatorModel;
  routeSub!: Subscription;
  meetingSub?: Subscription;
  meetingTypeSub?: Subscription;
  mode?: 'create' | 'update';
  meetingType?: string;
  readOnly = false;

  @Input()
  size?: 'full' | 'medium' = 'medium';

  @Input()
  actions?: boolean = true;

  constructor(
    private toastr: ToastrService,
    private meetingModuleService: MeetingModuleService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private meetingService: MeetingService,
    private formValueService: FormValueConverterService,
    private errorService: ErrorSupportService,
    private changeDetector: ChangeDetectorRef
  ) {}

  ngOnInit(): void {
    this.routeSub = this.activatedRoute.queryParams.subscribe(params => {
      this.mode = params['mode'];
      const id = params['id'];

      if (!this.mode && !id) {
        this.toastr.error('Lütfen link üzerinden bu sayfaya geliniz');
        this.router.navigate(['/']);
      }

      if (id) {
        this.readOnly = true;
      }

      if (this.mode === 'create') {
        this.meeting = new Meeting();
        this.meetingModuleService.addSingleEntity(this.meeting);
      } else {
        this.meeting = this.meetingModuleService.entityCheck(params);
      }

      this.formCreator = new DynamicFormCreatorModel([
        {
          id: 'type',
          label: 'Tip',
          type: 'selections',
          selectionType: 'meeting-types',
          placeholder: 'Toplantı Tipi Seçin',
          value: this.meeting.type,
          readOnly: this.readOnly,
          validators: [Validators.required],
        },
        {
          id: 'city',
          label: 'Merkez İl',
          type: 'selections',
          selectionType: 'city',
          value: this.meeting.city,
          placeholder: 'İl Seçin',
          readOnly: this.readOnly,
          validators: [Validators.required],
        },
        {
          id: 'totalPlaces',
          label: 'İl Sayısı:',
          type: 'number',
          value: this.meeting.totalPlaces,
          placeholder: 'İl Sayısı Giriniz',
          validators: [Validators.required],
        },
        {
          id: 'date',
          label: 'Tarih:',
          type: 'date',
          value: this.meeting.date?.format('YYYY-MM-DD'),
          placeholder: 'Tarih Giriniz',
          validators: [Validators.required],
        },
        {
          id: 'subject',
          label: 'Konu',
          type: 'selections',
          selectionType: 'meeting-subjects',
          value: this.meeting.subject,
          placeholder: 'Konu Seçiniz',
          validators: [Validators.required],
        },
        {
          id: 'unit',
          label: 'Birim',
          type: 'multi-selections',
          selectionType: 'unit',
          value: this.meeting.unit ? this.meeting.unit.split(',') : undefined,
          placeholder: 'Birim Seçin',
          listValue: true,
          validators: [Validators.required],
        },
        {
          id: 'totalAttendee',
          label: 'Toplam Katılımcı Sayısı:',
          type: 'number',
          value: this.meeting.totalAttendee,
          placeholder: 'Toplam Katılımcı Sayısını Giriniz',
          validators: [Validators.required],
        },
      ]);
    });
  }

  ngOnDestroy(): void {
    this.routeSub.unsubscribe();
    this.meetingSub?.unsubscribe();
    this.meetingTypeSub?.unsubscribe();
  }

  ngAfterViewInit(): void {
    this.createSubscriptionForMeetingTypeField();
    this.changeDetector.detectChanges();
  }

  createSubscriptionForMeetingTypeField(): void {
    const form = this.formCreator?.form;
    const field = form?.get('type');
    this.meetingTypeSub = field?.valueChanges.subscribe(val => {
      this.meetingType = val;
      if (val === 'MeetingType3') {
        this.formCreator?.addNewField({
          id: 'attendee',
          type: 'number',
          label: 'Katılımcı Sayısı',
          placeholder: 'Katılımcı Sayısını Giriniz',
          validators: [Validators.required],
        });
      } else {
        this.formCreator?.removeField('attendee');
      }
    });
  }

  createMeeting(): void {
    this.setFormValuesToEntity();
    this.meetingSub = this.meetingService.create(this.meeting).subscribe(
      res => {
        if (res.body) {
          this.toastr.success('Toplantı kaydı başarıyla yapıldı.');
          this.router.navigate(['/']);
        }
      },
      err => this.errorService.errorHandler(err)
    );
  }

  passToAttendeesCreation(): void {
    this.setFormValuesToEntity();

    if (!this.meeting.attendee) {
      this.toastr.error('Lütfen katılımcı sayısını giriniz.', 'Katılımcı Sayısı');
      return;
    }

    if (this.meeting.attendee > this.meeting.totalAttendee!) {
      this.toastr.error('Katılımcı sayısı toplam katılımcı sayısından fazla olamaz.', 'Yanlış Katılımcı Sayısı');
      return;
    }

    this.router.navigate(['/meeting', 'attendees']);
  }

  setFormValuesToEntity(): void {
    const units = this.formValueService.getFormValue(this.formCreator!, 'unit') as string[] | null;
    const date = this.formValueService.getFormValue(this.formCreator!, 'date') as string;

    this.meeting.type = this.formValueService.getFormValue(this.formCreator!, 'type') as string;
    this.meeting.city = this.formValueService.getFormValue(this.formCreator!, 'city') as string;
    this.meeting.totalPlaces = this.formValueService.getFormValue(this.formCreator!, 'totalPlaces') as number;
    this.meeting.date = dayjs(date);
    this.meeting.subject = this.formValueService.getFormValue(this.formCreator!, 'subject') as string;
    this.meeting.unit = units ? units.join(',') : null;
    this.meeting.totalAttendee = this.formValueService.getFormValue(this.formCreator!, 'totalAttendee') as number;
    this.meeting.finalized = false;
    this.meeting.attendee =
      this.meetingType === 'MeetingType3' ? (this.formValueService.getFormValue(this.formCreator!, 'attendee') as number) : null;
  }
}
