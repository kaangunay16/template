import { Component, Input, OnInit } from '@angular/core';
import { ControlValueAccessor, FormBuilder, FormGroup, NG_VALUE_ACCESSOR } from '@angular/forms';

import { ActualActivityService } from 'app/entities/actual-activity/service/actual-activity.service';
import { ActualActivity } from 'app/entities/actual-activity/actual-activity.model';
import { DynamicFormFunctions } from 'app/components/dynamic-form-creator/dynamic-form-field.model';
import { DYNAMIC_FIELD } from 'app/components/dynamic-form-creator/dyanmic-form.token';
import { DynamicFormService } from 'app/components/dynamic-form-creator/dynamic-form.service';
import { PersonModuleService } from 'app/pages/person/person-module.service';
import { FormFieldsService } from '../form-fields.service';

@Component({
  selector: 'jhi-actual-activity-field',
  templateUrl: './actual-activity-field.component.html',
  styleUrls: ['./actual-activity-field.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: ActualActivityFieldComponent,
      multi: true,
    },
    {
      provide: DYNAMIC_FIELD,
      useExisting: ActualActivityFieldComponent,
    },
  ],
})
export class ActualActivityFieldComponent implements OnInit, ControlValueAccessor, DynamicFormFunctions {
  form!: FormGroup;

  @Input()
  readOnly?: boolean = false;

  onTouched!: () => void;

  constructor(
    private formBuilder: FormBuilder,
    private formFieldsService: FormFieldsService,
    private service: ActualActivityService,
    private personModuleService: PersonModuleService,
    private dynamicFormService: DynamicFormService
  ) {}

  ngOnInit(): void {
    this.form = this.formBuilder.group({
      id: [{ value: null }],
      city: [{ value: null, disabled: this.readOnly }],
      startYear: [{ value: null, disabled: this.readOnly }],
      position: [{ value: null, disabled: this.readOnly }],
      deleted: [{ value: null }],
      person: [{ value: null }],
      field: [{ value: null }],
    });
  }

  registerOnChange(fn: (value: any) => void): void {
    this.form.valueChanges.subscribe(fn);
  }

  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

  writeValue(obj: ActualActivity | null): void {
    this.form.patchValue({
      id: obj ? obj.id : null,
      city: obj ? obj.city : null,
      startYear: obj ? obj.startYear : null,
      position: obj ? obj.position : null,
      deleted: obj ? obj.deleted : false,
      person: obj ? obj.person : null,
      field: obj ? obj.field : null,
    });
  }

  remove(): void {
    const id = this.form.get('id')?.value;

    if (id) {
      this.formFieldsService.remove(id, this.service);
      this.personModuleService.removeActualActivity(this.dynamicFormService.getEntityId(), id);
    }
  }

  stopActivity(): void {
    const id = this.form.get('id')?.value;

    if (!id) {
      return;
    }

    this.personModuleService.stopActualActivities(id, this.dynamicFormService.getEntityId());
  }
}
