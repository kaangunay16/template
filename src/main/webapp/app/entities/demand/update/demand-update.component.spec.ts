jest.mock('@angular/router');

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { of, Subject } from 'rxjs';

import { DemandService } from '../service/demand.service';
import { IDemand, Demand } from '../demand.model';

import { DemandUpdateComponent } from './demand-update.component';

describe('Demand Management Update Component', () => {
  let comp: DemandUpdateComponent;
  let fixture: ComponentFixture<DemandUpdateComponent>;
  let activatedRoute: ActivatedRoute;
  let demandService: DemandService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      declarations: [DemandUpdateComponent],
      providers: [FormBuilder, ActivatedRoute],
    })
      .overrideTemplate(DemandUpdateComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(DemandUpdateComponent);
    activatedRoute = TestBed.inject(ActivatedRoute);
    demandService = TestBed.inject(DemandService);

    comp = fixture.componentInstance;
  });

  describe('ngOnInit', () => {
    it('Should update editForm', () => {
      const demand: IDemand = { id: 456 };

      activatedRoute.data = of({ demand });
      comp.ngOnInit();

      expect(comp.editForm.value).toEqual(expect.objectContaining(demand));
    });
  });

  describe('save', () => {
    it('Should call update service on save for existing entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<Demand>>();
      const demand = { id: 123 };
      jest.spyOn(demandService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ demand });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: demand }));
      saveSubject.complete();

      // THEN
      expect(comp.previousState).toHaveBeenCalled();
      expect(demandService.update).toHaveBeenCalledWith(demand);
      expect(comp.isSaving).toEqual(false);
    });

    it('Should call create service on save for new entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<Demand>>();
      const demand = new Demand();
      jest.spyOn(demandService, 'create').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ demand });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: demand }));
      saveSubject.complete();

      // THEN
      expect(demandService.create).toHaveBeenCalledWith(demand);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).toHaveBeenCalled();
    });

    it('Should set isSaving to false on error', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<Demand>>();
      const demand = { id: 123 };
      jest.spyOn(demandService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ demand });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.error('This is an error!');

      // THEN
      expect(demandService.update).toHaveBeenCalledWith(demand);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).not.toHaveBeenCalled();
    });
  });
});
