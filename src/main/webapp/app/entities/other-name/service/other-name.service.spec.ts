import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { IOtherName, OtherName } from '../other-name.model';

import { OtherNameService } from './other-name.service';

describe('OtherName Service', () => {
  let service: OtherNameService;
  let httpMock: HttpTestingController;
  let elemDefault: IOtherName;
  let expectedResult: IOtherName | IOtherName[] | boolean | null;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    expectedResult = null;
    service = TestBed.inject(OtherNameService);
    httpMock = TestBed.inject(HttpTestingController);

    elemDefault = {
      id: 0,
      name: 'AAAAAAA',
    };
  });

  describe('Service methods', () => {
    it('should find an element', () => {
      const returnedFromService = Object.assign({}, elemDefault);

      service.find(123).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(elemDefault);
    });

    it('should create a OtherName', () => {
      const returnedFromService = Object.assign(
        {
          id: 0,
        },
        elemDefault
      );

      const expected = Object.assign({}, returnedFromService);

      service.create(new OtherName()).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'POST' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should update a OtherName', () => {
      const returnedFromService = Object.assign(
        {
          id: 1,
          name: 'BBBBBB',
        },
        elemDefault
      );

      const expected = Object.assign({}, returnedFromService);

      service.update(expected).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PUT' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should partial update a OtherName', () => {
      const patchObject = Object.assign({}, new OtherName());

      const returnedFromService = Object.assign(patchObject, elemDefault);

      const expected = Object.assign({}, returnedFromService);

      service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PATCH' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should return a list of OtherName', () => {
      const returnedFromService = Object.assign(
        {
          id: 1,
          name: 'BBBBBB',
        },
        elemDefault
      );

      const expected = Object.assign({}, returnedFromService);

      service.query().subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush([returnedFromService]);
      httpMock.verify();
      expect(expectedResult).toContainEqual(expected);
    });

    it('should delete a OtherName', () => {
      service.delete(123).subscribe(resp => (expectedResult = resp.ok));

      const req = httpMock.expectOne({ method: 'DELETE' });
      req.flush({ status: 200 });
      expect(expectedResult);
    });

    describe('addOtherNameToCollectionIfMissing', () => {
      it('should add a OtherName to an empty array', () => {
        const otherName: IOtherName = { id: 123 };
        expectedResult = service.addOtherNameToCollectionIfMissing([], otherName);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(otherName);
      });

      it('should not add a OtherName to an array that contains it', () => {
        const otherName: IOtherName = { id: 123 };
        const otherNameCollection: IOtherName[] = [
          {
            ...otherName,
          },
          { id: 456 },
        ];
        expectedResult = service.addOtherNameToCollectionIfMissing(otherNameCollection, otherName);
        expect(expectedResult).toHaveLength(2);
      });

      it("should add a OtherName to an array that doesn't contain it", () => {
        const otherName: IOtherName = { id: 123 };
        const otherNameCollection: IOtherName[] = [{ id: 456 }];
        expectedResult = service.addOtherNameToCollectionIfMissing(otherNameCollection, otherName);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(otherName);
      });

      it('should add only unique OtherName to an array', () => {
        const otherNameArray: IOtherName[] = [{ id: 123 }, { id: 456 }, { id: 47682 }];
        const otherNameCollection: IOtherName[] = [{ id: 123 }];
        expectedResult = service.addOtherNameToCollectionIfMissing(otherNameCollection, ...otherNameArray);
        expect(expectedResult).toHaveLength(3);
      });

      it('should accept varargs', () => {
        const otherName: IOtherName = { id: 123 };
        const otherName2: IOtherName = { id: 456 };
        expectedResult = service.addOtherNameToCollectionIfMissing([], otherName, otherName2);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(otherName);
        expect(expectedResult).toContain(otherName2);
      });

      it('should accept null and undefined values', () => {
        const otherName: IOtherName = { id: 123 };
        expectedResult = service.addOtherNameToCollectionIfMissing([], null, otherName, undefined);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(otherName);
      });

      it('should return initial array if no OtherName is added', () => {
        const otherNameCollection: IOtherName[] = [{ id: 123 }];
        expectedResult = service.addOtherNameToCollectionIfMissing(otherNameCollection, undefined, null);
        expect(expectedResult).toEqual(otherNameCollection);
      });
    });
  });

  afterEach(() => {
    httpMock.verify();
  });
});
