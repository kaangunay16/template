package tr.com.khg.template.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import javax.persistence.*;

/**
 * A AbroadActivity.
 */
@Entity
@Table(name = "ab_activity")
public class AbroadActivity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @Column(name = "country")
    private String country;

    @Column(name = "start_year")
    private Integer startYear;

    @Column(name = "end_year")
    private Integer endYear;

    @Column(name = "position")
    private String position;

    @Column(name = "deleted")
    private Boolean deleted;

    @ManyToOne
    @JsonIgnoreProperties(
        value = { "identity", "abroadActivities", "actualActivities", "pastActivities", "otherNames", "phones" },
        allowSetters = true
    )
    private Person person;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public AbroadActivity id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCountry() {
        return this.country;
    }

    public AbroadActivity country(String country) {
        this.setCountry(country);
        return this;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public Integer getStartYear() {
        return this.startYear;
    }

    public AbroadActivity startYear(Integer startYear) {
        this.setStartYear(startYear);
        return this;
    }

    public void setStartYear(Integer startYear) {
        this.startYear = startYear;
    }

    public Integer getEndYear() {
        return this.endYear;
    }

    public AbroadActivity endYear(Integer endYear) {
        this.setEndYear(endYear);
        return this;
    }

    public void setEndYear(Integer endYear) {
        this.endYear = endYear;
    }

    public String getPosition() {
        return this.position;
    }

    public AbroadActivity position(String position) {
        this.setPosition(position);
        return this;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public Boolean getDeleted() {
        return this.deleted;
    }

    public AbroadActivity deleted(Boolean deleted) {
        this.setDeleted(deleted);
        return this;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    public Person getPerson() {
        return this.person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }

    public AbroadActivity person(Person person) {
        this.setPerson(person);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof AbroadActivity)) {
            return false;
        }
        return id != null && id.equals(((AbroadActivity) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "AbroadActivity{" +
            "id=" + getId() +
            ", country='" + getCountry() + "'" +
            ", startYear=" + getStartYear() +
            ", endYear=" + getEndYear() +
            ", position='" + getPosition() + "'" +
            ", deleted='" + getDeleted() + "'" +
            "}";
    }
}
