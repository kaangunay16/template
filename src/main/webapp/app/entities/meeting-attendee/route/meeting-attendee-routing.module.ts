import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { MeetingAttendeeComponent } from '../list/meeting-attendee.component';
import { MeetingAttendeeDetailComponent } from '../detail/meeting-attendee-detail.component';
import { MeetingAttendeeUpdateComponent } from '../update/meeting-attendee-update.component';
import { MeetingAttendeeRoutingResolveService } from './meeting-attendee-routing-resolve.service';

const meetingAttendeeRoute: Routes = [
  {
    path: '',
    component: MeetingAttendeeComponent,
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: MeetingAttendeeDetailComponent,
    resolve: {
      meetingAttendee: MeetingAttendeeRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: MeetingAttendeeUpdateComponent,
    resolve: {
      meetingAttendee: MeetingAttendeeRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: MeetingAttendeeUpdateComponent,
    resolve: {
      meetingAttendee: MeetingAttendeeRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
];

@NgModule({
  imports: [RouterModule.forChild(meetingAttendeeRoute)],
  exports: [RouterModule],
})
export class MeetingAttendeeRoutingModule {}
