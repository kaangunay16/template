import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ComponentsModule } from '../../components/components.module';
import { SharedModule } from '../../shared/shared.module';
import { PersonRoutingModule } from './person-routing.module';
import { AbroadActivityComponent } from './abroad-activity/abroad-activity.component';
import { ActualActivityComponent } from './actual-activity/actual-activity.component';
import { IdentityInfoComponent } from './identity-info/identity-info.component';
import { OrganizationalInfoComponent } from './organizational-info/organizational-info.component';
import { PastActivityComponent } from './past-activity/past-activity.component';
import { PersonMainComponent } from './person-main/person-main.component';
import { PersonGetIdentityComponent } from './person-get-identity/person-get-identity.component';
import { PersonGetPersonInfoComponent } from './person-get-person-info/person-get-person-info.component';
import { PersonResultTableComponent } from './person-result-table/person-result-table.component';

@NgModule({
  declarations: [
    AbroadActivityComponent,
    ActualActivityComponent,
    IdentityInfoComponent,
    OrganizationalInfoComponent,
    PastActivityComponent,
    PersonMainComponent,
    PersonGetIdentityComponent,
    PersonGetPersonInfoComponent,
    PersonResultTableComponent,
  ],
  imports: [CommonModule, PersonRoutingModule, ComponentsModule, SharedModule],
})
export class PersonModule {}
