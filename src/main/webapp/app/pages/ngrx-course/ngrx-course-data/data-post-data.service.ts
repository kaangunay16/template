import { forwardRef, Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { DefaultDataService, HttpUrlGenerator } from '@ngrx/data';
import { Observable } from 'rxjs';

import { NgrxCourseDataModule } from './ngrx-course-data.module';
import { ISelection } from 'app/entities/selection/selection.model';
import { SelectionService } from 'app/entities/selection/service/selection.service';
import { map } from 'rxjs/operators';
import { Update } from '@ngrx/entity';

@Injectable({
  providedIn: forwardRef(() => NgrxCourseDataModule),
})
export class DataPostDataService extends DefaultDataService<ISelection> {
  constructor(http: HttpClient, httUrlGenerator: HttpUrlGenerator, private selectionService: SelectionService) {
    super('Selection', http, httUrlGenerator);
  }

  // Custom olarak tanımlama
  add(entity: ISelection): Observable<ISelection> {
    return this.selectionService.create(entity).pipe(
      map(response => ({
        id: response.body?.id,
        key: response.body?.key,
        value: response.body?.value,
        priority: response.body?.priority,
      }))
    );
  }

  update(update: Update<ISelection>): Observable<ISelection> {
    return this.selectionService.update({ ...update.changes }).pipe(map(response => response.body!));
  }

  delete(key: number): Observable<number> {
    return this.selectionService.delete(key).pipe(map(() => key));
  }
}
