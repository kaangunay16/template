import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NgrxPostEditComponent } from './ngrx-post-edit.component';

describe('NgrxPostEditComponent', () => {
  let component: NgrxPostEditComponent;
  let fixture: ComponentFixture<NgrxPostEditComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [NgrxPostEditComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NgrxPostEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
