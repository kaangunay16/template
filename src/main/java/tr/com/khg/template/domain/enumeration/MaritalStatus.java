package tr.com.khg.template.domain.enumeration;

/**
 * The MaritalStatus enumeration.
 */
public enum MaritalStatus {
    BEKAR,
    EVLI,
    DUL,
    BOSANMIS,
}
