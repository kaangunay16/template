import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RxjsCourseMainComponent } from './rxjs-course-main.component';

describe('RxjsCourseMainComponent', () => {
  let component: RxjsCourseMainComponent;
  let fixture: ComponentFixture<RxjsCourseMainComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [RxjsCourseMainComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RxjsCourseMainComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
