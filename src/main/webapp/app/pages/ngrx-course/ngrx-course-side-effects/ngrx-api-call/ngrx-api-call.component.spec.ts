import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NgrxApiCallComponent } from './ngrx-api-call.component';

describe('NgrxApiCallComponent', () => {
  let component: NgrxApiCallComponent;
  let fixture: ComponentFixture<NgrxApiCallComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [NgrxApiCallComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NgrxApiCallComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
