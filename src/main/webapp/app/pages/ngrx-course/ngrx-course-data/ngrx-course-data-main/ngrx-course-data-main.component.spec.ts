import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NgrxCourseDataMainComponent } from './ngrx-course-data-main.component';

describe('NgrxCourseDataMainComponent', () => {
  let component: NgrxCourseDataMainComponent;
  let fixture: ComponentFixture<NgrxCourseDataMainComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [NgrxCourseDataMainComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NgrxCourseDataMainComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
