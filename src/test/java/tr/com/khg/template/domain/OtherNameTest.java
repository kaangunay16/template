package tr.com.khg.template.domain;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import tr.com.khg.template.web.rest.TestUtil;

class OtherNameTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(OtherName.class);
        OtherName otherName1 = new OtherName();
        otherName1.setId(1L);
        OtherName otherName2 = new OtherName();
        otherName2.setId(otherName1.getId());
        assertThat(otherName1).isEqualTo(otherName2);
        otherName2.setId(2L);
        assertThat(otherName1).isNotEqualTo(otherName2);
        otherName1.setId(null);
        assertThat(otherName1).isNotEqualTo(otherName2);
    }
}
