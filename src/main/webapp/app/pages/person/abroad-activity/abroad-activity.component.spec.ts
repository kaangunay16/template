import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AbroadActivityComponent } from './abroad-activity.component';

describe('AbroadActivityComponent', () => {
  let component: AbroadActivityComponent;
  let fixture: ComponentFixture<AbroadActivityComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [AbroadActivityComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AbroadActivityComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
