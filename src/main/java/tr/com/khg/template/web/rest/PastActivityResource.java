package tr.com.khg.template.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.ResponseUtil;
import tr.com.khg.template.domain.PastActivity;
import tr.com.khg.template.repository.PastActivityRepository;
import tr.com.khg.template.service.PastActivityService;
import tr.com.khg.template.web.rest.errors.BadRequestAlertException;

/**
 * REST controller for managing {@link tr.com.khg.template.domain.PastActivity}.
 */
@RestController
@RequestMapping("/api")
public class PastActivityResource {

    private final Logger log = LoggerFactory.getLogger(PastActivityResource.class);

    private static final String ENTITY_NAME = "pastActivity";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final PastActivityService pastActivityService;

    private final PastActivityRepository pastActivityRepository;

    public PastActivityResource(PastActivityService pastActivityService, PastActivityRepository pastActivityRepository) {
        this.pastActivityService = pastActivityService;
        this.pastActivityRepository = pastActivityRepository;
    }

    /**
     * {@code POST  /past-activities} : Create a new pastActivity.
     *
     * @param pastActivity the pastActivity to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new pastActivity, or with status {@code 400 (Bad Request)} if the pastActivity has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/past-activities")
    public ResponseEntity<PastActivity> createPastActivity(@RequestBody PastActivity pastActivity) throws URISyntaxException {
        log.debug("REST request to save PastActivity : {}", pastActivity);
        if (pastActivity.getId() != null) {
            throw new BadRequestAlertException("A new pastActivity cannot already have an ID", ENTITY_NAME, "idexists");
        }
        PastActivity result = pastActivityService.save(pastActivity);
        return ResponseEntity
            .created(new URI("/api/past-activities/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /past-activities/:id} : Updates an existing pastActivity.
     *
     * @param id the id of the pastActivity to save.
     * @param pastActivity the pastActivity to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated pastActivity,
     * or with status {@code 400 (Bad Request)} if the pastActivity is not valid,
     * or with status {@code 500 (Internal Server Error)} if the pastActivity couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/past-activities/{id}")
    public ResponseEntity<PastActivity> updatePastActivity(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody PastActivity pastActivity
    ) throws URISyntaxException {
        log.debug("REST request to update PastActivity : {}, {}", id, pastActivity);
        if (pastActivity.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, pastActivity.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!pastActivityRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        PastActivity result = pastActivityService.save(pastActivity);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, pastActivity.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /past-activities/:id} : Partial updates given fields of an existing pastActivity, field will ignore if it is null
     *
     * @param id the id of the pastActivity to save.
     * @param pastActivity the pastActivity to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated pastActivity,
     * or with status {@code 400 (Bad Request)} if the pastActivity is not valid,
     * or with status {@code 404 (Not Found)} if the pastActivity is not found,
     * or with status {@code 500 (Internal Server Error)} if the pastActivity couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/past-activities/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<PastActivity> partialUpdatePastActivity(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody PastActivity pastActivity
    ) throws URISyntaxException {
        log.debug("REST request to partial update PastActivity partially : {}, {}", id, pastActivity);
        if (pastActivity.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, pastActivity.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!pastActivityRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<PastActivity> result = pastActivityService.partialUpdate(pastActivity);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, pastActivity.getId().toString())
        );
    }

    /**
     * {@code GET  /past-activities} : get all the pastActivities.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of pastActivities in body.
     */
    @GetMapping("/past-activities")
    public List<PastActivity> getAllPastActivities() {
        log.debug("REST request to get all PastActivities");
        return pastActivityService.findAll();
    }

    /**
     * {@code GET  /past-activities/:id} : get the "id" pastActivity.
     *
     * @param id the id of the pastActivity to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the pastActivity, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/past-activities/{id}")
    public ResponseEntity<PastActivity> getPastActivity(@PathVariable Long id) {
        log.debug("REST request to get PastActivity : {}", id);
        Optional<PastActivity> pastActivity = pastActivityService.findOne(id);
        return ResponseUtil.wrapOrNotFound(pastActivity);
    }

    /**
     * {@code DELETE  /past-activities/:id} : delete the "id" pastActivity.
     *
     * @param id the id of the pastActivity to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/past-activities/{id}")
    public ResponseEntity<Void> deletePastActivity(@PathVariable Long id) {
        log.debug("REST request to delete PastActivity : {}", id);
        pastActivityService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }
}
