package tr.com.khg.template.domain.request;

import java.io.Serializable;

public class Parameter implements Serializable {

    private String field;
    private String fieldType;
    private String value;
    private String sqlOperation;

    public Parameter() {}

    public String getField() {
        return field;
    }

    public void setField(String field) {
        this.field = field;
    }

    public String getFieldType() {
        return fieldType;
    }

    public void setFieldType(String fieldType) {
        this.fieldType = fieldType;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getSqlOperation() {
        return sqlOperation;
    }

    public void setSqlOperation(String sqlOperation) {
        this.sqlOperation = sqlOperation;
    }
}
