package tr.com.khg.template.service.impl;

import java.util.List;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tr.com.khg.template.domain.AbroadActivity;
import tr.com.khg.template.repository.AbroadActivityRepository;
import tr.com.khg.template.service.AbroadActivityService;

/**
 * Service Implementation for managing {@link AbroadActivity}.
 */
@Service
@Transactional
public class AbroadActivityServiceImpl implements AbroadActivityService {

    private final Logger log = LoggerFactory.getLogger(AbroadActivityServiceImpl.class);

    private final AbroadActivityRepository abroadActivityRepository;

    public AbroadActivityServiceImpl(AbroadActivityRepository abroadActivityRepository) {
        this.abroadActivityRepository = abroadActivityRepository;
    }

    @Override
    public AbroadActivity save(AbroadActivity abroadActivity) {
        log.debug("Request to save AbroadActivity : {}", abroadActivity);
        return abroadActivityRepository.save(abroadActivity);
    }

    @Override
    public Optional<AbroadActivity> partialUpdate(AbroadActivity abroadActivity) {
        log.debug("Request to partially update AbroadActivity : {}", abroadActivity);

        return abroadActivityRepository
            .findById(abroadActivity.getId())
            .map(existingAbroadActivity -> {
                if (abroadActivity.getCountry() != null) {
                    existingAbroadActivity.setCountry(abroadActivity.getCountry());
                }
                if (abroadActivity.getStartYear() != null) {
                    existingAbroadActivity.setStartYear(abroadActivity.getStartYear());
                }
                if (abroadActivity.getEndYear() != null) {
                    existingAbroadActivity.setEndYear(abroadActivity.getEndYear());
                }
                if (abroadActivity.getPosition() != null) {
                    existingAbroadActivity.setPosition(abroadActivity.getPosition());
                }
                if (abroadActivity.getDeleted() != null) {
                    existingAbroadActivity.setDeleted(abroadActivity.getDeleted());
                }

                return existingAbroadActivity;
            })
            .map(abroadActivityRepository::save);
    }

    @Override
    @Transactional(readOnly = true)
    public List<AbroadActivity> findAll() {
        log.debug("Request to get all AbroadActivities");
        return abroadActivityRepository.findAll();
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<AbroadActivity> findOne(Long id) {
        log.debug("Request to get AbroadActivity : {}", id);
        return abroadActivityRepository.findById(id);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete AbroadActivity : {}", id);
        abroadActivityRepository.deleteById(id);
    }
}
