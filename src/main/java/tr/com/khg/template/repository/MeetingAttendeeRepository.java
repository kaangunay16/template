package tr.com.khg.template.repository;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;
import tr.com.khg.template.domain.MeetingAttendee;

/**
 * Spring Data SQL repository for the MeetingAttendee entity.
 */
@SuppressWarnings("unused")
@Repository
public interface MeetingAttendeeRepository extends JpaRepository<MeetingAttendee, Long> {}
