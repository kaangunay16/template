import { NgModule } from '@angular/core';
import { SharedModule } from 'app/shared/shared.module';
import { OtherNameComponent } from './list/other-name.component';
import { OtherNameDetailComponent } from './detail/other-name-detail.component';
import { OtherNameUpdateComponent } from './update/other-name-update.component';
import { OtherNameDeleteDialogComponent } from './delete/other-name-delete-dialog.component';
import { OtherNameRoutingModule } from './route/other-name-routing.module';

@NgModule({
  imports: [SharedModule, OtherNameRoutingModule],
  declarations: [OtherNameComponent, OtherNameDetailComponent, OtherNameUpdateComponent, OtherNameDeleteDialogComponent],
  entryComponents: [OtherNameDeleteDialogComponent],
})
export class OtherNameModule {}
