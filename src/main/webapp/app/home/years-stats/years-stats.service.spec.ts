import { TestBed } from '@angular/core/testing';

import { YearsStatsService } from './years-stats.service';

describe('YearsStatsService', () => {
  let service: YearsStatsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(YearsStatsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
