import * as dayjs from 'dayjs';
import { IMeetingAttendee } from 'app/entities/meeting-attendee/meeting-attendee.model';
import { HasId } from '../form-entity.model';

export interface IMeeting extends HasId {
  id?: number;
  city?: string | null;
  totalPlaces?: number | null;
  date?: dayjs.Dayjs | null;
  subject?: string | null;
  unit?: string | null;
  totalAttendee?: number | null;
  attendee?: number | null;
  type?: string | null;
  finalized?: boolean | null;
  meetingAttendees?: IMeetingAttendee[] | null;
}

export class Meeting implements IMeeting {
  constructor(
    public id?: number,
    public city?: string | null,
    public totalPlaces?: number | null,
    public date?: dayjs.Dayjs | null,
    public subject?: string | null,
    public unit?: string | null,
    public totalAttendee?: number | null,
    public attendee?: number | null,
    public type?: string | null,
    public finalized?: boolean | null,
    public meetingAttendees?: IMeetingAttendee[] | null
  ) {
    this.finalized = this.finalized ?? false;
  }
}

export function getMeetingIdentifier(meeting: IMeeting): number | undefined {
  return meeting.id;
}
