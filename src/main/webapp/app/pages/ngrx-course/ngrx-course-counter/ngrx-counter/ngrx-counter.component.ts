import { Component } from '@angular/core';

@Component({
  selector: 'jhi-ngrx-counter',
  templateUrl: './ngrx-counter.component.html',
  styleUrls: ['./ngrx-counter.component.scss'],
})
export class NgrxCounterComponent {}
