import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MeetingFinalizeComponent } from './meeting-finalize.component';

describe('MeetingFinalizeComponent', () => {
  let component: MeetingFinalizeComponent;
  let fixture: ComponentFixture<MeetingFinalizeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [MeetingFinalizeComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MeetingFinalizeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
