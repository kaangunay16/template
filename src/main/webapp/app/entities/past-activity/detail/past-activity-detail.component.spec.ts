import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { PastActivityDetailComponent } from './past-activity-detail.component';

describe('PastActivity Management Detail Component', () => {
  let comp: PastActivityDetailComponent;
  let fixture: ComponentFixture<PastActivityDetailComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [PastActivityDetailComponent],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: { data: of({ pastActivity: { id: 123 } }) },
        },
      ],
    })
      .overrideTemplate(PastActivityDetailComponent, '')
      .compileComponents();
    fixture = TestBed.createComponent(PastActivityDetailComponent);
    comp = fixture.componentInstance;
  });

  describe('OnInit', () => {
    it('Should load pastActivity on init', () => {
      // WHEN
      comp.ngOnInit();

      // THEN
      expect(comp.pastActivity).toEqual(expect.objectContaining({ id: 123 }));
    });
  });
});
