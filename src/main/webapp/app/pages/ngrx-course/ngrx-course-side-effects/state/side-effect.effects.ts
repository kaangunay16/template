import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { ROUTER_NAVIGATION, RouterNavigatedAction } from '@ngrx/router-store';
import { Actions, ofType, createEffect } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { of } from 'rxjs';
import { catchError, exhaustMap, filter, map, switchMap, tap, withLatestFrom } from 'rxjs/operators';

import { SelectionService } from 'app/entities/selection/service/selection.service';
import { dummyAction, selectionRequestFail, selectionRequestStart, selectionRequestSuccess } from './side-effect.actions';
import { AppState } from '../../store/app.state';
import { setErrorMessage, setLoadingSpinner } from '../../store/shared/shared.action';
import { getSelections } from './side-effect.selector';

@Injectable({
  providedIn: 'root',
})
export class SideEffectsPageSideEffects {
  selectionStart$ = createEffect(() =>
    this.action$.pipe(
      ofType(selectionRequestStart),
      exhaustMap(action => {
        this.store.dispatch(setLoadingSpinner({ status: false }));
        this.store.dispatch(setErrorMessage({ errorMessage: '' }));
        return this.selectionService.findByKey(action.selectionType).pipe(
          map(data => selectionRequestSuccess({ selections: data })),
          catchError(() => {
            this.store.dispatch(setLoadingSpinner({ status: false }));
            return of(setErrorMessage({ errorMessage: 'An error occurred' }));
          })
        );
      })
    )
  );

  redirect$ = createEffect(
    () =>
      this.action$.pipe(
        ofType(...[selectionRequestFail]),
        tap(() => {
          this.router.navigate(['/404']);
        })
      ),
    { dispatch: false }
  );

  getSelection$ = createEffect(() =>
    this.action$.pipe(
      ofType(ROUTER_NAVIGATION),
      filter((r: RouterNavigatedAction) => r.payload.routerState.url.startsWith('/ngrx/side-effects/side')),
      map((r: RouterNavigatedAction) => (r.payload.routerState as any)['params']['id'] as string),
      withLatestFrom(this.store.select(getSelections)),
      switchMap(([id, selections]) => {
        if (!selections?.length) {
          return this.selectionService.findByKey(id).pipe(map(data => selectionRequestSuccess({ selections: data })));
        } else {
          // Halihazırda veri elimizde varsa tekrar çekmemesi için boş bir action oluşturuldu.
          return of(dummyAction());
        }
      })
    )
  );

  constructor(
    private action$: Actions,
    private selectionService: SelectionService,
    private store: Store<AppState>,
    private router: Router
  ) {}
}
