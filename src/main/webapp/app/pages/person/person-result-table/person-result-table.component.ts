import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';

import { QueryBuilderConfig } from 'app/components/models/query-builder-config';
import { TableConfig } from 'app/components/models/table-config.model';
import { IPerson } from 'app/entities/person/person.model';
import { PersonService } from 'app/entities/person/service/person.service';
import { PersonModuleService } from 'app/pages/person/person-module.service';
import { personTableConfigs } from 'app/pages/person/person-components.config';

@Component({
  selector: 'jhi-person-result-table',
  templateUrl: './person-result-table.component.html',
  styleUrls: ['./person-result-table.component.scss'],
})
export class PersonResultTableComponent implements OnInit, OnDestroy {
  data!: IPerson[] | null;
  queryBuilderConfig!: QueryBuilderConfig;
  routeSub!: Subscription;
  mode?: 'update' | 'search';
  tableConfig!: TableConfig;

  constructor(
    private personModuleService: PersonModuleService,
    private personService: PersonService,
    private activatedRoute: ActivatedRoute,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.routeSub = this.activatedRoute.queryParams.subscribe(params => {
      this.mode = params['mode'];
      this.data = this.personModuleService.entities;

      if ((!this.mode || this.mode === 'update') && !this.data) {
        this.router.navigate(['/person', 'get-person-info']);
      }

      this.tableConfig = {
        columns: personTableConfigs.columns,
        data: this.data,
        actions: personTableConfigs.actions,
      };

      this.queryBuilderConfig = {
        service: this.personService,
        fields: personTableConfigs.searchParameters,
        joins: ['abroadActivities', 'actualActivities', 'pastActivities', 'otherNames', 'phones'],
      };
    });
  }

  ngOnDestroy(): void {
    this.routeSub.unsubscribe();
  }

  resultChanged(result: IPerson[] | null): void {
    this.personModuleService.entities = result;
    this.data = result;
    this.tableConfig.data = result;
  }
}
