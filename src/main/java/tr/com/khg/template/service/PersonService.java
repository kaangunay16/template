package tr.com.khg.template.service;

import java.util.List;
import java.util.Optional;
import tr.com.khg.template.domain.Person;
import tr.com.khg.template.domain.request.Query;

/**
 * Service Interface for managing {@link Person}.
 */
public interface PersonService {
    /**
     * Save a person.
     *
     * @param person the entity to save.
     * @return the persisted entity.
     */
    Person save(Person person);

    /**
     * Partially updates a person.
     *
     * @param person the entity to update partially.
     * @return the persisted entity.
     */
    Optional<Person> partialUpdate(Person person);

    /**
     * Get all the people.
     *
     * @return the list of entities.
     */
    List<Person> findAll();

    /**
     * Get the "id" person.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<Person> findOne(Long id);

    /**
     * Delete the "id" person.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);

    /**
     * Get person who has "identity number"
     *
     * @param identityNo identity number of the person
     * @return the person who has the identity number
     */
    Optional<Person> findByIdentityNumber(String identityNo);

    /**
     * Converts person's actual activity to past activity
     *
     * @param actualActivityId id of actual activity to be converted
     * @param person the person to be updated
     * @return Updated person
     */
    Person convertActualToPastActivity(Long actualActivityId, Person person);

    /**
     * Search persons with parameters
     *
     * @param query searching parameters
     * @return found persons
     */
    List<Person> filterPerson(Query query);
}
