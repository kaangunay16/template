import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormGroupDirective } from '@angular/forms';

import { DynamicFormFunctions, IDynamicFormFieldModel } from '../dynamic-form-field.model';
import { DYNAMIC_FIELD } from '../dyanmic-form.token';

@Component({
  selector: 'jhi-dynamic-form-field',
  templateUrl: './dynamic-form-field.component.html',
  styleUrls: ['./dynamic-form-field.component.scss'],
})
export class DynamicFormFieldComponent implements OnInit {
  @ViewChild(DYNAMIC_FIELD)
  field?: DynamicFormFunctions;

  @Input() formItem!: IDynamicFormFieldModel;

  form!: FormGroup;

  constructor(private rootFormGroup: FormGroupDirective) {}

  ngOnInit(): void {
    this.form = this.rootFormGroup.control;
  }

  remove(): void {
    this.field?.remove();
  }
}
