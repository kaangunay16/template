package tr.com.khg.template.repository;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;
import tr.com.khg.template.domain.OtherName;

/**
 * Spring Data SQL repository for the OtherName entity.
 */
@SuppressWarnings("unused")
@Repository
public interface OtherNameRepository extends JpaRepository<OtherName, Long> {}
