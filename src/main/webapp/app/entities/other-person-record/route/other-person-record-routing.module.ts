import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { OtherPersonRecordComponent } from '../list/other-person-record.component';
import { OtherPersonRecordDetailComponent } from '../detail/other-person-record-detail.component';
import { OtherPersonRecordUpdateComponent } from '../update/other-person-record-update.component';
import { OtherPersonRecordRoutingResolveService } from './other-person-record-routing-resolve.service';

const otherPersonRecordRoute: Routes = [
  {
    path: '',
    component: OtherPersonRecordComponent,
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: OtherPersonRecordDetailComponent,
    resolve: {
      otherPersonRecord: OtherPersonRecordRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: OtherPersonRecordUpdateComponent,
    resolve: {
      otherPersonRecord: OtherPersonRecordRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: OtherPersonRecordUpdateComponent,
    resolve: {
      otherPersonRecord: OtherPersonRecordRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
];

@NgModule({
  imports: [RouterModule.forChild(otherPersonRecordRoute)],
  exports: [RouterModule],
})
export class OtherPersonRecordRoutingModule {}
