import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { DataPostResolver } from './data-post.resolver';
import { NgrxCourseDataMainComponent } from './ngrx-course-data-main/ngrx-course-data-main.component';
import { NgrxDataPostListComponent } from './ngrx-data-post-list/ngrx-data-post-list.component';
import { NgrxDataAddPostComponent } from './ngrx-data-add-post/ngrx-data-add-post.component';
import { NgrxDataEditPostComponent } from './ngrx-data-edit-post/ngrx-data-edit-post.component';
import { NgrxDataSinglePostComponent } from './ngrx-data-single-post/ngrx-data-single-post.component';

const routes: Routes = [
  {
    path: '',
    component: NgrxCourseDataMainComponent,
    children: [
      {
        path: '',
        component: NgrxDataPostListComponent,
        resolve: { dataPosts: DataPostResolver },
      },
      {
        path: 'add',
        component: NgrxDataAddPostComponent,
      },
      {
        path: 'edit/:id',
        component: NgrxDataEditPostComponent,
        resolve: { dataPosts: DataPostResolver },
      },
      {
        path: 'details/:id',
        component: NgrxDataSinglePostComponent,
        resolve: { dataPosts: DataPostResolver },
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class NgrxCourseDataRoutingModule {}
