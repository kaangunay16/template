import { routerReducer } from '@ngrx/router-store';

import { SHARED_STATE_NAME } from './shared/shared.selector';
import { sharedReducer } from './shared/shared.reducer';

export const appReducer = {
  [SHARED_STATE_NAME]: sharedReducer,
  router: routerReducer,
};
