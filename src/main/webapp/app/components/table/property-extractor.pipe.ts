import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'propertyExtractor',
})
export class PropertyExtractorPipe implements PipeTransform {
  transform(value: any, property: string, parser?: (arg: any) => string): string {
    const subProperties = property.split('.');
    subProperties.forEach(p => {
      value = Object.entries(value).find(entry => entry[0] === p);
      value = value[1];
    });

    if (Array.isArray(value) && parser) {
      return parser(value);
    }

    return value ? (value as string) : '';
  }
}
