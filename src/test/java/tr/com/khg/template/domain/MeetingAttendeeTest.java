package tr.com.khg.template.domain;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import tr.com.khg.template.web.rest.TestUtil;

class MeetingAttendeeTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(MeetingAttendee.class);
        MeetingAttendee meetingAttendee1 = new MeetingAttendee();
        meetingAttendee1.setId(1L);
        MeetingAttendee meetingAttendee2 = new MeetingAttendee();
        meetingAttendee2.setId(meetingAttendee1.getId());
        assertThat(meetingAttendee1).isEqualTo(meetingAttendee2);
        meetingAttendee2.setId(2L);
        assertThat(meetingAttendee1).isNotEqualTo(meetingAttendee2);
        meetingAttendee1.setId(null);
        assertThat(meetingAttendee1).isNotEqualTo(meetingAttendee2);
    }
}
