import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ParametersWrapperComponent } from './parameters-wrapper.component';

describe('ParametersWrapperComponent', () => {
  let component: ParametersWrapperComponent;
  let fixture: ComponentFixture<ParametersWrapperComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ParametersWrapperComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ParametersWrapperComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
