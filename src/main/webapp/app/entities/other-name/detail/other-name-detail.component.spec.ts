import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { OtherNameDetailComponent } from './other-name-detail.component';

describe('OtherName Management Detail Component', () => {
  let comp: OtherNameDetailComponent;
  let fixture: ComponentFixture<OtherNameDetailComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [OtherNameDetailComponent],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: { data: of({ otherName: { id: 123 } }) },
        },
      ],
    })
      .overrideTemplate(OtherNameDetailComponent, '')
      .compileComponents();
    fixture = TestBed.createComponent(OtherNameDetailComponent);
    comp = fixture.componentInstance;
  });

  describe('OnInit', () => {
    it('Should load otherName on init', () => {
      // WHEN
      comp.ngOnInit();

      // THEN
      expect(comp.otherName).toEqual(expect.objectContaining({ id: 123 }));
    });
  });
});
