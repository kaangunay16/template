package tr.com.khg.template.domain.enumeration;

/**
 * The DemandType enumeration.
 */
public enum DemandType {
    WAITING,
    CONFIRMED,
    DENIED,
}
