import { Directive } from '@angular/core';
import { AbstractControl, AsyncValidator, NG_ASYNC_VALIDATORS, ValidationErrors } from '@angular/forms';
import { Observable, of } from 'rxjs';

@Directive({
  selector: '[jhiDocumentValidator]',
  providers: [
    {
      provide: NG_ASYNC_VALIDATORS,
      useExisting: DocumentValidatorDirective,
      multi: true,
    },
  ],
})
export class DocumentValidatorDirective implements AsyncValidator {
  validate(control: AbstractControl): Promise<ValidationErrors | null> | Observable<ValidationErrors | null> {
    const value = control.value as string | null;

    if (value && value.length === 3) {
      return of(null);
    }

    return of({ invalidDocument: true });
  }
}
