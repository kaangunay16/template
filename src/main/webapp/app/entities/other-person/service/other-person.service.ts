import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { isPresent } from 'app/core/util/operators';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { IOtherPerson, getOtherPersonIdentifier } from '../other-person.model';
import { map } from 'rxjs/operators';
import * as dayjs from 'dayjs';
import { SearchQuery } from 'app/components/models/search-query.model';
import { Query } from 'app/components/models/query.model';

export type EntityResponseType = HttpResponse<IOtherPerson>;
export type EntityArrayResponseType = HttpResponse<IOtherPerson[]>;

@Injectable({ providedIn: 'root' })
export class OtherPersonService implements SearchQuery<IOtherPerson> {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/other-people');

  constructor(protected http: HttpClient, protected applicationConfigService: ApplicationConfigService) {}

  create(otherPerson: IOtherPerson): Observable<EntityResponseType> {
    return this.http.post<IOtherPerson>(this.resourceUrl, otherPerson, { observe: 'response' });
  }

  findByIdentityNumber(identityNumber: string): Observable<EntityResponseType> {
    return this.http
      .get<IOtherPerson>(`${this.resourceUrl}/identity=${identityNumber}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  findWithRecordId(recordId: number): Observable<EntityResponseType> {
    return this.http
      .get<IOtherPerson>(`${this.resourceUrl}/record-id=${recordId}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(otherPerson: IOtherPerson): Observable<EntityResponseType> {
    return this.http
      .put<IOtherPerson>(`${this.resourceUrl}/${getOtherPersonIdentifier(otherPerson) as number}`, otherPerson, {
        observe: 'response',
      })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  partialUpdate(otherPerson: IOtherPerson): Observable<EntityResponseType> {
    return this.http.patch<IOtherPerson>(`${this.resourceUrl}/${getOtherPersonIdentifier(otherPerson) as number}`, otherPerson, {
      observe: 'response',
    });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IOtherPerson>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IOtherPerson[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  addOtherPersonToCollectionIfMissing(
    otherPersonCollection: IOtherPerson[],
    ...otherPeopleToCheck: (IOtherPerson | null | undefined)[]
  ): IOtherPerson[] {
    const otherPeople: IOtherPerson[] = otherPeopleToCheck.filter(isPresent);
    if (otherPeople.length > 0) {
      const otherPersonCollectionIdentifiers = otherPersonCollection.map(otherPersonItem => getOtherPersonIdentifier(otherPersonItem)!);
      const otherPeopleToAdd = otherPeople.filter(otherPersonItem => {
        const otherPersonIdentifier = getOtherPersonIdentifier(otherPersonItem);
        if (otherPersonIdentifier == null || otherPersonCollectionIdentifiers.includes(otherPersonIdentifier)) {
          return false;
        }
        otherPersonCollectionIdentifiers.push(otherPersonIdentifier);
        return true;
      });
      return [...otherPeopleToAdd, ...otherPersonCollection];
    }
    return otherPersonCollection;
  }

  search(query: Query): Observable<HttpResponse<IOtherPerson[]>> {
    return this.http
      .post<IOtherPerson[]>(`${this.resourceUrl}/filter`, query, { observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  checkIfExists(identityNumbers: string[], city: string): Observable<HttpResponse<string[]>> {
    return this.http.post<string[]>(`${this.resourceUrl}/check-if-exists`, { identityNumbers, city }, { observe: 'response' });
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body?.identity) {
      res.body.identity.birthday = res.body.identity.birthday ? dayjs(res.body.identity.birthday) : undefined;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((otherPerson: IOtherPerson) => {
        if (otherPerson.identity) {
          otherPerson.identity.birthday = otherPerson.identity.birthday ? dayjs(otherPerson.identity.birthday) : undefined;
        }
      });
    }
    return res;
  }
}
