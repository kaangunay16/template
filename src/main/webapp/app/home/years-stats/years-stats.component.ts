import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { Chart } from 'chart.js';

import { IStatistic } from 'app/shared/model/statistic.model';
import { generateRandomColor } from 'app/core/util/charts-utils';
import { YearsStatsService } from './years-stats.service';

@Component({
  selector: 'jhi-years-stats',
  templateUrl: './years-stats.component.html',
  styleUrls: ['./years-stats.component.scss'],
})
export class YearsStatsComponent implements OnInit {
  chart?: Chart;
  url?: string;
  labels: string[] = [];
  data: number[] = [];
  loading = true;

  constructor(private changeDetectorRef: ChangeDetectorRef, private yearsStatisticService: YearsStatsService) {}

  ngOnInit(): void {
    this.loading = true;
    this.yearsStatisticService.getResults().subscribe(res => {
      this.separateDataAndLabels(res);
      this.loading = false;
      this.chart = this.createChart();
      this.url = this.chart.toBase64Image();
    });
  }

  separateDataAndLabels(statistics: IStatistic[]): void {
    statistics.forEach(statistic => {
      this.labels.push(statistic.key);
      this.data.push(statistic.value);
    });
  }

  createChart(): Chart {
    return new Chart('cities', {
      type: 'line',
      data: {
        labels: this.labels,
        datasets: [
          {
            label: 'Yıllara Göre İstatistik',
            data: this.data,
            backgroundColor: generateRandomColor(),
          },
        ],
      },
      options: {
        animation: {
          duration: 0,
        },
        responsive: true,
        maintainAspectRatio: false,
      },
    });
  }
}
