import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EntityDataModule } from '@ngrx/data';
import { StoreModule } from '@ngrx/store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { EffectsModule } from '@ngrx/effects';
import { StoreRouterConnectingModule } from '@ngrx/router-store';
import { DEBUG_INFO_ENABLED } from 'app/app.constants';

import { appReducer } from './store/app.reducer';
import { SharedModule } from 'app/shared/shared.module';
import { NgrxCourseRoutingModule } from './ngrx-course-routing.module';
import { NgrxCourseMainComponent } from './ngrx-course-main/ngrx-course-main.component';
import { ComponentsModule } from 'app/components/components.module';
import { NgrxLoadingSpinnerComponent } from './ngrx-loading-spinner/ngrx-loading-spinner.component';
import { CustomSerializer } from './store/router/custom-serializer';
import { entityConfig } from './ngrx-course-data/store/entity-metadata';

@NgModule({
  declarations: [NgrxCourseMainComponent, NgrxLoadingSpinnerComponent],
  imports: [
    CommonModule,
    SharedModule,
    NgrxCourseRoutingModule,
    ComponentsModule,
    EffectsModule.forRoot([]),
    StoreModule.forRoot(appReducer),
    StoreDevtoolsModule.instrument({
      maxAge: 25,
      logOnly: DEBUG_INFO_ENABLED,
      autoPause: true,
    }),
    StoreRouterConnectingModule.forRoot({
      serializer: CustomSerializer,
    }),
    EntityDataModule.forRoot(entityConfig),
  ],
})
export class NgrxCourseModule {}
