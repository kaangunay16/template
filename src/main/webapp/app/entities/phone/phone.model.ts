import { IPerson } from 'app/entities/person/person.model';
import { HasId } from '../form-entity.model';

export interface IPhone extends HasId {
  id?: number;
  phoneNumber?: string | null;
  startYear?: number | null;
  endYear?: number | null;
  deleted?: boolean | null;
  type?: string | null;
  person?: IPerson | null;
}

export class Phone implements IPhone {
  constructor(
    public id?: number,
    public phoneNumber?: string | null,
    public startYear?: number | null,
    public endYear?: number | null,
    public deleted?: boolean | null,
    public type?: string | null,
    public person?: IPerson | null
  ) {
    this.deleted = this.deleted ?? false;
  }
}

export function getPhoneIdentifier(phone: IPhone): number | undefined {
  return phone.id;
}
