import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ComponentsModule } from 'app/components/components.module';
import { SharedModule } from 'app/shared/shared.module';
import { OtherPersonDeleteRoutingModule } from './other-person-delete-routing.module';
import { OtherPersonDeleteMainComponent } from './other-person-delete-main/other-person-delete-main.component';
import { OtherPersonDeletionComponent } from './other-person-deletion/other-person-deletion.component';
import { OtherPersonDeletionDemandsComponent } from './other-person-deletion-demands/other-person-deletion-demands.component';
import { OtherPersonDeletionConfirmationComponent } from './other-person-deletion-confirmation/other-person-deletion-confirmation.component';

@NgModule({
  declarations: [
    OtherPersonDeleteMainComponent,
    OtherPersonDeletionComponent,
    OtherPersonDeletionDemandsComponent,
    OtherPersonDeletionConfirmationComponent,
  ],
  imports: [CommonModule, OtherPersonDeleteRoutingModule, ComponentsModule, SharedModule],
})
export class OtherPersonDeleteModule {}
