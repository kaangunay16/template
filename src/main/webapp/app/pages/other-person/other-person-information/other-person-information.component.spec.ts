import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OtherPersonInformationComponent } from './other-person-information.component';

describe('OtherPersonInformationComponent', () => {
  let component: OtherPersonInformationComponent;
  let fixture: ComponentFixture<OtherPersonInformationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [OtherPersonInformationComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OtherPersonInformationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
