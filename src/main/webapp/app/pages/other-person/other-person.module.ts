import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ComponentsModule } from 'app/components/components.module';
import { SharedModule } from 'app/shared/shared.module';
import { OtherPersonRoutingModule } from './other-person-routing.module';
import { OtherPersonMainComponent } from './other-person-main/other-person-main.component';
import { OtherPersonGetIdentityComponent } from './other-person-get-identity/other-person-get-identity.component';
import { OtherPersonInformationComponent } from './other-person-information/other-person-information.component';
import { OtherPersonPersonInfoComponent } from './other-person-person-info/other-person-person-info.component';
import { OtherPersonResultTableComponent } from './other-person-result-table/other-person-result-table.component';

@NgModule({
  declarations: [
    OtherPersonMainComponent,
    OtherPersonGetIdentityComponent,
    OtherPersonInformationComponent,
    OtherPersonPersonInfoComponent,
    OtherPersonResultTableComponent,
  ],
  imports: [CommonModule, OtherPersonRoutingModule, ComponentsModule, SharedModule],
})
export class OtherPersonModule {}
