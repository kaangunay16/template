import { Component, Input, OnInit } from '@angular/core';
import { ControlValueAccessor, FormBuilder, FormGroup, NG_VALUE_ACCESSOR } from '@angular/forms';

import { IActivityField } from '../../models/activity-field.model';
import { CityDistrict, ICityDistrict } from '../../models/city-district.model';

@Component({
  selector: 'jhi-activity-field',
  templateUrl: './activity-field.component.html',
  styleUrls: ['./activity-field.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: ActivityFieldComponent,
      multi: true,
    },
  ],
})
export class ActivityFieldComponent implements OnInit, ControlValueAccessor {
  form!: FormGroup;

  @Input()
  readOnly?: boolean = false;

  onTouched!: () => void;

  selection: ICityDistrict = new CityDistrict();

  constructor(private formBuilder: FormBuilder) {}

  ngOnInit(): void {
    this.form = this.formBuilder.group({
      organization: [{ value: null, disabled: this.readOnly }],
      status: [{ value: null, disabled: this.readOnly }],
      city: [{ value: null, disabled: this.readOnly }],
      district: [{ value: null, disabled: this.readOnly }],
      section: [{ value: null, disabled: this.readOnly }],
      institution: [{ value: null, disabled: this.readOnly }],
    });
  }

  registerOnChange(fn: (value: any) => void): void {
    this.form.valueChanges.subscribe(fn);
  }

  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

  writeValue(obj: IActivityField | null): void {
    this.selection.city = obj?.city;
    this.form.patchValue({
      organization: obj ? obj.organization : null,
      status: obj ? obj.status : null,
      city: obj ? obj.city : null,
      district: obj ? obj.district : null,
      section: obj ? obj.section : null,
      institution: obj ? obj.institution : null,
    });
  }

  organizationSelectionChanged($event: string): void {
    this.form.patchValue({
      organization: $event,
      city: null,
      district: null,
      section: null,
      institution: null,
    });
  }

  citySelectionChanged($event: string): void {
    this.selection.city = $event;
    this.selection.district = null;
    this.form.get('district')?.setValue(null);
  }
}
