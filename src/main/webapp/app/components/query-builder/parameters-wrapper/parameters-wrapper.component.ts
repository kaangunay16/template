import { Component, Input } from '@angular/core';

import { Parameter } from '../../models/parameter.model';

@Component({
  selector: 'jhi-parameters-wrapper',
  templateUrl: './parameters-wrapper.component.html',
  styleUrls: ['./parameters-wrapper.component.scss'],
})
export class ParametersWrapperComponent {
  @Input()
  params!: Parameter[];
}
