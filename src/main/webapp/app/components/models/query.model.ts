import { Parameter } from './parameter.model';

export interface Query {
  joins?: string[];
  parameters: Parameter[];
}
