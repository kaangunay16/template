import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { IActualActivity } from '../actual-activity.model';
import { ActualActivityService } from '../service/actual-activity.service';

@Component({
  templateUrl: './actual-activity-delete-dialog.component.html',
})
export class ActualActivityDeleteDialogComponent {
  actualActivity?: IActualActivity;

  constructor(protected actualActivityService: ActualActivityService, protected activeModal: NgbActiveModal) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.actualActivityService.delete(id).subscribe(() => {
      this.activeModal.close('deleted');
    });
  }
}
