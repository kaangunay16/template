import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

import { IMeeting } from 'app/entities/meeting/meeting.model';
import { PageService } from 'app/shared/services/page-service';
import { IMeetingAttendee, MeetingAttendee } from 'app/entities/meeting-attendee/meeting-attendee.model';
import { AttendeeField } from 'app/components/models/attendee-field.model';

@Injectable({
  providedIn: 'root',
})
export class MeetingModuleService extends PageService<IMeeting> {
  private _toRemoveAttendees?: IMeetingAttendee[];

  constructor(toastr: ToastrService, router: Router) {
    super(toastr, router);
  }

  get toRemoveAttendees(): IMeetingAttendee[] | undefined {
    return this._toRemoveAttendees;
  }

  addToRemoveAttendees(attendee: IMeetingAttendee): void {
    if (!this._toRemoveAttendees) {
      this._toRemoveAttendees = [];
    }

    this._toRemoveAttendees.push(attendee);
  }

  addAttendeeFieldToRemoveAttendees(attendee: AttendeeField): void {
    this.addToRemoveAttendees(this.convertAttendeeField(attendee));
  }

  clearToRemoveAttendees(): void {
    this._toRemoveAttendees = undefined;
  }

  convertAttendeeField(field: AttendeeField): IMeetingAttendee {
    const attendee = new MeetingAttendee();
    attendee.id = field.id;
    attendee.identity = field.identity;
    attendee.status = field.status;
    return attendee;
  }

  convertMeetingAttendee(attendee: IMeetingAttendee): AttendeeField {
    return {
      id: attendee.id,
      identityNumber: attendee.identity!.identityNo!,
      nameAndSurname: `${attendee.identity!.name!} ${attendee.identity!.surname!}`,
      status: attendee.status!,
      identity: attendee.identity!,
    };
  }
}
