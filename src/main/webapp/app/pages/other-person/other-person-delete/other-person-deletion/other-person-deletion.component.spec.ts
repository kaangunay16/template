import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OtherPersonDeletionComponent } from './other-person-deletion.component';

describe('OtherPersonDeletionComponent', () => {
  let component: OtherPersonDeletionComponent;
  let fixture: ComponentFixture<OtherPersonDeletionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [OtherPersonDeletionComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OtherPersonDeletionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
