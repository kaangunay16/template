package tr.com.khg.template.repository;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;
import tr.com.khg.template.domain.OtherPersonRecord;

/**
 * Spring Data SQL repository for the OtherPersonRecord entity.
 */
@SuppressWarnings("unused")
@Repository
public interface OtherPersonRecordRepository extends JpaRepository<OtherPersonRecord, Long> {}
