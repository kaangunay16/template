import { createEntityAdapter, EntityState } from '@ngrx/entity';

import { Post } from '../models/post.model';

export interface PostsState extends EntityState<Post> {
  count: number;
}

export const postAdapter = createEntityAdapter<Post>({
  // selectId: (post) => post.id! // Farklı primary key seçilmek istenirse
  sortComparer: sortByTitle, // Entity leri verilen sıraya göre tutar
});

export const initialState: PostsState = postAdapter.getInitialState({ count: 0 });

export function sortByTitle(a: Post, b: Post): number {
  return a.title.localeCompare(b.title);
}
