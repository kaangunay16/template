import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SelectionsRetrieverComponent } from './selections-retriever.component';

describe('SelectionsRetrieverComponent', () => {
  let component: SelectionsRetrieverComponent;
  let fixture: ComponentFixture<SelectionsRetrieverComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [SelectionsRetrieverComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SelectionsRetrieverComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
